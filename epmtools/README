EPMTOOLS

1. Conversion tools

  These are a set of tools to convert epm-files to portable grey- and
  pixmaps for manipulation with the netpbm/pbmplus tools. These tools
  only work with the uncompressed epm format.

1.1 epmtopgm

  usage: epmtopgm [epmfile]
  
  Converts each layer of the given epmfile to a portable greymap (pgm).
  When no epmfile is given, epmtopgm reads from standard in. 

1.2 epmtopm

  usage: epmtoppm epmfile color [...]

  Converts the given epmfile to a portable pixmap (ppm). For color 
  must be specified for each layer of the epmfile one. The colors
  can be either given by name (parsed from /usr/X11/lib/rgb.txt)
  or in rgb:#/#/# format (where # is a 1-4 digit hexadecimal 
  number). Specify "-" for the epmfile file to read it from standard
  input.
  
1.3 pgmtoepm

  usage: pgmtopm pgmfile [...]

  Converts a set of portable greymaps to an epmfile. The pgm files
  must be of the same size and depth. The result is written to 
  the standard output. Specify "-" for the pgmfile file to read it 
  from standard input.
  
1.4 ppmtopepm

  usage: ppmtoepm [-cch|-rgb] [ppmfile [epmfile]]

  Converts a portable pixmap to an epmfile. If no epmfile is specified
  the result will be written to standard output. If no no ppmfile is
  specified it will be read from standard input. The pimxap can be 
  interpreted as an "normal" rgb-pixmap and will be separated into a 
  red, a blue and green layer (-rgb, default). Use -cch to convert
  convert recolorable xblast block tiles into an epm. These blocks
  are always rendered in red and green. In order convert them to
  "colored" pixmap use:

  ppmtoepm -cch tile.ppm | epmtoppm - color1 color2 White >new.ppm


2.0 Compression

  I have included a simple runlength encoding into the epm format.
   
2.1 epmcompress

  usage: epmcompress epmfile [...]

  First RLE algorithm, only compresses sequences of 0s in an epmfile.
  Every epmfile given as argument will be compressed to this format.
  A backup of the orignal will be keeped with a trailing ~.

2.2 epmcompress2

  usage: epmcompress2 epmfile [...]

  Second RLE algorithm, only compresses sequences of any number in an 
  epmfile. Better for the sprite bitmaps with their grey background.
  Alas it is currenlty not supported by xblast. Every epmfile given as 
  argument will be compressed to this format. A backup of the orignal 
  will be keeped with a trailing ~.

2.3 epmuncompress epmfile [...]

  Converts the given epmfiles to uncompressed format. Only the first 
  format (epmcompress) is supported.


3. Other Tools

3.1. epmarrange 
   
   usage: epmarrange [datafile [outputfile]]

   Creates one big epm file by placing the epmfiles specified in the 
   datafile at given position. (Can be used to put all sprite image 
   into one file). If outputfile is ommitted the result will be written
   to standard output. If the datafile is ommitted it will be read from 
   standard input. The datafile has one line per epmfile to be used,
   each of the following format:
   
   xpos ypos epmfile.

2.2 pbmarrange

   usage: epmarrange [datafile [outputfile]]

   As epmarrange, but arranges portable bitmaps. (Useful for arranging
   sprite masks).

2.3 epmhalf

   usage: epmhalf [input [output]]

   Creates an epmfile half as wide and as high as the orignal.
   If output is omitted the result is written to standard output.If
   input is omitted the result is written to standard input.
 



