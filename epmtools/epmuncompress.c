/*
 * program epmcompress - compress Extented Pixmap Format epm using
 *                       simple Runlength Encoding
 *
 * (C) by Oliver Vogel (e-mail: vogel@ikp.uni-koeln.de)
 * April 5th, 1997
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licence as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * $Id$
 * $Log$
 * Revision 1.1  1999/04/04 11:43:54  xblast
 * Initial revision
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#ifdef __STDC__
int 
main (int argc,
      char *argv[])
#else
int 
main (argv, argv)
  int argc;
  char *argv[];
#endif
{
  FILE *fp;
  int width, height, maxval, depth;
  int arg, n_pixel, n_bytes;
  unsigned char *inbuf, *outbuf;
  unsigned char *src, *dst;
  unsigned zero_count;
  char magic[256];
  char file_name[1024];

  /* check args (part one) */
  if ( (argc < 2) || (0 == strcmp(argv[1],"-?") ) ) {
    fprintf(stderr, "usage: %s epmfile ...\n", argv[0]);
    return (argc != 2);
  }

  for (arg = 1; arg < argc; arg++) {

    /* open file */
    if (NULL == (fp = fopen(argv[arg],"r") ) ) {
      fprintf(stderr, "%s: failed to open file %s for reading\n", 
	      argv[0], argv[arg]);
      return 1;
    }

    /* read header */
    if (5 != fscanf(fp, "%s%d%d%d%d%*c", magic,&width,&height,&maxval,&depth) ) {
      fprintf(stderr, "%s: Failed to read epm header\n",argv[0]);
      return 1;
    }
    /* test magic */
    if (0 != strcmp(magic,"PZ")) {
      if (0 == strcmp(magic, "PX") ) {
	fprintf(stderr, "File \"%s\" is already uncompressed.\n", argv[arg]);
	fclose (fp);
	continue;
      }
      fprintf(stderr, "%s: Wrong magic word \"%s\".\n",argv[0],magic);
      return 1;
    }

    /* calc number of pixel per layer */
    n_pixel = width*height;
#ifdef DEBUG
    fprintf (stderr, "%d pixels\n", n_pixel);
#endif
    

    /* alloc input and color buffer */
    if ( (NULL == (inbuf  = malloc(2*depth*n_pixel*sizeof(char) ) ) ) ||
	 (NULL == (outbuf = calloc(depth*n_pixel,sizeof(char) ) ) ) ) {
      fprintf(stderr, "%s: Failed to alloc buffer\n",argv[0]);
      return 1;
    }
    
    /* read epm data */
    if (0 == (n_bytes = fread(inbuf, sizeof(char), 2*depth*n_pixel, fp) ) ) {
      perror(argv[0]);
      return 1;
    }
    fprintf (stderr, "File \"%s\": %6.1f%%.\n", argv[arg], 
	     100. * (1. - (double)n_bytes/(depth*n_pixel) ) );

    /* convert data */
    for (src = inbuf, dst=outbuf; (src < inbuf + n_bytes) && (dst < outbuf + depth*n_pixel); src++) {
      if (*src) {
	*dst = *src;
	dst ++;
      } else {
	zero_count = 0;
	do {
	  src ++;
	  zero_count += *src;
	} while (*src == 255);
	memset (dst, 0, zero_count);
	dst += zero_count;
      }
    }
    fclose (fp);

    /* rename old file */
    sprintf (file_name, "%s~", argv[arg]);
    if (rename (argv[arg], file_name)) {
      fprintf (stderr, "Failed to create backup file \"%s\".\n",
	       file_name);
      return 1;
    }
    /* write new file */
    if (NULL == (fp = fopen (argv[arg], "w") ) ) {
      fprintf(stderr, "%s: failed to open file %s for writing\n", 
	      argv[0], argv[arg]);
      return 1;
    }
    /* write header */
    fprintf (fp, "PX\n");
    fprintf (fp, "%d %d %d %d\n", width, height, maxval, depth);
    fwrite (outbuf, sizeof(char), dst - outbuf, fp);
    fclose (fp);
    /* free buffers */
    free (outbuf);
    free (inbuf);
  }
  return 0;
}




