/*
 * program epmtopgm - convert Extented Pixmap Format epm into 
 *                    one Portable Graymap per layer
 *
 * (C) by Oliver Vogel (e-mail: vogel@ikp.uni-koeln.de)
 * April 5th, 1997
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licence as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * $Id$
 * $Log$
 * Revision 1.1  1999/04/04 11:46:06  xblast
 * Initial revision
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __STDC__
static char*
prefix_from_filename (char *filename,
		      char *extension)
#else
static char*
prefix_from_filename (filename, extension)
  char *filename, extension;
#endif
{
  int flength, xlength;
  char *dest;

  flength = strlen(filename);
  xlength = strlen (extension);

  if ( (flength >= xlength) &&
       (0 == strcmp(filename + (flength - xlength), extension)) ) {
    /* cut away extension */
    flength -= xlength;
  }
    
  /* duplicate string */
  if (NULL == (dest = malloc((flength+1)*sizeof(char) ) ) ) {
    return NULL;
  }

  strncpy (dest, filename, flength);
  dest[flength]=0;

  return dest;
}

#ifdef __STDC__
int 
main (int argc, 
      char*argv[])
#else
int
main (argc, argv)
  int argc;
  char *argv[];
#endif
{
  char *prefix;
  FILE *fin, *fout;
  char magic[256];
  char outfile[1024];
  int width, height, depth, maxval;
  int layer, n_pixel;
  char *buf;

  /* check commandline args */
  if ( (1 == argc) || ( (2 == argc) && (0 == strcmp(argv[1], "-") ) ) ) {
    /* read from stdin */
    fin = stdin;
    prefix = "noname";
  } else if ( (2 == argc) && (0 != strcmp(argv[1],"-?") ) ) {
    if (NULL == (fin = fopen(argv[1],"r") ) ) {
      fprintf(stderr, "%s: failed to open file %s for reading\n",
	      argv[0], argv[1]);
      return 1;
    }
    prefix = prefix_from_filename (argv[1], ".epm");
  } else {
    fprintf(stderr, "usage: %s [epmfile]\n", argv[0]);
    return (argc != 2);
  }
  
  /* read header */
  if (5 != fscanf(fin, "%s%d%d%d%d%*c", magic,&width,&height,&maxval,&depth) ) {
    fprintf(stderr, "%s: Failed to read epm header\n",argv[0]);
    return 1;
  }
  /* test magic */
  if (0 != strcmp(magic,"PX")) {
    fprintf(stderr, "%s: Wrong magic word \"%s\".\n",argv[0],magic);
    return 1;
  }

  /* calc number of pixel per layer */
  n_pixel = width*height;

  /* alloc buffer */
  if (NULL == (buf = malloc(n_pixel*sizeof(char) ) ) ) {
    fprintf(stderr, "%s: Failed to alloc buffer\n",argv[0]);
    return 1;
  }

  for (layer=1; layer<=depth; layer++) {
    /* read one layer */
    if (n_pixel != fread (buf, sizeof(char), n_pixel, fin) ) {
      perror(argv[0]);
      return 1;
    }
    /* open output file */
    sprintf(outfile, "%s.%d", prefix, layer);
    if (NULL == (fout = fopen(outfile, "w") ) ) {
      fprintf(stderr, "%s: failed to open file %s for writing\n",
	      argv[0], outfile);
      return 1;
    }
    /* write header */
    fprintf(fout, "P5\n");
    fprintf(fout, "%d %d %d\n", width, height, maxval);
    /* write pixel data */
    if (n_pixel != fwrite (buf, sizeof(char), n_pixel, fout) ) {
      perror(argv[0]);
      return 1;
    }    
    if (fout != stdout) {
      fclose(fout);
    }
  }

  if (fout != stdin) {
    fclose(fin);
  }
  free(buf);

  return 0;
}
