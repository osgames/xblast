/*
 * program epmhalf - scale down epm by a factor of two
 *
 * (C) by Oliver Vogel (e-mail: vogel@ikp.uni-koeln.de)
 * April 5th, 1997
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licence as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * $Id$
 * $Log$
 * Revision 1.1  1999/04/04 11:45:18  xblast
 * Initial revision
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#ifdef __STDC__
int
main (int argc, 
      char *argv[])
#else
int
main (argv, argv)
     int argc;
     char *argv[];
#endif
{
  FILE *fin, *fout;
  int width, height, maxval, depth;
  int n_pixel, o_pixel;
  unsigned char *inbuf, *outbuf;
  unsigned char *pin, *pout;
  char magic[256];
  int d, y, x;

  /* check args */
  switch(argc) {
  case 1:
    fin  = stdin;
    fout = stdout;
    break;

  case 2:
    fout = stdout;
    /* open file */
    if (NULL == (fin = fopen(argv[1],"r") ) ) {
      fprintf(stderr, "%s: failed to open file %s for reading\n", 
	      argv[0], argv[1]);
      return 1;
    }
    break;

  case 3:
    /* open files */
    if (NULL == (fin = fopen(argv[1],"r") ) ) {
      fprintf(stderr, "%s: failed to open file %s for reading\n", 
	      argv[0], argv[1]);
      return 1;
    }
    if (NULL == (fout = fopen(argv[2],"w") ) ) {
      fprintf(stderr, "%s: failed to open file %s for writing\n", 
	      argv[0], argv[1]);
      return 1;
    }
    break;

  default:
    fprintf (stderr, "usage: %s [input [output]]\n", argv[0]);
    return 1;
  } 

  /* read header */
  if (5 != fscanf(fin, "%s%d%d%d%d%*c", magic,&width,&height,&maxval,&depth) ) {
    fprintf(stderr, "%s: Failed to read epm header\n",argv[0]);
    return 1;
  }
  /* test magic */
  if (0 != strcmp(magic,"PX")) {
    fprintf(stderr, "%s: Wrong magic word \"%s\".\n",argv[0],magic);
    return 1;
  }

  /* calc number of pixel per layer */
  n_pixel = width*height;
  o_pixel = (width/2)*(height/2);

  /* alloc input and output buffer */
  if ( (NULL == (inbuf  = malloc(depth*n_pixel*sizeof(char) ) ) ) ||
       (NULL == (outbuf = malloc(depth*o_pixel*sizeof(char) ) ) ) ) {
    fprintf(stderr, "%s: Failed to alloc buffers\n",argv[0]);
    return 1;
  }
  
  /* read epm data */
  if (depth*n_pixel != fread(inbuf, sizeof(char), depth*n_pixel, fin) ) {
    perror(argv[0]);
    return 1;
  }

  /* convert it */

  pout = outbuf;
  for (d=0; d<depth; d++) {
    for (y=0; y<height; y+=2) {
      pin = inbuf + d*n_pixel + y*width;
      for (x=0; x<width; x+=2) {
	*pout = (unsigned char)((pin[0]+pin[1]+pin[width]+pin[width+1])/4);
	pout ++;
	pin +=2;
      }
    }
  }
  
  /* write header */
  fprintf(fout,"PX\n");
  fprintf(fout,"%d %d %d %d\n", width/2, height/2, maxval, depth);
  fwrite(outbuf, sizeof(char), depth*o_pixel, fout);
  
  /* close files if neceassry */
  if (fin != stdin) {
    fclose(fin);
  }
  if (fout != stdout) {
    fclose(fout);
  }

  return 0;
}
