/*
 * program ppmtoepm - convert Portable Pixmap ppm into 
 *                    Extented Pixmap Format epm
 *
 * (C) by Oliver Vogel (e-mail: vogel@ikp.uni-koeln.de)
 * April 5th, 1997
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licence as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * $Id$
 * $Log$
 * Revision 1.1  1999/04/04 11:49:18  xblast
 * Initial revision
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __STDC__
int 
main (int argc, 
      char*argv[])
#else
int
main (argc, argv)
  int argc;
  char *argv[];
#endif
{
  static char magic[256];
  int width, height, maxval;
  int i, j, n_data, n_pixel;
  FILE *fp, *fout;
  char *ppm, *epm;
  char dummy;
  unsigned char *inbuf, *outbuf;
  int val, rgb_mode;

  /* check command line args */
  if ( (argc <= 1) || (argc >=5) ) {
    fprintf(stderr, "usage: %s [-cch|-rgb] [ppmfile [epmfile]]\n", argv[0]);
    return 1;
  } 
  /* parse command line */
  i=1;
  /* conversion mode */
  rgb_mode = 1;
  if (0 == strcmp(argv[i], "-cch") ) {
    rgb_mode = 0;
    i ++;
  } else if (0 == strcmp(argv[i], "-rgb") ) {
    rgb_mode =1;
    i ++;
  } else if ( ('-' == argv[i][0]) && ('\0' != argv[i][1]) ) {
    fprintf(stderr, "%s: unknown option %s\n", argv[0], argv[i]);
    return 1;
  }
  /* files */
  ppm = NULL;
  epm = NULL;
  /* input file */
  if (i < argc) {
    ppm = argv[i];
    i++;
    /* output file */
    if (i < argc) {
      epm = argv[i];
      i++;
    }
  }
  if (i != argc) {
    fprintf(stderr, "usage: %s [-cch|-rgb] [ppmfile [epmfile]]\n", argv[0]);
    return 1;
  } 
  

  /* open  ppm file */
  if ( (NULL == ppm) || (0 == strcmp(ppm, "-") ) ) {
    fp = stdin;
  } else if (NULL == (fp = fopen(ppm,"r") ) ) {
    fprintf(stderr, "%s: failed to open file \"%s\"\n", argv[0], ppm);
    return 1;
  }
  /* read header */
  fscanf(fp, "%s %d %d %d%c", magic, &width, &height, &maxval, &dummy);
  /* compare magic word */
  if (0 != strcmp("P6",magic)) {
    fprintf(stderr, "%s: wrong magic word in file \"%s\"\n", argv[0], ppm);
    return 1;
  }
  n_data  = 3 * width * height;
  n_pixel = width * height;
  /* alloc data buffer */
  if ( (NULL == (inbuf = malloc(n_data*sizeof(char) ) ) ) ||
       (NULL == (outbuf = malloc(n_data*sizeof(char) ) ) ) ) {
    fprintf(stderr, "%s: failed to alloc data buffer\n", argv[0]);
    return 1;
  }
  /* read data */
  if (n_data != fread(inbuf, sizeof(char), n_data, fp) ) {
    fprintf(stderr, "%s: premature end of file \"%s\"\n", argv[0], ppm);
    return 1;
  }
  /* close pgm file */
  fclose(fp);

  /* open epm file if necessary */
  if ( (NULL == epm) || (0 == strcmp(epm,"-") ) ) {
    fout = stdout;
  } else {
    if (NULL == (fout = fopen(epm,"w") ) ) {
      fprintf(stderr, "%s: failed to open file %s for writing\n", 
	      argv[0], epm);
    }
  }

  /* write header */
  fprintf(fout, "PX\n");
  fprintf(fout, "%d %d %d %d\n", width, height, maxval, 3);

  if (rgb_mode) {
    /* convert data */
    for (i=0, j=0; i<n_pixel; i++, j+=3) {
      /* channel 0 is red */
      outbuf[i] = inbuf[j];
      /* channel 1 is green */
      outbuf[i+n_pixel] = inbuf[j+1];
      /* channel is just blue */
      outbuf[i+2*n_pixel] = inbuf[j+2];
    }
  } else {
    /* convert data */
    for (i=0, j=0; i<n_pixel; i++, j+=3) {
      /* channel 0 is red - blue */
      if ( 0 > (val = inbuf[j] - inbuf[j+2]) ) {
	val = 0;
      }
      outbuf[i] = val;
      /* channel 1 is green - blue */
      if ( 0 > (val = inbuf[j+1] - inbuf[j+2]) ) {
	val = 0;
      }
      outbuf[i+n_pixel] = val;
      /* channel is just blue */
      outbuf[i+2*n_pixel] = inbuf[j+2];
    }
  }
  /* write data*/
  fwrite(outbuf, sizeof(char), n_data, fout);
  /* close epm file */
  fclose(fout);

  return 0;
}
