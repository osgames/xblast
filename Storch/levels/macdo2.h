/* XBlast 2.5.3 level */
/* level edited with BlastEd */
static BMLevelData macdo2 =
{
  /* BMLevel */
  {
    "Mac Donald's Electrify",
    "EPFL - cosun : Yves Bresson",
    "xblast.usemacdo2",
    "Ronald ???",
    GM_Random | GM_23456_Player | GM_All,
    (void *) &macdo2,
    NULL,
  },
  /* BMShrinkData */
  {
    shrink_void,
    SCRAMBLE_VOID,
    SCRAMBLE_VOID,
  },
  /* BMFuncData */
  {
    special_init_void,
    special_game_void,
    special_extra_electrify,
    special_key_electrify,
  },
  /* BMPlayerData */
  {
    4, 2,
    {
      {1,1},
      {1,13},
      {3,7},
      {8,7},
      {11,1},
      {11,13},
    },
    PM_Same, 0,
    IllRun, IllRun, IF_Kick,
  },
  /* BMBombData */
  {
    bomb_click_none, bomb_click_none, bomb_click_none,
    GoStop, FUSEshort,
    BMTnormal, BMTnormal, BMTnormal,
  },
  /* BMGraphicsData */
  {
    {
      { "rock_floor", "Black", "DarkRed", "Black" },
      { "rock_floor_S", "Yellow", "Yellow", "Black" },
      { "weight", "Black", "Black", "Black" },
      { "weight_R", "Yellow", "Yellow", "Black" },
      { "bricks", "Yellow", "Yellow", "Black" },
      { "brick_O", "Yellow", "Yellow", "Black" },
      EXTRA_BOMB,
      EXTRA_RANGE,
      EXTRA_TRAP,
      EXTRA_ELECTRIFY,
      { "score_floor", "Yellow", "Yellow", "RoyalBlue" },
    },
  },
  /* BMMapData */
  {
    ShadowNone, DEall,
    { 10, 20, 30, 30, 30 },
    {
      { B,B,B,B,B,B,B,B,B,B,B,B,B, },
      { B,_,_,_,_,_,_,_,_,_,_,_,B, },
      { B,_,_,_,_,_,_,_,_,_,_,_,B, },
      { B,_,_,_,q,q,q,q,q,q,q,_,B, },
      { B,_,_,q,_,_,_,_,_,_,_,_,B, },
      { B,_,q,_,_,_,_,_,_,_,_,_,B, },
      { B,_,_,q,_,_,_,_,_,_,_,_,B, },
      { B,_,_,_,q,q,q,_,_,_,_,_,B, },
      { B,_,_,q,_,_,_,_,_,_,_,_,B, },
      { B,_,q,_,_,_,_,_,_,_,_,_,B, },
      { B,_,_,q,_,_,_,_,_,_,_,_,B, },
      { B,_,_,_,q,q,q,q,q,q,q,_,B, },
      { B,_,_,_,_,_,_,_,_,_,_,_,B, },
      { B,_,_,_,_,_,_,_,_,_,_,_,B, },
      { B,B,B,B,B,B,B,B,B,B,B,B,B, },
    },
  },
};

/* end of file */
