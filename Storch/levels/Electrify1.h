/* XBlast 2.5.3 (2.6.1) level                               */
/* Level edited with BlastEd - (C)1998-1999 by Yves Bresson */
/* email: yves.bresson@epfl.ch                              */
/* website: http://get.to/xblast                            */

static BMLevelData Electrify1 =
{
  /* BMLevel */
  {
    "Electrify Them",
    "EPFL - insun : Yves Bresson",
    "xblast.useElectrify1",
    "GZZZZZ aouch!",
    GM_Random | GM_23456_Player | GM_All,
    (void *) &Electrify1,
    NULL,
  },
  /* BMShrinkData */
  {
    shrink_void,
    SCRAMBLE_VOID,
    SCRAMBLE_VOID,
  },
  /* BMFuncData */
  {
    special_init_void,
    special_game_void,
    special_extra_electrify,
    special_key_electrify,
  },
  /* BMPlayerData */
  {
    2, 1,
    {
      {3,4},
      {3,10},
      {4,3},
      {8,11},
      {9,4},
      {9,10},
    },
    PM_Vertical, 2,
    Healthy, Healthy, IF_None,
  },
  /* BMBombData */
  {
    bomb_click_none, bomb_click_none, bomb_click_none,
    GoStop, FUSEnormal,
    BMTnormal, BMTnormal, BMTnormal,
  },
  /* BMGraphicsData */
  {
    {
      { "chess_floor", "Black", "LightSlateBlue", "Black" },
      { "chess_floor_S", "Black", "LightSlateBlue", "Black" },
      { "chess_sphere", "Red", "OrangeRed", "SeaGreen" },
      { "chess_sphere", "Green", "OrangeRed", "SeaGreen" },
      { "chess_sphere", "Blue", "LightSlateBlue", "Gold" },
      { "chess_sphere_O", "Blue", "LightSlateBlue", "Gold" },
      {"chess_sphere", "LightRed", "OrangeRed", "SeaGreen"},
      {"chess_sphere", "LightGreen", "OrangeRed", "SeaGreen"},
      {"chess_sphere", "LightBlue", "LightSlateBlue", "Gold"},
      EXTRA_ELECTRIFY,
      { "chess_sphere", "Yellow", "LightSlateBlue", "Gold" },
    },
  },
  /* BMMapData */
  {
    ShadowBlock, DEget,
    { 10, 20, 30, 50, 55 },
    {
      { B,B,B,B,B,B,B,B,B,B,B,B,B, },
      { B,s,B,b,X,R,r,s,B,r,s,b,B, },
      { B,R,v,R,B,v,R,B,v,R,B,X,B, },
      { B,r,B,_,_,_,_,_,_,_,v,R,B, },
      { B,s,R,_,_,_,_,_,_,_,R,r,B, },
      { B,B,v,_,_,_,_,_,_,_,B,b,B, },
      { B,X,B,_,_,_,_,_,_,_,v,B,B, },
      { B,b,R,_,_,_,q,_,_,_,R,X,B, },
      { B,R,v,_,_,_,_,_,_,_,B,b,B, },
      { B,s,B,_,_,_,_,_,_,_,v,R,B, },
      { B,r,R,_,_,_,_,_,_,_,R,s,B, },
      { B,B,v,_,_,_,_,_,_,_,B,X,B, },
      { B,X,B,R,v,B,R,v,B,R,v,B,B, },
      { B,b,r,s,R,X,b,B,r,X,R,b,B, },
      { B,B,B,B,B,B,B,B,B,B,B,B,B, },
    },
  },
};

/* end of file */
