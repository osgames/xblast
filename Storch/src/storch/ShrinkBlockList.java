/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch;

/**
 *  Contains block numbers for describing a shrink.
 *
 *  @author     Tobias Johansson
 */
public class ShrinkBlockList{
	
	/**
	 *  An array of block numbers for solid blocks.
	 */
	public int[] solids;
	
	/**
	 *  An array of block numbers for void blocks.
	 */
	public int[] voids;
	
	/**
	 *  An array of block numbers for extra blocks.
	 */
	public int[] extras;
	
	/**
	 *  Constructs a new list of shrink blocks.
	 *
	 *  @param solids	An array of block numbers for solid blocks
	 *  @param voids	An array of block numbers for void blocks
	 *  @param extras	An array of block numbers for extra blocks
	 */
	public ShrinkBlockList(int[] solids, int[] voids, int[] extras){
		this.solids = solids;
		this.voids = voids;
		this.extras = extras;
	}
	
	/**
	 *  Constructs a new, empty list of shrink blocks.
	 */
	public ShrinkBlockList(){
		solids = new int[0];
		voids = new int[0];;
		extras = new int[0];;
	}
	
}