/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch;

/**
 * This is the class that contains all information about a level inside Storch.
 * There are also constants for all the values.
 *
 * @author   Tor Andr�
 * @author   Tobias Johansson
 */
public class Level implements Cloneable {

//Shrinks
    public final static int SHRINK_VOID = 0;
    public final static int SHRINK_SPIRAL = 1;
    public final static int SHRINK_SPEED_SPIRAL = 2;
    public final static int SHRINK_SPIRAL_PLUS = 3;
    public final static int SHRINK_SPIRAL_3 = 4;
    public final static int SHRINK_SPIRAL_23 = 5;
    public final static int SHRINK_SPIRAL_LEGO = 6;
    public final static int SHRINK_EARLY_SPIRAL = 7;
    public final static int SHRINK_COMPOUND = 8;
    public final static int SHRINK_COMPOUND_F = 9;
    public final static int SHRINK_COMPOUND_2_F = 10;
    public final static int SHRINK_LAZY_COMPOUND_F = 11;
    public final static int SHRINK_COMPOUND_SOLID = 12;
    public final static int SHRINK_SAVAGE_COMPOUND = 13;
    public final static int SHRINK_COMPOUND_EXTRA = 14;
    public final static int SHRINK_DOWN = 15;
    public final static int SHRINK_DOWN_F = 16;
    public final static int SHRINK_QUAD = 17;
    public final static int SHRINK_CONSTRICT_WAVE = 18;
    public final static int SHRINK_OUTWARD_SPIRAL = 19;
    public final static int SHRINK_DIAG = 20;//EPFL
    public final static int SHRINK_OUTWARD_COMPOUND_EXTRA = 21;//EPFL
    public final static int SHRINK_ISTY_SPIRAL_3 = 22;//EPFL
    public final static int SHRINK_ISTY_COMPOUND_2_F = 23;//EPFL

// special init functions
    public final static int SPECIAL_INIT_VOID = 0;
    public final static int SPECIAL_INIT_SPECIAL_BOMBS_30 = 1;
    public final static int SPECIAL_INIT_SPECIAL_BOMBS_12 = 2;
    public final static int SPECIAL_INIT_NASTY_WALLS = 3;
    public final static int SPECIAL_INIT_NASTY_WALLS_2 = 4;
    public final static int SPECIAL_INIT_SPECIAL_BOMBS_INFINITY = 5;// EPFL
    public final static int SPECIAL_INIT_FIRE_WALLS = 6;//EPFL

//special game_fucntions
    public final static int SPECIAL_GAME_VOID = 0;
    public final static int SPECIAL_GAME_NASTY_WALLS = 1;
    public final static int SPECIAL_GAME_HAUNT = 2;
    public final static int SPECIAL_GAME_HAUNT_FAST = 3;
    public final static int SPECIAL_GAME_NASTY_CEIL = 4;//EPFL

// special extra_functions
    public final static int SPECIAL_EXTRA_VOID = 0;
    public final static int SPECIAL_EXTRA_INVINCIBLE = 1;
    public final static int SPECIAL_EXTRA_KICK = 2;
    public final static int SPECIAL_EXTRA_TELEPORT = 3;
    public final static int SPECIAL_EXTRA_RC = 4;
    public final static int SPECIAL_EXTRA_IGNITE_ALL = 5;
    public final static int SPECIAL_EXTRA_AIR = 6;
    public final static int SPECIAL_EXTRA_SPECIAL_BOMB = 7;
    public final static int SPECIAL_EXTRA_JUNKIE = 8;
    public final static int SPECIAL_EXTRA_POISON = 9;
    public final static int SPECIAL_EXTRA_LONG_STUNNED = 10;
    public final static int SPECIAL_EXTRA_SPEED = 11;
    public final static int SPECIAL_EXTRA_SLOW = 12;
    public final static int SPECIAL_EXTRA_MAYHEM = 13;
    public final static int SPECIAL_EXTRA_HOLY_GRAIL = 14;
    public final static int SPECIAL_EXTRA_LIFE = 15;
    public final static int SPECIAL_EXTRA_MULTIPLE = 16;
    public final static int SPECIAL_EXTRA_CLOAK = 17;
    public final static int SPECIAL_EXTRA_STUN_OTHERS = 18;
    public final static int SPECIAL_EXTRA_MORPH = 19;
    // pgehler
    public final static int SPECIAL_EXTRA_STOP = 20;//EPFL
    public final static int SPECIAL_EXTRA_ELECTRIFY = 21;//EPFL
    public final static int SPECIAL_EXTRA_FROGGER = 22;//EPFL
    public final static int SPECIAL_EXTRA_PHANTOM = 23;//EPFL
    public final static int SPECIAL_EXTRA_GHOST = 24;//EPFL
    public final static int SPECIAL_EXTRA_THROUGH = 25;//EPFL
    public final static int SPECIAL_EXTRA_REVIVE = 26;//EPFL
    public final static int SPECIAL_EXTRA_STEAL = 27;//EPFL
    public final static int SPECIAL_EXTRA_SWAPPOSITION = 28;//EPFL
    public final static int SPECIAL_EXTRA_SWAPCOLOR = 29;//EPFL
    public final static int SPECIAL_EXTRA_DALEIF = 30;//EPFL
    public final static int SPECIAL_EXTRA_CHOICE = 31;//EPFL
    public final static int SPECIAL_EXTRA_EVILGRAIL = 32;//EPFL
    public final static int SPECIAL_EXTRA_JUMP = 33;//EPFL
    public final static int SPECIAL_EXTRA_PLAYERFART = 34;//EPFL
    public final static int SPECIAL_EXTRA_PLAYERANDBOMBFART = 35;//EPFL
    public final static int SPECIAL_EXTRA_SNIPE = 36;//Skywalker
    public final static int SPECIAL_EXTRA_SPEED2 = 37;//Skywalker
    public final static int NUMBER_OF_SPECIAL_EXTRAS = 43;//Skywalker

// ill
    public final static int ILL_HEALTHY = 0;
    public final static int ILL_BOMB = 1;
    public final static int ILL_SLOW = 2;
    public final static int ILL_RUN = 3;
    public final static int ILL_MINI = 4;
    public final static int ILL_EMPTY = 5;
    public final static int ILL_INVISIBLE = 6;
    public final static int ILL_MALFUNCTION = 7;
    public final static int ILL_REVERSE = 8;
    public final static int ILL_TELEPORT = 9;
    public final static int ILL_REVERSE2 = 10;

//bombs
    public final static int BOMB_NORMAL = 0;
    public final static int BOMB_NAPALM = 1;
    public final static int BOMB_FIRECRACKER = 2;
    public final static int BOMB_CONSTRUCTION = 3;
    public final static int BOMB_THREEBOMBS = 4;
    public final static int BOMB_GRENADE = 5;
    public final static int BOMB_TRIANGLEBOMBS = 6;
    public final static int BOMB_DESTRUCTION = 7;
    public final static int BOMB_FUNGUS = 8;
    public final static int BOMB_RENOVATION = 9;
    public final static int BOMB_PYRO = 10;
    public final static int BOMB_RANDOM = 11;
    public final static int BOMB_FIRECRACKER_2 = 12;//TNT
    public final static int BOMB_PYRO_2 = 13;//TNT
    public final static int BOMB_BLASTNOW = 14;//TNT
    public final static int BOMB_CLOSE = 15;//TNT
    public final static int BOMB_SHORT = 16;//TNT
    public final static int BOMB_SEARCH = 17;//pgehler EPFL
    public final static int BOMB_RING_OF_FIRE = 18;//EPFL
    public final static int BOMB_MINE = 19;//EPFL
    public final static int BOMB_DIAG_THREE_BOMBS = 20;//EPFL
    public final static int BOMB_SCISSOR = 21;//EPFL
    public final static int BOMB_SCISSOR_2 = 22;//EPFL
    public final static int BOMB_PARALLEL = 23;//EPFL
    public final static int BOMB_DISTANCE = 24;//EPFL
    public final static int BOMB_LUCKY = 25;//EPFL
    public final static int BOMB_PARASOL = 26;//EPFL
    public final static int BOMB_COMB = 27;//EPFL
    public final static int BOMB_FARPYRO = 28;//EPFL
    public final static int BOMB_NUCLEAR = 29;//EPFL
    public final static int BOMB_PROTECTBOMBS = 30;//EPFL
    public final static int BOMB_SNIPE = 31;//Skywalker

//bomb clicks
    public final static int BOMB_CLICK_NONE = 0;
    public final static int BOMB_CLICK_INITIAL = 1;
    public final static int BOMB_CLICK_THRU = 2;
    public final static int BOMB_CLICK_SNOOKER = 3;
    public final static int BOMB_CLICK_CONTACT = 4;
    public final static int BOMB_CLICK_CLOCKWISE = 5;
    public final static int BOMB_CLICK_ANTICLOCKWISE = 6;
    public final static int BOMB_CLICK_RANDOMDIR = 7;
    public final static int BOMB_CLICK_REBOUND = 8;
    public final static int BOMB_CLICK_SPLIT = 9;// EPFL

//direction
    public final static int GO_STOP = 0;
    public final static int GO_DOWN = 1;
    public final static int GO_UP = 2;
    public final static int GO_LEFT = 3;
    public final static int GO_RIGHT = 4;

//fuseTime
    public final static int FUSE_NORMAL = 0;
    public final static int FUSE_SHORT = 1;
    public final static int FUSE_LONG = 2;

//distribution
    public final static int DE_NONE = 0;
    public final static int DE_SINGLE = 1;
    public final static int DE_ALL = 2;
    public final static int DE_SPECIAL = 3;
    public final static int DE_GET = 4;
    public final static int DE_DOUBLE = 5;//TNT

//map constants
    public final static int MAP_FREE = 0;
    public final static int MAP_BLASTABLE = 1;
    public final static int MAP_SOLID = 2;
    public final static int MAP_EXPLODING_BOMB = 3;
    public final static int MAP_BOMB_EXTRA = 4;
    public final static int MAP_RANGE = 5;
    public final static int MAP_SPECIAL_EXTRA = 6;
    public final static int MAP_ILLNESS = 7;
    public final static int MAP_VOID = 8;

//PROBS
    public final static int PROB_BOMB_EXTRA = 0;
    public final static int PROB_RANGE = 1;
    public final static int PROB_ILL = 2;
    public final static int PROB_SPECIAL_EXTRA = 3;
    public final static int PROB_HIDDEN_BOMB = 4;

    public final static int PROB_NULL = 0;
    public final static int PROB_SCARCE = 6;
    public final static int PROB_RARE = 12;
    public final static int PROB_UNCOMMON = 25;
    public final static int PROB_COMMON = 50;
    public final static int PROB_PLENTIFUL = 75;
    public final static int PROB_ALL = 100;

//Load Status
    //Must be a multiple of 2
    public final static int LOAD_SUCCESS = 0;
    public final static int LOAD_GRAPHICS_ERROR = 1;
    public final static int LOAD_SPECIAL_EXTRA_IMAGE_ERROR = 2;
    public final static int LOAD_RISING_BLOCK = 4;
    public final static int LOAD_UNDEFINED_BLOCK = 8;
    public final static int LOAD_UNDEFINED_FILENAME = 16;

    
    public int COLORSALL=0, COLORSEXTRA=0;
    public String title, author, description;
    public boolean GM_2_Player, GM_3_Player, GM_4_Player, GM_5_Player, GM_6_Player;
    public boolean GM_Random, GM_SinglePlayer;
    public boolean GM_Team, GM_Double, GM_Single, GM_LR_Players;

    public int shrinkPattern, specialExtra, distribution;
    public int inIllness, revIllness, inExtra, revExtra, inGameSpecial, turnGameSpecial, specKey;
    public boolean inKick, revKick;

    public int[][] map = new int[15][13];
    public int[][] startPos = new int[6][2];

    public int[][] scrambleDraw = new int[143][2];
    public int[][] scrambleDel = new int[143][2];
    public int scrambleDrawCount = 0;
    public int scrambleDelCount = 0;
    public double scrambleDrawTime, scrambleDelTime;

    public int bombsAtStart, range, fuseTime, direction, defaultType, specialType,
            hiddenType, bombClick, wallClick, playerClick, hauntSpeed;

    public int PMRange = 2;
    public String PMMode = "PM_Polar";

    public Block freeBlock, blastedFloor, shadowedBlock, solidWall, risingWall, blastableBlock, blastedBlock, voidBlock,extraBlock;

    private double[] prob = new double[5];

    private Level lastSave;
    public boolean mapChanged;

    public int loadStatus;


    /**
     * Constructor for the Level object
     */
    public Level() {

        //  init GeneralPanel
        title = author = description = "";
        GM_2_Player = GM_3_Player = GM_4_Player = GM_5_Player = GM_6_Player = true;
        GM_Random = true;
        GM_Team = GM_Double = GM_Single = GM_LR_Players = true;

        // init GamePanel
        shrinkPattern = SHRINK_VOID;
        specialExtra = SPECIAL_EXTRA_VOID;
        distribution = DE_NONE;
        inKick = false;
        revKick = false;
        inExtra = SPECIAL_EXTRA_VOID;
        revExtra = SPECIAL_EXTRA_VOID;
        inIllness = ILL_HEALTHY;
        revIllness = ILL_HEALTHY;
        inGameSpecial = SPECIAL_INIT_VOID;
        turnGameSpecial = SPECIAL_GAME_VOID;

        //  init BombsPanel
        bombsAtStart = 1;
        range = 3;
        fuseTime = FUSE_NORMAL;
        direction = GO_STOP;
        defaultType = specialType = hiddenType = BOMB_NORMAL;
        bombClick = wallClick = playerClick = BOMB_CLICK_NONE;

        //  init startpos
        startPos[0][0] = 1;
        startPos[0][1] = 1;
        startPos[1][0] = 13;
        startPos[1][1] = 11;
        startPos[2][0] = 13;
        startPos[2][1] = 1;
        startPos[3][0] = 1;
        startPos[3][1] = 11;
        startPos[4][0] = 7;
        startPos[4][1] = 1;
        startPos[5][0] = 7;
        startPos[5][1] = 11;

        //  init map
        for(int i = 0; i <= 14; i++)
            map[i][12] = map[i][0] = Block.SOLID_WALL;
        for(int i = 1; i <= 11; i++)
            map[0][i] = map[14][i] = Block.SOLID_WALL;

        //  scramble shrink
        scrambleDrawTime = 0.33;
        scrambleDelTime = 0.67;

        setSaved();
        loadStatus = LOAD_SUCCESS;
    }


    /**
     * Gets the probability, in the standard (2.6) format, of the things under
     * the blasted blocks.
     *
     * @return   The probs in 2.6 format
     */
    public int[] getStdProbs() {
        double[] stdProb = new double[5];
        int[] intProb = new int[5];
        getTNTProbs();
        stdProb[PROB_BOMB_EXTRA] = 63 * (prob[PROB_BOMB_EXTRA] / 100.0);
        stdProb[PROB_RANGE] = stdProb[PROB_BOMB_EXTRA] + 63 * (prob[PROB_RANGE] / 100.0);
        stdProb[PROB_ILL] = stdProb[PROB_RANGE] + 63 * (prob[PROB_ILL] / 100.0);
        stdProb[PROB_SPECIAL_EXTRA] = stdProb[PROB_ILL] + 63 * (prob[PROB_SPECIAL_EXTRA] / 100.0);
        stdProb[PROB_HIDDEN_BOMB] = stdProb[PROB_SPECIAL_EXTRA] + 63 * (prob[PROB_HIDDEN_BOMB] / 100.0);
        for(int i = 0; i < stdProb.length; i++) {
            if(stdProb[i] < 0)
                stdProb[i] = 0;
            intProb[i] = (int) Math.round(stdProb[i]);
        }

        return intProb;
    }
public String[] getSpecialExtraNames(){
    String[] names=new String[NUMBER_OF_SPECIAL_EXTRAS+1];
                 names[0]= "void";
                 names[1] = "void";
                 names[2] = "invincible";
                 names[3] = "kick_bomb";
                 names[4] = "q3a_beam";
                 names[5] = "remote_control";
                 names[6] = "ignite";
                 names[7] = "air_pump";
                 names[8] = "bomb";
                 names[9] = "triangle_bomb";
                 names[10] = "construction";
                 names[11] = "firecracker";
                 names[12] = "search";
                 names[13] = "mines";
                 names[14] = "napalm";
                 names[15] = "syringe";
                 names[16] = "poison";
                 names[17] = "pow";
                 names[18] = "speed";
                 names[19] = "slow";
                 names[20] = "mayhem";
                 names[21] = "holygrail";
                 names[22] = "life";
                 names[23] = "multiple";
                 names[24] = "cloak";
                 names[25] = "pow";
                 names[26] = "morph";
                 names[27] = "stop";
                 names[28] = "electrify";
                 names[29] = "frogger";
                 names[30] = "snipe";
                 names[31] = "phantom";
                 names[32] = "ghost";
                 names[33] = "through";
                 names[34] = "revive";
                 names[35] = "steal";
                 names[36] = "swapposition";
                 names[37] = "swapcolor";
                 names[38] = "daleif";
                 names[39] = "choice";
                 names[40] = "evilgrail";
                 names[41] = "jump";
                 names[42] = "fart";
                 names[43] = "speed2";
                
        
       return names;
}
public String getSpecialExtraName(){
    String specialExtraName = "void";
            switch (specialExtra) {
             case SPECIAL_EXTRA_VOID:
                 specialExtraName = "void";
                 break;
             case SPECIAL_EXTRA_INVINCIBLE:
                 specialExtraName = "invincible";
                 break;
             case SPECIAL_EXTRA_KICK:
                 specialExtraName = "kick_bomb";
                 break;
             case SPECIAL_EXTRA_TELEPORT:
                 specialExtraName = "q3a_beam";
                 break;
             case SPECIAL_EXTRA_RC:
                 specialExtraName = "remote_control";
                 break;
             case SPECIAL_EXTRA_IGNITE_ALL:
                 specialExtraName = "ignite";
                 break;
             case SPECIAL_EXTRA_AIR:
                 specialExtraName = "air_pump";
                 break;
             case SPECIAL_EXTRA_SPECIAL_BOMB:
                 int type = specialType;
                 if(type == BOMB_NORMAL) {
                     specialExtraName = "bomb";
                 }
                 else if(type == BOMB_THREEBOMBS || type == BOMB_TRIANGLEBOMBS) {
                     specialExtraName = "triangle_bomb";
                 }
                 else if(type == BOMB_CONSTRUCTION || type == BOMB_DESTRUCTION || type == BOMB_RENOVATION || type == BOMB_PROTECTBOMBS) {
                     specialExtraName = "construction";
                 }
                 else if(type == BOMB_FIRECRACKER || type == BOMB_PYRO || type == BOMB_FIRECRACKER_2 || type == BOMB_PYRO_2 || type == BOMB_RING_OF_FIRE || type == BOMB_FARPYRO) {
                     specialExtraName = "firecracker";
                 }
                 else if(type == BOMB_SEARCH) {
                     specialExtraName = "search";
                 }
                 else if(type == BOMB_MINE) {
                     specialExtraName = "mines";
                 }
                 else {
                     specialExtraName = "napalm";
                 }
                 break;
             case SPECIAL_EXTRA_JUNKIE:
                 specialExtraName = "syringe";
                 break;
             case SPECIAL_EXTRA_POISON:
                 specialExtraName = "poison";
                 break;
             case SPECIAL_EXTRA_LONG_STUNNED:
                 specialExtraName = "pow";
                 break;
             case SPECIAL_EXTRA_SPEED:
                 specialExtraName = "speed";
                 break;
             case SPECIAL_EXTRA_SPEED2:
                 specialExtraName = "speed2";
                 break;
             case SPECIAL_EXTRA_SLOW:
                 specialExtraName = "slow";
                 break;
             case SPECIAL_EXTRA_MAYHEM:
                 specialExtraName = "mayhem";
                 break;
             case SPECIAL_EXTRA_HOLY_GRAIL:
                 specialExtraName = "holygrail";
                 break;
             case SPECIAL_EXTRA_LIFE:
                 specialExtraName = "life";
                 break;
             case SPECIAL_EXTRA_MULTIPLE:
                 specialExtraName = "multiple";
                 break;
             case SPECIAL_EXTRA_CLOAK:
                 specialExtraName = "cloak";
                 break;
             case SPECIAL_EXTRA_STUN_OTHERS:
                 specialExtraName = "pow";
                 break;
             case SPECIAL_EXTRA_MORPH:
                 specialExtraName = "morph";
                 break;
             case SPECIAL_EXTRA_STOP://EPFL
                 specialExtraName = "stop";
                 break;
             case SPECIAL_EXTRA_ELECTRIFY://EPFL
                 specialExtraName = "electrify";
                 break;
             case SPECIAL_EXTRA_FROGGER://EPFL
                 specialExtraName = "frogger";
                 break;
             case SPECIAL_EXTRA_SNIPE://Skywalker
                 specialExtraName = "snipe";
                 break;
             case SPECIAL_EXTRA_PHANTOM://EPFL
                 specialExtraName = "phantom";
                 break;
             case SPECIAL_EXTRA_GHOST://EPFL
                 specialExtraName = "ghost";
                 break;
             case SPECIAL_EXTRA_THROUGH://EPFL
                 specialExtraName = "through";
                 break;
             case SPECIAL_EXTRA_REVIVE://EPFL
                 specialExtraName = "revive";
                 break;
             case SPECIAL_EXTRA_STEAL://EPFL
                 specialExtraName = "steal";
                 break;
             case SPECIAL_EXTRA_SWAPPOSITION://EPFL
                 specialExtraName = "swapposition";
                 break;
             case SPECIAL_EXTRA_SWAPCOLOR://EPFL
                 specialExtraName = "swapcolor";
                 break;
             case SPECIAL_EXTRA_DALEIF://EPFL
                 specialExtraName = "daleif";
                 break;
             case SPECIAL_EXTRA_CHOICE://EPFL
                 specialExtraName = "choice";
                 break;
             case SPECIAL_EXTRA_EVILGRAIL://EPFL
                 specialExtraName = "evilgrail";
                 break;
             case SPECIAL_EXTRA_JUMP://EPFL
                 specialExtraName = "jump";
                 break;
             case SPECIAL_EXTRA_PLAYERFART://EPFL
                 specialExtraName = "fart";
                 break;
             case SPECIAL_EXTRA_PLAYERANDBOMBFART://EPFL
                 specialExtraName = "fart";
                 break;
            }
        
       return specialExtraName;
}
    /**
     * Gets the probability, in the TNT format, of the things under the blasted
     * blocks.
     *
     * @return   The probs in TNT format
     */
    public int[] getTNTProbs() {
        double sum = 0;
        int[] slh = new int[5];

        for(int i = 0; i < prob.length; i++)
            sum += prob[i];
        if(sum > 100) {
            for(int i = 0; i < prob.length; i++)
                prob[i] = prob[i] * (100.0 / sum);
        }
        for(int i = 0; i < prob.length; i++)
            slh[i] = (int) Math.round(prob[i]);
        return slh;
    }


    /**
     * Sets the prob attribute of the Level object
     *
     * @param typ    The type of probability you want to set
     * @param value  The new prob value
     */
    public void setProb(int typ, int value) {
        prob[typ] = value;
    }


    /**
     * Generates a filname but NO file extention
     *
     * @return   The fileName
     */
    public String getFileName() {
        boolean lastSkipped = true;
        StringBuffer s = new StringBuffer();
        for(int i = 0; i < title.length(); i++) {
            char c = title.charAt(i);
            if(c >= 'a' && c <= 'z' && lastSkipped) {
                s.append(Character.toUpperCase(c));
                lastSkipped = false;
            }
            else if(Character.isDigit(c)) {
                s.append(c);
                lastSkipped = true;
            }
            else if(!isValidLetter(c))
                lastSkipped = true;
            else {
                s.append(c);
                lastSkipped = false;
            }
        }
        return s.toString();
    }


    /**
     * Checks if a letter is valid as a name on a struct in XBlast
     *
     * @param c  The letter to check
     * @return   True if the letter is valid, otherwise false
     */
    private boolean isValidLetter(char c) {
        return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
    }


    /**
     * Checks if the <code>Level</code> has been saved to disk
     *
     * @return   True if saved, otherwise false
     */
    public boolean isSaved() {
        return title.equals(lastSave.title) && author.equals(lastSave.author) && description.equals(lastSave.description) &&
                GM_2_Player == lastSave.GM_2_Player && GM_3_Player == lastSave.GM_3_Player && GM_4_Player == lastSave.GM_4_Player && GM_5_Player == lastSave.GM_5_Player && GM_6_Player == lastSave.GM_6_Player &&
                GM_Random == lastSave.GM_Random && GM_SinglePlayer == lastSave.GM_SinglePlayer &&
                GM_Team == lastSave.GM_Team && GM_Double == lastSave.GM_Double && GM_Single == lastSave.GM_Single && GM_LR_Players == lastSave.GM_LR_Players &&
                shrinkPattern == lastSave.shrinkPattern && specialExtra == lastSave.specialExtra && distribution == lastSave.distribution &&
                inIllness == lastSave.inIllness && revIllness == lastSave.revIllness && inExtra == lastSave.inExtra && revExtra == lastSave.revExtra && inGameSpecial == lastSave.inGameSpecial && turnGameSpecial == lastSave.turnGameSpecial && specKey == lastSave.specKey &&
                inKick == lastSave.inKick && revKick == lastSave.revKick &&
                scrambleDrawTime == lastSave.scrambleDrawTime && scrambleDelTime == lastSave.scrambleDelTime &&
                bombsAtStart == lastSave.bombsAtStart && range == lastSave.range && fuseTime == lastSave.fuseTime && direction == lastSave.direction && defaultType == lastSave.defaultType && specialType == lastSave.specialType &&
                hiddenType == lastSave.hiddenType && bombClick == lastSave.bombClick && wallClick == lastSave.wallClick && playerClick == lastSave.playerClick && hauntSpeed == lastSave.hauntSpeed &&
                freeBlock.equals(lastSave.freeBlock) && solidWall.equals(lastSave.solidWall) && blastableBlock.equals(lastSave.blastableBlock) && voidBlock.equals(lastSave.voidBlock) &&
                extraBlock.equals(lastSave.extraBlock) &&
                prob[0] == lastSave.prob[0] && prob[1] == lastSave.prob[1] && prob[2] == lastSave.prob[2] && prob[3] == lastSave.prob[3] && prob[4] == lastSave.prob[4] &&
                !mapChanged;
    }


    /**
     * Marks this level as saved.
     */
    public void setSaved() {
        try {
            lastSave = (Level) clone();
        } catch(CloneNotSupportedException e) {
            System.out.println(e);
        }
        mapChanged = false;
        lastSave.prob = new double[prob.length];
        for(int i = 0; i < prob.length; i++)
            lastSave.prob[i] = prob[i];
    }


    /**
     * Gets the number of players.
     *
     * @return   The number of players
     */
    public int getNbrOfPlayers() {
        int nbrOfPlayers = 0;

        if(GM_2_Player)
            nbrOfPlayers = 2;
        if(GM_3_Player)
            nbrOfPlayers = 3;
        if(GM_4_Player)
            nbrOfPlayers = 4;
        if(GM_5_Player)
            nbrOfPlayers = 5;
        if(GM_6_Player)
            nbrOfPlayers = 6;

        return nbrOfPlayers;
    }


    /**
     * Checks if the level uses any EPFL features
     *
     * @return   true if EPFL feature is used, false otherwise.
     */
    public boolean isEPFL() {
        return (specialExtra >= SPECIAL_EXTRA_STOP && specialExtra <= SPECIAL_EXTRA_PLAYERANDBOMBFART) ||
                (inExtra >= SPECIAL_EXTRA_STOP && inExtra <= SPECIAL_EXTRA_PLAYERANDBOMBFART) ||
                (revExtra >= SPECIAL_EXTRA_STOP && revExtra <= SPECIAL_EXTRA_PLAYERANDBOMBFART) ||
                (shrinkPattern >= SHRINK_DIAG && shrinkPattern <= SHRINK_ISTY_COMPOUND_2_F) ||
                (inGameSpecial >= SPECIAL_INIT_SPECIAL_BOMBS_INFINITY && inGameSpecial <= SPECIAL_INIT_FIRE_WALLS) ||
                turnGameSpecial == SPECIAL_GAME_NASTY_CEIL ||
                (defaultType >= BOMB_SEARCH && defaultType <= BOMB_PROTECTBOMBS) ||
                (specialType >= BOMB_SEARCH && specialType <= BOMB_PROTECTBOMBS) ||
                (hiddenType >= BOMB_SEARCH && hiddenType <= BOMB_PROTECTBOMBS) ||
                bombClick == BOMB_CLICK_SPLIT;
    }

}

