/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch;

import swingwt.awt.*;
import java.io.*;
import storch.gui.GraphicsData;

/**
 * This class describes a block on the map. It contains information about
 * the block type, filname of the blocks image and the colors.
 *
 * @author   Tobias Johansson
 */
public class Block{

	public static final int OTHER_BLOCK  = -1;
	public static final int FREE_BLOCK  = Level.MAP_FREE;
	public static final int SOLID_WALL  = Level.MAP_SOLID;
	public static final int BLASTABLE_BLOCK  = Level.MAP_BLASTABLE;
	public static final int VOID_BLOCK  = Level.MAP_VOID;
	
	public static final int BOMB_EXTRA  = Level.MAP_BOMB_EXTRA;
	public static final int RANGE_EXTRA  = Level.MAP_RANGE;
	public static final int SPECIAL_EXTRA  = Level.MAP_SPECIAL_EXTRA;
	public static final int ILLNESS  = Level.MAP_ILLNESS;
	public static final int FREE_W_BOMB  = Level.MAP_EXPLODING_BOMB;
	public static final int SCRAMBLE_DRAW  = 9;
	public static final int SCRAMBLE_DELETE  = 10;
		
	private String fileName;
	private Color color0;
	private Color color1;
	private Color color2;
    public int colors;
	private Image theImage;
	private int type;
	
	/**
	 * Constructor for the Block object.
	 *
	 * @param type      The block type. For the diferent types see the Fields of this class.
	 * @param fileName  The file name for this blocks image.
	 * @param color0    Color0 for this blocks image.
	 * @param color1    Color1 for this blocks image.
	 * @param color2    Color2 for this blocks image.
	 */
	public Block(int type, String fileName, Color color0, Color color1, Color color2){
		this.type = type;
		this.fileName = fileName;
		this.color0 = color0;
		this.color1 = color1;
		this.color2 = color2;
		this.colors=1;
	
	}
	
	/**
	 * Constructor for the Block object.
	 * This constructor gives <code>color0</code> the default value black.
	 *
	 * @param type      The block type. For the diferent types see the Fields of this class.
	 * @param fileName  The file name for this blocks image.
	 * @param color1    Color1 for this blocks image.
	 * @param color2    Color2 for this blocks image.
	 */
	public Block(int type, String fileName, Color color1, Color color2){
	   
		this(type, fileName, Color.black, color1, color2);
	}

	
	/**
	 * Gets the fileName attribute of the Block object
	 *
	 * @return   The fileName
	 */
	public String getFileName() {
		return fileName;
	}


	/**
	 * Sets the fileName attribute of the Block object
	 *
	 * @param fileName  The new fileName
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
		createImage();
	}


	/**
	 * Gets the color0 attribute of the Block object
	 *
	 * @return   The color0 value
	 */
	public Color getColor0() {
		return color0;
	}


	/**
	 * Sets the color0 attribute of the Block object
	 *
	 * @param color0  The new color0 value
	 */
	public void setColor0(Color color0) {
		this.color0 = color0;
		createImage();
	}


	/**
	 * Gets the color1 attribute of the Block object
	 *
	 * @return   The color1 value
	 */
	public Color getColor1() {
		return color1;
	}


	/**
	 * Sets the color1 attribute of the Block object
	 *
	 * @param color1  The new color1 value
	 */
	public void setColor1(Color color1) {
		this.color1 = color1;
		createImage();
	}


	/**
	 * Gets the color2 attribute of the Block object
	 *
	 * @return   The color2 value
	 */
	public Color getColor2() {
		return color2;
	}


	/**
	 * Sets the color2 attribute of the Block object
	 *
	 * @param color2  The new color2 value
	 */
	public void setColor2(Color color2) {
		this.color2 = color2;
		createImage();
	}
	
	
	/**
	 * Gets the image representation of this block.
	 *
	 * @return   The <code>Image</code>
	 */
	 public Image getImage(){
	 	if(theImage == null)
	 		createImage();
		return theImage;
	}
	
	
	/**
	 * Creates the image representation of this block, based on current filename and color settings.
	 */	
	private void createImage(){
	    try {System.out.println("create image "+fileName);
			byte[] imageData = GraphicsData.getInstance().getImageData(fileName);
Toolkit toolkit = Toolkit.getDefaultToolkit();
			if(imageData != null){
				DataInputStream in = new DataInputStream(new ByteArrayInputStream(imageData));
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				
				
				GIFEditor editor = new GIFEditor(in, out);
				
				editor.editColorTable(color0, color1, color2);
			
				theImage = Toolkit.getDefaultToolkit().createImage(out.toByteArray());
				    
			
				    
				in.close();
				out.close();
			}else{
                            
                              if(fileName.equals("doom")){
                        fileName="choice";
                    }
                           System.out.println(" couldnt load file "+fileName+" trying something else");
                           String fileName1 = "extra_"+fileName;
                           imageData = GraphicsData.getInstance().getImageData(fileName1);
			if(imageData != null){
				DataInputStream in = new DataInputStream(new ByteArrayInputStream(imageData));
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				
				
				GIFEditor editor = new GIFEditor(in, out);
				
				editor.editColorTable(color0, color1, color2);
				theImage = Toolkit.getDefaultToolkit().createImage(out.toByteArray());
				
				//	
				in.close();
				out.close();
			}else{
 System.out.println(" couldnt load file "+fileName1+" trying something else");
String fileName2 = "extra_"+fileName+".gif";
                           imageData = GraphicsData.getInstance().getImageData(fileName2);
			if(imageData != null){
				DataInputStream in = new DataInputStream(new ByteArrayInputStream(imageData));
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				
				GIFEditor editor = new GIFEditor(in, out);
				editor.editColorTable(color0, color1, color2);
				theImage = Toolkit.getDefaultToolkit().createImage(out.toByteArray());
				
				in.close();
				out.close();
			}else{
 System.out.println("didnt work "+fileName2+" to load");
                            
                        }
                           
			}
			}

	    }
		catch (FileNotFoundException e) {
			System.out.println("Block: Image file not found");
			System.out.println(Storch.IMAGE_PATH + fileName + ".gif");
		}
		catch (IOException e) {
			System.out.println("Block: Error converting image");
		}
	}
	
	
	/**
	 * Checks if this block is equal to <code>b</code>.
	 *
	 * @param b  The block to compare to
	 * @return   True if the blocks have the same attributes, otherwise false
	 */
	public boolean equals(Block b){
		return fileName.equals(b.fileName) && color0.getRGB() == b.getColor0().getRGB() 
			&& color1.getRGB() == b.getColor1().getRGB() && color2.getRGB() == b.getColor2().getRGB();
	}	
}
