/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch;

import java.util.Properties;
import java.io.*;

/**
 * This class is used to save and retrive settings for Storch.
 *
 * @author   Tor Andr�
 */
public class Settings {
    private static Settings instance = null;
    private Properties prop;
    private File propFile;


    /**
     * Constructor for the Settings object
     */
    private Settings() {
        InputStream inStream = null;
        propFile = new File(System.getProperties().getProperty("user.home") + File.separator + ".storch");
        prop = new Properties();

        if(propFile.exists()) {
            try {
                inStream = new FileInputStream(propFile);
            } catch(FileNotFoundException e) {}
        }
        else {
            inStream = getClass().getResourceAsStream(Storch.DATA_PATH + "defprops");
        }
        try {
            prop.load(inStream);
        } catch(IOException e) {}
    }


    /**
     * Returns a instance of the Settings class.
     *
     * @return   a Settings object.
     */
    public static Settings instance() {
        if(instance == null)
            instance = new Settings();
        return instance;
    }


    /**
     * Enables/disables the EPFL warning.
     *
     * @param b  true if the warning should be displayed
     */
    public void setEPFLWarning(boolean b) {
        prop.setProperty("EPFLWarning", b ? "true" : "false");
    }


    /**
     * Checks if the EPFL warning should be shown
     *
     * @return   true if the warning is enabled.
     */
    public boolean getEPFLWarning() {
        String val = prop.getProperty("EPFLWarning", "true");
        return val.equals("true");
    }


    /**
     * Gets the levelDir attribute of the Settings object
     *
     * @return   The levelDir
     */
    public String getLevelDir() {
        return prop.getProperty("levelDir", new File("levels").getAbsolutePath());
    }


    /**
     * Gets the docDir attribute of the Settings object
     *
     * @return   The docDir
     */
    public String getDocDir() {
        return prop.getProperty("docDir", new File("doc").getAbsolutePath());
    }


    /**
     * Sets the levelDir attribute of the Settings object
     *
     * @param dir  The new levelDir value
     */
    public void setLevelDir(String dir) {
        prop.setProperty("levelDir", dir);
    }


    /**
     * Sets the docDir attribute of the Settings object
     *
     * @param dir  The new docDir value
     */
    public void setDocDir(String dir) {
        prop.setProperty("docDir", dir);
    }


    /**
     * Gets the EPFLSupport attribute of the Settings object
     *
     * @return   The EPFLSupport value
     */
    public boolean getEPFLSupport() {
        String val = prop.getProperty("EPFLSupport", "true");
        return val.equals("true");
    }


    /**
     * Sets the EPFLSupport attribute of the Settings object
     *
     * @param b  The new EPFLSupport value
     */
    public void setEPFLSupport(boolean b) {
        prop.setProperty("EPFLSupport", b ? "true" : "false");

    }


    /**
     * Saves the settings to file
     */
    public void save() {
        FileOutputStream outStream;
        try {
            outStream = new FileOutputStream(propFile);
            prop.store(outStream, "Storch");
            outStream.close();
        } catch(Exception e) {}
    }
}
