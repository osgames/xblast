/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

import swingwtx.swing.*;
import swingwt.awt.*;

import storch.*;

/**
 *  A panel for setting the probabilities for what will appear under blasted blocks.
 *
 *  @author    Tobias Johansson
 */
class ProbsPanel extends JPanel {

    private JComboBox[] probs;


    /**
     *  Constructor for the <code>ProbsPanel</code> object.
     */
    public ProbsPanel() {
        super();
        probs = new JComboBox[5];

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(gridbag);

        gbc.anchor = GridBagConstraints.EAST;
        gbc.gridx = 0;

        add(new JLabel("Bomb extra:   "), gbc);
        add(new JLabel("Range extra:   "), gbc);
        add(new JLabel("Illness:   "), gbc);
        add(new JLabel("Special extra:  "), gbc);
        add(new JLabel("Hidden bomb:   "), gbc);

        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridx = 1;
        for(int i = 0; i < 5; i++) {
            probs[i] = new JComboBox();
            probs[i].addItem(new ComboBoxItem("Never", Level.PROB_NULL));
            probs[i].addItem(new ComboBoxItem("Scarce", Level.PROB_SCARCE));
            probs[i].addItem(new ComboBoxItem("Rare", Level.PROB_RARE));
            probs[i].addItem(new ComboBoxItem("Uncommon", Level.PROB_UNCOMMON));
            probs[i].addItem(new ComboBoxItem("Common", Level.PROB_COMMON));
            probs[i].addItem(new ComboBoxItem("Plentiful", Level.PROB_PLENTIFUL));
            add(probs[i], gbc);
        }

        probs[0].setToolTipText("The probability for a bomb extra to appear");
        probs[1].setToolTipText("The probability for a range extra to appear");
        probs[2].setToolTipText("The probability for an illness to appear");
        probs[3].setToolTipText("The probability for a special extra to appear");
        probs[4].setToolTipText("The probability for a hidden bomb to appear");

        setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Under blasted blocks"));
        load();
    }


    /**
     *  Loads the probability data from the current open level.
     */
    public void load() {
        Level theLevel = Storch.theLevel;
        int[] tntProbs = theLevel.getTNTProbs();
        for(int i = 0; i < 5; i++) {
            GUI.setValue(probs[i], int2type(tntProbs[i]));
        }
    }


    /**
     *  Saves the probability data to the current open level.
     */
    public void save() {
        Level theLevel = Storch.theLevel;

        theLevel.setProb(Level.PROB_BOMB_EXTRA, ((ComboBoxItem) probs[0].getSelectedItem()).value);
        theLevel.setProb(Level.PROB_RANGE, ((ComboBoxItem) probs[1].getSelectedItem()).value);
        theLevel.setProb(Level.PROB_ILL, ((ComboBoxItem) probs[2].getSelectedItem()).value);
        theLevel.setProb(Level.PROB_SPECIAL_EXTRA, ((ComboBoxItem) probs[3].getSelectedItem()).value);
        theLevel.setProb(Level.PROB_HIDDEN_BOMB, ((ComboBoxItem) probs[4].getSelectedItem()).value);
    }


    /**
     *  Translates a probability to one of 6 predefined 'classes' of probability.
     *
     *  @param  prob  The probability value
     *  @return       An integer defining the probability 'class'. Possible return values are 
     *				  Level.PROB_NULL, Level.PROB_SCARCE, Level.PROB_RARE, Level.PROB_UNCOMMON,
     *				  Level.PROB_COMMON and Level.PROB_PLENTIFUL.
     */
    private int int2type(int prob) {
        int result = Level.PROB_NULL;
        if(prob > (Level.PROB_NULL + Level.PROB_SCARCE) / 2) {
            result = Level.PROB_SCARCE;
        }
        if(prob > (Level.PROB_SCARCE + Level.PROB_RARE) / 2) {
            result = Level.PROB_RARE;
        }
        if(prob > (Level.PROB_RARE + Level.PROB_UNCOMMON) / 2) {
            result = Level.PROB_UNCOMMON;
        }
        if(prob > (Level.PROB_UNCOMMON + Level.PROB_COMMON) / 2) {
            result = Level.PROB_COMMON;
        }
        if(prob > (Level.PROB_COMMON + Level.PROB_PLENTIFUL) / 2) {
            result = Level.PROB_PLENTIFUL;
        }
        return result;
    }
}
