/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

import storch.*;

/**
 *  Represents a graphics style of a level. A graphics style is defined by a name and four 
 *  <code>Block</code> objects, representing free, solid, blastable and void blocks.
 *
 *  @author     Tobias Johansson
 */
class GraphicsStyle {

    public String name;
    public Block free;
    public Block solid;
    public Block blastable;
    public Block voidBlock;
    public Block extraBlock;


    /**
     *  Constructor for the <code>GraphicsStyle</code> object.
     *
     *  @param  name       The name of this graphics style
     *  @param  free       A <code>Block</code> object representing a free block
     *  @param  solid      A <code>Block</code> object representing a solid block
     *  @param  blastable  A <code>Block</code> object representing a blastable block
     *  @param  voidBlock  A <code>Block</code> object representing a void block
     *  @param  extraBlock  A <code>Block</code> object representing a extra block
     */
    public GraphicsStyle(String name, Block free, Block solid, Block blastable, Block voidBlock, Block extraBlock) {
        this.name = name;
        this.free = free;
        this.solid = solid;
        this.blastable = blastable;
        this.voidBlock = voidBlock;
        this.extraBlock = extraBlock;
    }


    /**
     *  Returns the name of this style.
     *
     *  @return    The name of this style
     */
    public String toString() {
        return name;
    }
}
