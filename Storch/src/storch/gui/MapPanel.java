/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

import swingwtx.swing.*;
import swingwt.awt.*;
import swingwt.awt.event.*;
import storch.*;

/**
 *  A panel containing a <code>Map</code> and other related objects.
 *
 *  @author     Tobias Johansson
 */
class MapPanel extends JPanel implements ActionListener {

    private Map theMap;
    private ScramblePanel scrambleShrinkPanel;
    private BlockChooser chooser;
    private JButton clearButton;
    private TimeSliderPanel slider;


    /**
     *  Constructor for the <code>MapPanel</code> object.
     */
    public MapPanel() {
        super();

        chooser = new BlockChooser();
        scrambleShrinkPanel = new ScramblePanel();
        slider = new TimeSliderPanel();
        theMap = new Map(chooser, scrambleShrinkPanel, slider);
        clearButton = new JButton("Clear map");
System.out.println("    news created ");

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(gridbag);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridx = 0;
        gbc.gridheight = 2;
        add(theMap, gbc);
        gbc.gridheight = 1;
        gbc.insets = new Insets(0, 6, 0, 0);
        add(chooser, gbc);
        gbc.gridx = 1;
        add(scrambleShrinkPanel, gbc);
        add(slider, gbc);
        gbc.fill = GridBagConstraints.NONE;
        add(clearButton, gbc);
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Map"));

        chooser.setToolTipText("Click on an image to assign it to the left or right mouse button");
        theMap.setToolTipText("Click to place blocks on map. Drag the white heads to change players start positions");
        clearButton.setToolTipText("Clear the map");

        clearButton.addActionListener(this);
    }


    /**
     *  Loads the map data from the current open level.
     */
    public void load() {
        chooser.load();
        theMap.load();
        scrambleShrinkPanel.load();
        slider.setTime(0);
    }


    /**
     *  Saves the map data to the current open level.
     */
    public void save() {
        theMap.save();
        scrambleShrinkPanel.save();
    }


    /**
     *  Called by the <code>ScramblePanel</code> to repaint the map.
     */
    public void notifyScrambleChange() {
        theMap.repaint();
    }


    /**
     *  Called by the <code>GraphicsPanel</code> to repaint the map.
     */
    public void notifyGraphicsChange() {
        chooser.load();
        chooser.repaint();
        theMap.repaint();
    }
    
    /**
     *  Called by the <code>TimeSliderPanel</code> to repaint the map.
     */
    public void notifyTimeChange() {
        theMap.repaint();
    }


    /**
     *  Called when the Clear button is pressed. Clears the map.
     *
     *  @param  e  The corresponding <code>ActionEvent</code>
     */
    public void actionPerformed(ActionEvent e) {
        theMap.clear();
        Storch.theLevel.mapChanged = true;
    }

}
