/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

import swingwtx.swing.*;
import swingwt.awt.*;
import swingwt.awt.event.*;
import swingwt.awt.image.*;
import storch.*;

/**
 *  Represents the map on which the user places blocks to define the appearance of his/her level.
 *
 *  @author     Tobias Johansson
 */
class Map extends JPanel implements MouseListener, MouseMotionListener{

    private int[][] map;
    private int[][] startPositions;
    private BlockChooser chooser;
    private ScramblePanel scramble;
    private TimeSliderPanel slider;
    private Image head;
    private Cursor headCursor, defaultCursor;
    private final int SQUARE_WIDTH = 28, SQUARE_HEIGHT = 21;
    private int nbrOfPlayers, markedHead;
    private int lastClickedSquare;
    private int lastBlockType;


    /**
     *  Constructor for the <code>Map</code> object.
     *
     *  @param  chooser   The <code>BlockChooser</code> for this <code>Map</code>
     *  @param  scramble  The <code>ScramblePanel</code> that defines the scrable draw and delete blocks
     */
    public Map(BlockChooser chooser, ScramblePanel scramble, TimeSliderPanel slider) {
        super(true);

        markedHead = -1;
        lastClickedSquare = -1;
        lastBlockType = Block.FREE_BLOCK;
        this.chooser = chooser;
        this.scramble = scramble;
        this.slider = slider;

        head = Toolkit.getDefaultToolkit().getImage(getClass().getResource(Storch.IMAGE_PATH + "head.gif"));
        Image headCursorImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource(Storch.IMAGE_PATH + "head_32x32.gif"));
	// headCursor = Toolkit.getDefaultToolkit().createCustomCursor(headCursorImage, new Point(10, 10), "headCursor");
        defaultCursor = Cursor.getDefaultCursor();
        
        addMouseListener(this);
        addMouseMotionListener(this);

        load();
    }

    /**
     *  Called when the user clicks somewhere on the map. Places the desired block on the corresponding
     *  place on the map.
     *
     *  @param  e  The corresponding <code>MouseEvent</code>
     */
    public void mousePressed(MouseEvent e) {
        int x = e.getX() / SQUARE_WIDTH;
        int y = e.getY() / SQUARE_HEIGHT;
        
        lastClickedSquare = y * 15 + x;
        
        if(x > 0 && x < 14 && y > 0 && y < 12) {
            if(slider.getTime() == 0){
	            for(int i = 0; i < nbrOfPlayers; i++) {
	                if(x == startPositions[i][0] && y == startPositions[i][1]) {
	                    markedHead = i;
	                    //setCursor(headCursor);
	                    repaint(x * SQUARE_WIDTH, y * SQUARE_HEIGHT, SQUARE_WIDTH, SQUARE_HEIGHT);
	                }
	        	}
	        }
			int type = Block.FREE_BLOCK;
			if((e.getModifiers() & e.BUTTON3_MASK) > 0) {
				type = chooser.getRightButton();
			} else if((e.getModifiers() & e.BUTTON1_MASK) > 0) {
				type = chooser.getLeftButton();
			}
            placeBlockOnSquare(type, x, y);
        }
    }


    /**
     *  Empty method, only needed to implement the <code>MouseListener</code> interface.
     *
     *  @param  e  A <code>MouseEvent</code>
     */
    public void mouseClicked(MouseEvent e) { }


    /**
     *  Empty method, only needed to implement the <code>MouseListener</code> interface.
     *
     *  @param  e  A <code>MouseEvent</code>
     */
    public void mouseEntered(MouseEvent e) { }


    /**
     *  Empty method, only needed to implement the <code>MouseListener</code> interface.
     *
     *  @param  e  A <code>MouseEvent</code>
     */
    public void mouseExited(MouseEvent e) { }


    /**
     *  Called when the user releases the mouse button. Updates the players' start positions if necessary.
     *
     *  @param  e  A corresponding <code>MouseEvent</code>
     */
    public void mouseReleased(MouseEvent e) {
        if(markedHead > -1){
	        int x = e.getX() / SQUARE_WIDTH;
	        int y = e.getY() / SQUARE_HEIGHT;
	        if(x > 0 && x < 14 && y > 0 && y < 12) {
	            startPositions[markedHead][0] = x;
	            startPositions[markedHead][1] = y;
	        }
	        markedHead = -1;
	        setCursor(defaultCursor);
	        repaint();
	    }
    }
    
    
    /**
     *  Called when the user drags the mouse. Checks if the mouse pointer enters a new square, 
     *  in which case it is counted as a click.
     *
     *  @param  e  A corresponding <code>MouseEvent</code>
     */
    public void mouseDragged(MouseEvent e) {
    	int x = e.getX() / SQUARE_WIDTH;
        int y = e.getY() / SQUARE_HEIGHT;
        int squareNbr = y * 15 + x;
        if(x > 0 && x < 14 && y > 0 && y < 12 && squareNbr != lastClickedSquare){
        	lastClickedSquare = squareNbr;
        	placeBlockOnSquare(lastBlockType, x, y);
        }
	}

	
	/**
     *  Empty method, only needed to implement the <code>MouseMotionListener</code> interface.
     *
     *  @param  e  A <code>MouseEvent</code>
     */
    public void mouseMoved(MouseEvent e) { }
    

	/**
     *  Places a block on a square on the map.
     *
     *  @param  blockType	The type of block to place on the square
     *  @param  x  			The x coordinate of the square
     *  @param  y  			The y coordinate of the square
     */
	private void placeBlockOnSquare(int blockType, int x, int y){
		if(markedHead < 0) {
			int time = slider.getTime();
	        if(blockType == Block.SCRAMBLE_DRAW) {
	            if(time < scramble.getDrawTime())
	            	slider.setTime(scramble.getDrawTime());
	            if(scramble.getDrawMap()[x][y] != blockType)
	            	scramble.getDrawMap()[x][y] = blockType;
	            else
	            	scramble.getDrawMap()[x][y] = map[x][y];
	        } 
	        else if(blockType == Block.SCRAMBLE_DELETE) {
	            if(time < scramble.getDeleteTime())
	            	slider.setTime(scramble.getDeleteTime());
	            if(scramble.getDeleteMap()[x][y] != blockType)
	            	scramble.getDeleteMap()[x][y] = blockType;
	            else
	            	scramble.getDeleteMap()[x][y] = map[x][y];
	        } 
	        else {
	            map[x][y] = blockType;
	        }
	        lastBlockType = blockType;
	        Storch.theLevel.mapChanged = true;
	        repaint(x * SQUARE_WIDTH, y * SQUARE_HEIGHT, SQUARE_WIDTH, SQUARE_HEIGHT);
	   }
	}


    /**
     *  Paints the graphics of the map
     *
     *  @param  g  The <code>Graphics</code> object to paint on
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
				
		int time = slider.getTime();
		int draw = scramble.getDrawTime();
		int del = scramble.getDeleteTime();

        paintMap(g);
           
        int tmptime = 0;	
        if(time >= draw && draw <= del){
        	paintShrink(0, draw, g);
        	scrambleDraw(g);
        	tmptime = draw;
        	if(time >= del){
        		paintShrink(draw, del, g);
        		scrambleDelete(g);
        		tmptime = del;
        	}
        }
        else if(time >= del && del <= draw){
        	paintShrink(0, del, g);
        	scrambleDelete(g);
        	tmptime = del;
        	if(time >= draw){
        		paintShrink(del, draw, g);
        		scrambleDraw(g);
        		tmptime = draw;
        	}
        }
        paintShrink(tmptime, time, g);
        
        if(slider.getTime() == 0)
        	paintPlayers(g);
    }


    /**
     *  Paints the normal blocks (free, solid, blastable, void) on the map graphics.
     *
     *  @param  g  The <code>Graphics</code> object to paint on
     */
    private void paintMap(Graphics g) {
        for(int x = 0; x < 15; x++) {
            for(int y = 0; y < 13; y++) {
                if(map[x][y] == Block.FREE_W_BOMB) {
                    g.drawImage(chooser.type2Image(Block.FREE_BLOCK), x * SQUARE_WIDTH, y * SQUARE_HEIGHT, this);
                }
                g.drawImage(chooser.type2Image(map[x][y]), x * SQUARE_WIDTH, y * SQUARE_HEIGHT, this);
            }
        }
    }
    
    
    /**
     *  Paints shrink blocks, that appear between two times, on the map graphics.
     *
     *  @param  time1	A time, in percent of total game time.
     *  @param  time2	A time, in percent of total game time.
     *  @param  g		The <code>Graphics</code> object to paint on
     */
    private void paintShrink(int time1, int time2, Graphics g){
    	ShrinkData shrink = new ShrinkData();
    	ShrinkBlockList blocks = shrink.getChanges(Storch.theLevel.shrinkPattern, time1, time2);
    	int x, y;
    	for(int i = 0; i < blocks.solids.length; i++){
    		x = blocks.solids[i] % 15;
    		y = blocks.solids[i] / 15;
    		g.drawImage(chooser.type2Image(Block.SOLID_WALL), x * SQUARE_WIDTH, y * SQUARE_HEIGHT, this);
    	}
    	for(int i = 0; i < blocks.voids.length; i++){
    		x = blocks.voids[i] % 15;
    		y = blocks.voids[i] / 15;
    		g.drawImage(chooser.type2Image(Block.VOID_BLOCK), x * SQUARE_WIDTH, y * SQUARE_HEIGHT, this);
    	}
    	for(int i = 0; i < blocks.extras.length; i++){
    		x = blocks.extras[i] % 15;
    		y = blocks.extras[i] / 15;
    		g.drawImage(chooser.type2Image(Block.BLASTABLE_BLOCK), x * SQUARE_WIDTH, y * SQUARE_HEIGHT, this);
    	}
    }


    /**
     *  Paints the scramble draw blocks on the map graphics.
     *
     *  @param  g  The <code>Graphics</code> object to paint on
     */
    private void scrambleDraw(Graphics g) {
        int[][] drawMap = scramble.getDrawMap();
        for(int x = 1; x < 14; x++) {
            for(int y = 1; y < 12; y++) {
                if(drawMap[x][y] == Block.SCRAMBLE_DRAW) {
                    g.drawImage(chooser.type2Image(Block.SCRAMBLE_DRAW), x * SQUARE_WIDTH, y * SQUARE_HEIGHT, this);
                    g.setColor(Color.green);
                    g.drawRect(x * SQUARE_WIDTH, y * SQUARE_HEIGHT, SQUARE_WIDTH - 1, SQUARE_HEIGHT - 1);
                }
            }
        }
    }


    /**
     *  Paints the scramble delete blocks on the map graphics.
     *
     *  @param  g  The <code>Graphics</code> object to paint on
     */
    private void scrambleDelete(Graphics g) {
        int[][] delMap = scramble.getDeleteMap();
        for(int x = 1; x < 14; x++) {
            for(int y = 1; y < 12; y++) {
                if(delMap[x][y] == Block.SCRAMBLE_DELETE) {
                    g.drawImage(chooser.type2Image(Block.SCRAMBLE_DELETE), x * SQUARE_WIDTH, y * SQUARE_HEIGHT, this);
                    g.setColor(Color.red);
                    g.drawRect(x * SQUARE_WIDTH, y * SQUARE_HEIGHT, SQUARE_WIDTH - 1, SQUARE_HEIGHT - 1);
                }
            }
        }
    }


    /**
     *  Paints the players' start positions on the map graphics
     *
     *  @param  g  The <code>Graphics</code> object to paint on
     */
    private void paintPlayers(Graphics g) {
        for(int i = 0; i < nbrOfPlayers; i++) {
            if(markedHead != i) {
                g.drawImage(head, startPositions[i][0] * SQUARE_WIDTH + 5, startPositions[i][1] * SQUARE_HEIGHT + 2, this);
            }
        }
    }


    /**
     *  Gets the preferred size of the map
     *
     *  @return    The preferred size
     */
    public Dimension getPreferredSize() {
        return getMinimumSize();
    }


    /**
     *  Gets the minimum size of the map
     *
     *  @return    The minimum size
     */
    public synchronized Dimension getMinimumSize() {
        return new Dimension(420, 273);
    }


    /**
     *  Clears and repaints the map.
     */
    public void clear() {
        for(int x = 1; x < 14; x++) {
            for(int y = 1; y < 12; y++) {
                map[x][y] = Block.FREE_BLOCK;
                scramble.getDrawMap()[x][y] = Block.FREE_BLOCK;
                scramble.getDeleteMap()[x][y] = Block.FREE_BLOCK;
            }
        }
        repaint();
    }


    /**
     *  Loads the map data from the current open level.
     */
    public void load() {
        Level theLevel = Storch.theLevel;
        map = theLevel.map;
        startPositions = theLevel.startPos;
        nbrOfPlayers = theLevel.getNbrOfPlayers();
    }


    /**
     *  Saves the map data to the current open level.
     */
    public void save() { 
    	// does nothing because the theLevel.map = map
    }
}
