/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.gui;

import swingwtx.swing.*;

import swingwt.awt.*;
import swingwt.awt.event.*;
import storch.*;

/**
 * A panel for setting bomb options
 *
 * @author   Tobias Johansson
 */
class BombsPanel extends JPanel {
    private JTextField nbrBombs, range;
    private JComboBox fuse, dir, defType, specType, hidType, bClick, wClick, pClick;
    private ComboBoxItem search_epfl, ringofire, mine, diagthreebombs, scissor, scissor2, parallel, distance, parasol, lucky, comb, farpyro, nuclear, protectbombs;
    private ComboBoxItem bomb_click_split_epfl;

    /**
     * Constructor for the <code>BombsPanel</code> object
     */
    public BombsPanel() {
        super();

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        setLayout(gridbag);

        //EPFL bombs
        search_epfl = new ComboBoxItem("Search (EPFL)", Level.BOMB_SEARCH);
        ringofire = new ComboBoxItem("Ring of Fire (EPFL)", Level.BOMB_RING_OF_FIRE);
        mine = new ComboBoxItem("Mine (EPFL)", Level.BOMB_MINE);
        diagthreebombs = new ComboBoxItem("Diagonal 3 (EPFL)", Level.BOMB_DIAG_THREE_BOMBS);
        scissor = new ComboBoxItem("Scissor < (EPFL)", Level.BOMB_SCISSOR);
        scissor2 = new ComboBoxItem("Scissor2 > (EPFL)", Level.BOMB_SCISSOR_2);
        parallel = new ComboBoxItem("Parallel (EPFL)", Level.BOMB_PARALLEL);
        distance = new ComboBoxItem("Distance (EPFL)", Level.BOMB_DISTANCE);
        parasol = new ComboBoxItem("Parasol (EPFL)", Level.BOMB_PARASOL);
        lucky = new ComboBoxItem("Lucky (EPFL)", Level.BOMB_LUCKY);
        comb = new ComboBoxItem("Comb (EPFL)", Level.BOMB_COMB);
        farpyro = new ComboBoxItem("Far pyro (EPFL)", Level.BOMB_FARPYRO);
        nuclear = new ComboBoxItem("Nuclear (EPFL)", Level.BOMB_NUCLEAR);
        protectbombs = new ComboBoxItem("Protect bombs (EPFL)", Level.BOMB_PROTECTBOMBS);

	//EPFL bombClick
	bomb_click_split_epfl = new ComboBoxItem("Splits in 3 new bombs (EPFL)", Level.BOMB_CLICK_SPLIT);

        // JLabels
        c.gridx = 0;
        c.anchor = GridBagConstraints.EAST;

        add(new JLabel("Nbr of bombs at start:  "), c);
        add(new JLabel("Initial range:  "), c);
        add(new JLabel("Fuse time:  "), c);
        add(new JLabel("Moving direction:  "), c);
        add(new JLabel("Default bomb type:  "), c);
        add(new JLabel("Special bomb type:  "), c);
        add(new JLabel("Hidden bomb type:  "), c);
        add(new JLabel("Bomb hits bomb:  "), c);
        add(new JLabel("Bomb hits wall:  "), c);
        add(new JLabel("Bomb hits player:  "), c);

        //JTextFields and JComboBoxs
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 1;
        nbrBombs = new JTextField(2);
        nbrBombs.setBackground(Color.white);
        add(nbrBombs, c);
        range = new JTextField(2);
        range.setBackground(Color.white);
        add(range, c);
        c.fill = GridBagConstraints.HORIZONTAL;
        fuse = new JComboBox();
        fuse.addItem(new ComboBoxItem("Normal", Level.FUSE_NORMAL));
        fuse.addItem(new ComboBoxItem("Short", Level.FUSE_SHORT));
        fuse.addItem(new ComboBoxItem("Long", Level.FUSE_LONG));
        add(fuse, c);
        dir = new JComboBox();
        dir.addItem(new ComboBoxItem("None", Level.GO_STOP));
        dir.addItem(new ComboBoxItem("Down", Level.GO_DOWN));
        dir.addItem(new ComboBoxItem("Up", Level.GO_UP));
        dir.addItem(new ComboBoxItem("Left", Level.GO_LEFT));
        dir.addItem(new ComboBoxItem("Right", Level.GO_RIGHT));
        add(dir, c);
        defType = new JComboBox();
        defType.addItem(new ComboBoxItem("Normal", Level.BOMB_NORMAL));
        defType.addItem(new ComboBoxItem("Napalm", Level.BOMB_NAPALM));
        defType.addItem(new ComboBoxItem("Grenade", Level.BOMB_GRENADE));
        defType.addItem(new ComboBoxItem("Firecracker", Level.BOMB_FIRECRACKER));
        defType.addItem(new ComboBoxItem("Pyro bombs", Level.BOMB_PYRO));
        defType.addItem(new ComboBoxItem("Construction bombs", Level.BOMB_CONSTRUCTION));
        defType.addItem(new ComboBoxItem("Destruction bombs", Level.BOMB_DESTRUCTION));
        defType.addItem(new ComboBoxItem("Renovation bombs", Level.BOMB_RENOVATION));
        defType.addItem(new ComboBoxItem("3 bombs in a row", Level.BOMB_THREEBOMBS));
        defType.addItem(new ComboBoxItem("Triangle", Level.BOMB_TRIANGLEBOMBS));
        defType.addItem(new ComboBoxItem("Fungus", Level.BOMB_FUNGUS));
        defType.addItem(new ComboBoxItem("Random", Level.BOMB_RANDOM));
        defType.addItem(new ComboBoxItem("Short (TNT)", Level.BOMB_SHORT));
        defType.addItem(new ComboBoxItem("Close (TNT)", Level.BOMB_CLOSE));
        defType.addItem(new ComboBoxItem("Blast now (TNT)", Level.BOMB_BLASTNOW));
        defType.addItem(new ComboBoxItem("Firecracker 2 (TNT)", Level.BOMB_FIRECRACKER_2));
        defType.addItem(new ComboBoxItem("Pyro 2 (TNT)", Level.BOMB_PYRO_2));
        add(defType, c);
        specType = new JComboBox();
        specType.addItem(new ComboBoxItem("Normal", Level.BOMB_NORMAL));
        specType.addItem(new ComboBoxItem("Napalm", Level.BOMB_NAPALM));
        specType.addItem(new ComboBoxItem("Grenade", Level.BOMB_GRENADE));
        specType.addItem(new ComboBoxItem("Firecracker", Level.BOMB_FIRECRACKER));
        specType.addItem(new ComboBoxItem("Pyro bombs", Level.BOMB_PYRO));
        specType.addItem(new ComboBoxItem("Construction bombs", Level.BOMB_CONSTRUCTION));
        specType.addItem(new ComboBoxItem("Destruction bombs", Level.BOMB_DESTRUCTION));
        specType.addItem(new ComboBoxItem("Renovation bombs", Level.BOMB_RENOVATION));
        specType.addItem(new ComboBoxItem("3 bombs in a row", Level.BOMB_THREEBOMBS));
        specType.addItem(new ComboBoxItem("Triangle", Level.BOMB_TRIANGLEBOMBS));
        specType.addItem(new ComboBoxItem("Fungus", Level.BOMB_FUNGUS));
        specType.addItem(new ComboBoxItem("Random", Level.BOMB_RANDOM));
        specType.addItem(new ComboBoxItem("Short (TNT)", Level.BOMB_SHORT));
        specType.addItem(new ComboBoxItem("Close (TNT)", Level.BOMB_CLOSE));
        specType.addItem(new ComboBoxItem("Blast now (TNT)", Level.BOMB_BLASTNOW));
        specType.addItem(new ComboBoxItem("Firecracker 2 (TNT)", Level.BOMB_FIRECRACKER_2));
        specType.addItem(new ComboBoxItem("Pyro 2 (TNT)", Level.BOMB_PYRO_2));
        add(specType, c);
        hidType = new JComboBox();
        hidType.addItem(new ComboBoxItem("Normal", Level.BOMB_NORMAL));
        hidType.addItem(new ComboBoxItem("Napalm", Level.BOMB_NAPALM));
        hidType.addItem(new ComboBoxItem("Grenade", Level.BOMB_GRENADE));
        hidType.addItem(new ComboBoxItem("Firecracker", Level.BOMB_FIRECRACKER));
        hidType.addItem(new ComboBoxItem("Pyro bombs", Level.BOMB_PYRO));
        hidType.addItem(new ComboBoxItem("Construction bombs", Level.BOMB_CONSTRUCTION));
        hidType.addItem(new ComboBoxItem("Destruction bombs", Level.BOMB_DESTRUCTION));
        hidType.addItem(new ComboBoxItem("Renovation bombs", Level.BOMB_RENOVATION));
        hidType.addItem(new ComboBoxItem("3 bombs in a row", Level.BOMB_THREEBOMBS));
        hidType.addItem(new ComboBoxItem("Triangle", Level.BOMB_TRIANGLEBOMBS));
        hidType.addItem(new ComboBoxItem("Fungus", Level.BOMB_FUNGUS));
        hidType.addItem(new ComboBoxItem("Random", Level.BOMB_RANDOM));
        hidType.addItem(new ComboBoxItem("Short (TNT)", Level.BOMB_SHORT));
        hidType.addItem(new ComboBoxItem("Close (TNT)", Level.BOMB_CLOSE));
        hidType.addItem(new ComboBoxItem("Blast now (TNT)", Level.BOMB_BLASTNOW));
        hidType.addItem(new ComboBoxItem("Firecracker 2 (TNT)", Level.BOMB_FIRECRACKER_2));
        hidType.addItem(new ComboBoxItem("Pyro 2 (TNT)", Level.BOMB_PYRO_2));
        add(hidType, c);
        bClick = new JComboBox();
        bClick.addItem(new ComboBoxItem("Nothing happens", Level.BOMB_CLICK_NONE));
        bClick.addItem(new ComboBoxItem("Snooker", Level.BOMB_CLICK_SNOOKER));
        bClick.addItem(new ComboBoxItem("Explode", Level.BOMB_CLICK_CONTACT));
        bClick.addItem(new ComboBoxItem("Rebound", Level.BOMB_CLICK_REBOUND));
        bClick.addItem(new ComboBoxItem("Rebounded clockwise", Level.BOMB_CLICK_CLOCKWISE));
        bClick.addItem(new ComboBoxItem("Rebounded anticlockwise", Level.BOMB_CLICK_ANTICLOCKWISE));
        bClick.addItem(new ComboBoxItem("Rebounded randomly", Level.BOMB_CLICK_RANDOMDIR));
        add(bClick, c);
        wClick = new JComboBox();
        wClick.addItem(new ComboBoxItem("Nothing happens", Level.BOMB_CLICK_NONE));
        wClick.addItem(new ComboBoxItem("Explode", Level.BOMB_CLICK_CONTACT));
        wClick.addItem(new ComboBoxItem("Rebound", Level.BOMB_CLICK_REBOUND));
        wClick.addItem(new ComboBoxItem("Rebounded clockwise", Level.BOMB_CLICK_CLOCKWISE));
        wClick.addItem(new ComboBoxItem("Rebounded anticlockwise", Level.BOMB_CLICK_ANTICLOCKWISE));
        wClick.addItem(new ComboBoxItem("Rebounded randomly", Level.BOMB_CLICK_RANDOMDIR));
        add(wClick, c);
        pClick = new JComboBox();
        pClick.addItem(new ComboBoxItem("Player stunned", Level.BOMB_CLICK_NONE));
        pClick.addItem(new ComboBoxItem("Continues through", Level.BOMB_CLICK_THRU));
        pClick.addItem(new ComboBoxItem("Explode", Level.BOMB_CLICK_CONTACT));
        pClick.addItem(new ComboBoxItem("Rebound", Level.BOMB_CLICK_REBOUND));
        pClick.addItem(new ComboBoxItem("Rebounded clockwise", Level.BOMB_CLICK_CLOCKWISE));
        pClick.addItem(new ComboBoxItem("Rebounded anticlockwise", Level.BOMB_CLICK_ANTICLOCKWISE));
        pClick.addItem(new ComboBoxItem("Rebounded randomly", Level.BOMB_CLICK_RANDOMDIR));
        add(pClick, c);

        nbrBombs.setToolTipText("Number of bombs each player has at start");
        range.setToolTipText("The initial blast range (in squares) of the bombs");
        fuse.setToolTipText("The time it takes before a bomb explodes");
        dir.setToolTipText("The moving direction of the bombs");
        defType.setToolTipText("The type of bombs that are dropped with default bomb button");
        specType.setToolTipText("The type of bombs that are dropped with the special button");
        hidType.setToolTipText("The type of bombs that appear under blasted blocks");
        bClick.setToolTipText("What happens when a moving bomb hits another bomb");
        wClick.setToolTipText("What happens when a moving bomb hits a wall");
        pClick.setToolTipText("What happens when a moving bomb hits a player");

        setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Bombs options"));

        if(Settings.instance().getEPFLSupport()) {
            enableEPFL();
        }

        load();
    }


    /**
     * Loads the bomb data from the current open level.
     */
    public void load() {
        Level theLevel = Storch.theLevel;
        nbrBombs.setText("" + theLevel.bombsAtStart);
        range.setText("" + theLevel.range);
        GUI.setValue(fuse, theLevel.fuseTime);
        GUI.setValue(dir, theLevel.direction);
        GUI.setValue(defType, theLevel.defaultType);
        GUI.setValue(specType, theLevel.specialType);
        GUI.setValue(hidType, theLevel.hiddenType);
        GUI.setValue(bClick, theLevel.bombClick);
        GUI.setValue(wClick, theLevel.wallClick);
        GUI.setValue(pClick, theLevel.playerClick);
    }


    /**
     * Saves the bomb data to the current open level.
     */
    public void save() {
        Level theLevel = Storch.theLevel;
        theLevel.bombsAtStart = Integer.parseInt(nbrBombs.getText());
        theLevel.range = Integer.parseInt(range.getText());
        theLevel.fuseTime = ((ComboBoxItem) fuse.getSelectedItem()).value;
        theLevel.direction = ((ComboBoxItem) dir.getSelectedItem()).value;
        theLevel.defaultType = ((ComboBoxItem) defType.getSelectedItem()).value;
        theLevel.specialType = ((ComboBoxItem) specType.getSelectedItem()).value;
        theLevel.hiddenType = ((ComboBoxItem) hidType.getSelectedItem()).value;
        theLevel.bombClick = ((ComboBoxItem) bClick.getSelectedItem()).value;
        theLevel.wallClick = ((ComboBoxItem) wClick.getSelectedItem()).value;
        theLevel.playerClick = ((ComboBoxItem) pClick.getSelectedItem()).value;
    }


    /**
     * Disables the EPFL support
     */
    public void disableEPFL() {
        defType.removeItem(search_epfl);
        specType.removeItem(search_epfl);
        hidType.removeItem(search_epfl);
        defType.removeItem(ringofire);
        specType.removeItem(ringofire);
        hidType.removeItem(ringofire);
        defType.removeItem(mine);
        specType.removeItem(mine);
        hidType.removeItem(mine);
        defType.removeItem(diagthreebombs);
        specType.removeItem(diagthreebombs);
        hidType.removeItem(diagthreebombs);
        defType.removeItem(scissor);
        specType.removeItem(scissor);
        hidType.removeItem(scissor);
        defType.removeItem(scissor2);
        specType.removeItem(scissor2);
        hidType.removeItem(scissor2);
        defType.removeItem(parallel);
        specType.removeItem(parallel);
        hidType.removeItem(parallel);
        defType.removeItem(distance);
        specType.removeItem(distance);
        hidType.removeItem(distance);
        defType.removeItem(parasol);
        specType.removeItem(parasol);
        hidType.removeItem(parasol);
        defType.removeItem(lucky);
        specType.removeItem(lucky);
        hidType.removeItem(lucky);
        defType.removeItem(comb);
        specType.removeItem(comb);
        hidType.removeItem(comb);
        defType.removeItem(farpyro);
        specType.removeItem(farpyro);
        hidType.removeItem(farpyro);
        defType.removeItem(nuclear);
        specType.removeItem(nuclear);
        hidType.removeItem(nuclear);
        defType.removeItem(protectbombs);
        specType.removeItem(protectbombs);
        hidType.removeItem(protectbombs);

	bClick.removeItem(bomb_click_split_epfl);
	pClick.removeItem(bomb_click_split_epfl);
	wClick.removeItem(bomb_click_split_epfl);
    }


    /**
     * Enables the EPFL support
     */
    public void enableEPFL() {
        defType.addItem(search_epfl);
        specType.addItem(search_epfl);
        hidType.addItem(search_epfl);
        defType.addItem(ringofire);
        specType.addItem(ringofire);
        hidType.addItem(ringofire);
        defType.addItem(mine);
        specType.addItem(mine);
        hidType.addItem(mine);
        defType.addItem(diagthreebombs);
        specType.addItem(diagthreebombs);
        hidType.addItem(diagthreebombs);
        defType.addItem(scissor);
        specType.addItem(scissor);
        hidType.addItem(scissor);
        defType.addItem(scissor2);
        specType.addItem(scissor2);
        hidType.addItem(scissor2);
        defType.addItem(parallel);
        specType.addItem(parallel);
        hidType.addItem(parallel);
        defType.addItem(distance);
        specType.addItem(distance);
        hidType.addItem(distance);
        defType.addItem(parasol);
        specType.addItem(parasol);
        hidType.addItem(parasol);
        defType.addItem(lucky);
        specType.addItem(lucky);
        hidType.addItem(lucky);
        defType.addItem(comb);
        specType.addItem(comb);
        hidType.addItem(comb);
        defType.addItem(farpyro);
        specType.addItem(farpyro);
        hidType.addItem(farpyro);
        defType.addItem(nuclear);
        specType.addItem(nuclear);
        hidType.addItem(nuclear);
        defType.addItem(protectbombs);
        specType.addItem(protectbombs);
        hidType.addItem(protectbombs);

	bClick.addItem(bomb_click_split_epfl);
	pClick.addItem(bomb_click_split_epfl);
	wClick.addItem(bomb_click_split_epfl);
    }

}
