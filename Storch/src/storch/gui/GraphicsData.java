/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

import storch.*;
import swingwt.awt.*;
import java.io.*;
import java.util.*;

/**
 *  This class holds information about the available blocks and their corresponding filenames, as well
 *  as the available graphics styles. The information is read from files at startup and is then made
 *  available to the Storch graphical user interface through this class. This class is a singleton. Use 
 *	<code>getInstance</code> to obtain a <code>GraphicsData</code> instance.
 *
 *  @author     Tobias Johansson
 */

public class GraphicsData {
	private static final GraphicsData instance = new GraphicsData();
	
    private int nbrOfFree, nbrOfSolid, nbrOfBlast, nbrOfBlocks,nbrOfBE, nbrOfSpecialExtras ;
    private String[][] free, solid, blast, allBlocks;
    private String[] allBE;
    private Vector imageData;

    private int nbrOfStyles;
    private GraphicsStyle[] graphicsStyles;

    private ColorTable colors = new ColorTable();
    
    


    /**
     *  Constructor for the <code>GraphicsData</code> object. This class is a singleton. Use 
     *	<code>getInstance</code> to obtain a <code>GraphicsData</code> instance.
     */
    protected GraphicsData() {
        loadGIFs();
        loadGraphicsStyles();
    }
    
    /**
     *	Returns the instance of <code>GraphicsData</code>.
     */
    public static GraphicsData getInstance(){
    	return instance;
    }


    /**
     *  Reads the file 'GIFs' for information on the available filenames to use in XBlast levels.
     *	Also preloads the image data for the images used in Storch, in order to speed up image
     *	displaying.
     */
    public void loadGIFs() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(Storch.DATA_PATH + "GIFs")));
            in.readLine();
            nbrOfFree = Integer.parseInt(in.readLine());
            nbrOfSolid = Integer.parseInt(in.readLine());
            nbrOfBlast = Integer.parseInt(in.readLine());
            nbrOfSpecialExtras = Integer.parseInt(in.readLine());
            nbrOfBlocks = nbrOfFree + nbrOfSolid + nbrOfBlast;
          //  System.out.println("nbrOfBlocks "+nbrOfBlocks+" nbrOfFree "+nbrOfFree+" nbrOfFree "+nbrOfFree+" nbrOfBlast "+nbrOfBlast);
            nbrOfBE = nbrOfBlocks +  nbrOfSpecialExtras ;
            //System.out.println("      nbrOfBE"+nbrOfBE+"  nbrOfBlocks "+nbrOfBlocks+"  nbrOfSpecialExtras "+nbrOfSpecialExtras) ;        
            free = new String[4][nbrOfFree];
            solid = new String[3][nbrOfSolid];
            blast = new String[3][nbrOfBlast];
            allBlocks = new String[2][nbrOfBlocks];
            allBE = new String[nbrOfBE];
            imageData = new Vector();
            for(int i = 0; i < nbrOfFree; i++) {
                for(int j = 0; j < 4; j++) {
                    String s = in.readLine();
                    free[j][i] = s;
                    if(j < 2) {
                        allBlocks[j][i] = s;
                         if(j==0){
                            allBE[i]=s;
                        }
                    }
                }
                imageData.add(loadImageData(free[1][i]));
            }
            for(int i = 0; i < nbrOfSolid; i++) {
                for(int j = 0; j < 3; j++) {
                    String s = in.readLine();
                    solid[j][i] = s;
                    if(j < 2) {
                        allBlocks[j][i + nbrOfFree] = s;
                         if(j==0){
                            allBE[i+nbrOfFree ]=s;
                        }
                    }
                }
                imageData.add(loadImageData(solid[1][i]));
            }
            for(int i = 0; i < nbrOfBlast; i++) {
                for(int j = 0; j < 3; j++) {
                    String s = in.readLine();
                    blast[j][i] = s;
                    if(j < 2) {
                        allBlocks[j][i + nbrOfFree + nbrOfSolid] = s;
                        if(j==0){
                            allBE[i+nbrOfFree + nbrOfSolid]=s;
                        }
                    }
                }
                imageData.add(loadImageData(blast[1][i]));
            }
            //System.out.println(" nbr "+nbrOfSpecialExtras ); 
            for(int i =0; i<nbrOfSpecialExtras ;i++){
                    String s = in.readLine();
                            allBE[i+nbrOfBlast +nbrOfFree + nbrOfSolid]=s;
       //     System.out.println(" nbr "+i+" "+s );
                imageData.add(loadImageData("extra_"+s));
		//	System.out.println(" printing "+s);
                            
            }
        } catch(IOException x) {
            System.out.println("Error reading file 'GIFs'.");
            System.exit(0);
        }
    }


    /**
     *  Gets the names of the available free blocks.
     *
     *  @return    An array of <code>String</code>s containing the names of the available free blocks
     */
    public String[] getFree() {
        return free[0];
    }


    /**
     *  Gets the names of the available solid blocks.
     *
     *  @return    An array of <code>String</code>s containing the names of the available solid blocks
     */
    public String[] getSolid() {
        return solid[0];
    }


    /**
     *  Gets the names of the available blastable blocks.
     *
     *  @return    An array of <code>String</code>s containing the names of the available blastable blocks
     */
    public String[] getBlast() {
        return blast[0];
    }


    /**
     *  Gets the names of all the available blocks.
     *
     *  @return    An array of <code>String</code>s containing the names of all the available blocks
     */
    public String[] getAllBlocks() {
        return allBlocks[0];
    }


    /**
     *  Looks up the name of the block with a specified filename
     *
     *  @param  filename  The filename of a block
     *  @return           The name that corresponds to the given filename, or null if the filename is not found
     */
    public String getName(String filename) {
        String result = null;
        int i = 0;
        while(i < nbrOfBlocks && !allBlocks[1][i].equals(filename)) {
            i++;
        }
        if(i < nbrOfBlocks) {
            result = allBlocks[0][i];
        }
        return result;
    }


    /**
     *  Looks up the filename of the block with a specified name
     *
     *  @param  name      The name of a block
     *  @return           The filename that corresponds to the given name, or null if the name is not found
     */
    public String getFileName(String name) {
        String result = null;
        int i = 0;
        while(i < nbrOfBlocks && !allBlocks[0][i].equals(name)) {
            i++;
        }
        if(i < nbrOfBlocks) {
            result = allBlocks[1][i];
        }
        return result;
    }
    
    /**
     *  Returns the image of the block with a specified filename
     *
     *  @param  filename  The filename of a block
     *  @return           The image that corresponds to the given filename, or null if the filename is not found
     */
    public byte[] getImageData(String filename) {
        byte[] result = null;
        int i = 0;
	//System.out.println(" get imagedata "+filename+" numogBlocks "+nbrOfBlocks);
        while(i < nbrOfBlocks && !allBlocks[1][i].equals(filename)) {
            i++;
        }
        if(i < nbrOfBlocks) {
            result = (byte[]) imageData.get(i);
        }
        else{
            String fileName1;
            if(filename.startsWith("extra_")){
             fileName1=filename.substring("extra_".length(),filename.length());
            }else{
            fileName1=filename;
            }
           while(i < nbrOfBE && 
           !allBE[i].equals( fileName1 ) ) { 
	           // System.out.println("get imagedata"+i+" |"+fileName1+"|"+allBE[i]+"|");
               i++;
           }
            if(i < nbrOfBE) {
		// System.out.println(allBE[i]+"  "+imageData.get(i));
                
            result = (byte[]) imageData.get(i);
        }
        }
        return result;
    }


    /**
     *  Looks up the 'S-filename' (shadowed free block) of the block with a specified name.
     *
     *  @param  name      The name of a block
     *  @return           The filename that corresponds to the given name, or null if the name is not found
     */
    public String getSFile(String filename) {
        String result = filename;
        int i = 0;
        while(i < nbrOfFree && !free[1][i].equals(filename)) {
            i++;
        }
        if(i < nbrOfFree) {
            result = free[2][i];
        }
        return result;
    }


    /**
     *  Looks up the 'X-filename' (blasted free block) of the block with a specified name.
     *
     *  @param  name      The name of a block
     *  @return           The filename that corresponds to the given name, or null if the name is not found
     */
    public String getXFile(String filename) {
        String result = filename;
        int i = 0;
        while(i < nbrOfFree && !free[1][i].equals(filename)) {
            i++;
        }
        if(i < nbrOfFree) {
            result = free[3][i];
        }
        return result;
    }


    /**
     *  Looks up the 'R-filename' (rising solid block) of the block with a specified name.
     *
     *  @param  name      The name of a block
     *  @return           The filename that corresponds to the given name, or null if the name is not found
     */
    public String getRFile(String filename) {
        String result = filename;
        int i = 0;
        while(i < nbrOfSolid && !solid[1][i].equals(filename)) {
            i++;
        }
        if(i < nbrOfSolid) {
            result = solid[2][i];
        }
        return result;
    }


    /**
     *  Looks up the 'O-filename' (cracked blastable block) of the block with a specified name.
     *
     *  @param  name      The name of a block
     *  @return           The filename that corresponds to the given name, or null if the name is not found
     */
    public String getOFile(String filename) {
        String result = filename;
        int i = 0;
        while(i < nbrOfBlast && !blast[1][i].equals(filename)) {
            i++;
        }
        if(i < nbrOfBlast) {
            result = blast[2][i];
        }
        return result;
    }


    /**
     *  Gets the available graphics styles
     *
     *  @return    An array of <code>GraphicsStyle</code> objects containing all the available graphics styles
     */
    public GraphicsStyle[] getGraphicsStyles() {
        return graphicsStyles;
    }


    /**
     *  Reads the available graphics styles from the 'graphics_styles' file.
     */
    public void loadGraphicsStyles() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(Storch.DATA_PATH + "graphics_styles")));
            in.readLine();
            nbrOfStyles = Integer.parseInt(in.readLine());
            graphicsStyles = new GraphicsStyle[nbrOfStyles];
            for(int k = 0; k < nbrOfStyles; k++) {
                in.readLine();
                String name = in.readLine();

                String fileName = parseString(in);
                Color color0 = colors.getColor(parseString(in));
                Color color1 = colors.getColor(parseString(in));
                Color color2 = colors.getColor(parseString(in));
                Block free = new Block(Block.FREE_BLOCK, fileName, color0, color1, color2);

                fileName = parseString(in);
                color0 = colors.getColor(parseString(in));
                color1 = colors.getColor(parseString(in));
                color2 = colors.getColor(parseString(in));
                Block solid = new Block(Block.SOLID_WALL, fileName, color0, color1, color2);

                fileName = parseString(in);
                color0 = colors.getColor(parseString(in));
                color1 = colors.getColor(parseString(in));
                color2 = colors.getColor(parseString(in));
                Block blast = new Block(Block.BLASTABLE_BLOCK, fileName, color0, color1, color2);

                fileName = parseString(in);
                color0 = colors.getColor(parseString(in));
                color1 = colors.getColor(parseString(in));
                color2 = colors.getColor(parseString(in));
                //System.out.println("loading block "+(String) fileName);
                Block voidblock = new Block(Block.VOID_BLOCK, fileName, color0, color1, color2);
                Block extrablock = new Block(Block.SPECIAL_EXTRA, fileName, color0, color1, color2);

                graphicsStyles[k] = new GraphicsStyle(name, free, solid, blast, voidblock,extrablock);

            }
        } catch(IOException x) {
            System.out.println("Error reading Storch Graphics Styles file. ");
            System.exit(0);
        }
    }


    /**
     *  Help method to read data from file
     *
     *  @param  in               A <code>BufferedReader</code> to read from
     *  @return                  The parsed <code>String</code>
     *  @exception  IOException  If an IO error occurs while reading
     */
    private String parseString(BufferedReader in) throws IOException {
        StringBuffer text = new StringBuffer();
        char c = (char) in.read();
        while(c != ',' && c != '\n') {
            if(c != ' ' && c != '\r') {
                text.append(c);
            }
            c = (char) in.read();
        }
        return text.toString();
    }
    
    /**
     *	Loads the image data from the image file with the specified filename (without .gif-extension).
     *	Returns the data as a byte array.
     *
     *	@param	fileName	The filename (without .gif-extension) of the image file
     *
     *	@return				A byte array containing the gif-file.
     */
    private byte[] loadImageData(String fileName){
    	byte[] theImage = null;
    	try {
			InputStream inStream = getClass().getResourceAsStream(Storch.IMAGE_PATH + fileName + ".gif");		
			if(inStream != null){
				DataInputStream in = new DataInputStream(inStream);
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				int i  = in.read();
				while (i != -1) {
					out.write(i);
					i = in.read();
				}							
				theImage = out.toByteArray();
				in.close();
				out.close();
			}	
		}
		catch (FileNotFoundException e) {
			System.out.println("GraphicsData: Image file not found");
			System.out.println(Storch.IMAGE_PATH + fileName + ".gif");
		}
		catch (IOException e) {
			System.out.println("GraphicsData: Error reading file");
			System.out.println(Storch.IMAGE_PATH + fileName + ".gif");
		}
		return theImage;
    }
    
    /*
     *	Checks that the filenames in a level are correct and can be showed in Storch. If not, 
     *	they are replaced with default names and a warning flag is set in the <code>Level</code> object.
     */
    public void checkBlockFileNames(Level theLevel){
    	if(getName(theLevel.freeBlock.getFileName()) == null){
    		theLevel.freeBlock.setFileName("score_floor");
    		theLevel.shadowedBlock.setFileName("score_floor");
    		theLevel.blastedFloor.setFileName("score_floor");
    		theLevel.loadStatus = theLevel.loadStatus | Level.LOAD_UNDEFINED_FILENAME;
    	}
    	if(getName(theLevel.solidWall.getFileName()) == null){
    		theLevel.solidWall.setFileName("dark_block");
    		theLevel.risingWall.setFileName("dark_block_R");
    		theLevel.loadStatus = theLevel.loadStatus | Level.LOAD_UNDEFINED_FILENAME;
    	}
    	if(getName(theLevel.blastableBlock.getFileName()) == null){
    		theLevel.blastableBlock.setFileName("extra");
    		theLevel.blastedBlock.setFileName("extra_O");
    		theLevel.loadStatus = theLevel.loadStatus | Level.LOAD_UNDEFINED_FILENAME;
    	}
    	if(getName(theLevel.voidBlock.getFileName()) == null){
    		theLevel.voidBlock.setFileName("score_floor");
    		theLevel.loadStatus = theLevel.loadStatus | Level.LOAD_UNDEFINED_FILENAME;
    	}//   System.out.println(" filname loading ");
       /*System.out.println(" filname loading "+theLevel.extraBlock.getFileName());
    	if(getName(theLevel.extraBlock.getFileName()) == null){
    		theLevel.extraBlock.setFileName("extra");
    		theLevel.extraBlock.setFileName("extra_O");
    		theLevel.loadStatus = theLevel.loadStatus | Level.LOAD_UNDEFINED_FILENAME;
    	}*/  	
    }
}
