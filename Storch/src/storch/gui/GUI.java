/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.gui;

import swingwtx.swing.*;
import swingwtx.swing.event.*;
import swingwt.awt.*;
import swingwt.awt.*;
import swingwt.awt.event.*;
import java.io.File;

import storch.*;
import storch.io.*;

/**
 * This is the core class of the graphical user interface, and represents the
 * Storch window.
 *
 * @author   Tobias Johansson
 */
public class GUI extends JFrame implements ActionListener, WindowListener, ChangeListener {

    private OptionsPanel optionsPan;
    private GraphicsPanel graphPan;
    private MapPanel mapPan;

    private StdLoader stdLoader;
    private TNTLoader tntLoader;
    private StdSaver stdSaver;
    private TNTSaver tntSaver;

    private String lastLoadDir;
    private String lastSaveDir;
    private String lastLevelDir;

    private Image blastaMan;

    private LevelDocDialog leveldoc;

    protected final static int FORMAT_STD = 0;
    protected final static int FORMAT_TNT = 1;


    /**
     * Constructor for the <code>GUI</code> object.
     *
     * @param title  The title that will be displayed in the title bar of the
     *      main window
     */
    public GUI(String title) {

        super(title);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(Storch.IMAGE_PATH + "head.gif")));
        Container contentPane = getContentPane();
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        blastaMan = Toolkit.getDefaultToolkit().getImage(getClass().getResource(Storch.IMAGE_PATH + "BlastaMan.jpg"));
	// prepareImage(blastaMan, -1, -1, this);

        /*
         *  Loaders & savers
         */
        stdLoader = new StdLoader();
        tntLoader = new TNTLoader();
        stdSaver = new StdSaver();
        tntSaver = new TNTSaver();

        /*
         *  Directories
         */
        File levelDir = new File(Settings.instance().getLevelDir());
        if(!levelDir.exists()) {
            levelDir.mkdirs();
        }
        lastLoadDir = lastSaveDir = lastLevelDir = levelDir.getAbsolutePath();
        leveldoc = new LevelDocDialog(this, lastLevelDir, new File(Settings.instance().getDocDir()).getAbsolutePath());

        /*
         *  Menu:
         */
        JMenuBar mb = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        JMenuItem miNew = new JMenuItem("New level", KeyEvent.VK_N);
        miNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        JMenuItem miLoad = new JMenuItem("Load level", KeyEvent.VK_L);
        miLoad.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK));
        JMenuItem miSaveStd = new JMenuItem("Save as 2.6 level", KeyEvent.VK_S);
        miSaveStd.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        JMenuItem miSaveTNT = new JMenuItem("Save as TNT level", KeyEvent.VK_T);
        miSaveTNT.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK));
        JMenuItem miExit = new JMenuItem("Exit Storch", KeyEvent.VK_X);
        miExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
        fileMenu.add(miNew);
        fileMenu.add(miLoad);
        fileMenu.add(miSaveStd);
        fileMenu.add(miSaveTNT);
        fileMenu.add(miExit);
        miNew.setActionCommand("New");
        miLoad.setActionCommand("Load");
        miSaveStd.setActionCommand("SaveStd");
        miSaveTNT.setActionCommand("SaveTNT");
        miExit.setActionCommand("Exit");
        miNew.addActionListener(this);
        miLoad.addActionListener(this);
        miSaveStd.addActionListener(this);
        miSaveTNT.addActionListener(this);
        miExit.addActionListener(this);
        mb.add(fileMenu);
        JMenu toolsMenu = new JMenu("Tools");
        toolsMenu.setMnemonic(KeyEvent.VK_T);
        JMenuItem miLevelDoc = new JMenuItem("LevelDoc HTML-documentation", KeyEvent.VK_L);
        JMenuItem miStd2TNT = new JMenuItem("2.6-to-TNT conversion", KeyEvent.VK_2);
        JMenuItem miTNT2Std = new JMenuItem("TNT-to-2.6 conversion", KeyEvent.VK_T);
	JMenuItem miSettings = new JMenuItem("Settings", KeyEvent.VK_P);
        toolsMenu.add(miLevelDoc);
        toolsMenu.add(miStd2TNT);
        toolsMenu.add(miTNT2Std);
	toolsMenu.add(miSettings);
        miLevelDoc.setActionCommand("LevelDoc");
        miStd2TNT.setActionCommand("Std2TNT");
        miTNT2Std.setActionCommand("TNT2Std");
	miSettings.setActionCommand("Settings");
        miLevelDoc.addActionListener(this);
        miStd2TNT.addActionListener(this);
        miTNT2Std.addActionListener(this);
	miSettings.addActionListener(this);
        mb.add(toolsMenu);
        JMenu helpMenu = new JMenu("Help");
        helpMenu.setMnemonic(KeyEvent.VK_H);
        JMenuItem miAbout = new JMenuItem("About Storch", KeyEvent.VK_A);
        helpMenu.add(miAbout);
        miAbout.setActionCommand("About");
        miAbout.addActionListener(this);
        mb.add(helpMenu);
        setJMenuBar(mb);

        /*
         *  Panels
         */
        mapPan = new MapPanel();
        graphPan = new GraphicsPanel(mapPan, this);
        optionsPan = new OptionsPanel();
        JPanel designPan = new JPanel();
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        designPan.setLayout(gridbag);
        gbc.gridx = 0;
        designPan.add(mapPan, gbc);
        designPan.add(graphPan, gbc);
        designPan.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Map design"));

        /*
         *  Tabs
         */
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Level options", optionsPan);
        tabbedPane.addTab("Map design", designPan);
        tabbedPane.addChangeListener(this);
        contentPane.add(tabbedPane);

        Storch.theLevel.setSaved();

        addWindowListener(this);
        pack();
        show();
    }


    /**
     * Exits Storch.
     */
    public void exit() {
        boolean cont = true;
        if(!Storch.theLevel.isSaved()) {
            if(JOptionPane.showOptionDialog(this, "Current level is not saved. Exit anyway?",
                    "Exit Storch", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null)
                     != JOptionPane.YES_OPTION) {
                cont = false;
            }
        }
        if(cont) {
            Settings.instance().save();
            System.exit(0);
        }
    }


    /**
     * Loads a level file. Brings up the load dialog box and loads the level
     * into the GUI.
     */
    public void loadLevel() {
        //System.out.println("Load from dir: " + lastLoadDir);
        JFileChooser chooser = new JFileChooser(lastLoadDir);
        chooser.setDialogType(JFileChooser.OPEN_DIALOG);
        chooser.setDialogTitle("Load level");
        chooser.addChoosableFileFilter(new StandardFileFilter());
        chooser.addChoosableFileFilter(new TNTFileFilter());
        chooser.setFileFilter(new LevelFileFilter());

        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                File theFile = chooser.getSelectedFile();
                if(LevelFileFilter.getExtension(theFile).equals("xal")) {
                    Storch.theLevel = tntLoader.load(theFile);
                }
                else if(LevelFileFilter.getExtension(theFile).equals("h")) {
                    Storch.theLevel = stdLoader.load(theFile);
                }
		if(Storch.theLevel.isEPFL() && !Settings.instance().getEPFLSupport()){
		  JOptionPane.showMessageDialog(this, "The level you are loading is an EPFL level. \nEPFL support will be enabled.", "EPFL load", JOptionPane.WARNING_MESSAGE);
		  Settings.instance().setEPFLSupport(true);
		  optionsPan.enableEPFL();
		}
                GraphicsData.getInstance().checkBlockFileNames(Storch.theLevel);
                optionsPan.load();
                graphPan.load();
                mapPan.load();
                lastLoadDir = chooser.getCurrentDirectory().toString();
                getContentPane().repaint();
                Storch.theLevel.setSaved();
                if(Storch.theLevel.loadStatus != Level.LOAD_SUCCESS) {
                    String warningMessage = "Warning! The level may not have been properly loaded.\n\n";
                    if((Storch.theLevel.loadStatus & Level.LOAD_GRAPHICS_ERROR) > 0) {
                        warningMessage += "The graphics section of the level contains an unknown block or a misplaced Special Extra. \nThe affected blocks have been replaced with black squares in Storch.\n\n";
                    }
                    if((Storch.theLevel.loadStatus & Level.LOAD_SPECIAL_EXTRA_IMAGE_ERROR) > 0) {
                        warningMessage += "The graphics section of the level contains an unknown Special Extra definition. \nThe special extra image has been changed to a proper one.\n\n";
                    }
                    if((Storch.theLevel.loadStatus & Level.LOAD_RISING_BLOCK) > 0) {
                        warningMessage += "The map of the loaded level contains rising walls. \nThese blocks have been changed to solid walls.\n\n";
                    }
                    if((Storch.theLevel.loadStatus & Level.LOAD_UNDEFINED_BLOCK) > 0) {
                        warningMessage += "The map of the loaded level contains undefined blocks. \nThese blocks have been changed to void blocks.\n\n";
                    }
                    if((Storch.theLevel.loadStatus & Level.LOAD_UNDEFINED_FILENAME) > 0) {
                        warningMessage += "The graphics section of the level contains an unknown or misplaced filename. \nThe affeced blocks have been assigned default filenames\n\n";
                    }
                    JOptionPane.showMessageDialog(this, warningMessage, "Load warning", JOptionPane.WARNING_MESSAGE);
                }
            } catch(LoadException ioe) {
                JOptionPane.showMessageDialog(this, "Load failed!\n\n" + ioe, "Load failed", JOptionPane.ERROR_MESSAGE);
            }

        }
    }


    /**
     * Save the current level in the selected format. Brings up the save dialog
     * box and saves the level file.
     *
     * @param format  The file format to save in. One of <code>FORMAT_STD</code>
     *      or <code>FORMAT_TNT</code>
     */
    public void saveLevel(int format) {
        String fileFormat;
        String fileExtension;
        Saver theSaver;
        LevelFileFilter theFilter;
        if(format == FORMAT_TNT) {
            fileFormat = "TNT";
            fileExtension = ".xal";
            theSaver = tntSaver;
            theFilter = new TNTFileFilter();
        }
        else {
            fileFormat = "standard";
            fileExtension = ".h";
            theSaver = stdSaver;
            theFilter = new StandardFileFilter();
        }
        JFileChooser chooser = new JFileChooser(lastSaveDir);
        chooser.setDialogTitle("Save " + fileFormat + " level");
        chooser.setFileFilter(theFilter);
        chooser.setSelectedFile(new File(lastSaveDir + File.separator + Storch.theLevel.getFileName() + fileExtension));

        int returnVal = chooser.showSaveDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                File theFile = chooser.getSelectedFile();
                if(!theFile.getName().endsWith(fileExtension)) {
                    theFile = new File(theFile.getPath() + fileExtension);
                }
                boolean cont = true;
                if(theFile.exists()) {
                    if(JOptionPane.showOptionDialog(this, "File " + theFile.toString() + " exists. Overwrite existing file?",
                            "File already exists", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null)
                             != JOptionPane.YES_OPTION) {
                        cont = false;
                    }
                }
                if(cont) {
                    theSaver.save(theFile, Storch.theLevel);
                    lastSaveDir = chooser.getCurrentDirectory().toString() + File.separator;
                    Storch.theLevel.setSaved();
                }
            } catch(Exception ioe) {
                JOptionPane.showMessageDialog(this, fileFormat.replace('s', 'S') + " save failed!\n\n" + ioe, "Save failed", JOptionPane.ERROR_MESSAGE);
            }
        }
    }


    /**
     * Converts levels from one of the two level format to the other. Brings up
     * a file chooser dialog and converts the selected levels.
     *
     * @param sourceFormat  Description of the Parameter
     */
    public void convert(int sourceFormat) {
        String fromFormat;
        String toFormat;
        LevelFileFilter theFilter;
        Converter conv;
        if(sourceFormat == FORMAT_TNT) {
            fromFormat = "TNT";
            toFormat = "2.6";
            theFilter = new TNTFileFilter();
            conv = new TNT2Std();
        }
        else {
            fromFormat = "2.6";
            toFormat = "TNT";
            theFilter = new StandardFileFilter();
            conv = new Std2TNT();
        }
        JFileChooser chooser = new JFileChooser(lastLevelDir);
        chooser.setDialogTitle(fromFormat + "-to-" + toFormat + " - Select level files");
        chooser.setMultiSelectionEnabled(true);
        chooser.setFileFilter(theFilter);

        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            File[] theFiles = chooser.getSelectedFiles();
            lastLevelDir = chooser.getCurrentDirectory().toString();
            int fileCount = 0;
            for(int i = 0; i < theFiles.length; i++) {
                try {
                    conv.convert(theFiles[i]);
                    fileCount++;
                } catch(Exception e) {
                    JOptionPane.showMessageDialog(this, e.getMessage(), "Conversion error", JOptionPane.ERROR_MESSAGE);
                }
            }
            if(fileCount == 1) {
                JOptionPane.showMessageDialog(this, "" + theFiles[0].getName() + " converted to " + toFormat + " level", "Conversion done", JOptionPane.INFORMATION_MESSAGE);
            }
            else if(fileCount > 1) {
                JOptionPane.showMessageDialog(this, "" + fileCount + " " + fromFormat + " levels converted to " + toFormat + " levels", "Conversion done", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    /*
     *  ActionListener methods:
     */

    /**
     * Called when an action is performed in the GUI-frame, typically when a
     * menu item is selected.
     *
     * @param e  The corresponding <code>ActionEvent</code>
     */
    public void actionPerformed(ActionEvent e) {
        optionsPan.save();
        graphPan.save();
        mapPan.save();
        String command = e.getActionCommand();
        if(command == "New") {
            boolean cont = true;
            if(!Storch.theLevel.isSaved()) {
                if(JOptionPane.showOptionDialog(this, "Current level is not saved. Clear level anyway?",
                        "Load level", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null)
                         != JOptionPane.YES_OPTION) {
                    cont = false;
                }
            }
            if(cont) {
                Storch.theLevel = new Level();
                graphPan.save();
                optionsPan.load();
                mapPan.load();
                Storch.theLevel.setSaved();
                getContentPane().repaint();
            }
        }
        else if(command == "Load") {
            boolean cont = true;
            if(!Storch.theLevel.isSaved()) {
                if(JOptionPane.showOptionDialog(this, "Current level is not saved. Load new level anyway?",
                        "Load level", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null)
                         != JOptionPane.YES_OPTION) {
                    cont = false;
                }
            }
            if(cont) {
                loadLevel();
            }
        }
        else if(command == "SaveStd") {
            if(Storch.theLevel.title.trim().equals("")) {
                JOptionPane.showMessageDialog(this, "Enter a title before saving", "Title missing", JOptionPane.ERROR_MESSAGE);
            }
            else {
                AdvancedWarning warn = new AdvancedWarning("This level uses EFPL features and can only be played in the EFPL,tnt version of XBlast");
                if(Storch.theLevel.isEPFL() && Settings.instance().getEPFLWarning()) {
                    JOptionPane.showMessageDialog(this, warn, "EFPL warning", JOptionPane.WARNING_MESSAGE);
                    Settings.instance().setEPFLWarning(warn.getAnswer());
                }
                saveLevel(FORMAT_STD);
            }
        }
        else if(command == "SaveTNT") {
            if(Storch.theLevel.title.trim().equals("")) {
                JOptionPane.showMessageDialog(this, "Enter a title before saving", "Title missing", JOptionPane.ERROR_MESSAGE);
            }
            else {
                if(Storch.theLevel.isEPFL()) {
                    JOptionPane.showMessageDialog(this, "This level uses EFPL features and may not have all the stuff ( how many specialbombs at start)", "Unable to save in TNT format", JOptionPane.ERROR_MESSAGE);
                    
                    saveLevel(FORMAT_TNT);
                }
                else
                    saveLevel(FORMAT_TNT);
            }
        }
        else if(command == "Exit") {
            exit();
        }
        else if(command == "About") {
            AboutDialog ad = new AboutDialog(this, blastaMan);
        }
        else if(command == "LevelDoc") {
            leveldoc.pack();
            leveldoc.show();
        }
        else if(command == "Std2TNT") {
            convert(FORMAT_STD);
        }
        else if(command == "TNT2Std") {
            convert(FORMAT_TNT);
        }
	else if(command == "Settings"){
	  boolean epflPreSett = Settings.instance().getEPFLSupport();
	   new SettingsDialog(this);
	   if(epflPreSett && !Settings.instance().getEPFLSupport())
	      optionsPan.disableEPFL();
	  else if(!epflPreSett && Settings.instance().getEPFLSupport())
	      optionsPan.enableEPFL();
	}
    }


    /*
     *  WindowListener methods:
     */
    /**
     * Empty method, only needed to implement the <code>WindowListener</code>
     * interface.
     *
     * @param e  A <code>WindowEvent</code>
     */
    public void windowOpened(WindowEvent e) { }


    /**
     * Called when the user closes the window. Exits Storch.
     *
     * @param e  Description of the Parameter
     */
    public void windowClosing(WindowEvent e) {
        exit();
    }


    /**
     * Empty method, only needed to implement the <code>WindowListener</code>
     * interface.
     *
     * @param e  A <code>WindowEvent</code>
     */
    public void windowClosed(WindowEvent e) { }


    /**
     * Empty method, only needed to implement the <code>WindowListener</code>
     * interface.
     *
     * @param e  A <code>WindowEvent</code>
     */
    public void windowIconified(WindowEvent e) { }


    /**
     * Empty method, only needed to implement the <code>WindowListener</code>
     * interface.
     *
     * @param e  A <code>WindowEvent</code>
     */
    public void windowDeiconified(WindowEvent e) { }


    /**
     * Empty method, only needed to implement the <code>WindowListener</code>
     * interface.
     *
     * @param e  A <code>WindowEvent</code>
     */
    public void windowActivated(WindowEvent e) { }


    /**
     * Empty method, only needed to implement the <code>WindowListener</code>
     * interface.
     *
     * @param e  A <code>WindowEvent</code>
     */
    public void windowDeactivated(WindowEvent e) { }


    /*
     *  ItemListener methods:
     */
    /**
     * Called when the user clicks a tab to change between the option panel and
     * the graphics panel.
     *
     * @param e  A corresponding <code>ChangeEvent</code>
     */
    public void stateChanged(ChangeEvent e) {
        optionsPan.save();
        graphPan.save();
        mapPan.save();
        optionsPan.load();
        graphPan.load();
        mapPan.load();
    }


    /**
     * Sets a specified <code>JComboBox</code> to a <code>ComboBoxItem</code>
     * with a specified value
     *
     * @param combo  The combo box to set
     * @param value  The new value
     */
    public static void setValue(JComboBox combo, int value) {
        int i = 0;
        boolean found = false;
        while(!found && i < combo.getItemCount()) {
            if(((ComboBoxItem) combo.getItemAt(i)).value == value) {
                found = true;
                combo.setSelectedIndex(i);
            }
            i++;
        }
    }
}
