/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

import swingwt.awt.*;
import swingwtx.swing.*;

/**
 *  A panel with a specified background color, and optionally displaying the name of the color.
 *
 *  @author     Tobias Johansson
 */

public class ColorPanel extends JPanel {
    private Color c;
    private String name;
    private int fontSize;
    private boolean text;


    /**
     *  Constructor for a <code>ColorPanel</code> object with text.
     *
     *  @param  c         The color of this panel
     *  @param  name      The name of the color
     *  @param  fontSize  The size of the text to display in the panel
     */
    public ColorPanel(Color c, String name, int fontSize) {
        super();
        this.c = c;
        this.name = name;
        this.fontSize = fontSize;
        text = true;
    }


    /**
     *  Constructor for a  <code>ColorPanel</code> object without text.
     *
     *  @param  c     The color of this panel
     *  @param  name  The name of the color
     */
    public ColorPanel(Color c, String name) {
        super();
        this.c = c;
        this.name = name;
        text = false;
    }


    /**
     *  Paints this panel.
     *
     *  @param  g  The <code>Graphics</code> object to paint on
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        setBackground(c);
        Dimension d = getSize();
        if(text) {
            g.setColor(Color.black);
            g.setFont(new Font("Serif", Font.PLAIN, fontSize));
            if(c.getRed() + c.getGreen() + c.getBlue() > 375) {
                g.setColor(Color.black);
            } else {
                g.setColor(Color.white);
            }
            g.drawString(name, 3, d.height / 2 + fontSize / 2);
        }
    }


    /**
     *  Gets the preferred size of this <code>ColorPanel</code>.
     *
     *  @return    The preferred size
     */
    public Dimension getPreferredSize() {
        return getMinimumSize();
    }


    /**
     *  Gets the minimum size of this <code>ColorPanel</code>.
     *
     *  @return    The minimum size
     */
    public synchronized Dimension getMinimumSize() {
        Dimension min;
        if(text) {
            min = new Dimension(20 * fontSize / 2, fontSize + 6);
        } else {
            min = new Dimension(15, 15);
        }
        return min;
    }


    /**
     *  Sets the color attribute of the ColorPanel object
     *
     *  @param  col  The new color
     *  @param  n    The name of the new color
     */
    public void setColor(Color col, String n) {
        c = col;
        name = n;
        repaint();
    }


    /**
     *  Gets the color attribute of the ColorPanel object
     *
     *  @return    The color
     */
    public Color getColor() {
        return c;
    }


    /**
     *  Gets the name attribute of the ColorPanel object
     *
     *  @return    The name
     */
    public String getName() {
        return name;
    }
}
