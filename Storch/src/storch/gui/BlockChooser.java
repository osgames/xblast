/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.gui;

import swingwtx.swing.*;
import swingwt.awt.*;
import swingwt.awt.event.*;
import storch.*;

/**
 * A panel to choose a type of block to use for drawing the level map. Shows all
 * available blocks and lets the user choose by clicking with the mouse.
 *
 * @author   Tobias Johansson
 */
class BlockChooser extends JPanel implements MouseListener {
    private Image free, solid, blast, vd,bombExtra, rangeExtra, specialExtra, illness, freeBomb, scDraw, scDelete;
    private int leftButton, rightButton;


    /**
     * Constructor for the <code>BlockChooser</code> object.
     */
    public BlockChooser() {
        super();
        leftButton = Block.SOLID_WALL;
        rightButton = Block.FREE_BLOCK;
        addMouseListener(this);

        bombExtra = Toolkit.getDefaultToolkit().getImage(getClass().getResource(Storch.IMAGE_PATH + "extra_bomb.gif"));
        rangeExtra = Toolkit.getDefaultToolkit().getImage(getClass().getResource(Storch.IMAGE_PATH + "extra_range.gif"));
        illness = Toolkit.getDefaultToolkit().getImage(getClass().getResource(Storch.IMAGE_PATH + "extra_trap.gif"));
        freeBomb = Toolkit.getDefaultToolkit().getImage(getClass().getResource(Storch.IMAGE_PATH + "evil_bomb.gif"));
        MediaTracker tracker = new MediaTracker(this);
        tracker.addImage(bombExtra, 1);
        tracker.addImage(rangeExtra, 2);
        tracker.addImage(illness, 3);
        tracker.addImage(freeBomb, 4);
        try {
            tracker.waitForAll();
        } catch(InterruptedException ie) {
        }

        load();
    }


    /**
     * Called when a mouse button is pressed. Checks which block is clicked on,
     * and with which mouse button.
     *
     * @param e  A <code>MouseEvent</code> created when a mousebutton is pressed
     */
    public void mousePressed(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        if(x > 5 && x < 338 && y > 5 && y < 26) {
            int imageNbr = (x - 4) / 30;
            int blockType = getType(imageNbr);
            if((e.getModifiers() & e.BUTTON3_MASK) > 0) {
                rightButton = blockType;
            }
            else if((e.getModifiers() & e.BUTTON1_MASK) > 0) {
                leftButton = blockType;
            }
        }
        repaint();
    }


    /**
     * Empty method, only needed to implement the <code>MouseListener</code>
     * interface.
     *
     * @param e  A <code>MouseEvent</code>
     */
    public void mouseClicked(MouseEvent e) { }


    /**
     * Empty method, only needed to implement the <code>MouseListener</code>
     * interface.
     *
     * @param e  A <code>MouseEvent</code>
     */
    public void mouseEntered(MouseEvent e) { }


    /**
     * Empty method, only needed to implement the <code>MouseListener</code>
     * interface.
     *
     * @param e  A <code>MouseEvent</code>
     */
    public void mouseExited(MouseEvent e) { }


    /**
     * Empty method, only needed to implement the <code>MouseListener</code>
     * interface.
     *
     * @param e  A <code>MouseEvent</code>
     */
    public void mouseReleased(MouseEvent e) { }


    /**
     * Paints the graphics of the <code>BlockChooser</code>.
     *
     * @param g  The <code>Graphics</code> object to paint on
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.clearRect(5, 5, 333, 57);
        g.drawImage(free, 5, 5, this);
        g.drawImage(solid, 35, 5, this);
        g.drawImage(blast, 65, 5, this);
        g.drawImage(vd, 95, 5, this);
        g.drawImage(bombExtra, 125, 5, this);
        g.drawImage(rangeExtra, 155, 5, this);
        g.drawImage(specialExtra, 185, 5, this);
        g.drawImage(illness, 215, 5, this);
        g.drawImage(free, 245, 5, this);
        g.drawImage(freeBomb, 245, 5, this);
        g.drawImage(scDraw, 275, 5, this);
        g.setColor(Color.green);
        g.drawRect(275, 5, 28, 21);
        g.drawImage(scDelete, 305, 5, this);
        g.setColor(Color.red);
        g.drawRect(305, 5, 28, 21);

        if(leftButton == Block.FREE_W_BOMB) {
            g.drawImage(free, 125, 31, this);
        }
        g.drawImage(type2Image(leftButton), 125, 31, this);
        if(leftButton == Block.SCRAMBLE_DRAW) {
            g.setColor(Color.green);
            g.drawRect(125, 31, 28, 21);
        }
        else if(leftButton == Block.SCRAMBLE_DELETE) {
            g.setColor(Color.red);
            g.drawRect(125, 31, 28, 21);
        }
        g.setColor(Color.black);
        g.drawString(type2String(leftButton), 15, 47);

        if(rightButton == Block.FREE_W_BOMB) {
            g.drawImage(free, 185, 31, this);
        }
        g.drawImage(type2Image(rightButton), 185, 31, this);
        if(rightButton == Block.SCRAMBLE_DRAW) {
            g.setColor(Color.green);
            g.drawRect(185, 31, 28, 21);
        }
        else if(rightButton == Block.SCRAMBLE_DELETE) {
            g.setColor(Color.red);
            g.drawRect(185, 31, 28, 21);
        }
        g.setColor(Color.black);
        g.drawString(type2String(rightButton), 217, 47);
    }


    /**
     * Gets the preferred size of the <code>BlockChooser</code>.
     *
     * @return   The preferred size of the <code>BlockChooser</code>
     */
    public Dimension getPreferredSize() {
        return getMinimumSize();
    }


    /**
     * Gets the minimum size of the <code>BlockChooser</code>.
     *
     * @return   The minimum size value
     */
    public synchronized Dimension getMinimumSize() {
        return new Dimension(343, 67);
    }


    /**
     * Gets the block type associated with the left mouse button
     *
     * @return   The block type associated with the left mouse button.
     */
    public int getLeftButton() {
        return leftButton;
    }


    /**
     * Gets the block type associated with the right mouse button.
     *
     * @return   The block type associated with the right mouse button
     */
    public int getRightButton() {
        return rightButton;
    }


    /**
     * Returns the current <code>Image<code> object associated with a block type.
     *
     *
     *
     *
     *
     * @param blockType  The block type
     * @return           The current <code>Image</code> object associated with
     *      the block type
     */
    public Image type2Image(int blockType) {
        Image res = free;
        switch (blockType) {
         case Block.FREE_BLOCK:
             res = free;
             break;
         case Block.SOLID_WALL:
             res = solid;
             break;
         case Block.BLASTABLE_BLOCK:
             res = blast;
             break;
         case Block.VOID_BLOCK:
             res = vd;
             break;
         case Block.BOMB_EXTRA:
             res = bombExtra;
             break;
         case Block.RANGE_EXTRA:
             res = rangeExtra;
             break;
         case Block.SPECIAL_EXTRA:
             res = specialExtra;
             break;
         case Block.ILLNESS:
             res = illness;
             break;
         case Block.FREE_W_BOMB:
             res = freeBomb;
             break;
         case Block.SCRAMBLE_DRAW:
             res = scDraw;
             break;
         case Block.SCRAMBLE_DELETE:
             res = scDelete;
             break;
        }
        return res;
    }


    /**
     * Returns a <code>String</code> describing a block type.
     *
     * @param blockType  The block type
     * @return           A description of the block type, shown in the
     *      mouse-over tooltip.
     */
    private static String type2String(int blockType) {
        String res = "";
        switch (blockType) {
         case Block.FREE_BLOCK:
             res = "Free square";
             break;
         case Block.SOLID_WALL:
             res = "Solid wall";
             break;
         case Block.BLASTABLE_BLOCK:
             res = "Blastable block";
             break;
         case Block.VOID_BLOCK:
             res = "Void block";
             break;
         case Block.BOMB_EXTRA:
             res = "Bomb extra";
             break;
         case Block.RANGE_EXTRA:
             res = "Range extra";
             break;
         case Block.SPECIAL_EXTRA:
             res = "Special extra";
             break;
         case Block.ILLNESS:
             res = "Illness";
             break;
         case Block.FREE_W_BOMB:
             res = "Square with bomb";
             break;
         case Block.SCRAMBLE_DRAW:
             res = "Scramble draw";
             break;
         case Block.SCRAMBLE_DELETE:
             res = "Scramble delete";
             break;
        }
        return res;
    }


    /**
     * Gets the block type associated with a specified image number
     *
     * @param imageNbr  The image number (counted from left to right)
     * @return          The type value
     */
    private static int getType(int imageNbr) {
        int res = Block.SCRAMBLE_DELETE;
        switch (imageNbr) {
         case 0:
             res = Block.FREE_BLOCK;
             break;
         case 1:
             res = Block.SOLID_WALL;
             break;
         case 2:
             res = Block.BLASTABLE_BLOCK;
             break;
         case 3:
             res = Block.VOID_BLOCK;
             break;
         case 4:
             res = Block.BOMB_EXTRA;
             break;
         case 5:
             res = Block.RANGE_EXTRA;
             break;
         case 6:
             res = Block.SPECIAL_EXTRA;
             break;
         case 7:
             res = Block.ILLNESS;
             break;
         case 8:
             res = Block.FREE_W_BOMB;
             break;
         case 9:
             res = Block.SCRAMBLE_DRAW;
             break;
         case 10:
             res = Block.SCRAMBLE_DELETE;
             break;
        }
        return res;
    }


    /**
     * Loads the current images for the different block types into the chooser.
     */
    public void load() {
        if(Storch.theLevel.freeBlock != null) {
            free = scDelete = Storch.theLevel.freeBlock.getImage();
            solid = scDraw = Storch.theLevel.solidWall.getImage();
            blast = Storch.theLevel.blastableBlock.getImage();
            vd = Storch.theLevel.voidBlock.getImage();
        //    specialExtra = Storch.theLevel.extraBlock.getImage();
            //bombExtra = Storch.theLevel.extraBlock.getImage();
            String specialExtraName = "void"; 
   /*         switch (Storch.theLevel.specialExtra) {
             case Level.SPECIAL_EXTRA_VOID:
                 specialExtraName = "void";
                 break;
             case Level.SPECIAL_EXTRA_INVINCIBLE:
                 specialExtraName = "invincible";
                 break;
             case Level.SPECIAL_EXTRA_KICK:
                 specialExtraName = "kick_bomb";
                 break;
             case Level.SPECIAL_EXTRA_TELEPORT:
                 specialExtraName = "q3a_beam";
                 break;
             case Level.SPECIAL_EXTRA_RC:
                 specialExtraName = "remote_control";
                 break;
             case Level.SPECIAL_EXTRA_IGNITE_ALL:
                 specialExtraName = "ignite";
                 break;
             case Level.SPECIAL_EXTRA_AIR:
                 specialExtraName = "air_pump";
                 break;
             case Level.SPECIAL_EXTRA_SPECIAL_BOMB:
                 int type = Storch.theLevel.specialType;
                 if(type == Level.BOMB_NORMAL) {
                     specialExtraName = "bomb";
                 }
                 else if(type == Level.BOMB_THREEBOMBS || type == Level.BOMB_TRIANGLEBOMBS) {
                     specialExtraName = "triangle_bomb";
                 }
                 else if(type == Level.BOMB_CONSTRUCTION || type == Level.BOMB_DESTRUCTION || type == Level.BOMB_RENOVATION || type == Level.BOMB_PROTECTBOMBS) {
                     specialExtraName = "construction";
                 }
                 else if(type == Level.BOMB_FIRECRACKER || type == Level.BOMB_PYRO || type == Level.BOMB_FIRECRACKER_2 || type == Level.BOMB_PYRO_2 || type == Level.BOMB_RING_OF_FIRE || type == Level.BOMB_FARPYRO) {
                     specialExtraName = "firecracker";
                 }
                 else if(type == Level.BOMB_SEARCH) {
                     specialExtraName = "search";
                 }
                 else if(type == Level.BOMB_MINE) {
                     specialExtraName = "mines";
                 }
                 else {
                     specialExtraName = "napalm";
                 }
                 break;
             case Level.SPECIAL_EXTRA_JUNKIE:
                 specialExtraName = "syringe";
                 break;
             case Level.SPECIAL_EXTRA_POISON:
                 specialExtraName = "poison";
                 break;
             case Level.SPECIAL_EXTRA_LONG_STUNNED:
                 specialExtraName = "pow";
                 break;
             case Level.SPECIAL_EXTRA_SPEED:
                 specialExtraName = "speed";
                 break;
             case Level.SPECIAL_EXTRA_SLOW:
                 specialExtraName = "slow";
                 break;
             case Level.SPECIAL_EXTRA_MAYHEM:
                 specialExtraName = "mayhem";
                 break;
             case Level.SPECIAL_EXTRA_HOLY_GRAIL:
                 specialExtraName = "holygrail";
                 break;
             case Level.SPECIAL_EXTRA_LIFE:
                 specialExtraName = "life";
                 break;
             case Level.SPECIAL_EXTRA_MULTIPLE:
                 specialExtraName = "multiple";
                 break;
             case Level.SPECIAL_EXTRA_CLOAK:
                 specialExtraName = "cloak";
                 break;
             case Level.SPECIAL_EXTRA_STUN_OTHERS:
                 specialExtraName = "pow";
                 break;
             case Level.SPECIAL_EXTRA_MORPH:
                 specialExtraName = "morph";
                 break;
             case Level.SPECIAL_EXTRA_STOP://EPFL
                 specialExtraName = "stop";
                 break;
             case Level.SPECIAL_EXTRA_ELECTRIFY://EPFL
                 specialExtraName = "electrify";
                 break;
             case Level.SPECIAL_EXTRA_FROGGER://EPFL
                 specialExtraName = "frogger";
                 break;
             case Level.SPECIAL_EXTRA_SNIPE://Skywalker
                 specialExtraName = "snipe";
                 break;
             case Level.SPECIAL_EXTRA_PHANTOM://EPFL
                 specialExtraName = "phantom";
                 break;
             case Level.SPECIAL_EXTRA_GHOST://EPFL
                 specialExtraName = "ghost";
                 break;
             case Level.SPECIAL_EXTRA_THROUGH://EPFL
                 specialExtraName = "through";
                 break;
             case Level.SPECIAL_EXTRA_REVIVE://EPFL
                 specialExtraName = "revive";
                 break;
             case Level.SPECIAL_EXTRA_STEAL://EPFL
                 specialExtraName = "steal";
                 break;
             case Level.SPECIAL_EXTRA_SWAPPOSITION://EPFL
                 specialExtraName = "swapposition";
                 break;
             case Level.SPECIAL_EXTRA_SWAPCOLOR://EPFL
                 specialExtraName = "swapcolor";
                 break;
             case Level.SPECIAL_EXTRA_DALEIF://EPFL
                 specialExtraName = "daleif";
                 break;
             case Level.SPECIAL_EXTRA_CHOICE://EPFL
                 specialExtraName = "choice";
                 break;
             case Level.SPECIAL_EXTRA_EVILGRAIL://EPFL
                 specialExtraName = "evilgrail";
                 break;
             case Level.SPECIAL_EXTRA_JUMP://EPFL
                 specialExtraName = "jump";
                 break;
             case Level.SPECIAL_EXTRA_PLAYERFART://EPFL
                 specialExtraName = "fart";
                 break;
             case Level.SPECIAL_EXTRA_PLAYERANDBOMBFART://EPFL
                 specialExtraName = "fart";
                 break;
            }*/
           if((specialExtra = Storch.theLevel.extraBlock.getImage())== null){
            specialExtraName=Storch.theLevel.getSpecialExtraName();
            specialExtra = Toolkit.getDefaultToolkit().getImage(getClass().getResource(Storch.IMAGE_PATH  + specialExtraName + ".gif"));
            }
        }
        else {
            free = Toolkit.getDefaultToolkit().getImage(getClass().getResource(Storch.IMAGE_PATH + "init.gif"));
            solid = blast = vd = scDraw = scDelete = specialExtra = free;
        }
        MediaTracker tracker = new MediaTracker(this);
        tracker.addImage(specialExtra, 1);
        try {
            tracker.waitForAll();
        } catch(InterruptedException ie) {
        }
    }
}
