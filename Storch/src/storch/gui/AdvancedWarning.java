/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.gui;

import swingwtx.swing.*;
import swingwt.awt.*;
import swingwt.awt.event.*;

/**
 * This class can be used in a <code>JOptionPane</code> to get a message and a
 * checkbox with the option to disable this warning.
 *
 * @author   Tor Andr�
 */
class AdvancedWarning extends JPanel {

    private JCheckBox showBox;

    /**
     * Constructor for the AdvancedWarning object
     *
     * @param message  The message to display
     */
    public AdvancedWarning(String message) {
        super();
        GridBagConstraints gbc = new GridBagConstraints();

        showBox = new JCheckBox("Don't show this warning again", false);

        setLayout(new GridBagLayout());
        gbc.insets = new Insets(3, 3, 3, 3);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridwidth = 2;

        add(new JLabel(message), gbc);
        add(showBox, gbc);
    }


    /**
     * Gets the answer to the question if the user want to se thi warning again.
     *
     * @return   false if the warning should be disabled, true otherwise.
     */
    public boolean getAnswer() {
        return !showBox.isSelected();
    }

}
