/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

import swingwt.awt.*;
import swingwt.awt.event.*;
import swingwtx.swing.*;
import storch.*;

/**
 *  A dialog box that lets the user choose a color from a predefined color table, by clicking with
 *  the mouse.
 *
 *  @author     Tobias Johansson
 */
public class ColorChooser extends JDialog implements MouseListener {
    private ColorPanel selectedPanel;
    private ColorPanel[] cp;
    private ColorTable table;
    private JPanel p1, p2;
    private JLabel l;


    /**
     *  Constructor for the ColorChooser object
     *
     *  @param  root   The main Storch window
     *  @param  table  The <code>ColorTable</code> to use for this <code>ColorChooser</code>
     */
    public ColorChooser(Frame root, ColorTable table) {
        super(root, "Select a color", true);
        this.table = table;
        cp = new ColorPanel[140];

        p1 = new JPanel();
        p1.setDoubleBuffered(true);
        p2 = new JPanel();
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        p1.setLayout(new GridLayout(20, 7, 1, 1));
        for(int i = 0; i < 140; i++) {
            cp[i] = new ColorPanel(table.getColor(i), table.getName(i));
            cp[i].addMouseListener(this);
            p1.add(cp[i]);
        }
        l = new JLabel("Select a color");
        p2.add(l);
        contentPane.add(p1, BorderLayout.CENTER);
        contentPane.add(p2, BorderLayout.SOUTH);
        p1.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Color Chooser"));
        pack();
    }

    /*
     *  MouseListener methods:
     */
     
    /**
     *  Called when user clicks with the mouse on one of the <code>ColorPanel</code>s.
     *  Selects the color and disposes the <code>ColorChooser</code>.
     *
     *  @param  e  A corresponding <code>MouseEvent</code>.
     */
    public void mouseClicked(MouseEvent e) {
        selectedPanel = (ColorPanel) e.getComponent();
        dispose();
    }


    /**
     *  Empty method, only needed to implement the <code>MouseListener</code> interface.
     *
     *  @param  e  A <code>MouseEvent</code>
     */
    public void mousePressed(MouseEvent e) { }


    /**
     *  Empty method, only needed to implement the <code>MouseListener</code> interface.
     *
     *  @param  e  A <code>MouseEvent</code>
     */
    public void mouseReleased(MouseEvent e) { }


    /**
     *  Called when the mouse pointer enters one of the <code>ColorPanel</code>s. Displays the name
     *  of the color.
     *
     *  @param  e  A corresponding <code>MouseEvent</code>.
     */
    public void mouseEntered(MouseEvent e) {
        l.setText(((ColorPanel) e.getComponent()).getName());
    }


    /**
     *  Empty method, only needed to implement the <code>MouseListener</code> interface.
     *
     *  @param  e  A <code>MouseEvent</code>
     */
    public void mouseExited(MouseEvent e) { }


    /**
     *  Gets the selected color.
     *
     *  @return    A <code>Color</code> object with the selected color, or null if no color is selected
     */
    public Color getSelectedColor() {
        return selectedPanel != null ? selectedPanel.getColor() : null;
    }

	/**
	 *	Shows the color chooser.
	 */
	public void show(){
		selectedPanel = null;
		super.show();
	}

}
