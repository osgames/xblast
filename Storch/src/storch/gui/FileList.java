/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

import java.util.*;
import java.io.*;
import swingwtx.swing.*;

/**
 *  A list of selected files.
 *
 *  @author    Tobias Johansson
 */
class FileList extends JList{

	private Vector files;
	
	/**
     *  Constructor for the <code>GeneralPanel</code> object
     */
	public FileList(){
		super();
		files = new Vector();
		setListData(files);
	}
	
	/**
     *  Adds a number of files to this list.
     *
     *	@param	theFiles	An array of <code>File</code> objects to save in this list.
     */
	public void addFiles(File[] theFiles){
		for(int i = 0; i < theFiles.length; i++){
			File current = theFiles[i];
			boolean found = false;
			for(int j = 0; !found && j < files.size(); j++){
				if(current.equals(files.get(j))){
					found = true;
				}
			}
			if(!found){
				files.add(current);	
			}
		}
		Collections.sort(files);
		setListData(files);
	}
	
	/**
     *  Removes the currently selected files from the list.
     */
	public void removeSelected(){
		int[] selected = getSelectedIndices();
		for(int i = selected.length - 1; i >= 0; i--){
			files.removeElementAt(selected[i]);
		}
		setListData(files);		
	}
	
	/**
     *  Returns the <code>File</code> objects that are saved in this list.
     *
     *	@return		An array of the <code>File</code> objects in this list.
     */
	public File[] getFiles(){
		File[] retval = new File[files.size()];
		for(int i = 0; i < retval.length; i++){
			retval[i] = (File) files.get(i);
		}
		return retval;
	}
}