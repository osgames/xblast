/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.gui;

import swingwtx.swing.*;
import swingwt.awt.*;

import storch.Level;

/**
 * A panel for setting level options. Acts as a container for one <code>GeneralPanel</code>
 * , one <code>GamePanel</code>, one <code>BombsPanel</code> and one <code>ProbsPanel</code>
 * .
 *
 * @author   Tobias Johansson
 */
class OptionsPanel extends JPanel {

    private ProbsPanel probsPan;
    private GamePanel gamePan;
    private GeneralPanel genPan;
    private BombsPanel bombsPan;


    /**
     * Constructor for the <code>OptionsPanel</code> object.
     */
    public OptionsPanel() {
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        setLayout(gridbag);

        JPanel leftPanel = new JPanel();
        JPanel rightPanel = new JPanel();

        GridBagLayout gridbagLeft = new GridBagLayout();
        GridBagConstraints gbcLeft = new GridBagConstraints();
        leftPanel.setLayout(gridbagLeft);
        gbcLeft.fill = GridBagConstraints.BOTH;

        GridBagLayout gridbagRight = new GridBagLayout();
        GridBagConstraints gbcRight = new GridBagConstraints();
        rightPanel.setLayout(gridbagRight);
        gbcRight.fill = GridBagConstraints.BOTH;

        genPan = new GeneralPanel();
        bombsPan = new BombsPanel();
        probsPan = new ProbsPanel();
        gamePan = new GamePanel();

        gbcLeft.gridx = 0;
        leftPanel.add(genPan, gbcLeft);
        leftPanel.add(gamePan, gbcLeft);
        gbcRight.gridx = 0;
        rightPanel.add(bombsPan, gbcRight);
        rightPanel.add(probsPan, gbcRight);

        gbc.gridy = 0;

        add(leftPanel, gbc);
        add(rightPanel, gbc);

        setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Level options"));

    }


    /**
     * Loads the options from the current open level.
     */
    public void load() {
        genPan.load();
        gamePan.load();
        bombsPan.load();
        probsPan.load();
    }


    /**
     * Disables the EPFL support
     */
    public void disableEPFL() {
        bombsPan.disableEPFL();
        gamePan.disableEPFL();
    }


    /**
     * Enables the EPFL support
     */
    public void enableEPFL() {
        bombsPan.enableEPFL();
        gamePan.enableEPFL();
    }


    /**
     * Saves the options to the current open level.
     */
    public void save() {
        genPan.save();
        gamePan.save();
        bombsPan.save();
        probsPan.save();
    }

}
