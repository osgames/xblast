/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

/**
 *  Represents a selectable item in a combo box. Has a name that is shown in the list, and an
 *  associated integer value.
 *
 *  @author     Tobias Johansson
 */
class ComboBoxItem {
    public String name;
    public int value;


    /**
     *  Constructor for the <code>ComboBoxItem</code> object
     *
     *  @param  name   The name of this item. Will show in the combo box list.
     *  @param  value  The value associated to this item. Will not show in the combo box list.
     */
    public ComboBoxItem(String name, int value) {
        this.name = name;
        this.value = value;
    }


    /**
     *  Returns a <code>String</code> representation of this <code>ComboBoxItem</code> object.
     *
     *  @return    A <code>String</code> representation of this <code>ComboBoxItem</code> object
     */
    public String toString() {
        return name;
    }
}
