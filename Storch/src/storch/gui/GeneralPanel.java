/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

import swingwtx.swing.*;
import swingwt.awt.*;

import storch.*;

/**
 *  A panel for setting general options such as title, description, game mode, etc.
 *
 *  @author    Tobias Johansson
 */
class GeneralPanel extends JPanel {

    private JCheckBox play2, play3, play4, play5, play6, single, dble, team, randPos, leftRight;
    private JTextField title, author, descr;


    /**
     *  Constructor for the <code>GeneralPanel</code> object
     */
    public GeneralPanel() {
        super();

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        setLayout(gridbag);

        gbc.gridx = 0;
        gbc.anchor = GridBagConstraints.EAST;
        add(new JLabel("Title:  "), gbc);
        add(new JLabel("Author:  "), gbc);
        add(new JLabel("Description:  "), gbc);
        add(new JLabel("Number of players:  "), gbc);
        add(new JLabel("Game modes:  "), gbc);
        add(new JLabel("Random start pos:  "), gbc);
        add(new JLabel("Left & right players:  "), gbc);

        gbc.gridx = 1;
        gbc.gridwidth = 5;
        gbc.anchor = GridBagConstraints.WEST;
        title = new JTextField(18);
        title.setBackground(Color.white);
        add(title, gbc);
        author = new JTextField(18);
        author.setBackground(Color.white);
        add(author, gbc);
        descr = new JTextField(18);
        descr.setBackground(Color.white);
        add(descr, gbc);
        gbc.gridx = GridBagConstraints.RELATIVE;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        add(play2 = new JCheckBox("2"), gbc);
        add(play3 = new JCheckBox("3"), gbc);
        add(play4 = new JCheckBox("4"), gbc);
        add(play5 = new JCheckBox("5"), gbc);
        add(play6 = new JCheckBox("6"), gbc);
        gbc.gridy = 4;
        gbc.gridwidth = 2;
        add(single = new JCheckBox("Single"), gbc);
        add(dble = new JCheckBox("Double"), gbc);
        gbc.gridwidth = 1;
        add(team = new JCheckBox("Team"), gbc);
        gbc.gridx = 1;
        gbc.gridy = GridBagConstraints.RELATIVE;
        gbc.gridwidth = 5;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(randPos = new JCheckBox(), gbc);
        add(leftRight = new JCheckBox(), gbc);
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Level info"));

        title.setToolTipText("The name of your level (required)");
        author.setToolTipText("Your name");
        descr.setToolTipText("Some short words about the level");
        play2.setToolTipText("Allow 2 player game on this level");
        play3.setToolTipText("Allow 3 player game on this level");
        play4.setToolTipText("Allow 4 player game on this level");
        play5.setToolTipText("Allow 5 player game on this level");
        play6.setToolTipText("Allow 6 player game on this level");
        single.setToolTipText("Allow single game mode (no teams)");
        dble.setToolTipText("Allow double game mode");
        team.setToolTipText("Allow team game mode");
        randPos.setToolTipText("Allow random start positions");
        leftRight.setToolTipText("Allow two players on one keyboard");

        load();
    }


    /**
     *  Loads the game data from the current open level.
     */
    public void load() {
        Level theLevel = Storch.theLevel;
        title.setText(theLevel.title);
        author.setText(theLevel.author);
        descr.setText(theLevel.description);
        play2.setSelected(theLevel.GM_2_Player);
        play3.setSelected(theLevel.GM_3_Player);
        play4.setSelected(theLevel.GM_4_Player);
        play5.setSelected(theLevel.GM_5_Player);
        play6.setSelected(theLevel.GM_6_Player);
        single.setSelected(theLevel.GM_Single);
        dble.setSelected(theLevel.GM_Double);
        team.setSelected(theLevel.GM_Team);
        randPos.setSelected(theLevel.GM_Random);
        leftRight.setSelected(theLevel.GM_LR_Players);
    }


    /**
     *  Saves the general data to the current open level.
     */
    public void save() {
        Level theLevel = Storch.theLevel;
        theLevel.title = title.getText();
        theLevel.author = author.getText();
        theLevel.description = descr.getText();
        theLevel.GM_2_Player = play2.isSelected();
        theLevel.GM_3_Player = play3.isSelected();
        theLevel.GM_4_Player = play4.isSelected();
        theLevel.GM_5_Player = play5.isSelected();
        theLevel.GM_6_Player = play6.isSelected();
        theLevel.GM_Single = single.isSelected();
        theLevel.GM_Team = team.isSelected();
        theLevel.GM_Double = dble.isSelected();
        theLevel.GM_Random = randPos.isSelected();
        theLevel.GM_LR_Players = leftRight.isSelected();
    }

}
