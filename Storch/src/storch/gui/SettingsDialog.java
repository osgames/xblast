/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.gui;

import swingwtx.swing.*;
import swingwt.awt.*;
import swingwt.awt.event.*;
import java.io.File;
import storch.Settings;

/**
 * A dialog box for changing the settings.
 *
 * @author   Tor Andr�
 */
class SettingsDialog extends JDialog implements ActionListener, ItemListener {

    private JButton okButton, cancelButton, resetButton, browseLevel, browseDoc;
    private JTextField levelField, docField;
    private JCheckBox epflWarningBox, epflSupportBox;
    private final int MIN_LENGTH = 25;
    private final int LEVEL = 0;
    private final int DOC = 1;



    /**
     * Constructor for the SettingsDialog object
     *
     * @param theWindow  The parent window
     */
    public SettingsDialog(JFrame theWindow) {
        super(theWindow, "Settings", true);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(3, 3, 3, 3);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridwidth = 2;
        gbc.gridy = 0;

        getContentPane().setLayout(new BorderLayout());

        browseLevel = new JButton("Browse...");
        browseDoc = new JButton("Browse...");
        browseLevel.addActionListener(this);
        browseDoc.addActionListener(this);

        JPanel textPanel = new JPanel();
        JPanel buttonPanel = new JPanel();

        textPanel.setLayout(new GridBagLayout());

        textPanel.add(new JLabel("Default level directory:"), gbc);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridy = 1;
        textPanel.add(levelField = new JTextField(), gbc);
        gbc.gridx = 2;
        gbc.fill = GridBagConstraints.NONE;
        textPanel.add(browseLevel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        textPanel.add(new JLabel("Default levelDoc directory:"), gbc);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridy = 3;
        textPanel.add(docField = new JTextField(), gbc);
        gbc.gridx = 2;
        gbc.fill = GridBagConstraints.NONE;
        textPanel.add(browseDoc, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.WEST;
        textPanel.add(epflSupportBox = new JCheckBox("EPFL support enabled"), gbc);
        gbc.gridy = 5;
        textPanel.add(epflWarningBox = new JCheckBox("Show EPFL warning"), gbc);

        epflSupportBox.addItemListener(this);

        getContentPane().add(textPanel, BorderLayout.CENTER);

        buttonPanel.add(okButton = new JButton("OK"));
        buttonPanel.add(resetButton = new JButton("Reset defaults"));
        buttonPanel.add(cancelButton = new JButton("Cancel"));
        okButton.addActionListener(this);
        resetButton.addActionListener(this);
        cancelButton.addActionListener(this);

        getContentPane().add(buttonPanel, BorderLayout.SOUTH);

        init(Settings.instance());
        pack();
        setVisible(true);
    }


    /**
     * Initsializes the settings
     *
     * @param sett  A Setting objekt with the values
     */
    private void init(Settings sett) {
        String levelDir = sett.getLevelDir();
        String docDir = sett.getDocDir();
        int length = MIN_LENGTH;

        levelField.setColumns(length);
        docField.setColumns(length);
        levelField.setText(levelDir);
        docField.setText(docDir);
        epflWarningBox.setSelected(sett.getEPFLWarning());
        epflSupportBox.setSelected(sett.getEPFLSupport());
	epflWarningBox.setEnabled(epflSupportBox.isSelected());
    }


    /**
     * Opens a directory selector
     *
     * @param type  <code>LEVEL</code> or <code>DOC</code>
     * @return      The selected directory as a String, or null if none is
     *      selected
     */
    private String browse(int type) {
        String title;
        String startDir;
        if(type == LEVEL) {
            title = "Settings - Select new level directory";
            startDir = levelField.getText();
        }
        else {
            title = "Settings - Select new levelDoc directory";
            startDir = docField.getText();
        }

        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle(title);
        chooser.setSelectedFile(new File(startDir));
        chooser.setMultiSelectionEnabled(false);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            String dest = chooser.getSelectedFile().toString();
            return dest;
        }
        else
            return null;
    }


    /**
     * Called when epflSupportBox is changed
     *
     * @param e  ItemEvent
     */
    public void itemStateChanged(ItemEvent e) {
        epflWarningBox.setEnabled(epflSupportBox.isSelected());
    }


    /**
     * Handles buttons
     *
     * @param e  An ActionEvent
     */
    public void actionPerformed(ActionEvent e) {
        JComponent source = (JComponent) e.getSource();

        if(source == okButton) {
            Settings.instance().setLevelDir(levelField.getText());
            Settings.instance().setDocDir(docField.getText());
            Settings.instance().setEPFLWarning(epflWarningBox.isSelected());
            Settings.instance().setEPFLSupport(epflSupportBox.isSelected());
            dispose();
        }
        else if(source == cancelButton) {
            dispose();
        }
        else if(source == resetButton) {
            levelField.setText(new File("levels").getAbsolutePath());
            docField.setText(new File("doc").getAbsolutePath());
            epflWarningBox.setSelected(true);
            epflSupportBox.setSelected(true);
        }
        else if(source == browseLevel) {
            String dir = browse(LEVEL);
            if(dir != null)
                levelField.setText(dir);
        }
        else if(source == browseDoc) {
            String dir = browse(DOC);
            if(dir != null)
                docField.setText(dir);
        }
    }

}
