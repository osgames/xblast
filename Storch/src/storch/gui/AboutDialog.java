/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

import swingwt.awt.*;
import swingwt.awt.event.*;
import swingwtx.swing.*;
import storch.*;

/**
 *  A dialog box showing some general information about Storch.
 *
 *  @author     Tobias Johansson
 */

class AboutDialog extends JDialog implements ActionListener {
    private JButton okButton = new JButton("OK");
    private ImagePanel imgPan;
    private JPanel textPan, aboutPan;


    /**
     *  Constructor for the AboutDialog object.
     *
     *  @param  theWindow  The main Storch window
     *  @param  blastaMan  The image that will be shown in the dialog box
     */
    public AboutDialog(JFrame theWindow, Image blastaMan) {
        super(theWindow, "Storch " + Storch.VERSION, true);

        textPan = new JPanel();
        aboutPan = new JPanel();

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        aboutPan.setLayout(gridbag);
        textPan.setLayout(new GridLayout(0, 1));

        gbc.fill = GridBagConstraints.NONE;
        textPan.setBackground(new Color(200, 255, 255));
        aboutPan.setBackground(new Color(0, 120, 120));
        imgPan = new ImagePanel(blastaMan);
        textPan.add(new JLabel("Storch " + Storch.VERSION, JLabel.CENTER));
        textPan.add(new JLabel("by Tobias Johansson (d97tj@efd.lth.se)", JLabel.CENTER));
        textPan.add(new JLabel("and Tor Andr� (d97tan@efd.lth.se)", JLabel.CENTER));
        textPan.add(new JLabel("Get the latest version at:", JLabel.CENTER));
        textPan.add(new JLabel("http://storch.sourceforge.net", JLabel.CENTER));
        textPan.add(new JLabel("Storch is distributed under the", JLabel.CENTER));
        textPan.add(new JLabel("Gnu General Public Licence (GPL)", JLabel.CENTER));
        textPan.add(new JLabel("View the README and LICENCE.txt", JLabel.CENTER));
        textPan.add(new JLabel("files for more info!", JLabel.CENTER));
        textPan.add(okButton);
        okButton.addActionListener(this);

        textPan.setBorder(BorderFactory.createLoweredBevelBorder());
        aboutPan.setBorder(BorderFactory.createLoweredBevelBorder());

        aboutPan.add(imgPan);
        aboutPan.add(textPan);
        getContentPane().add(aboutPan);

		MediaTracker tracker = new MediaTracker(this);
		tracker.addImage(blastaMan, 0);
		try{
			tracker.waitForAll();
		}
		catch(InterruptedException ie){}

        pack();
        show();
    }


    /**
     *  Called when the OK-button is pressed. Closes the dialog box.
     *
     *  @param  e  An <code>ActionEvent</code> created when the button is pressed
     */
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == okButton) {
            dispose();
        }
    }
}

