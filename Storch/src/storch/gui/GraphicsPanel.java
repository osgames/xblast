/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

import swingwtx.swing.*;
import swingwt.awt.*;
import swingwt.awt.event.*;
import storch.*;

/**
 *  A panel for setting graphics options. Also contains a <code>MapPanel</code>.
 *
 *  @author     Tobias Johansson
 */
public class GraphicsPanel extends JPanel implements MouseListener, ItemListener {
    private JPanel northPan, centerPan;
    private JComboBox stylesCombo, freeCombo, solidCombo, blastCombo, voidCombo,extraCombo;
    private ColorPanel freeCol0, freeCol1, freeCol2, solidCol0, solidCol1, solidCol2, blastCol0, blastCol1, blastCol2, voidCol0, voidCol1, voidCol2, extraCol0, extraCol1, extraCol2;
    private Block freeBlock, solidBlock, blastBlock, voidBlock,extraBlock;
    private ImagePanel freeImg, solidImg, blastImg, voidImg,extraImg;
    private ColorChooser chooser;
    private ColorTable colors = new ColorTable();
    private Cursor waitCursor = new Cursor(Cursor.WAIT_CURSOR);
    private Cursor normalCursor = new Cursor(Cursor.DEFAULT_CURSOR);
    private GraphicsData gData;
    private MapPanel theMap;
    private ColorPanel lastClicked, lastEntered;
	private Frame theWindow;
         private JCheckBox colorsAll, colorsExtra;

    /**
     *  Constructor for the <code>GraphicsPanel</code> object.
     *
     *  @param  theMap  The <code>MapPanel</code> that will be held in this <code>GraphicsPanel</code> object.
     */
    public GraphicsPanel(MapPanel theMap, Frame theWindow) {

		gData = GraphicsData.getInstance();
        this.theMap = theMap;
      	this.theWindow = theWindow;
		chooser = new ColorChooser(theWindow, colors);
		
        // Layout
        setLayout(new BorderLayout());
        northPan = new JPanel();
        centerPan = new JPanel();
        northPan.setLayout(new FlowLayout());
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        centerPan.setLayout(gridbag);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(northPan, BorderLayout.NORTH);
        add(centerPan);
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Graphics"));

        // Graphics Styles
        stylesCombo = new JComboBox();
        GraphicsStyle[] styles = gData.getGraphicsStyles();
        for(int i = 0; i < styles.length; i++) {
            stylesCombo.addItem(styles[i]);
        }
        stylesCombo.addItem(new GraphicsStyle("Custom", styles[0].free, styles[0].solid, styles[0].blastable, styles[0].voidBlock, styles[0].extraBlock));

        // Block Combos
        String[] names;

        freeCombo = new JComboBox();
        names = gData.getFree();
        for(int i = 0; i < names.length; i++) {
            freeCombo.addItem(names[i]);
        }

        solidCombo = new JComboBox();
        names = gData.getSolid();
        for(int i = 0; i < names.length; i++) {
            solidCombo.addItem(names[i]);
        }

        blastCombo = new JComboBox();
        names = gData.getBlast();
        for(int i = 0; i < names.length; i++) {
            blastCombo.addItem(names[i]);
        }

        voidCombo = new JComboBox();
        names = gData.getAllBlocks();
        for(int i = 0; i < names.length; i++) {
            voidCombo.addItem(names[i]);
        }
       
        
         extraCombo = new JComboBox();
        names = gData.getAllBlocks();
        for(int i = 0; i < names.length; i++) {
            extraCombo.addItem(names[i]);
        }
        names=Storch.theLevel.getSpecialExtraNames();
       for(int i=0;i< Storch.theLevel.NUMBER_OF_SPECIAL_EXTRAS+1;i++){
           
            extraCombo.addItem(names[i]);
        }

        // Color Panels
        int fontSize = 11;
        freeCol0 = new ColorPanel(Color.black, "Black", fontSize);
        freeCol1 = new ColorPanel(Color.black, "Black", fontSize);
        freeCol2 = new ColorPanel(Color.black, "Black", fontSize);
        solidCol0 = new ColorPanel(Color.black, "Black", fontSize);
        solidCol1 = new ColorPanel(Color.black, "Black", fontSize);
        solidCol2 = new ColorPanel(Color.black, "Black", fontSize);
        blastCol0 = new ColorPanel(Color.black, "Black", fontSize);
        blastCol1 = new ColorPanel(Color.black, "Black", fontSize);
        blastCol2 = new ColorPanel(Color.black, "Black", fontSize);
        voidCol0 = new ColorPanel(Color.black, "Black", fontSize);
        voidCol1 = new ColorPanel(Color.black, "Black", fontSize);
        voidCol2 = new ColorPanel(Color.black, "Black", fontSize);
        extraCol0 = new ColorPanel(Color.black, "Black", fontSize);
        extraCol1 = new ColorPanel(Color.black, "Black", fontSize);
        extraCol2 = new ColorPanel(Color.black, "Black", fontSize);

        // Image Panels
        Image init_image = Toolkit.getDefaultToolkit().getImage(getClass().getResource(Storch.IMAGE_PATH + "init.gif"));
        freeImg = new ImagePanel(init_image);
        solidImg = new ImagePanel(init_image);
        blastImg = new ImagePanel(init_image);
        voidImg = new ImagePanel(init_image);
        extraImg = new ImagePanel(init_image);

        // Add components
        northPan.add(new JLabel("Graphics Style:"));
        northPan.add(stylesCombo);
         colorsAll=new JCheckBox("Colors for Blocks");
        northPan.add(colorsAll);
         colorsExtra=new JCheckBox("Colors for Extra");
         northPan.add(colorsExtra);

        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(2, 2, 2, 2);
        gbc.gridx = 0;
         
        centerPan.add(new JLabel(""), gbc);
        centerPan.add(freeImg, gbc);
        centerPan.add(solidImg, gbc);
        centerPan.add(blastImg, gbc);
        centerPan.add(voidImg, gbc);
        centerPan.add(extraImg, gbc);

        gbc.gridx = 1;
        centerPan.add(new JLabel("Block:"), gbc);
        centerPan.add(new JLabel("Floor"), gbc);
        centerPan.add(new JLabel("Solid wall"), gbc);
        centerPan.add(new JLabel("Blastable"), gbc);
        centerPan.add(new JLabel("Void block"), gbc);
        centerPan.add(new JLabel("Extra"), gbc);

        gbc.gridx = 2;
        centerPan.add(new JLabel("Bitmap:"), gbc);
        centerPan.add(freeCombo, gbc);
        centerPan.add(solidCombo, gbc);
        centerPan.add(blastCombo, gbc);
        centerPan.add(voidCombo, gbc);
        centerPan.add(extraCombo, gbc);

        gbc.gridx = 3;
        centerPan.add(new JLabel("Color 0:"), gbc);
        centerPan.add(freeCol0, gbc);
        centerPan.add(solidCol0, gbc);
        centerPan.add(blastCol0, gbc);
        centerPan.add(voidCol0, gbc);
        centerPan.add(extraCol0, gbc);

        gbc.gridx = 4;
        centerPan.add(new JLabel("Color 1:"), gbc);
        centerPan.add(freeCol1, gbc);
        centerPan.add(solidCol1, gbc);
        centerPan.add(blastCol1, gbc);
        centerPan.add(voidCol1, gbc);
        centerPan.add(extraCol1, gbc);

        gbc.gridx = 5;
        centerPan.add(new JLabel("Color 2:"), gbc);
        centerPan.add(freeCol2, gbc);
        centerPan.add(solidCol2, gbc);
        centerPan.add(blastCol2, gbc);
        centerPan.add(voidCol2, gbc);
        centerPan.add(extraCol2, gbc);

        // Add listeners
        freeCol0.addMouseListener(this);
        freeCol1.addMouseListener(this);
        freeCol2.addMouseListener(this);
        solidCol0.addMouseListener(this);
        solidCol1.addMouseListener(this);
        solidCol2.addMouseListener(this);
        blastCol0.addMouseListener(this);
        blastCol1.addMouseListener(this);
        blastCol2.addMouseListener(this);
        voidCol0.addMouseListener(this);
        voidCol1.addMouseListener(this);
        voidCol2.addMouseListener(this);
        extraCol0.addMouseListener(this);
        extraCol1.addMouseListener(this);
        extraCol2.addMouseListener(this);

        stylesCombo.addItemListener(this);
        freeCombo.addItemListener(this);
        solidCombo.addItemListener(this);
        blastCombo.addItemListener(this);
        voidCombo.addItemListener(this);
        extraCombo.addItemListener(this);
        //colorsAll.addItemListener(this);
       //colorsExtra.addItemListener(this);
        

        // Tool Tips
        stylesCombo.setToolTipText("Choose a pre-defined graphics style to work with");
        freeCombo.setToolTipText("Select the bitmap to use for free blocks");
        solidCombo.setToolTipText("Select the bitmap to use for solid walls");
        blastCombo.setToolTipText("Select the bitmap to use for blastable blocks");
        voidCombo.setToolTipText("Select the bitmap to use for void blocks");
        extraCombo.setToolTipText("Select the bitmap to use for your extra");
        freeCol0.setToolTipText("Click to change the 'foreground' color for free blocks");
        freeCol1.setToolTipText("Click to change the 'background' color for free blocks");
        freeCol2.setToolTipText("Click to change the 'additional' color for free blocks");
        solidCol0.setToolTipText("Click to change the 'foreground' color for solid walls");
        solidCol1.setToolTipText("Click to change the 'background' color for solid walls");
        solidCol2.setToolTipText("Click to change the 'additional' color for solid walls");
        blastCol0.setToolTipText("Click to change the 'foreground' color for blastable blocks");
        blastCol1.setToolTipText("Click to change the 'background' color for blastable blocks");
        blastCol2.setToolTipText("Click to change the 'additional' color for blastable blocks");
        voidCol0.setToolTipText("Click to change the 'foreground' color for void blocks");
        voidCol1.setToolTipText("Click to change the 'background' color for void blocks");
        voidCol2.setToolTipText("Click to change the 'additional' color for void blocks");
        extraCol0.setToolTipText("Click to change the 'foreground' color for extra blocks");
        extraCol1.setToolTipText("Click to change the 'background' color for extra blocks");
        colorsAll.setToolTipText("Click to disable colors for blocks");
        colorsExtra.setToolTipText("Click to disable colors for special extras");

        load();
    }


    /**
     *  Empty method, only needed to implement the <code>MouseListener</code> interface.
     *
     *  @param  e  A <code>MouseEvent</code>
     */
    public void mouseClicked(MouseEvent e) {}


    /**
     *  Called when the user presses a mouse button in one of the <code>ColorPanel</code>s. The panel is 
     *  remembered as the last clicked. Cursor is changed to a drag-cursor.
     *
     *  @param  e  The corresponding <code>MouseEvent</code>
     */
    public void mousePressed(MouseEvent e) { 
    	lastClicked = (ColorPanel) e.getComponent();
    	setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }


    /**
     *  Called when the user releases the mouse button. If the mouse pointer is over one of the
     *  <code>ColorPanel</code>s, the color of that panel is updated, and also the block images. 
     *  If a color is dragged from another panel, the new color is obtained from that. Otherwise 
     *	a <code>ColorChooser</code> is brought up to let the user select a new color.
     *
     *  @param  e  The corresponding <code>MouseEvent</code>
     */
    public void mouseReleased(MouseEvent e) { 
    	setCursor(Cursor.getDefaultCursor());
    	if(lastClicked != null && lastEntered != null){
    		ColorPanel source = lastEntered;
	    	if(lastClicked == source){
		        Point frameOffset = theWindow.getLocationOnScreen();
		        Point clickPoint = source.getLocationOnScreen();
		        Point sourcePoint = new Point(clickPoint.x - frameOffset.x, clickPoint.y - frameOffset.y);
		        chooser.setLocation(sourcePoint.x - chooser.getWidth() - 1, sourcePoint.y - chooser.getHeight() + source.getHeight());
		        chooser.show();
		        Color col = chooser.getSelectedColor();
		        if(col != null) {
		            source.setColor(col, colors.getName(col));
		        }
	        }
	        else{
	    		source.setColor(lastClicked.getColor(), lastClicked.getName());
	    	}
	    	updateImages();
			checkStyles();
	    }
    }


    /**
     *  Called when the mouse pointer enters one of the <code>ColorPanel</code>s. The panel is 
     *  remembered as the last entered.
     *
     *  @param  e  The corresponding <code>MouseEvent</code>
     */
    public void mouseEntered(MouseEvent e) { 
    	lastEntered = (ColorPanel) e.getComponent();
    }


    /**
     *  Called when the mouse pointer exits one of the <code>ColorPanel</code>s. The remembered
     *	last entered panel is forgotten.
     *
     *  @param  e  The corresponding <code>MouseEvent</code>
     */
    public void mouseExited(MouseEvent e) { 
    	lastEntered = null;
    }


    /**
     *  Updates the images in this <code>GraphicsPanel</code> according to the current settings.
     */
    public void updateImages() {
    	freeBlock = new Block(Block.FREE_BLOCK, gData.getFileName((String) freeCombo.getSelectedItem()), freeCol0.getColor(), freeCol1.getColor(), freeCol2.getColor());
        solidBlock = new Block(Block.SOLID_WALL, gData.getFileName((String) solidCombo.getSelectedItem()), solidCol0.getColor(), solidCol1.getColor(), solidCol2.getColor());
        blastBlock = new Block(Block.BLASTABLE_BLOCK, gData.getFileName((String) blastCombo.getSelectedItem()), blastCol0.getColor(), blastCol1.getColor(), blastCol2.getColor());
        voidBlock = new Block(Block.VOID_BLOCK, gData.getFileName((String) voidCombo.getSelectedItem()), voidCol0.getColor(), voidCol1.getColor(), voidCol2.getColor());
      
       // System.out.println( "void "+voidCombo.getSelectedItem()+"   "+ gData.getFileName((String) voidCombo.getSelectedItem()));
        if( gData.getFileName((String) extraCombo.getSelectedItem() )==null){
       // System.out.println( "void1 "+extraCombo.getSelectedItem());
        extraBlock = new Block(Block.SPECIAL_EXTRA, (String) extraCombo.getSelectedItem(), extraCol0.getColor(), extraCol1.getColor(), extraCol2.getColor());
        
        }
        else{
       // System.out.println( " kbgnlf "+extraCombo.getSelectedItem());
            extraBlock = new Block(Block.SPECIAL_EXTRA,gData.getFileName((String) extraCombo.getSelectedItem() ), extraCol0.getColor(), extraCol1.getColor(), extraCol2.getColor());
        }

       // System.out.println(" blu");
        Image i0 = freeBlock.getImage();
    	Image i1 = solidBlock.getImage();
    	Image i2 = blastBlock.getImage();
    	Image i3 = voidBlock.getImage();
	System.out.println(" extra block file "+extraBlock.getFileName());
    	Image i4 = extraBlock.getImage();
  
        MediaTracker tracker = new MediaTracker(this); 
        tracker.addImage(i0, 0);
        tracker.addImage(i1, 1);
        tracker.addImage(i2, 2);
        tracker.addImage(i3, 3);
        tracker.addImage(i4, 4);
        try{
        	tracker.waitForAll();
        }
        catch(InterruptedException ie){}
        
        freeImg.setImage(i0);
        solidImg.setImage(i1);
        blastImg.setImage(i2);
        voidImg.setImage(i3); 
        extraImg.setImage(i4); 
        
        save();
	System.out.println(" exiting update images");
    }


    /**
     *  Called when any one of the combo boxes has been changed. Updates the appropriate image(s).
     *
     *  @param  e  Description of the Parameter
     */
    public void itemStateChanged(ItemEvent e) {
        if(e.getStateChange() == ItemEvent.SELECTED) {
            JComboBox source = (JComboBox) e.getSource();
        //    JCheckBox sourcec = (JCheckBox) e.getSource();
            source.removeItemListener(this);
            if(source == stylesCombo) {
                applyGraphicsStyle();
            } else if(source == freeCombo) {
                freeBlock = new Block(Block.FREE_BLOCK, gData.getFileName((String) freeCombo.getSelectedItem()), freeCol0.getColor(), freeCol1.getColor(), freeCol2.getColor());
                freeImg.setImage(freeBlock.getImage());
                checkStyles();
            } else if(source == solidCombo) {
                solidBlock = new Block(Block.SOLID_WALL, gData.getFileName((String) solidCombo.getSelectedItem()), solidCol0.getColor(), solidCol1.getColor(), solidCol2.getColor());
                solidImg.setImage(solidBlock.getImage());
                checkStyles();
            } else if(source == blastCombo) {
                blastBlock = new Block(Block.BLASTABLE_BLOCK, gData.getFileName((String) blastCombo.getSelectedItem()), blastCol0.getColor(), blastCol1.getColor(), blastCol2.getColor());
                blastImg.setImage(blastBlock.getImage());
                checkStyles();
            } else if(source == voidCombo) {
                voidBlock = new Block(Block.VOID_BLOCK, gData.getFileName((String) voidCombo.getSelectedItem()), voidCol0.getColor(), voidCol1.getColor(), voidCol2.getColor());
		System.out.println("voidcombo"+	gData.getFileName((String) voidCombo.getSelectedItem()));
                voidImg.setImage(voidBlock.getImage());
                checkStyles();
            }else if(source == extraCombo) {
              //  System.out.println(" source "+extraCombo.getSelectedItem());
		if(null==gData.getFileName((String) extraCombo.getSelectedItem())){
		extraBlock = new Block(Block.SPECIAL_EXTRA,  (String)extraCombo.getSelectedItem(), extraCol0.getColor(), extraCol1.getColor(), extraCol2.getColor());
		}else{
		    extraBlock = new Block(Block.SPECIAL_EXTRA,gData.getFileName((String) extraCombo.getSelectedItem())  , extraCol0.getColor(), extraCol1.getColor(), extraCol2.getColor());
		}
              //  System.out.println(" extra name "+extraBlock.getFileName()+" wno "+ (String)extraCombo.getSelectedItem());
                extraImg.setImage(extraBlock.getImage());
                checkStyles();
            }
            /*else if( sourcec == colorsAll){
            } else if( sourcec == colorsExtra){
            }*/
            save();
            source.addItemListener(this);
        }
    }


    /**
     *  Applies the current selected graphics style. Updates this <code>GraphicsPanel</code> as well 
     *	as all images.
     */
    public void applyGraphicsStyle() {
        GraphicsStyle currentStyle = (GraphicsStyle) stylesCombo.getSelectedItem();

        freeBlock = currentStyle.free;
        solidBlock = currentStyle.solid;
        blastBlock = currentStyle.blastable;
        voidBlock = currentStyle.voidBlock;
        extraBlock = currentStyle.extraBlock;

        setColors();

        setCombos();

        updateImages();
    }


    /**
     *  Updates the <code>ColorPanel</code>s in this panel according to the current block settings.
     */
    public void setColors() {
        Color col;
        col = freeBlock.getColor0();
        freeCol0.setColor(col, colors.getName(col));
        col = freeBlock.getColor1();
        freeCol1.setColor(col, colors.getName(col));
        col = freeBlock.getColor2();
        freeCol2.setColor(col, colors.getName(col));
        col = solidBlock.getColor0();
        solidCol0.setColor(col, colors.getName(col));
        col = solidBlock.getColor1();
        solidCol1.setColor(col, colors.getName(col));
        col = solidBlock.getColor2();
        solidCol2.setColor(col, colors.getName(col));
        col = blastBlock.getColor0();
        blastCol0.setColor(col, colors.getName(col));
        col = blastBlock.getColor1();
        blastCol1.setColor(col, colors.getName(col));
        col = blastBlock.getColor2();
        blastCol2.setColor(col, colors.getName(col));
        col = voidBlock.getColor0();
        voidCol0.setColor(col, colors.getName(col));
        col = voidBlock.getColor1();
        voidCol1.setColor(col, colors.getName(col));
        col = voidBlock.getColor2();
        voidCol2.setColor(col, colors.getName(col));
	if(extraBlock.colors==1){
        col = extraBlock.getColor0();
        extraCol0.setColor(col, colors.getName(col));
        col = extraBlock.getColor1();
        extraCol1.setColor(col, colors.getName(col));
        col = extraBlock.getColor2();
        extraCol2.setColor(col, colors.getName(col));
	}
    }


    /**
     *  Updates the combo boxes in this panel according to the current block settings.
     */
    public void setCombos() {
        freeCombo.removeItemListener(this);
        solidCombo.removeItemListener(this);
        blastCombo.removeItemListener(this);
        voidCombo.removeItemListener(this);
        extraCombo.removeItemListener(this);
        String freeName = gData.getName(freeBlock.getFileName());
        if(!findItem(freeCombo, freeName)) {
            freeCombo.addItem(freeName);
        }
        freeCombo.setSelectedItem(freeName);
        String solidName = gData.getName(solidBlock.getFileName());
        if(!findItem(solidCombo, solidName)) {
            solidCombo.addItem(solidName);
        }
        solidCombo.setSelectedItem(solidName);
        String blastName = gData.getName(blastBlock.getFileName());
        if(!findItem(blastCombo, blastName)) {
            blastCombo.addItem(blastName);
        }
        blastCombo.setSelectedItem(blastName);
        String voidName = gData.getName(voidBlock.getFileName());
        if(!findItem(voidCombo, voidName)) {
            voidCombo.addItem(voidName);
        }
        voidCombo.setSelectedItem(voidName);
       //  System.out.println(" ha "+extraBlock.getFileName());
         String extraName;
         if(gData.getName(extraBlock.getFileName())!=null){
         extraName= gData.getName(extraBlock.getFileName());
         }
         else{
              extraName = extraBlock.getFileName();
         }
        if(!findItem(extraCombo, extraName)) {
            if(extraName.startsWith("extra_")){}
            else{
            extraCombo.addItem(extraName);
            }
        }
        extraCombo.setSelectedItem(extraName);
        freeCombo.addItemListener(this);
        solidCombo.addItemListener(this);
        blastCombo.addItemListener(this);
        voidCombo.addItemListener(this);
        extraCombo.addItemListener(this);
    }


    /**
     *  Loads the graphics settings from the current open level.
     */
    public void load() {
        if(Storch.theLevel.freeBlock != null) {
            freeBlock = Storch.theLevel.freeBlock;
            solidBlock = Storch.theLevel.solidWall;
            blastBlock = Storch.theLevel.blastableBlock;
            voidBlock = Storch.theLevel.voidBlock;
            extraBlock = Storch.theLevel.extraBlock;
            setColors();
            setCombos();
            freeImg.setImage(freeBlock.getImage());
        	solidImg.setImage(solidBlock.getImage());
        	blastImg.setImage(blastBlock.getImage());
       		extraImg.setImage(extraBlock.getImage());
       		checkStyles();
        } else {
            stylesCombo.setSelectedIndex(0);
            applyGraphicsStyle();
        }
    }


    /**
     *  Saves the graphics settings to the current open level.
     */
    public void save() {
        Level theLevel = Storch.theLevel;

        theLevel.freeBlock = freeBlock;
        theLevel.solidWall = solidBlock;
        theLevel.blastableBlock = blastBlock;
        theLevel.voidBlock = voidBlock;
        theLevel.extraBlock = extraBlock;

        theLevel.blastedFloor = new Block(Block.OTHER_BLOCK, gData.getXFile(freeBlock.getFileName()), freeBlock.getColor0(), freeBlock.getColor1(), freeBlock.getColor2());
        theLevel.shadowedBlock = new Block(Block.OTHER_BLOCK, gData.getSFile(freeBlock.getFileName()), freeBlock.getColor0(), freeBlock.getColor1(), freeBlock.getColor2());
        theLevel.blastedBlock = new Block(Block.OTHER_BLOCK, gData.getOFile(blastBlock.getFileName()), freeBlock.getColor0(), blastBlock.getColor1(), blastBlock.getColor2());
        theLevel.risingWall = new Block(Block.OTHER_BLOCK, gData.getRFile(solidBlock.getFileName()), freeBlock.getColor0(), solidBlock.getColor1(), solidBlock.getColor2());
        if(colorsAll.isSelected()){
                theLevel.COLORSALL=1;}
        else{
            
                theLevel.COLORSALL=0;}
        if(colorsExtra.isSelected()){
                theLevel.COLORSEXTRA=1;}
        else{
                theLevel.COLORSEXTRA=0;}
            
        theMap.notifyGraphicsChange();
    }


    /**
     *  Checks if a combobox contains an item with the specified name.
     *
     *  @param  combo     The <code>JComboBox</code> in which to look for the item
     *  @param  itemName  The item name looked for
     *  @return           true if the combo box contains an item with the specified name, false otherwise.
     */
    public boolean findItem(JComboBox combo, String itemName) {
        int i = 0;
        boolean found = false;
        while(!found && i < combo.getItemCount()) {
         //   System.out.println(" "+itemName+" "+combo.getItemAt(i));
            if(((String) combo.getItemAt(i)).equals(itemName)) {
                found = true;
            }
            i++;
        }
        return found;
    }


    /**
     *  Checks if the current graphics settings is one of the predefined graphics styles. If so, 
     *  its name will be selected in the graphics style selector.
     */
    public void checkStyles() {
        int i = 0;
        boolean found = false;
        while(!found && i < stylesCombo.getItemCount()) {
            GraphicsStyle aStyle = (GraphicsStyle) stylesCombo.getItemAt(i);
            if(aStyle.free.equals(freeBlock) && aStyle.solid.equals(solidBlock) &&
                    aStyle.blastable.equals(blastBlock) && aStyle.voidBlock.equals(voidBlock)
                    && aStyle.extraBlock.equals(extraBlock)) {
                found = true;
                if(stylesCombo.getSelectedIndex() != i) {
                    stylesCombo.setSelectedIndex(i);
                }
            }
            i++;
        }
        if(!found) {
            GraphicsStyle customStyle = (GraphicsStyle) stylesCombo.getItemAt(i - 1);
            customStyle.free = freeBlock;
            customStyle.solid = solidBlock;
            customStyle.blastable = blastBlock;
            customStyle.voidBlock = voidBlock;
            customStyle.extraBlock = extraBlock;
            stylesCombo.setSelectedIndex(i - 1);
        }
    }
}
