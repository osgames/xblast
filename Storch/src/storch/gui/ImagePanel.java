/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

import swingwt.awt.*;
import swingwtx.swing.*;

/**
 *  A panel displaying a specified image.
 *
 *  @author     Tobias Johansson
 */

class ImagePanel extends JPanel {
    private Image pic;

    /**
     *  Constructor for the <code>ImagePanel</code> object.
     *
     *	@param  pic  The image to show
     */
    public ImagePanel(Image pic) {
        super();
        setImage(pic);
    }


    /**
     *  Paints this panel.
     *
     *  @param  g  The <code>Graphics</code> object to paint on
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(pic, getSize().width / 2 - pic.getWidth(this) / 2, getSize().height / 2 - pic.getHeight(this) / 2, this);
    }


    /**
     *  Gets the preferred size of this <code>ColorPanel</code>.
     *
     *  @return    The preferred size
     */
    public Dimension getPreferredSize() {
        return getMinimumSize();
    }


    /**
     *  Gets the minimum size of this <code>ColorPanel</code>.
     *
     *  @return    The minimum size
     */
    public synchronized Dimension getMinimumSize() {
        return new Dimension(pic.getWidth(this), pic.getHeight(this));
    }


    /**
     *  Sets the image attribute of the ImagePanel object.
     *
     *  @param  i  The new image
     */
    public void setImage(Image i) {
        pic = i;
        repaint();
    }


    /**
     *  Flushes all resources being used by the <code>Image</code> object.
     */
    public void flush() {
        //pic.flush();
    }
}

