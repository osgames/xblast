/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.gui;

import swingwtx.swing.*;
import swingwt.awt.*;
import swingwt.awt.event.*;
import java.io.*;
import storch.*;
import storch.io.*;

/**
 * A dialog box for creating LevelDoc documentation for selected levels.
 *
 * @author   Tobias Johansson
 */
class LevelDocDialog extends JDialog implements ActionListener, KeyListener, ItemListener {

    private FileList levelList;
    private JButton addButton, removeButton;

    private JTextField archiveName, targetDir;
    private JButton browseButton;

    private JButton okButton, cancelButton;

    private JCheckBox useDefaultDir;

    private String lastLevelDir;
    private String lastDocDir;


    /**
     * Constructor for the <code>LevelDocDialog</code> object
     *
     * @param theWindow  The Storch main window
     * @param levelDir   The default path to the level files
     * @param docDir     The default path to the LevelDoc output directory
     */
    public LevelDocDialog(JFrame theWindow, String levelDir, String docDir) {
        super(theWindow, "LevelDoc", true);

        lastLevelDir = levelDir;
        lastDocDir = docDir;

        getContentPane().setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        JPanel levelListPanel = new JPanel();
        JPanel infoPanel = new JPanel();
        JPanel buttonPanel = new JPanel();

        levelListPanel.setLayout(new GridBagLayout());
        gbc.insets = new Insets(3, 3, 3, 3);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridwidth = 2;
        levelList = new FileList();
        JScrollPane listScrollPane = new JScrollPane(levelList);
        levelListPanel.add(listScrollPane, gbc);
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        addButton = new JButton("Add");
        levelListPanel.add(addButton, gbc);
        gbc.gridx = 1;
        removeButton = new JButton("Remove");
        levelListPanel.add(removeButton, gbc);
        levelListPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Levels"));

        infoPanel.setLayout(new GridBagLayout());
        gbc.gridx = 0;
        gbc.anchor = GridBagConstraints.EAST;
        infoPanel.add(new JLabel("Name of level archive:"), gbc);
        infoPanel.add(new JLabel("Target directory:"), gbc);
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        archiveName = new JTextField("MyLevels", 18);
        archiveName.setBackground(Color.white);
        infoPanel.add(archiveName, gbc);
        targetDir = new JTextField(new File(Settings.instance().getDocDir()).getAbsolutePath() + File.separator + archiveName.getText(), 18);
        targetDir.setBackground(Color.white);
        infoPanel.add(targetDir, gbc);
        useDefaultDir = new JCheckBox("Use default target directory", true);
        infoPanel.add(useDefaultDir, gbc);
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridx = 2;
        gbc.gridy = 1;
        browseButton = new JButton("Browse...");
        infoPanel.add(browseButton, gbc);
        infoPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Target LevelDoc archive"));

        okButton = new JButton("Generate LevelDoc");
        buttonPanel.add(okButton);
        cancelButton = new JButton("Cancel");
        buttonPanel.add(cancelButton);

        gbc.gridx = 0;
        gbc.gridy = GridBagConstraints.RELATIVE;
        gbc.gridheight = 2;
        getContentPane().add(levelListPanel, gbc);
        gbc.gridx = 1;
        gbc.gridheight = 1;
        getContentPane().add(infoPanel, gbc);

        getContentPane().add(buttonPanel, gbc);

        okButton.addActionListener(this);
        cancelButton.addActionListener(this);
        addButton.addActionListener(this);
        removeButton.addActionListener(this);
        browseButton.addActionListener(this);

        archiveName.addKeyListener(this);

        useDefaultDir.addItemListener(this);

        targetDir.setEnabled(false);
        browseButton.setEnabled(false);

        levelList.setToolTipText("List of level files to include in LevelDoc archive");
        addButton.setToolTipText("Add level files to list");
        removeButton.setToolTipText("Remove level files from list");
        browseButton.setToolTipText("Browse your system to locate a target directory for the LevelDoc archive");
        okButton.setToolTipText("Generate LevelDoc in the specified target directory for the selected levels ");
        cancelButton.setToolTipText("Return to main window without generating LevelDoc");
        archiveName.setToolTipText("The name to use for the generated archive");
        targetDir.setToolTipText("The target directory for the LevelDoc archive");

        pack();
    }


    /**
     * Called when the "Use default target directory" checkbox is changed.
     * Enables/disables automatic choosing of target directory.
     *
     * @param e  The corresponding <code>ItemEvent</code>
     */
    public void itemStateChanged(ItemEvent e) {
        boolean state = useDefaultDir.isSelected();
        targetDir.setEnabled(!state);
        browseButton.setEnabled(!state);
        if(state)
            targetDir.setText(new File(Settings.instance().getDocDir()).getAbsolutePath() + File.separator + archiveName.getText());
    }


    /**
     * Empty method, only needed to implement the <code>KeyListener</code>
     * interface.
     *
     * @param e  A <code>KeyEvent</code>
     */
    public void keyPressed(KeyEvent e) { }


    /**
     * Called when the user has hit and released a key in the archive name text
     * field. If default target directory is chosen, the directory name is
     * updated.
     *
     * @param e  The corresponding <code>KeyEvent</code>
     */
    public void keyReleased(KeyEvent e) {
        if(useDefaultDir.isSelected())
            targetDir.setText(new File(Settings.instance().getDocDir()).getAbsolutePath() + File.separator + archiveName.getText());
    }


    /**
     * Empty method, only needed to implement the <code>KeyListener</code>
     * interface.
     *
     * @param e  A <code>KeyEvent</code>
     */
    public void keyTyped(KeyEvent e) { }



    /**
     * Called when the OK-button is pressed. Closes the dialog box.
     *
     * @param e  An <code>ActionEvent</code> created when the OK button is
     *      pressed
     */
    public void actionPerformed(ActionEvent e) {
        JComponent source = (JComponent) e.getSource();
        if(source == okButton) {
            File theFiles[] = levelList.getFiles();
            String target = targetDir.getText().trim();
            String name = archiveName.getText().trim();
            if(theFiles.length == 0) {
                JOptionPane.showMessageDialog(this, "No levels selected for LevelDoc generation!", "Cannot generate LevelDoc", JOptionPane.ERROR_MESSAGE);
            }
            else if(target.equals("")) {
                JOptionPane.showMessageDialog(this, "No target directory selected!", "Cannot generate LevelDoc", JOptionPane.ERROR_MESSAGE);
            }
            else if(name.equals("")) {
                JOptionPane.showMessageDialog(this, "Enter a name for the LevelDoc archive before generating!", "Cannot generate LevelDoc", JOptionPane.ERROR_MESSAGE);
            }
            else {
                new LevelDocGeneratorThread(this, name, theFiles, target);
            }
        }
        else if(source == cancelButton) {
            dispose();
        }
        else if(source == addButton) {
            addFiles();
        }
        else if(source == removeButton) {
            levelList.removeSelected();
        }
        else if(source == browseButton) {
            browseDir();
        }
    }


    /**
     * Opens a <code>FileChooser</code> to let the user add files to the list.
     */
    public void addFiles() {
        JFileChooser chooser = new JFileChooser(lastLevelDir);
        chooser.setDialogTitle("LevelDoc - Select level files");
        chooser.setMultiSelectionEnabled(true);
        chooser.addChoosableFileFilter(new StandardFileFilter());
        chooser.addChoosableFileFilter(new TNTFileFilter());
        chooser.setFileFilter(new LevelFileFilter());

        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            File[] theFiles = chooser.getSelectedFiles();
            levelList.addFiles(theFiles);
            lastLevelDir = chooser.getCurrentDirectory().toString();
        }
    }


    /**
     * Opens a <code>FileChooser</code> to let the user select output directory.
     */
    public void browseDir() {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("LevelDoc - Select LevelDoc directory");
        chooser.setSelectedFile(new File(lastDocDir));
        chooser.setMultiSelectionEnabled(false);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            String dest = chooser.getSelectedFile().toString();
            lastDocDir = dest;
            targetDir.setText(dest);
        }
    }
    
    
    /**
     * Shows this dialog and sets the correct default directory.
     */
    public void show() {
    	targetDir.setText(new File(Settings.instance().getDocDir()).getAbsolutePath() + File.separator + archiveName.getText());  
    	super.show();
    }
      
    


    /**
     * A thread that runs the LevelDoc generation.
     *
     * @author   Tobias Johansson
     */
    class LevelDocGeneratorThread extends Thread {

        private JDialog dialog;
        private String archiveName;
        private File[] files;
        private String target;
        JDialog progressDialog;
        JLabel currentFile;
        JProgressBar progress;


        /**
         * Creates and starts a <code>LevelDocGeneratorThread</code>.
         *
         * @param dialog       The LevelDoc dialog box
         * @param archiveName  The name of the LevelDoc archive
         * @param files        The files to include in the LevelDoc archive
         * @param target       The destination directory
         */
        public LevelDocGeneratorThread(JDialog dialog, String archiveName, File[] files, String target) {
            this.dialog = dialog;
            this.archiveName = archiveName;
            this.files = files;
            this.target = target;
            JPanel generatePanel = new JPanel();
            generatePanel.setLayout(new GridLayout(3, 1));
            generatePanel.add(new JLabel("Generating LevelDoc..."));
            currentFile = new JLabel();
            generatePanel.add(currentFile);
            progress = new JProgressBar(0, files.length);
            generatePanel.add(progress);
            JOptionPane thePane = (JOptionPane)new JPanel();
	    //  progressDialog = thePane.createDialog(dialog, "LevelDoc");
	    // start();
	    // progressDialog.show();
        }


        /**
         * Generates the LevelDoc.
         */
        public void run() {
            try {
                synchronized(this) {
                    wait(100);
                }
                LevelDoc archive = new LevelDoc(archiveName, target);
                int fileCount = 0;
                for(int i = 0; i < files.length; i++) {
                    currentFile.setText(files[i].getName());
                    progress.setValue(i);
                    try {
                        archive.docLevel(files[i]);
                        fileCount++;
                    } catch(Exception e) {
                        JOptionPane.showMessageDialog(progressDialog, e.getMessage(), "LevelDoc", JOptionPane.ERROR_MESSAGE);
                    }
                }
                if(fileCount > 0) {
                    progress.setValue(files.length);
                    archive.printIndex();
                    synchronized(this) {
                        wait(100);
                    }
                    progressDialog.dispose();
                    JOptionPane.showMessageDialog(dialog, "HTML documentation generated in " + target, "LevelDoc", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch(InterruptedException ie) {} catch(Exception e) {
                progressDialog.dispose();
                JOptionPane.showMessageDialog(dialog, e.getMessage(), "LevelDoc", JOptionPane.ERROR_MESSAGE);
            }
            dialog.dispose();
        }
    }
}
