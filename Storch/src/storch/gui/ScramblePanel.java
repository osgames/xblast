/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.gui;

import swingwtx.swing.*;
import swingwtx.swing.event.*;
import swingwt.awt.*;
import storch.*;

/**
 *  A panel for setting scramble draw and delete options.
 *
 *  @author     Tobias Johansson
 */
class ScramblePanel extends JPanel implements ChangeListener {

    private JSlider scrambleDrawTime, scrambleDeleteTime;
    private JLabel drawTimeLabel, deleteTimeLabel;
    private int[][] scrambleDrawMap, scrambleDelMap;
    private final int DEFAULT_SCRAMBLE_DRAW_TIME = 33, DEFAULT_SCRAMBLE_DELETE_TIME = 67;


    /**
     *  Constructor for the <code>ScramblePanel</code> object.
     */
    public ScramblePanel() {
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(gridbag);
        
        gbc.gridx = 0;
        
        drawTimeLabel = new JLabel("", JLabel.CENTER);
		add(drawTimeLabel, gbc);
		
        scrambleDrawTime = new JSlider(JSlider.HORIZONTAL, 0, 100, DEFAULT_SCRAMBLE_DRAW_TIME);
		scrambleDrawTime.setMajorTickSpacing(50);
		scrambleDrawTime.setMinorTickSpacing(10);
		scrambleDrawTime.setPaintTicks(true);
		scrambleDrawTime.setPaintLabels(true);
		scrambleDrawTime.addChangeListener(this);
		add(scrambleDrawTime, gbc);
		
		add(new JLabel(" "), gbc);
		
		deleteTimeLabel = new JLabel("", JLabel.CENTER);
		add(deleteTimeLabel, gbc);
        scrambleDeleteTime = new JSlider(JSlider.HORIZONTAL, 0, 100, DEFAULT_SCRAMBLE_DELETE_TIME);
		scrambleDeleteTime.setMajorTickSpacing(50);
		scrambleDeleteTime.setMinorTickSpacing(10);
		scrambleDeleteTime.setPaintTicks(true);
		scrambleDeleteTime.setPaintLabels(true);
		scrambleDeleteTime.addChangeListener(this);
		add(scrambleDeleteTime, gbc);
        
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Scramble draw & delete"));

        scrambleDrawTime.setToolTipText("Time in % of game time when to draw scramble draw blocks");
        scrambleDeleteTime.setToolTipText("Time in % of game time when to delete scramble delete blocks");
        setDrawTime(DEFAULT_SCRAMBLE_DRAW_TIME);
        setDeleteTime(DEFAULT_SCRAMBLE_DELETE_TIME);

        load();
    }


	/**
     *  Called when one of the sliders in this panel is changed. Notifies the map for repaint.
     *
     *  @param	e	The corresponding <code>ChangeEvent</code> 
     */
	public void stateChanged(ChangeEvent e) {
		JSlider slider = (JSlider) e.getSource();
		if(slider == scrambleDrawTime)
			drawTimeLabel.setText("Scramble draw time: " + slider.getValue() + " %");
		else if(slider == scrambleDeleteTime)
			deleteTimeLabel.setText("Scramble delete time: " + slider.getValue() + " %");
		((MapPanel) getParent()).notifyScrambleChange();
	}


    /**
     *  Gets the chosen scramble draw time.
     *
     *  @return    The chosen scramble draw time
     */
    public int getDrawTime() {
        return scrambleDrawTime.getValue();
    }
    
    
    /**
     *  Sets the scramble draw time.
     *
     *  @param	time	The new scramble draw time
     */
    public void setDrawTime(int time){
    	scrambleDrawTime.setValue(time);
        drawTimeLabel.setText("Scramble draw time: " + time + " %");
    }


    /**
     *  Gets the chosen scramble delete time.
     *
     *  @return    The chosen scramble delete time
     */
    public int getDeleteTime() {
        return scrambleDeleteTime.getValue();
    }
    
    
    /**
     *  Sets the scramble delete time.
     *
     *  @param	time	The new scramble delete time
     */
    public void setDeleteTime(int time){
    	scrambleDeleteTime.setValue(time);
        deleteTimeLabel.setText("Scramble delete time: " + time + " %");
    }


    /**
     *  Returns a 15x13 map, showing the location of the scramble draw blocks.
     *
     *  @return    A 15x13 map, showing the location of the scramble draw blocks
     */
    public int[][] getDrawMap() {
        return scrambleDrawMap;
    }


    /**
     *  Returns a 15x13 map, showing the location of the scramble delete blocks.
     *
     *  @return    A 15x13 map, showing the location of the scramble delete blocks
     */
    public int[][] getDeleteMap() {
        return scrambleDelMap;
    }


    /**
     *  Loads the scramble draw and delete data from the current open level.
     */
    public void load() {
        Level theLevel = Storch.theLevel;

        scrambleDrawMap = new int[15][13];
        for(int i = 0; i < theLevel.scrambleDrawCount; i++) {
            int x = theLevel.scrambleDraw[i][0];
            int y = theLevel.scrambleDraw[i][1];
            scrambleDrawMap[x][y] = Block.SCRAMBLE_DRAW;
        }
        setDrawTime((int) (theLevel.scrambleDrawTime * 100));
        
        scrambleDelMap = new int[15][13];
        for(int i = 0; i < theLevel.scrambleDelCount; i++) {
            int x = theLevel.scrambleDel[i][0];
            int y = theLevel.scrambleDel[i][1];
            scrambleDelMap[x][y] = Block.SCRAMBLE_DELETE;
        }
        setDeleteTime((int) (theLevel.scrambleDelTime * 100));
    }


    /**
     *  Saves the scramble draw and delete data to the current open level.
     */
    public void save() {
        Level theLevel = Storch.theLevel;

        theLevel.scrambleDrawCount = 0;
        for(int x = 0; x < 15; x++) {
            for(int y = 0; y < 13; y++) {
                if(scrambleDrawMap[x][y] == Block.SCRAMBLE_DRAW) {
                    theLevel.scrambleDraw[theLevel.scrambleDrawCount][0] = x;
                    theLevel.scrambleDraw[theLevel.scrambleDrawCount][1] = y;
                    theLevel.scrambleDrawCount++;
                }
            }
        }
        theLevel.scrambleDrawTime = (double) scrambleDrawTime.getValue() / 100.0;

        theLevel.scrambleDelCount = 0;
        for(int x = 0; x < 15; x++) {
            for(int y = 0; y < 13; y++) {
                if(scrambleDelMap[x][y] == Block.SCRAMBLE_DELETE) {
                    theLevel.scrambleDel[theLevel.scrambleDelCount][0] = x;
                    theLevel.scrambleDel[theLevel.scrambleDelCount][1] = y;
                    theLevel.scrambleDelCount++;
                }
            }
        }
        theLevel.scrambleDelTime = (double) scrambleDeleteTime.getValue() / 100.0;
    }
}
