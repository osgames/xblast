/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.gui;

import java.io.File;
import swingwtx.swing.*;
import swingwtx.swing.filechooser.*;

/**
 *  This class is used to filter out the TNT level files in the swing <code>FileChooser</code>.
 *
 *  @author     Tobias Johansson
 */
public class TNTFileFilter extends LevelFileFilter {

    /**
     *  Checks if a specified <code>File</code> object will pass this filter.
     *
     *  @param  f  The file to check
     *  @return    true if the file will pass the filter, false otherwise.
     */
    public boolean accept(File f) {
        if(f.isDirectory()) {
            return true;
        }

        String extension = getExtension(f);
        if(extension != null) {
            if(extension.equals("xal")) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }


    /**
     *  Returns a description of this filter that will be shown in the <code>FileChooser</code>.
     *
     *  @return    A <code>String</code> description of this filter.
     */
    public String getDescription() {
        return "XBlast TNT levels (*.xal)";
    }
}

