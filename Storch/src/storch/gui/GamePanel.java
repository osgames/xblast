/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.gui;

import swingwtx.swing.*;
import swingwt.awt.*;

import storch.*;

/**
 * A panel for setting game options such as shrinks and extras.
 *
 * @author   Tobias Johansson
 */
class GamePanel extends JPanel {

    private JComboBox shrink, spExtra, distr, inExtra, inIll, revIll, inSpec, gameSpec;
    private JCheckBox inKick, revKick;
    private JRadioButton init, rev, both;
    private ComboBoxItem stop_epfl, el_epfl, frogger_epfl, phantom_epfl, ghost_epfl, through_epfl,
            phoenix_epfl, steal_epfl, swap_pos_epfl, swap_col_epfl, daleif_epfl, choice_epfl,
            evil_grail_epfl, jump_epfl, p_fart_epfl, pb_fart_epfl,snipe;
    private ComboBoxItem shrink_diag, shrink_outward_compound_extra, shrink_isty_spiral_3, shrink_isty_compound_2_f;
    private ComboBoxItem init_bombs_infinity, init_fire_walls, game_nasty_ceil;


    /**
     * Constructor for the <code>GamePanel</code> object.
     */
    public GamePanel() {
        super();

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(gridbag);

        gbc.gridx = 0;
        gbc.anchor = GridBagConstraints.EAST;
        gbc.fill = GridBagConstraints.NONE;

        //EPFL extras
        stop_epfl = new ComboBoxItem("Stop (EPFL)", Level.SPECIAL_EXTRA_STOP);
        el_epfl = new ComboBoxItem("Electrify (EPFL)", Level.SPECIAL_EXTRA_ELECTRIFY);
        frogger_epfl = new ComboBoxItem("Frogger (EPFL)", Level.SPECIAL_EXTRA_FROGGER);
        phantom_epfl = new ComboBoxItem("Phantom (EPFL)", Level.SPECIAL_EXTRA_PHANTOM);
        ghost_epfl = new ComboBoxItem("Ghost (EPFL)", Level.SPECIAL_EXTRA_GHOST);
        through_epfl = new ComboBoxItem("Through (EPFL)", Level.SPECIAL_EXTRA_THROUGH);
        phoenix_epfl = new ComboBoxItem("Phoenix (EPFL)", Level.SPECIAL_EXTRA_REVIVE);
        steal_epfl = new ComboBoxItem("Steal (EPFL)", Level.SPECIAL_EXTRA_STEAL);
        swap_pos_epfl = new ComboBoxItem("Swap position (EPFL)", Level.SPECIAL_EXTRA_SWAPPOSITION);
        swap_col_epfl = new ComboBoxItem("Swap color (EPFL)", Level.SPECIAL_EXTRA_SWAPCOLOR);
        daleif_epfl = new ComboBoxItem("Daleif (EPFL)", Level.SPECIAL_EXTRA_DALEIF);
        choice_epfl = new ComboBoxItem("Choice (EPFL)", Level.SPECIAL_EXTRA_CHOICE);
        evil_grail_epfl = new ComboBoxItem("Evil Grail (EPFL)", Level.SPECIAL_EXTRA_EVILGRAIL);
        jump_epfl = new ComboBoxItem("Jump (EPFL)", Level.SPECIAL_EXTRA_JUMP);
        p_fart_epfl = new ComboBoxItem("Player fart (EPFL)", Level.SPECIAL_EXTRA_PLAYERFART);
        pb_fart_epfl = new ComboBoxItem("Player and bomb fart (EPFL)", Level.SPECIAL_EXTRA_PLAYERANDBOMBFART);

        
        //Skywalker
        snipe = new ComboBoxItem("Snipe", Level.SPECIAL_EXTRA_SNIPE);
	//EPFL shrinks
	shrink_diag = new ComboBoxItem("Diagonal (EPFL)", Level.SHRINK_DIAG);
	shrink_outward_compound_extra = new ComboBoxItem("Outward compound shrink (EPFL)", Level.SHRINK_OUTWARD_COMPOUND_EXTRA);
	shrink_isty_spiral_3 = new ComboBoxItem("3 level spiral shrinking (EPFL)", Level.SHRINK_ISTY_SPIRAL_3);
	shrink_isty_compound_2_f = new ComboBoxItem("2 level compound ISTY shrinking (EPFL)", Level.SHRINK_ISTY_COMPOUND_2_F);

	//EPFL init and game
	init_bombs_infinity = new ComboBoxItem("255 Special bombs (EPFL)", Level.SPECIAL_INIT_SPECIAL_BOMBS_INFINITY);
	init_fire_walls = new ComboBoxItem("Fire walls (EPFL)", Level.SPECIAL_INIT_FIRE_WALLS);
	game_nasty_ceil = new ComboBoxItem("Nasty Ceiling (EPFL)", Level.SPECIAL_GAME_NASTY_CEIL);

        add(new JLabel("Shrink pattern:  "), gbc);
        add(new JLabel("Special extra:  "), gbc);
        add(new JLabel("Distribution:  "), gbc);
        add(new JLabel("Initial/revive extra:  "), gbc);
        add(new JLabel(""), gbc);
        add(new JLabel("Initial kick:  "), gbc);
        add(new JLabel("Revive kick:  "), gbc);
        add(new JLabel("Initial illness:  "), gbc);
        add(new JLabel("Revive illness:  "), gbc);
        add(new JLabel("Inital special:  "), gbc);
        add(new JLabel("Game special:  "), gbc);

        gbc.gridx = 1;
        gbc.gridwidth = 3;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;

        shrink = new JComboBox();
        shrink.addItem(new ComboBoxItem("No shrink", Level.SHRINK_VOID));
        shrink.addItem(new ComboBoxItem("Spiral", Level.SHRINK_SPIRAL));
        shrink.addItem(new ComboBoxItem("Speed spiral", Level.SHRINK_SPEED_SPIRAL));
        shrink.addItem(new ComboBoxItem("Spiral plus", Level.SHRINK_SPIRAL_PLUS));
        shrink.addItem(new ComboBoxItem("3 level spiral", Level.SHRINK_SPIRAL_3));
        shrink.addItem(new ComboBoxItem("2-3 level spiral", Level.SHRINK_SPIRAL_23));
        shrink.addItem(new ComboBoxItem("Spiral Lego shrink", Level.SHRINK_SPIRAL_LEGO));
        shrink.addItem(new ComboBoxItem("Shrink early spiral", Level.SHRINK_EARLY_SPIRAL));
        shrink.addItem(new ComboBoxItem("Continous compound", Level.SHRINK_COMPOUND));
        shrink.addItem(new ComboBoxItem("Fancy compound", Level.SHRINK_COMPOUND_F));
        shrink.addItem(new ComboBoxItem("2 level fancy compound", Level.SHRINK_COMPOUND_2_F));
        shrink.addItem(new ComboBoxItem("Lazy fancy compound", Level.SHRINK_LAZY_COMPOUND_F));
        shrink.addItem(new ComboBoxItem("Solid compound", Level.SHRINK_COMPOUND_SOLID));
        shrink.addItem(new ComboBoxItem("Savage compound", Level.SHRINK_SAVAGE_COMPOUND));
        shrink.addItem(new ComboBoxItem("Compound extra", Level.SHRINK_COMPOUND_EXTRA));
        shrink.addItem(new ComboBoxItem("Downward shrink", Level.SHRINK_DOWN));
        shrink.addItem(new ComboBoxItem("Fancy downward shrink", Level.SHRINK_DOWN_F));
        shrink.addItem(new ComboBoxItem("Quad shrink", Level.SHRINK_QUAD));
        shrink.addItem(new ComboBoxItem("Wave", Level.SHRINK_CONSTRICT_WAVE));
        shrink.addItem(new ComboBoxItem("Outward spiral", Level.SHRINK_OUTWARD_SPIRAL));
        add(shrink, gbc);

        spExtra = new JComboBox();
        spExtra.addItem(new ComboBoxItem("None", Level.SPECIAL_EXTRA_VOID));
        spExtra.addItem(new ComboBoxItem("Invincibility", Level.SPECIAL_EXTRA_INVINCIBLE));
        spExtra.addItem(new ComboBoxItem("Kick", Level.SPECIAL_EXTRA_KICK));
        spExtra.addItem(new ComboBoxItem("Teleporter", Level.SPECIAL_EXTRA_TELEPORT));
        spExtra.addItem(new ComboBoxItem("Remote control", Level.SPECIAL_EXTRA_RC));
        spExtra.addItem(new ComboBoxItem("Ignite", Level.SPECIAL_EXTRA_IGNITE_ALL));
        spExtra.addItem(new ComboBoxItem("Airpump", Level.SPECIAL_EXTRA_AIR));
        spExtra.addItem(new ComboBoxItem("Special bomb", Level.SPECIAL_EXTRA_SPECIAL_BOMB));
        spExtra.addItem(new ComboBoxItem("Cloak", Level.SPECIAL_EXTRA_CLOAK));
        spExtra.addItem(new ComboBoxItem("Junkie", Level.SPECIAL_EXTRA_JUNKIE));
        spExtra.addItem(new ComboBoxItem("Holy Grail", Level.SPECIAL_EXTRA_HOLY_GRAIL));
        spExtra.addItem(new ComboBoxItem("Extra life", Level.SPECIAL_EXTRA_LIFE));
        spExtra.addItem(new ComboBoxItem("Mayhem", Level.SPECIAL_EXTRA_MAYHEM));
        spExtra.addItem(new ComboBoxItem("Speed", Level.SPECIAL_EXTRA_SPEED));
        spExtra.addItem(new ComboBoxItem("Speed2", Level.SPECIAL_EXTRA_SPEED2));
        spExtra.addItem(new ComboBoxItem("Slow", Level.SPECIAL_EXTRA_SLOW));
        spExtra.addItem(new ComboBoxItem("Stun others", Level.SPECIAL_EXTRA_STUN_OTHERS));
        spExtra.addItem(new ComboBoxItem("Long stunned", Level.SPECIAL_EXTRA_LONG_STUNNED));
        spExtra.addItem(new ComboBoxItem("Poison", Level.SPECIAL_EXTRA_POISON));
        spExtra.addItem(new ComboBoxItem("Multiple", Level.SPECIAL_EXTRA_MULTIPLE));
        spExtra.addItem(new ComboBoxItem("Morph", Level.SPECIAL_EXTRA_MORPH));

        add(spExtra, gbc);

        distr = new JComboBox();
        distr.addItem(new ComboBoxItem("None", Level.DE_NONE));
        distr.addItem(new ComboBoxItem("1 distributed after death", Level.DE_SINGLE));
        distr.addItem(new ComboBoxItem("All distributed after death", Level.DE_ALL));
        distr.addItem(new ComboBoxItem("Reappears after pickup", Level.DE_GET));
        distr.addItem(new ComboBoxItem("2 appears after 1 pickup", Level.DE_DOUBLE));
        distr.addItem(new ComboBoxItem("1 appears after 3 pickups", Level.DE_SPECIAL));
        add(distr, gbc);

        inExtra = new JComboBox();
        inExtra.addItem(new ComboBoxItem("None", Level.SPECIAL_EXTRA_VOID));
        inExtra.addItem(new ComboBoxItem("Remote control", Level.SPECIAL_EXTRA_RC));
        inExtra.addItem(new ComboBoxItem("Teleporter", Level.SPECIAL_EXTRA_TELEPORT));
        inExtra.addItem(new ComboBoxItem("Airpump", Level.SPECIAL_EXTRA_AIR));
        inExtra.addItem(new ComboBoxItem("Cloak", Level.SPECIAL_EXTRA_CLOAK));
        inExtra.addItem(new ComboBoxItem("Morph", Level.SPECIAL_EXTRA_MORPH));

        add(inExtra, gbc);

        gbc.gridwidth = 1;
        ButtonGroup bGroup = new ButtonGroup();
        bGroup.add(init = new JRadioButton("Initial"));
        add(init, gbc);
        gbc.gridx = GridBagConstraints.RELATIVE;
        gbc.gridy = 4;
        bGroup.add(rev = new JRadioButton("Revive"));
        add(rev, gbc);
        bGroup.add(both = new JRadioButton("Both", true));
        add(both, gbc);

        gbc.gridwidth = 3;
        gbc.gridx = 1;
        gbc.gridy = GridBagConstraints.RELATIVE;
        add(inKick = new JCheckBox(), gbc);

        add(revKick = new JCheckBox(), gbc);

        inIll = new JComboBox();
        inIll.addItem(new ComboBoxItem("None (healthy)", Level.ILL_HEALTHY));
        inIll.addItem(new ComboBoxItem("Random bombing", Level.ILL_BOMB));
        inIll.addItem(new ComboBoxItem("Slow motion", Level.ILL_SLOW));
        inIll.addItem(new ComboBoxItem("Running", Level.ILL_RUN));
        inIll.addItem(new ComboBoxItem("Mini bombs", Level.ILL_MINI));
        inIll.addItem(new ComboBoxItem("Can't drop bombs", Level.ILL_EMPTY));
        inIll.addItem(new ComboBoxItem("Invisible", Level.ILL_INVISIBLE));
        inIll.addItem(new ComboBoxItem("No working bombs", Level.ILL_MALFUNCTION));
        inIll.addItem(new ComboBoxItem("Moonwalking", Level.ILL_REVERSE));        
        inIll.addItem(new ComboBoxItem("Crazywalking", Level.ILL_REVERSE2));
        inIll.addItem(new ComboBoxItem("Random teleporting", Level.ILL_TELEPORT));
        add(inIll, gbc);

        revIll = new JComboBox();
        revIll.addItem(new ComboBoxItem("None (healthy)", Level.ILL_HEALTHY));
        revIll.addItem(new ComboBoxItem("Random bombing", Level.ILL_BOMB));
        revIll.addItem(new ComboBoxItem("Slow motion", Level.ILL_SLOW));
        revIll.addItem(new ComboBoxItem("Running", Level.ILL_RUN));
        revIll.addItem(new ComboBoxItem("Mini bombs", Level.ILL_MINI));
        revIll.addItem(new ComboBoxItem("Can't drop bombs", Level.ILL_EMPTY));
        revIll.addItem(new ComboBoxItem("Invisible", Level.ILL_INVISIBLE));
        revIll.addItem(new ComboBoxItem("No working bombs", Level.ILL_MALFUNCTION));
        revIll.addItem(new ComboBoxItem("Moonwalking", Level.ILL_REVERSE));      
        revIll.addItem(new ComboBoxItem("Crazywalking", Level.ILL_REVERSE2));
        revIll.addItem(new ComboBoxItem("Random teleporting", Level.ILL_TELEPORT));
        add(revIll, gbc);

        inSpec = new JComboBox();
        inSpec.addItem(new ComboBoxItem("None", Level.SPECIAL_INIT_VOID));
        inSpec.addItem(new ComboBoxItem("12 special bombs", Level.SPECIAL_INIT_SPECIAL_BOMBS_12));
        inSpec.addItem(new ComboBoxItem("30 special bombs", Level.SPECIAL_INIT_SPECIAL_BOMBS_30));
        inSpec.addItem(new ComboBoxItem("Nasty walls", Level.SPECIAL_INIT_NASTY_WALLS));
        inSpec.addItem(new ComboBoxItem("Even nastier walls", Level.SPECIAL_INIT_NASTY_WALLS_2));
        add(inSpec, gbc);

        gameSpec = new JComboBox();
        gameSpec.addItem(new ComboBoxItem("None", Level.SPECIAL_GAME_VOID));
        gameSpec.addItem(new ComboBoxItem("Nasty walls", Level.SPECIAL_GAME_NASTY_WALLS));
        gameSpec.addItem(new ComboBoxItem("Haunted bombs", Level.SPECIAL_GAME_HAUNT));
        gameSpec.addItem(new ComboBoxItem("Fast haunted bombs", Level.SPECIAL_GAME_HAUNT_FAST));
        add(gameSpec, gbc);

        shrink.setToolTipText("Select how the level will shrink");
        spExtra.setToolTipText("Select a special extra that can be picked up during play");
        distr.setToolTipText("Select how the picked up special extras will reappear");
        inExtra.setToolTipText("Select an inital or revive extra");
        inIll.setToolTipText("Select an initial illness (lost after death)");
        revIll.setToolTipText("Select a revive illness (received after death)");
        inSpec.setToolTipText("Select an initial game special");
        gameSpec.setToolTipText("Select a repeating game special");
        inKick.setToolTipText("Initial kick (lost after death)");
        revKick.setToolTipText("Revive kick (received after death)");
        init.setToolTipText("The extra is initial only (lost after death)");
        rev.setToolTipText("The extra is revive only (received after death)");
        both.setToolTipText("The extra is initial and revive (kept after death)");

        setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Game options"));

        if(Settings.instance().getEPFLSupport())
            enableEPFL();

        load();
    }


    /**
     * Enables the EPFL support
     */
    public void enableEPFL() {
        spExtra.addItem(stop_epfl);
        spExtra.addItem(el_epfl);
        spExtra.addItem(frogger_epfl);
        spExtra.addItem(snipe);
        spExtra.addItem(phantom_epfl);
        spExtra.addItem(ghost_epfl);
        spExtra.addItem(through_epfl);
        spExtra.addItem(phoenix_epfl);
        spExtra.addItem(steal_epfl);
        spExtra.addItem(swap_pos_epfl);
        spExtra.addItem(swap_col_epfl);
        spExtra.addItem(daleif_epfl);
        spExtra.addItem(choice_epfl);
        spExtra.addItem(evil_grail_epfl);
        spExtra.addItem(jump_epfl);
        spExtra.addItem(p_fart_epfl);
        spExtra.addItem(pb_fart_epfl);

        inExtra.addItem(stop_epfl);
        inExtra.addItem(el_epfl);
        inExtra.addItem(frogger_epfl);
        inExtra.addItem(snipe);
        inExtra.addItem(through_epfl);
        inExtra.addItem(daleif_epfl);
        inExtra.addItem(choice_epfl);
       // inExtra.addItem(jump_epfl);
        inExtra.addItem(p_fart_epfl);
        inExtra.addItem(pb_fart_epfl);

	shrink.addItem(shrink_diag);
	shrink.addItem(shrink_outward_compound_extra);
	shrink.addItem(shrink_isty_spiral_3);
	shrink.addItem(shrink_isty_compound_2_f);

	inSpec.addItem(init_bombs_infinity);
	inSpec.addItem(init_fire_walls);

	gameSpec.addItem(game_nasty_ceil);
    }


    /**
     * Disables the EPFL support
     */
    public void disableEPFL() {
        spExtra.removeItem(stop_epfl);
        spExtra.removeItem(el_epfl);
        spExtra.removeItem(frogger_epfl);
        spExtra.removeItem(snipe);
        spExtra.removeItem(phantom_epfl);
        spExtra.removeItem(ghost_epfl);
        spExtra.removeItem(through_epfl);
        spExtra.removeItem(phoenix_epfl);
        spExtra.removeItem(steal_epfl);
        spExtra.removeItem(swap_pos_epfl);
        spExtra.removeItem(swap_col_epfl);
        spExtra.removeItem(daleif_epfl);
        spExtra.removeItem(choice_epfl);
        spExtra.removeItem(evil_grail_epfl);
        spExtra.removeItem(jump_epfl);
        spExtra.removeItem(p_fart_epfl);
        spExtra.removeItem(pb_fart_epfl);

        inExtra.removeItem(stop_epfl);
        inExtra.removeItem(el_epfl);
        inExtra.removeItem(frogger_epfl);
        inExtra.removeItem(snipe);
        inExtra.removeItem(through_epfl);
        inExtra.removeItem(daleif_epfl);
        inExtra.removeItem(choice_epfl);
      //  inExtra.removeItem(jump_epfl);
        inExtra.removeItem(p_fart_epfl);
        inExtra.removeItem(pb_fart_epfl);

	shrink.removeItem(shrink_diag);
	shrink.removeItem(shrink_outward_compound_extra);
	shrink.removeItem(shrink_isty_spiral_3);
	shrink.removeItem(shrink_isty_compound_2_f);

	inSpec.removeItem(init_bombs_infinity);
	inSpec.removeItem(init_fire_walls);

	gameSpec.removeItem(game_nasty_ceil);
    }


    /**
     * Loads the game data from the current open level.
     */
    public void load() {
        Level theLevel = Storch.theLevel;

        GUI.setValue(shrink, theLevel.shrinkPattern);
        GUI.setValue(spExtra, theLevel.specialExtra);
        GUI.setValue(distr, theLevel.distribution);
        GUI.setValue(inExtra, theLevel.inExtra);
        init.setSelected(true);
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_VOID) {
            GUI.setValue(inExtra, theLevel.revExtra);
            rev.setSelected(true);
        }
        if(theLevel.inExtra == theLevel.revExtra) {
            both.setSelected(true);
        }
        GUI.setValue(inIll, theLevel.inIllness);
        GUI.setValue(revIll, theLevel.revIllness);
        GUI.setValue(inSpec, theLevel.inGameSpecial);
        GUI.setValue(gameSpec, theLevel.turnGameSpecial);
        inKick.setSelected(theLevel.inKick);
        revKick.setSelected(theLevel.revKick);
    }


    /**
     * Saves the game data to the current open level.
     */
    public void save() {
        Level theLevel = Storch.theLevel;

        theLevel.shrinkPattern = ((ComboBoxItem) shrink.getSelectedItem()).value;
        theLevel.specialExtra = ((ComboBoxItem) spExtra.getSelectedItem()).value;
        theLevel.distribution = ((ComboBoxItem) distr.getSelectedItem()).value;
        if(init.isSelected()) {
            theLevel.inExtra = ((ComboBoxItem) inExtra.getSelectedItem()).value;
            theLevel.revExtra = Level.SPECIAL_EXTRA_VOID;
        }
        else if(rev.isSelected()) {
            theLevel.revExtra = ((ComboBoxItem) inExtra.getSelectedItem()).value;
            theLevel.inExtra = Level.SPECIAL_EXTRA_VOID;
        }
        else {
            theLevel.inExtra = ((ComboBoxItem) inExtra.getSelectedItem()).value;
            theLevel.revExtra = ((ComboBoxItem) inExtra.getSelectedItem()).value;
        }
        theLevel.inIllness = ((ComboBoxItem) inIll.getSelectedItem()).value;
        theLevel.revIllness = ((ComboBoxItem) revIll.getSelectedItem()).value;
        theLevel.inGameSpecial = ((ComboBoxItem) inSpec.getSelectedItem()).value;
        theLevel.turnGameSpecial = ((ComboBoxItem) gameSpec.getSelectedItem()).value;
        theLevel.inKick = inKick.isSelected();
        theLevel.revKick = revKick.isSelected();
    }
}
