/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.gui;

import swingwt.awt.*;
import swingwtx.swing.*;
import swingwtx.swing.event.*;


/**
 *  A panel for setting the level time to show in the design map.
 *
 *  @author     Tobias Johansson
 */
class TimeSliderPanel extends JPanel implements ChangeListener {
	
	private JSlider slider;
	private JLabel timeLabel;
	
	/**
     *  Constructor for the <code>TimeSliderPanel</code> object.
     */
	public TimeSliderPanel(){
		GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        setLayout(gridbag);
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.CENTER;
					
		timeLabel = new JLabel();
		add(timeLabel, gbc);
		
		gbc.gridx = 0;
		slider = new JSlider(JSlider.HORIZONTAL, 0, 100, 0);
		slider.setMajorTickSpacing(50);
		slider.setMinorTickSpacing(10);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.addChangeListener(this);

		add(slider, gbc);
		
		setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Game time"));
		slider.setToolTipText("Time, in % of game time, to show in map");
		setTime(0);
	}
	
	/**
     *  Called when the slider in this panel is changed. Notifies the map for repaint.
     *
     *  @param	e	The corresponding <code>ChangeEvent</code> 
     */
	public void stateChanged(ChangeEvent e) {
		timeLabel.setText("Show map at: " + slider.getValue() + " %");
		((MapPanel) getParent()).notifyTimeChange();
	}
	
	
	/**
     *  Gets the chosen time.
     *
     *  @return    The chosen time to show
     */
	public int getTime(){
		return slider.getValue();
	}
	
	
	/**
     *  Sets the time to show.
     *
     *  @param	time	The new time to show
     */
	public void setTime(int time){
		slider.setValue(time);
		timeLabel.setText("Show map at: " + time + " %");
	}

}
