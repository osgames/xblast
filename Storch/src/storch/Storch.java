/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch;

import storch.gui.*;
import storch.io.*;
import java.lang.*;
import java.io.File;

/**
 * This is the main class of Storch
 *
 * @author   Tobias Johansson
 * @author   Tor Andr�
 */
public class Storch {
    public final static String VERSION = "2.23";
    public final static String IMAGE_PATH = "/images/";
    public final static String DATA_PATH = "/data/";

    public static Level theLevel = new Level();


    /**
     * The main program for the Storch class
     *
     * @param args  The command line arguments
     */
    public static void main(String[] args) {
        // java -jar Storch.jar [-d|-doc [archiveName] levelFiles [targetDir]]
        // java storch.Storch [-d|-doc [archiveName] levelFiles [targetDir]]
        int size = args.length;
        for (int i=0;i<size;i++){
System.out.println(args[i]+"\n");
        }
        if(size == 0) {
            new GUI("Storch " + Storch.VERSION);
        }
        else  if(size > 2 && (args[0].equals("-d") || args[0].equals("-doc"))) {
            boolean error = false;
            LevelDoc doc;
            File[] files;
            String targetDir;
            int firstIndex = 2;
            int lastIndex;
            String archiveName = args[1];
            
            System.out.println(archiveName);
           
            if((new File(archiveName)).isFile()) {
            	archiveName = "MyLevels";
            	firstIndex = 1;
            }
            File f = new File(args[size - 1]);
            if(!f.isFile()) {
                targetDir = f.getAbsolutePath();
                files = new File[size - firstIndex - 1];
                lastIndex = size - 1;
            }
            else {
                targetDir = new File(Settings.instance().getDocDir() + File.separator + archiveName).getAbsolutePath();
                files = new File[size - firstIndex];
                lastIndex = size;
            }
            for(int i = firstIndex; i < lastIndex; i++) {
                File theFile = new File(args[i]);
                if(theFile.isFile()) {
                    files[i - firstIndex] = theFile;
                }
                else {
                    System.out.println("Unable to open " + args[i]);
                    error = true;
                }
            }
            if(files.length < 1) {
                System.out.println("No files to LevelDoc");
                error = true;
            }
            if(!error) {
                try {
                    doc = new LevelDoc(archiveName, targetDir);
                    for(int i = 0; i < files.length; i++)
                        doc.docLevel(files[i]);
                    doc.printIndex();
                    System.out.println("LevelDoc archive '" + archiveName + "'");
                    System.out.print("containing ");
                    if(files.length == 1)
                        System.out.println("1 level");
                    else
                        System.out.println(files.length + " levels");
                    System.out.println("created in " + targetDir);
                    System.exit(0);
                } catch(Exception e) {
                    System.out.println(e);
                }
            }
            else {
                printUsage();
            }
        }
        else if(size>1&&(args[0].equals("-conv"))){
            
        
        String fromFormat;
        String toFormat;
        LevelFileFilter theFilter;
        Converter conv;
            String archiveName = args[1];
            File theFiles=new File(archiveName);
            
           
              theFilter = new StandardFileFilter();
            conv = new Std2TNT();
            System.out.println("converting file");
             try {
                     conv.convert(theFiles);
              System.out.println("file converted");
             }
             catch(Exception e) {
                  System.out.println("Error ");
                  e.printStackTrace();
                                    }
        }
        else{
            printUsage();
        }
    }


    /**
     * Prints usage help on screen.
     */
    private static void printUsage() {
        System.out.println("Usage: java -jar Storch.jar [-d|-doc [archiveName} levelFiles [targetDir]]");
        System.out.println("   or: java storch.Storch [-d|-doc [archiveName] levelFiles [targetDir]]");
    }
}
