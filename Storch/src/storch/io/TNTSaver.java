/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.io;

import storch.Level;
import storch.ColorTable;
import java.io.*;
import swingwt.awt.Color;

/**
 * This class is used to save a level in the TNT format.
 *
 * @author   Tor Andr�
 */
public class TNTSaver implements Saver {
    private Level theLevel;
    private Level2TNT converter;


    /**
     * Saves the level to file in the TNT format.
     *
     * @param saveFile           The destination for the save.
     * @param inLevel            The <code>Level</code> you want to save.
     * @exception SaveException  If unable to save the level.
     */
    public void save(File saveFile, Level inLevel) throws SaveException {
        PrintWriter out = null;
        theLevel = inLevel;

     /*   if(theLevel.isEPFL())
            throw new SaveException("Level uses EPFL features and can not be saved in TNT format");
*/
        converter = new Level2TNT(theLevel);
        String levelName = theLevel.getFileName();
 System.out.println("saving to "+levelName);
        try {
            out = new PrintWriter(new BufferedWriter(new FileWriter(saveFile)));
        } catch(IOException e) {
            throw new SaveException("Unable to create outstream for " + saveFile.getName());
        }

        try {
            out.println("; Level created by Storch " + storch.Storch.VERSION);
            out.println("; Written by Tobias Johansson (d97tj@efd.lth.se)");
            out.println("; Co-writer Tor Andr� (d97tan@efd.lth.se)");
            out.println("; http://storch.sourceforge.net/");
            out.println("");
            out.println("; XBlast TNT level");
            out.println("");

            out.println("[info]");
            out.println("name=" + theLevel.title);
            out.println("author=" + theLevel.author);
            out.println("hint=" + theLevel.description);
            out.println("gameMode=" + converter.getGameMode());
            out.println("");

            out.println("[shrink]");
            if(theLevel.shrinkPattern != Level.SHRINK_VOID)
                out.println("type=" + converter.getShrink());
            out.println("");

            if(theLevel.scrambleDrawCount > 0) {
                out.println("[scrambleDraw]");
                out.println("time=" + theLevel.scrambleDrawTime);
                out.println("numBlocks=" + theLevel.scrambleDrawCount);
                String pos;
                for(int i = 0; i < theLevel.scrambleDrawCount; i++) {
                    if(i < 10)
                        pos = "pos00" + i;
                    else if(i < 100)
                        pos = "pos0" + i;
                    else
                        pos = "pos" + i;

                    out.println(pos + "=" + theLevel.scrambleDraw[i][0] + " " + theLevel.scrambleDraw[i][1]);
                }
            }
            out.println("");

            if(theLevel.scrambleDelCount > 0) {
                out.println("[scrambleDel]");
                out.println("time=" + theLevel.scrambleDelTime);
                out.println("numBlocks=" + theLevel.scrambleDelCount);
                String pos;
                for(int i = 0; i < theLevel.scrambleDelCount; i++) {
                    if(i < 10)
                        pos = "pos00" + i;
                    else if(i < 100)
                        pos = "pos0" + i;
                    else
                        pos = "pos" + i;

                    out.println(pos + "=" + theLevel.scrambleDel[i][0] + " " + theLevel.scrambleDel[i][1]);
                }
            }
            out.println("");

            out.println("[func]");
            String[] extra = converter.getSpecialExtra();
            if(extra[converter.NAME] != null)
                out.println("extra=" + extra[converter.NAME]);
            if(extra[converter.SKEY] != null){
                out.println("key=" + extra[converter.SKEY]);
            }
            out.println("");

            out.println("[player]");
            out.println("bombs=" + theLevel.bombsAtStart);
            out.println("range=" + theLevel.range);
            int nbrOfSpecial = converter.getSpecialBombs();
                System.out.println("specialBombs=" + nbrOfSpecial);
            if(nbrOfSpecial > 0)
                out.println("specialBombs=" + nbrOfSpecial);
            for(int i = 1; i <= 6; i++)
                out.println("pos" + i + "=" + theLevel.startPos[i - 1][0] + " " + theLevel.startPos[i - 1][1]);
            String inVirus = converter.getInitialIll();
            if(inVirus != null && !inVirus.equals("healthy"))
                out.println("initVirus=" + inVirus);
            String revVirus = converter.getRevIll();
            if(revVirus != null && !revVirus.equals("healthy"))
                out.println("reviveVirus=" + revVirus);
            String inExtra = converter.getInitialExtra();
            if(inExtra != null)
                out.println("initExtra=" + inExtra);
            String revExtra = converter.getRevExtra();
            if(revExtra != null)
                out.println("reviveExtra=" + revExtra);
            out.println("");

            out.println("[bombs]");
            if(theLevel.bombClick != Level.BOMB_CLICK_NONE)
                out.println("bombClick=" + converter.getBombClick(theLevel.bombClick));
            if(theLevel.wallClick != Level.BOMB_CLICK_NONE)
                out.println("wallClick=" + converter.getBombClick(theLevel.wallClick));
            if(theLevel.playerClick != Level.BOMB_CLICK_NONE)
                out.println("playerClick=" + converter.getBombClick(theLevel.playerClick));
            if(theLevel.direction != Level.GO_STOP)
                out.println("direction=" + converter.getDir());
	    if(theLevel.fuseTime != Level.FUSE_NORMAL)
                out.println("fuseTime=" + converter.getFuseTime());
	    if(theLevel.defaultType != Level.BOMB_NORMAL)
                out.println("default=" + converter.getBombType(theLevel.defaultType));
            if(theLevel.specialType != Level.BOMB_NORMAL)
                out.println("special=" + converter.getBombType(theLevel.specialType));
            if(theLevel.hiddenType != Level.BOMB_NORMAL)
                out.println("hidden=" + converter.getBombType(theLevel.hiddenType));
            String haunt;
            if((haunt = converter.getHauntSpeed()) != null)
                out.println("haunt=" + haunt);

            String nasty;
            if((nasty = converter.getNasty()) != null)
                out.println(nasty);

            out.println("");

            out.println("[graphics]");
            out.println("block00=" + theLevel.freeBlock.getFileName() + "\t\t" + converter.getHex(theLevel.freeBlock.getColor0()) + " " + converter.getHex(theLevel.freeBlock.getColor1()) + " " + converter.getHex(theLevel.freeBlock.getColor2()));
            out.println("block01=" + theLevel.blastedFloor.getFileName() + "\t\t" + converter.getHex(theLevel.blastedFloor.getColor0()) + " " + converter.getHex(theLevel.blastedFloor.getColor1()) + " " + converter.getHex(theLevel.blastedFloor.getColor2()));
            out.println("block02=" + theLevel.solidWall.getFileName() + "\t\t" + converter.getHex(theLevel.solidWall.getColor0()) + " " + converter.getHex(theLevel.solidWall.getColor1()) + " " + converter.getHex(theLevel.solidWall.getColor2()));
            out.println("block03=" + theLevel.risingWall.getFileName() + "\t\t" + converter.getHex(theLevel.risingWall.getColor0()) + " " + converter.getHex(theLevel.risingWall.getColor1()) + " " + converter.getHex(theLevel.risingWall.getColor2()));
            out.println("block04=" + theLevel.blastableBlock.getFileName() + "\t\t" + converter.getHex(theLevel.blastableBlock.getColor0()) + " " + converter.getHex(theLevel.blastableBlock.getColor1()) + " " + converter.getHex(theLevel.blastableBlock.getColor2()));
            out.println("block05=" + theLevel.blastedBlock.getFileName() + "\t\t" + converter.getHex(theLevel.blastedBlock.getColor0()) + " " + converter.getHex(theLevel.blastedBlock.getColor1()) + " " + converter.getHex(theLevel.blastedBlock.getColor2()));
            out.println("block06=bomb");
            out.println("block07=range");
            out.println("block08=trap");
            String filename=theLevel.extraBlock.getFileName();
            if(filename.startsWith("extra_"))  {
                if(theLevel.COLORSEXTRA==1){
            out.println("block09=" + filename.substring("extra_".length(),filename.length() ) + "\t\t" + converter.getHex(theLevel.extraBlock.getColor0()) + " " + converter.getHex(theLevel.extraBlock.getColor1()) + " " + converter.getHex(theLevel.extraBlock.getColor2()));
                }
                else{
		    
                    out.println("block09=" + filename.substring("extra_".length(),filename.length() ));
                }
            }
            else{
                if(theLevel.COLORSEXTRA==1){
            out.println("block09=" + filename + "\t\t" + converter.getHex(theLevel.extraBlock.getColor0()) + " " + converter.getHex(theLevel.extraBlock.getColor1()) + " " + converter.getHex(theLevel.extraBlock.getColor2()));
            }
else{
    out.println("block09=" + filename );
                }
	    }
	
            
                if(theLevel.COLORSALL==1){
            out.println("block10=" + theLevel.voidBlock.getFileName() + "\t\t" + converter.getHex(theLevel.voidBlock.getColor0()) + " " + converter.getHex(theLevel.voidBlock.getColor1()) + " " + converter.getHex(theLevel.voidBlock.getColor2()));
                }
                else{
            out.println("block10=" + theLevel.voidBlock.getFileName());
                }
                    
            out.println("");

            out.flush();

            out.println("[map]");
            int[] prob = theLevel.getTNTProbs();
            if(theLevel.distribution != Level.DE_NONE)
                out.println("extraDistribution=" + converter.getDistribution());
            if(prob[Level.PROB_BOMB_EXTRA] != 0)
                out.println("probBomb=" + prob[Level.PROB_BOMB_EXTRA]);
            if(prob[Level.PROB_RANGE] != 0)
                out.println("probRange=" + prob[Level.PROB_RANGE]);
            if(prob[Level.PROB_ILL] != 0)
                out.println("probVirus=" + prob[Level.PROB_ILL]);
            if(prob[Level.PROB_SPECIAL_EXTRA] != 0)
                out.println("probSpecial=" + prob[Level.PROB_SPECIAL_EXTRA]);
            if(prob[Level.PROB_HIDDEN_BOMB] != 0)
                out.println("probHidden=" + prob[Level.PROB_HIDDEN_BOMB]);

            String row = "";
            String s1 = "";

            for(int y = 0; y < 13; y++) {
                if(y < 10)
                    row = "row0" + y;
                else
                    row = "row" + y;
                out.print(row + "=");
                for(int x = 0; x < 15; x++) {
                    switch (theLevel.map[x][y]) {
                     case Level.MAP_SOLID:
                         s1 = "B";
                         break;
                     case Level.MAP_BLASTABLE:
                         s1 = "X";
                         break;
                     case Level.MAP_FREE:
                         s1 = "_";
                         break;
                     case Level.MAP_EXPLODING_BOMB:
                         s1 = "e";
                         break;
                     case Level.MAP_BOMB_EXTRA:
                         s1 = "b";
                         break;
                     case Level.MAP_RANGE:
                         s1 = "r";
                         break;
                     case Level.MAP_ILLNESS:
                         s1 = "s";
                         break;
                     case Level.MAP_SPECIAL_EXTRA:
                         s1 = "q";
                         break;
                     case Level.MAP_VOID:
                         s1 = "v";
                         break;
                    }
                    out.print(s1);
                }
                out.print("\n");
            }
            out.close();
        } catch(Exception e) {
            throw new SaveException("An error occurred when saving " + inLevel.title);
        }
    }
}
