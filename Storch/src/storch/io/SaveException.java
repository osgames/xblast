/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package storch.io;

/**
 * Signals that some problem occurred while trying to save a Level.
 * 
 * @author Tor Andr�
 */
public class SaveException extends Exception{
	
	/**
	 *  Constructs a SaveException with null as its error detail message.
	 */
	public SaveException(){
		super();
	}
	
	/**
	 * Constructs a SaveException with the specified detail message
	 */
	public SaveException(String s){
		super(s);
	}
	
}
