/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.io;

import storch.Level;

/**
 * This class is used to convert data from the TNT format into the format used
 * internal in Storch.
 *
 * @author   Tor Andr�
 */
class TNT2Level {

    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param c  The TNT representation of the block
     * @return   The Storch representation of the block
     */
    protected int getMapInt(char c) {
        switch (c) {
         case '_':
             return Level.MAP_FREE;
         case 'R': //Rising wall
         case 'B':
             return Level.MAP_SOLID;
         case 'X':
             return Level.MAP_BLASTABLE;
         case 'b':
             return Level.MAP_BOMB_EXTRA;
         case 'r':
             return Level.MAP_RANGE;
         case 's':
             return Level.MAP_ILLNESS;
         case 'q':
             return Level.MAP_SPECIAL_EXTRA;
         case 'v':
             return Level.MAP_VOID;
         case 'e':
             return Level.MAP_EXPLODING_BOMB;
         case 'V':// #define V -1
             return Level.MAP_VOID;
        }
        return Level.MAP_FREE;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The TNT representation of this attribute
     * @return      The Storch representation
     */
    protected int getDistribution(String name) {

        if(name.equals("all"))
            return Level.DE_ALL;
        if(name.equals("double"))
            return Level.DE_DOUBLE;
        if(name.equals("get"))
            return Level.DE_GET;
        if(name.equals("single"))
            return Level.DE_SINGLE;
        if(name.equals("special"))
            return Level.DE_SPECIAL;

        return Level.DE_NONE;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The TNT representation of this attribute
     * @return      The Storch representation
     */
    protected int getBombClick(String name) {
        if(name.equals("anticlockwise"))
            return Level.BOMB_CLICK_ANTICLOCKWISE;
        if(name.equals("clockwise"))
            return Level.BOMB_CLICK_CLOCKWISE;
        if(name.equals("contact"))
            return Level.BOMB_CLICK_CONTACT;
        if(name.equals("initial"))
            return Level.BOMB_CLICK_INITIAL;
        if(name.equals("randomdir"))
            return Level.BOMB_CLICK_RANDOMDIR;
        if(name.equals("rebound"))
            return Level.BOMB_CLICK_REBOUND;
        if(name.equals("snooker"))
            return Level.BOMB_CLICK_SNOOKER;
        if(name.equals("thru"))
            return Level.BOMB_CLICK_THRU;

        return Level.BOMB_CLICK_NONE;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The TNT representation of this attribute
     * @return      The Storch representation
     */
    protected int getBombDir(String name) {
        if(name.equals("down"))
            return Level.GO_DOWN;
        if(name.equals("left"))
            return Level.GO_LEFT;
        if(name.equals("right"))
            return Level.GO_RIGHT;
        if(name.equals("up"))
            return Level.GO_UP;

        return Level.GO_STOP;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The TNT representation of this attribute
     * @return      The Storch representation
     */
    protected int getFuseTime(String name) {
        if(name.equals("long"))
            return Level.FUSE_LONG;
        if(name.equals("short"))
            return Level.FUSE_SHORT;
        return Level.FUSE_NORMAL;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The TNT representation of this attribute
     * @return      The Storch representation
     */
    protected int getBombType(String name) {
        if(name.equalsIgnoreCase("blastNow"))
            return Level.BOMB_BLASTNOW;
        if(name.equals("close"))
            return Level.BOMB_CLOSE;
        if(name.equals("construction"))
            return Level.BOMB_CONSTRUCTION;
        if(name.equals("destruction"))
            return Level.BOMB_DESTRUCTION;
        if(name.equals("firecracker"))
            return Level.BOMB_FIRECRACKER;
        if(name.equals("firecracker2"))
            return Level.BOMB_FIRECRACKER_2;
        if(name.equals("fungus"))
            return Level.BOMB_FUNGUS;
        if(name.equals("grenade"))
            return Level.BOMB_GRENADE;
        if(name.equals("napalm"))
            return Level.BOMB_NAPALM;
        if(name.equals("pyro"))
            return Level.BOMB_PYRO;
        if(name.equals("pyro2"))
            return Level.BOMB_PYRO_2;
        if(name.equals("random"))
            return Level.BOMB_RANDOM;
        if(name.equals("renovation"))
            return Level.BOMB_RENOVATION;
        if(name.equals("short"))
            return Level.BOMB_SHORT;
        if(name.equals("threebombs"))
            return Level.BOMB_THREEBOMBS;
        if(name.equals("trianglebombs"))
            return Level.BOMB_TRIANGLEBOMBS;

        if(name.equals("ringofire"))
            return Level.BOMB_RING_OF_FIRE;//Only in EPFL
        if(name.equals("mine"))
            return Level.BOMB_MINE;//Only in EPFL
        if(name.equals("diagthreebombs"))
            return Level.BOMB_DIAG_THREE_BOMBS;//Only in EPFL
        if(name.equals("scissor"))
            return Level.BOMB_SCISSOR;//Only in EPFL
        if(name.equals("scissor2"))
            return Level.BOMB_SCISSOR_2;//Only in EPFL
        if(name.equals("parallel"))
            return Level.BOMB_PARALLEL;//Only in EPFL
        if(name.equals("distance"))
            return Level.BOMB_DISTANCE;//Only in EPFL
        if(name.equals("lucky"))
            return Level.BOMB_LUCKY;//Only in EPFL
        if(name.equals("parasol"))
            return Level.BOMB_PARASOL;//Only in EPFL
        if(name.equals("comb"))
            return Level.BOMB_COMB;//Only in EPFL
        if(name.equals("farpyro"))
            return Level.BOMB_FARPYRO;//Only in EPFL
        if(name.equals("nuclear"))
            return Level.BOMB_NUCLEAR;//Only in EPFL
        if(name.equals("protectbombs"))
            return Level.BOMB_PROTECTBOMBS;//Only in EPFL
        if(name.equals("snipe"))
            return Level.BOMB_PROTECTBOMBS;//Skywalker

        return Level.BOMB_NORMAL;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The TNT representation of this attribute
     * @return      The Storch representation
     */
    protected int getIll(String name) {
        if(name.equals("bomb"))
            return Level.ILL_BOMB;
        if(name.equals("empty"))
            return Level.ILL_EMPTY;
        if(name.equals("invisible"))
            return Level.ILL_INVISIBLE;
        if(name.equals("malfunction"))
            return Level.ILL_MALFUNCTION;
        if(name.equals("mini"))
            return Level.ILL_MINI;
        if(name.equals("reverse"))
            return Level.ILL_REVERSE;
        if(name.equals("reverse2"))
            return Level.ILL_REVERSE2;
        if(name.equals("run"))
            return Level.ILL_RUN;
        if(name.equals("slow"))
            return Level.ILL_SLOW;
        if(name.equals("teleport"))
            return Level.ILL_TELEPORT;

        return Level.ILL_HEALTHY;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The TNT representation of this attribute
     * @return      The Storch representation
     */
    protected int getSpecialExtra(String name) {
        name = name.trim();
        if(name.equals("airpump") || name.equals("air"))
            return Level.SPECIAL_EXTRA_AIR;
        if(name.equals("cloak"))
            return Level.SPECIAL_EXTRA_CLOAK;
        if(name.equals("holyGrail"))
            return Level.SPECIAL_EXTRA_HOLY_GRAIL;
        if(name.equals("igniteAll"))
            return Level.SPECIAL_EXTRA_IGNITE_ALL;
        if(name.equals("invincible"))
            return Level.SPECIAL_EXTRA_INVINCIBLE;
        if(name.equals("junkie"))
            return Level.SPECIAL_EXTRA_JUNKIE;
        if(name.equals("kick"))
            return Level.SPECIAL_EXTRA_KICK;
        if(name.equals("life"))
            return Level.SPECIAL_EXTRA_LIFE;
        if(name.equals("longStunned"))
            return Level.SPECIAL_EXTRA_LONG_STUNNED;
        if(name.equals("mayhem"))
            return Level.SPECIAL_EXTRA_MAYHEM;
        if(name.equals("morph"))
            return Level.SPECIAL_EXTRA_MORPH;
        
        if(name.equals("stop"))//EPFL
            return Level.SPECIAL_EXTRA_STOP;
        if(name.equals("revive"))//EPFL
            return Level.SPECIAL_EXTRA_REVIVE;
        if(name.equals("electrify"))//EPFL
            return Level.SPECIAL_EXTRA_ELECTRIFY;
        if(name.equals("frogger"))//EPFL
            return Level.SPECIAL_EXTRA_FROGGER;
        if(name.equals("phantom"))//EPFL
            return Level.SPECIAL_EXTRA_PHANTOM;
        if(name.equals("ghost"))//EPFL
            return Level.SPECIAL_EXTRA_GHOST;
        if(name.equals("through"))//EPFL
            return Level.SPECIAL_EXTRA_THROUGH;
        if(name.equals("steal"))//EPFL
            return Level.SPECIAL_EXTRA_STEAL;
        if(name.equals("swapposition"))//EPFL
            return Level.SPECIAL_EXTRA_SWAPPOSITION;
        if(name.equals("swapcolor"))//EPFL
            return Level.SPECIAL_EXTRA_SWAPCOLOR;
        if(name.equals("daleif"))//EPFL
            return Level.SPECIAL_EXTRA_DALEIF;
        if(name.equals("choicebombtype"))//EPFL
            return Level.SPECIAL_EXTRA_CHOICE;
        if(name.equals("evilgrail"))//EPFL
            return Level.SPECIAL_EXTRA_EVILGRAIL;
        if(name.equals("jump"))//EPFL
            return Level.SPECIAL_EXTRA_JUMP;
        if(name.equals("farter"))//EPFL
            return Level.SPECIAL_EXTRA_PLAYERFART;
        if(name.equals("bfarter"))//EPFL
            return Level.SPECIAL_EXTRA_PLAYERANDBOMBFART;
        if(name.equals("multiple"))            
            return Level.SPECIAL_EXTRA_MULTIPLE;
        if(name.equals("poison"))
            return Level.SPECIAL_EXTRA_POISON;
        if(name.equals("snipe"))
            return Level.SPECIAL_EXTRA_SNIPE;
        if(name.equals("rc"))
            return Level.SPECIAL_EXTRA_RC;
        if(name.equals("slow"))
            return Level.SPECIAL_EXTRA_SLOW;
        if(name.equals("specialBomb"))
            return Level.SPECIAL_EXTRA_SPECIAL_BOMB;
        if(name.equals("speed"))
            return Level.SPECIAL_EXTRA_SPEED;
        if(name.equals("speed2"))
            return Level.SPECIAL_EXTRA_SPEED2;
        if(name.equals("stunOthers"))
            return Level.SPECIAL_EXTRA_STUN_OTHERS;
        if(name.equals("teleport"))
            return Level.SPECIAL_EXTRA_TELEPORT;

        return Level.SPECIAL_EXTRA_VOID;
    }
}

