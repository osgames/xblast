/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.io;

import storch.Level;
import swingwt.awt.Color;

/**
 * This class is used to convert data from the format used in the <code>Level</code>
 * object into the format used in a TNT level file.
 *
 * @author   Tor Andr�
 */
class Level2TNT {
    private Level theLevel;
    protected final static int NAME = 0, SKEY = 1, PICTURE = 2;
    
    
    /**
     * Constructor for the <code>Level2TNT</code> object
     *
     * @param theLevel  The Level to convert
     */
    protected Level2TNT(Level theLevel) {
        this.theLevel = theLevel;
    }
    
    
    /**
     * Gets the specialBombs attribute of the <code>Level</code> object
     *
     * @return   The specialBomb types in TNT format
     */
    protected int getSpecialBombs() {
        int bombs = -1;
         if(theLevel.inGameSpecial == Level.SPECIAL_INIT_SPECIAL_BOMBS_INFINITY)
         {bombs=255;
         }
         else
        if(theLevel.inGameSpecial == Level.SPECIAL_INIT_SPECIAL_BOMBS_30)
            bombs = 30;
        else if(theLevel.inGameSpecial == Level.SPECIAL_INIT_SPECIAL_BOMBS_12)
            bombs = 12;
        return bombs;
    }
    
    
    /**
     * Gets the initialExtra attribute of the <code>Level</code> object
     *
     * @return   The initialExtra value in TNT format
     */
    protected String getInitialExtra() {
        String extra = null;
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_RC)
            extra = "rc";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_TELEPORT)
            extra = "teleport";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_AIR)
            extra = "airpump";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_CLOAK)
            extra = "cloak";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_MORPH)
            extra = "morph";
        if(theLevel.inKick) {
            if(extra == null)
                extra = "kick";
            else
            extra += " kick";
        }
        
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_CHOICE){
            if(extra == null)
                extra = "choicebombtype";
            else
            extra += " choicebombtype";
        }
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_SNIPE){
            if(extra == null)
                extra = "snipe";
            else
            extra += " snipe";
        }
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_PLAYERFART){
            if(extra == null)
                extra = "farter";
            else
            extra += " farter";
        }
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_REVIVE){
            if(extra == null)
                extra = "revive";
            else
            extra += " revive";
        }
        
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_PLAYERANDBOMBFART){
            if(extra == null)
                extra = "bfarter";
            else
            extra += " bfarter";
        }
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_FROGGER){
            if(extra == null)
                extra = "frogger";
            else
            extra += " frogger";
        }
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_SWAPPOSITION){
            if(extra == null)
                extra = "swapposition";
            extra += " swapposition";
        }
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_SWAPCOLOR){
            if(extra == null)
                extra = "swapcolor";
            else
            extra += " swapcolor";
        }
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_ELECTRIFY){
            if(extra == null)
                extra = "electrify";
            else
            extra += " electrify";
        }
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_STOP){
            if(extra == null)
                extra = "stop";
            else
            extra += " stop";
        }
        return extra;
    }
    
    
    /**
     * Gets the revExtra attribute of the <code>Level</code> object
     *
     * @return   The revExtra value in TNT format
     */
    protected String getRevExtra() {
        String extra = null;
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_RC)
            extra = "rc";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_TELEPORT)
            extra = "teleport";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_AIR)
            extra = "airpump";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_CLOAK)
            extra = "cloak";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_MORPH)
            extra = "morph";
     
        if(theLevel.revKick) {
            if(extra == null)
                extra = "kick";
            else
            extra += " kick";
        }
        
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_CHOICE){
            if(extra == null)
                extra = "choicebombtype";
            else
            extra += " choicebombtype";
        }
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_SNIPE){
            if(extra == null)
                extra = "snipe";
            else
            extra += " snipe";
        }
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_PLAYERFART){
            if(extra == null)
                extra = "farter";
            else
            extra += " farter";
        }
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_REVIVE){
            if(extra == null)
                extra = "revive";
            else
            extra += " revive";
        }
        
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_PLAYERANDBOMBFART){
            if(extra == null)
                extra = "bfarter";
            else
            extra += " bfarter";
        }
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_FROGGER){
            if(extra == null)
                extra = "frogger";
            else
            extra += " frogger";
        }
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_SWAPPOSITION){
            if(extra == null)
                extra = "swapposition";
            extra += " swapposition";
        }
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_SWAPCOLOR){
            if(extra == null)
                extra = "swapcolor";
            else
            extra += " swapcolor";
        }
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_ELECTRIFY){
            if(extra == null)
                extra = "electrify";
            else
            extra += " electrify";
        }
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_STOP){
            if(extra == null)
                extra = "stop";
            else
            extra += " stop";
        }
        return extra;
    }
    
    
    /**
     * Gets the revIll attribute of the <code>Level</code> object
     *
     * @return   The revIll value in TNT format
     */
    protected String getRevIll() {
        String s1 = null;
        switch (theLevel.revIllness) {
            case Level.ILL_HEALTHY:
                s1 = "healthy";
                break;
            case Level.ILL_BOMB:
                s1 = "bomb";
                break;
            case Level.ILL_SLOW:
                s1 = "slow";
                break;
            case Level.ILL_RUN:
                s1 = "run";
                break;
            case Level.ILL_MINI:
                s1 = "mini";
                break;
            case Level.ILL_EMPTY:
                s1 = "empty";
                break;
            case Level.ILL_INVISIBLE:
                s1 = "invisible";
                break;
            case Level.ILL_MALFUNCTION:
                s1 = "malfunction";
                break;
            case Level.ILL_REVERSE:
                s1 = "reverse";
                break;
            case Level.ILL_REVERSE2:
                s1 = "reverse2";
                break;
            case Level.ILL_TELEPORT:
                s1 = "teleport";
                break;
        }
        return s1;
    }
    
    
    /**
     * Gets the initialIll attribute of the <code>Level</code> object
     *
     * @return   The initialIll value in TNT format
     */
    protected String getInitialIll() {
        String s1 = null;
        switch (theLevel.inIllness) {
            case Level.ILL_HEALTHY:
                s1 = "healthy";
                break;
            case Level.ILL_BOMB:
                s1 = "bomb";
                break;
            case Level.ILL_SLOW:
                s1 = "slow";
                break;
            case Level.ILL_RUN:
                s1 = "run";
                break;
            case Level.ILL_MINI:
                s1 = "mini";
                break;
            case Level.ILL_EMPTY:
                s1 = "empty";
                break;
            case Level.ILL_INVISIBLE:
                s1 = "invisible";
                break;
            case Level.ILL_MALFUNCTION:
                s1 = "malfunction";
                break;
            case Level.ILL_REVERSE:
                s1 = "reverse";
                break;
            case Level.ILL_REVERSE2:
                s1 = "reverse2";
                break;
            case Level.ILL_TELEPORT:
                s1 = "teleport";
                break;
        }
        return s1;
    }
    
    
    /**
     * Gets the nasty attribute of the <code>Level</code> object
     *
     * @return   The nasty value in TNT format
     */
    protected String getNasty() {
        String nasty = null;
        if(theLevel.turnGameSpecial == Level.SPECIAL_GAME_NASTY_WALLS || theLevel.inGameSpecial == Level.SPECIAL_INIT_NASTY_WALLS)
            nasty = "nastyGentle=1\nnastyRange=1";
        if(theLevel.inGameSpecial == Level.SPECIAL_INIT_NASTY_WALLS_2)
            nasty = "nastyGentle=2\nnastyRange=2";
        
        return nasty;
    }
    
    
    /**
     * Gets the distribution attribute of the <code>Level</code> object
     *
     * @return   The distribution value in TNT format
     */
    protected String getDistribution() {
        String s1 = "";
        switch (theLevel.distribution) {
            case Level.DE_NONE:
                s1 = "none";
                break;
            case Level.DE_SINGLE:
                s1 = "single";
                break;
            case Level.DE_ALL:
                s1 = "all";
                break;
            case Level.DE_SPECIAL:
                s1 = "special";
                break;
            case Level.DE_GET:
                s1 = "get";
                break;
            case Level.DE_DOUBLE:
                s1 = "double";
                break;
        }
        return s1;
    }
    
    
    /**
     * Converts a <code>swingwt.awt.Color</code> into HEX
     *
     * @param col  The color to convert
     * @return     The hex value
     */
    protected String getHex(Color col) {
        String hex1 = Integer.toHexString(col.getRGB()).toUpperCase();
        return "#" + hex1.substring(2);
    }
    
    
    /**
     * Gets the hauntSpeed attribute of the <code>Level</code> object
     *
     * @return   The hauntSpeed value in TNT format
     */
    protected String getHauntSpeed() {
        String s2 = null;
        if(theLevel.turnGameSpecial == Level.SPECIAL_GAME_HAUNT)
            s2 = "slow";
        if(theLevel.turnGameSpecial == Level.SPECIAL_GAME_HAUNT_FAST)
            s2 = "fast";
        
        return s2;
    }
    
    
    /**
     * Gets the dir attribute of the <code>Level</code> object
     *
     * @return   The dir value in TNT format
     */
    protected String getDir() {
        String s1 = "";
        switch (theLevel.direction) {
            case Level.GO_STOP:
                s1 = "stop";
                break;
            case Level.GO_DOWN:
                s1 = "down";
                break;
            case Level.GO_UP:
                s1 = "up";
                break;
            case Level.GO_LEFT:
                s1 = "left";
                break;
            case Level.GO_RIGHT:
                s1 = "right";
                break;
        }
        return s1;
    }
    
    
    /**
     * Gets the fuseTime attribute of the <code>Level</code> object
     *
     * @return   The fuseTime value in TNT format
     */
    protected String getFuseTime() {
        String s2 = "";
        switch (theLevel.fuseTime) {
            case Level.FUSE_NORMAL:
                s2 = "normal";
                break;
            case Level.FUSE_SHORT:
                s2 = "short";
                break;
            case Level.FUSE_LONG:
                s2 = "long";
                break;
        }
        return s2;
    }
    
    
    /**
     * Converts a bombClick value in <code>Level</code> format to TNT format
     *
     * @param type  The bombClick type
     * @return      The bombClick type in TNT format
     */
    protected String getBombClick(int type) {
        String bomb = null;
        switch (type) {
            case Level.BOMB_CLICK_NONE:
                bomb = "none";
                break;
            case Level.BOMB_CLICK_INITIAL:
                bomb = "initial";
                break;
            case Level.BOMB_CLICK_SNOOKER:
                bomb = "snooker";
                break;
            case Level.BOMB_CLICK_CONTACT:
                bomb = "contact";
                break;
            case Level.BOMB_CLICK_CLOCKWISE:
                bomb = "clockwise";
                break;
            case Level.BOMB_CLICK_ANTICLOCKWISE:
                bomb = "anticlockwise";
                break;
            case Level.BOMB_CLICK_RANDOMDIR:
                bomb = "randomdir";
                break;
            case Level.BOMB_CLICK_REBOUND:
                bomb = "rebound";
                break;
            case Level.BOMB_CLICK_THRU:
                bomb = "thru";
                break;
        }
        return bomb;
    }
    
    
    /**
     * Converts a playerClick value in <code>Level</code> format to TNT format
     *
     * @param type  The playerClick type
     * @return      The playerClick type in TNT format
     */
    protected String getBombType(int type) {
        String def = null;
        
        switch (type) {
            case Level.BOMB_NORMAL:
                def = "normal";
                break;
            case Level.BOMB_NAPALM:
                def = "napalm";
                break;
            case Level.BOMB_FIRECRACKER:
                def = "firecracker";
                break;
            case Level.BOMB_CONSTRUCTION:
                def = "construction";
                break;
            case Level.BOMB_DESTRUCTION:
                def = "destruction";
                break;
            case Level.BOMB_RENOVATION:
                def = "renovation";
                break;
            case Level.BOMB_THREEBOMBS:
                def = "threebombs";
                break;
            case Level.BOMB_TRIANGLEBOMBS:
                def = "trianglebombs";
                break;
            case Level.BOMB_GRENADE:
                def = "grenade";
                break;
            case Level.BOMB_FUNGUS:
                def = "fungus";
                break;
            case Level.BOMB_PYRO:
                def = "pyro";
                break;
            case Level.BOMB_SEARCH://EPFL
                def = "search";
                break;
            case Level.BOMB_RANDOM:
                def = "random";
                break;
            case Level.BOMB_FIRECRACKER_2://Only in TNT
                def = "firecracker2";
                break;
            case Level.BOMB_PYRO_2://Only in TNT
                def = "pyro2";
                break;
            case Level.BOMB_BLASTNOW://Only in TNT
                def = "blastnow";
                break;
            case Level.BOMB_CLOSE://Only in TNT
                def = "close";
                break;
            case Level.BOMB_SHORT://Only in TNT
                def = "short";
                break;
                
            case Level.BOMB_RING_OF_FIRE://Only in EPFL
                def = "ringofire";
                break;
            case Level.BOMB_MINE://Only in EPFL
                def = "mine";
                break;
            case Level.BOMB_DIAG_THREE_BOMBS://Only in EPFL
                def = "diagthreebombs";
                break;
            case Level.BOMB_SCISSOR://Only in EPFL
                def = "scissor";
                break;
            case Level.BOMB_SCISSOR_2://Only in EPFL
                def = "scissor2";
                break;
            case Level.BOMB_PARALLEL://Only in EPFL
                def = "parallel";
                break;
            case Level.BOMB_DISTANCE://Only in EPFL
                def = "distance";
                break;
            case Level.BOMB_LUCKY://Only in EPFL
                def = "lucky";
                break;
            case Level.BOMB_PARASOL://Only in EPFL
                def = "parasol";
                break;
            case Level.BOMB_COMB://Only in EPFL
                def = "comb";
                break;
            case Level.BOMB_FARPYRO://Only in EPFL
                def = "farpyro";
                break;
            case Level.BOMB_NUCLEAR://Only in EPFL
                def = "nuclear";
                break;
            case Level.BOMB_PROTECTBOMBS://Only in EPFL
                def = "protectbombs";
                break;
            case Level.BOMB_SNIPE://Only in EPFL
                def = "snipe";
                break;
        }
        return def;
    }
    
    
    /**
     * Gets the specialExtra attribute of the <code>Level</code> object
     *
     * @return   The specialExtra value in TNT format
     */
    protected String[] getSpecialExtra() {
        String s1 = null;
        String s2 = null;
        String s3 = null;
        String[] res = new String[3];
        
        switch (theLevel.specialExtra) {
            case Level.SPECIAL_EXTRA_VOID:
                s3 = "bomb";
                break;
            case Level.SPECIAL_EXTRA_INVINCIBLE:
                s1 = "invincible";
                s3 = "invincible";
                break;
            case Level.SPECIAL_EXTRA_KICK:
                s1 = "kick";
                s3 = "kick_bomb";
                break;
            case Level.SPECIAL_EXTRA_TELEPORT:
                s1 = "teleport";
                s2 = "teleport";
                s3 = "q3a_beam";
                break;
            case Level.SPECIAL_EXTRA_RC:
                s1 = "rc";
                s2 = "rc";
                s3 = "remote_control";
                break;
            case Level.SPECIAL_EXTRA_IGNITE_ALL:
                s1 = "igniteAll";
                s3 = "ignite";
                break;
            case Level.SPECIAL_EXTRA_AIR:
                s1 = "air";
                s2 = "air";
                s3 = "air_pump";
                break;
            case Level.SPECIAL_EXTRA_SPECIAL_BOMB:
                s1 = "specialBomb";
                s2 = "specialBomb";
                int type = theLevel.specialType;
                if(type == Level.BOMB_NORMAL)
                    s3 = "bomb";
                else if(type == Level.BOMB_THREEBOMBS || type == Level.BOMB_TRIANGLEBOMBS)
                    s3 = "triangle_bomb";
                else if(type == Level.BOMB_CONSTRUCTION || type == Level.BOMB_DESTRUCTION || type == Level.BOMB_RENOVATION)
                    s3 = "construction";
                else if(type == Level.BOMB_FIRECRACKER || type == Level.BOMB_PYRO || type == Level.BOMB_FIRECRACKER_2 || type == Level.BOMB_PYRO_2)
                    s3 = "firecracker";
                else if(type == Level.BOMB_SEARCH){
                    s3="search";}
                else if(type == Level.BOMB_SNIPE){
                    s3="snipe";}
                else
                    s3 = "napalm";
                break;
            case Level.SPECIAL_EXTRA_CLOAK:
                s1 = "cloak";
                s2 = "cloak";
                s3 = "cloak";
                break;
            case Level.SPECIAL_EXTRA_JUNKIE:
                s1 = "junkie";
                s3 = "syringe";
                break;
            case Level.SPECIAL_EXTRA_HOLY_GRAIL:
                s1 = "holyGrail";
                s3 = "holygrail";
                break;
            case Level.SPECIAL_EXTRA_LIFE:
                s1 = "life";
                s3 = "life";
                break;
            case Level.SPECIAL_EXTRA_MAYHEM:
                s1 = "mayhem";
                s3 = "mayhem";
                break;
            case Level.SPECIAL_EXTRA_SPEED:
                s1 = "speed";
                s3 = "speed";
                break;
            case Level.SPECIAL_EXTRA_SPEED2:
                s1 = "speed2";
                s3 = "speed";
                break;
            case Level.SPECIAL_EXTRA_SLOW:
                s1 = "slow";
                s3 = "slow";
                break;
            case Level.SPECIAL_EXTRA_SNIPE:
                s1 = "snipe";
                s2 = "snipe";
                s3 = "snipe";
		System.out.println(" s2 "+s2);
                break;
            case Level.SPECIAL_EXTRA_STUN_OTHERS:
                s1 = "stunOthers";
                s3 = "pow";
                break;
            case Level.SPECIAL_EXTRA_LONG_STUNNED:
                s1 = "longStunned";
                s3 = "pow";
                break;
            case Level.SPECIAL_EXTRA_POISON:
                s1 = "poison";
                s3 = "poison";
                break;
            case Level.SPECIAL_EXTRA_MULTIPLE:
                s1 = "multiple";
                s3 = "multiple";
                break;
            case Level.SPECIAL_EXTRA_MORPH:
                s1 = "morph";
                s2 = "morph";
                s3 = "morph";
                break;
            case Level.SPECIAL_EXTRA_CHOICE:
                s1 = "choicebombtype";
                s2 = "choicebombtype";
                s3 = "doom";
                break;
            case Level.SPECIAL_EXTRA_STOP:
                s1 = "stop";
                s2 = "stop";
                s3 = "EPFL_stop";
                break;
            case Level.SPECIAL_EXTRA_SWAPPOSITION:
                s1 = "swapposition";
                s3 = "swapposition";
                break;
            case Level.SPECIAL_EXTRA_SWAPCOLOR:
                s1 = "swapcolor";
                s3 = "swapcolor";
                break;
            case Level.SPECIAL_EXTRA_PLAYERFART:
                s1 = "farter";
                s2 = "farter";
                s3 = "fart";
                break; case Level.SPECIAL_EXTRA_PHANTOM:
                    s1 = "phantom";
                    s3 = "phantom";
                    break;
            case Level.SPECIAL_EXTRA_ELECTRIFY:
                s1 = "electrify";
                s2 = "electrify";
                s3 = "electrify";
                break;
                
            case Level.SPECIAL_EXTRA_REVIVE:
                s1 = "revive";
                
                s3 = "phoenix";
                break;
            case Level.SPECIAL_EXTRA_PLAYERANDBOMBFART:
                s1 = "bfarter";
                s2 = "bfarter";
                s3 = "fart";
                break;
            case Level.SPECIAL_EXTRA_FROGGER:
                s1 = "frogger";
                s2 = "frogger";
                s3 = "frogger";
                break;
            case Level.SPECIAL_EXTRA_EVILGRAIL:
                s1 = "evilgrail";
                
                s3 = "evilgrail";
                break;
            case Level.SPECIAL_EXTRA_STEAL:
                s1 = "steal";
                
                s3 = "steal";
                break; 
            case Level.SPECIAL_EXTRA_DALEIF:
                s1 = "daleif";
                
                s3 = "daleif";
                break;
        }
        
        if(s2 == null && (theLevel.inGameSpecial == Level.SPECIAL_INIT_SPECIAL_BOMBS_30 || theLevel.inGameSpecial == Level.SPECIAL_INIT_SPECIAL_BOMBS_12))
            s2 = "specialBomb";
        
        if(s2 == null) {
            if(theLevel.inExtra == Level.SPECIAL_EXTRA_RC || theLevel.revExtra == Level.SPECIAL_EXTRA_RC)
                s2 = "rc";
            if(theLevel.inExtra == Level.SPECIAL_EXTRA_TELEPORT || theLevel.revExtra == Level.SPECIAL_EXTRA_TELEPORT)
                s2 = "teleport";
            if(theLevel.inExtra == Level.SPECIAL_EXTRA_AIR || theLevel.revExtra == Level.SPECIAL_EXTRA_AIR)
                s2 = "air";
            if(theLevel.inExtra == Level.SPECIAL_EXTRA_CLOAK || theLevel.revExtra == Level.SPECIAL_EXTRA_CLOAK)
                s2 = "cloak"; 
	   
        }
        
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_RC || theLevel.revExtra == Level.SPECIAL_EXTRA_RC || theLevel.specialExtra == Level.SPECIAL_EXTRA_RC)
            s2 = "rc";
        
	if(theLevel.inExtra == Level.SPECIAL_EXTRA_SNIPE || 
	   theLevel.revExtra == Level.SPECIAL_EXTRA_SNIPE|| 
	   theLevel.specialExtra == Level.SPECIAL_EXTRA_SNIPE)
                s2 = "snipe";
	
	if(theLevel.inExtra == Level.SPECIAL_EXTRA_MORPH || 
	   theLevel.revExtra == Level.SPECIAL_EXTRA_MORPH|| 
	   theLevel.specialExtra == Level.SPECIAL_EXTRA_SNIPE)
                s2 = "morph";
	
        res[NAME] = s1;
        res[SKEY] = s2;
        res[PICTURE] = s3;
        return res;
    }
    
    
    /**
     * Gets the shrink attribute of the <code>Level</code> object
     *
     * @return   The shrink type in TNT format
     */
    protected String getShrink() {
        String s1 = "";
        switch (theLevel.shrinkPattern) {
            case Level.SHRINK_SPIRAL:
                s1 = "spiral";
                break;
            case Level.SHRINK_SPEED_SPIRAL:
                s1 = "speedSpiral";
                break;
            case Level.SHRINK_SPIRAL_PLUS:
                s1 = "spiralPlus";
                break;
            case Level.SHRINK_SPIRAL_3:
                s1 = "spiral3";
                break;
            case Level.SHRINK_SPIRAL_23:
                s1 = "spiral23";
                break;
            case Level.SHRINK_SPIRAL_LEGO:
                s1 = "spiralLego";
                break;
            case Level.SHRINK_EARLY_SPIRAL:
                s1 = "earlySpiral";
                break;
            case Level.SHRINK_COMPOUND:
                s1 = "compound";
                break;
            case Level.SHRINK_COMPOUND_F:
                s1 = "compoundF";
                break;
            case Level.SHRINK_COMPOUND_2_F:
                s1 = "compound2F";
                break;
            case Level.SHRINK_LAZY_COMPOUND_F:
                s1 = "lazyCompoundF";
                break;
            case Level.SHRINK_COMPOUND_SOLID:
                s1 = "compoundSolid";
                break;
            case Level.SHRINK_SAVAGE_COMPOUND:
                s1 = "savageCompound";
                break;
            case Level.SHRINK_COMPOUND_EXTRA:
                s1 = "compoundExtra";
                break;
            case Level.SHRINK_DOWN:
                s1 = "down";
                break;
            case Level.SHRINK_DOWN_F:
                s1 = "downF";
                break;
            case Level.SHRINK_QUAD:
                s1 = "quad";
                break;
            case Level.SHRINK_CONSTRICT_WAVE:
                s1 = "constrictWave";
                break;
            case Level.SHRINK_OUTWARD_SPIRAL:
                s1 = "outwardSpiral";
                break;
        }
        return s1;
    }
    
    
    /**
     * Gets the gameMode attribute of the <code>Level</code> object
     *
     * @return   The gameMode value in TNT format
     */
    protected String getGameMode() {
        StringBuffer gameMode = new StringBuffer("R23456STDL");
        if(!theLevel.GM_Random)
            gameMode.setCharAt(0, '-');
        if(!theLevel.GM_2_Player)
            gameMode.setCharAt(1, '-');
        if(!theLevel.GM_3_Player)
            gameMode.setCharAt(2, '-');
        if(!theLevel.GM_4_Player)
            gameMode.setCharAt(3, '-');
        if(!theLevel.GM_5_Player)
            gameMode.setCharAt(4, '-');
        if(!theLevel.GM_6_Player)
            gameMode.setCharAt(5, '-');
        if(!theLevel.GM_Single)
            gameMode.setCharAt(6, '-');
        if(!theLevel.GM_Team)
            gameMode.setCharAt(7, '-');
        if(!theLevel.GM_Double)
            gameMode.setCharAt(8, '-');
        if(!theLevel.GM_LR_Players)
            gameMode.setCharAt(9, '-');
        
        return gameMode.toString();
    }
}
