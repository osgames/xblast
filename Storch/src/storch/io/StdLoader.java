/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.io;

import java.io.*;
import storch.*;
import swingwt.awt.Color;

/**
 * This class is used to load a level stored in the standard (2.6) format
 *
 * @author   Tor Andr�
 */
public class StdLoader implements Loader {

    private BufferedReader in;
    private Level theLevel;
    private Std2Level converter;
    private StreamTokenizer lex;
    private Scramble scramble1, scramble2;

    //private final boolean VERBOSE = true;
    private final boolean VERBOSE = false;

    private String[] definedBlocks = {"EXTRA_BOMB", "EXTRA_RANGE", "EXTRA_TRAP", "EXTRA_KICK",
            "EXTRA_INVINC", "EXTRA_BUTTON", "EXTRA_CONSTR", "EXTRA_RC",
            "EXTRA_BEAM", "EXTRA_AIRPUMP", "EXTRA_NAPALM", "EXTRA_FIRECRACKER",
            "EXTRA_SYRINGE", "EXTRA_POISON", "EXTRA_SPEED", "EXTRA_SLOW",
            "EXTRA_MAYHEM", "EXTRA_HOLYGRAIL", "EXTRA_MULTIPLE", "EXTRA_LIFE",
            "EXTRA_CLOAK", "EXTRA_TRIANGLE", "EXTRA_POW", "EXTRA_MORPH", "EXTRA_STOP",
            "EXTRA_ELECTRIFY", "EXTRA_FROG", "EXTRA_PHANTOM", "EXTRA_GHOST",
            "EXTRA_THROUGH", "EXTRA_REVIVE", "EXTRA_STEAL", "EXTRA_SWAPPOSITION",
            "EXTRA_SWAPCOLOR", "EXTRA_DALEIF", "EXTRA_CHOICE", "EXTRA_EVILGRAIL",
            "EXTRA_JUMP", "EXTRA_FART", "EXTRA_SEARCH", "EXTRA_SUCKER", "EXTRA_MINES"
            };


    /**
     * Initializes the loader
     *
     * @param loadFile           The file to save to
     * @exception LoadException
     */
    private void init(File loadFile) throws LoadException {
        scramble1 = null;
        scramble2 = null;

        try {
            in = new BufferedReader(new FileReader(loadFile));
        } catch(IOException e) {
            throw new LoadException("Unable to open the file " + loadFile.getName());
        }

        theLevel = new Level();
        theLevel.GM_2_Player = theLevel.GM_3_Player = theLevel.GM_4_Player = theLevel.GM_5_Player = theLevel.GM_6_Player = false;
        theLevel.GM_Random = false;
        theLevel.GM_Team = theLevel.GM_Double = theLevel.GM_Single = theLevel.GM_LR_Players = false;
        converter = new Std2Level();
        lex = new StreamTokenizer(in);
        lex.eolIsSignificant(false);
        lex.lowerCaseMode(false);
        lex.parseNumbers();
        lex.quoteChar('"');
        lex.slashSlashComments(true);
        lex.slashStarComments(true);
        lex.whitespaceChars(123, 125);
        lex.whitespaceChars(0, 33);
        lex.whitespaceChars(',', ',');
        lex.whitespaceChars('(', ')');
        lex.wordChars('A', 'z');

        lex.ordinaryChar(';');
        lex.ordinaryChar('=');
        lex.ordinaryChar('*');
        lex.ordinaryChar('/');
    }


    /**
     * Loads a level stored in standard (2.6) format.
     *
     * @param loadFile           The file to load
     * @return                   A <code>Level</code> object containing the
     *      loaded data
     * @exception LoadException  If unable to load the level
     */
    public Level load(File loadFile) throws LoadException {
        try {
            init(loadFile);
            while(lex.nextToken() != lex.TT_EOF) {
                if(lex.ttype == lex.TT_WORD && lex.sval.equals("static")) {
                    lex.nextToken();
                    if(lex.ttype == lex.TT_WORD && lex.sval.equals("BMPosition")) {
                        lex.nextToken();
                        if(scramble1 == null) {
                            scramble1 = new Scramble(lex.sval);
                            parseScramble(scramble1);
                        }
                        else {
                            scramble2 = new Scramble(lex.sval);
                            parseScramble(scramble2);
                        }
                    }
                    else if(lex.ttype == lex.TT_WORD && lex.sval.equals("BMLevelData")) {
                        parseLevel();
                    }
                }
            }
        } catch(LoadException e) {
            throw (LoadException) e.fillInStackTrace();
        } catch(IOException e) {
            throw new LoadException("Error parsing " + loadFile.getName());
        }

        return theLevel;
    }


    /**
     * Parses the file
     *
     * @exception LoadException  If parse error
     */
    private void parseLevel() throws LoadException {
        try {
            while(lex.ttype != '=')
                lex.nextToken();
            lex.nextToken();
            theLevel.title = lex.sval;
            lex.nextToken();
            theLevel.author = lex.sval;
            lex.nextToken();
            lex.nextToken();
            theLevel.description = lex.sval;
            lex.nextToken();
            while(!lex.sval.equals("void")) {
                setGM(lex.sval);
                lex.nextToken();
            }

            while(lex.ttype != lex.TT_WORD || (lex.ttype == lex.TT_WORD && !lex.sval.equals("NULL")))
                lex.nextToken();

            lex.nextToken();
            theLevel.shrinkPattern = converter.getShrink(lex.sval);

            for(int i = 0; i < 2; i++) {
                double startTime = 1;
                int count = 0;
                lex.nextToken();

                   
                if(lex.ttype != lex.TT_WORD || (lex.ttype == lex.TT_WORD && !lex.sval.equals("SCRAMBLE_VOID"))) {
                    if(lex.ttype == lex.TT_NUMBER)
                        startTime = lex.nval;
                    lex.nextToken();
                    if(lex.ttype == '*') {
                        lex.nextToken();
                        if(lex.ttype == lex.TT_NUMBER)
                            startTime *= lex.nval;
                        lex.nextToken();
                    }
                    if(lex.ttype == '/') {
                        lex.nextToken();
                        if(lex.ttype == lex.TT_NUMBER)
                            startTime /= lex.nval;
                    }
                    lex.nextToken();
                    count = (int) lex.nval;
                    if(lex.ttype == lex.TT_NUMBER)
                        lex.nextToken();
                    debugPrint("scramble namnet: " + lex.sval);
                    if(lex.sval.equals(scramble1.getName())) {
                        if(i == 0) {
                            theLevel.scrambleDrawTime = startTime;
                            theLevel.scrambleDrawCount = count;
                            theLevel.scrambleDraw = scramble1.getData();
                        }
                        else {
                            theLevel.scrambleDelTime = startTime;
                            theLevel.scrambleDelCount = count;
                            theLevel.scrambleDel = scramble1.getData();
                        }
                    }
                    else {
                        if(i == 0) {
                            theLevel.scrambleDrawTime = startTime;
                            theLevel.scrambleDrawCount = count;
                            theLevel.scrambleDraw = scramble2.getData();
                        }
                        else {
                            theLevel.scrambleDelTime = startTime;
                            theLevel.scrambleDelCount = count;
                            theLevel.scrambleDel = scramble2.getData();
                        }
                    }

                }
            }

            debugPrint("Innan gameSpecial");

            lex.nextToken();
            theLevel.inGameSpecial = converter.getInSpecial(lex.sval);
            lex.nextToken();
            theLevel.turnGameSpecial = converter.getTurnSpecial(lex.sval);
            lex.nextToken();
            theLevel.specialExtra = converter.getSpecialExtra(lex.sval);
            lex.nextToken();

            lex.nextToken();
            theLevel.bombsAtStart = (int) lex.nval;
            lex.nextToken();
            theLevel.range = (int) lex.nval;

            for(int i = 0; i < 6; i++) {
                lex.nextToken();
                
            System.out.println("player data "+lex);
           
                if(lex.ttype != lex.TT_NUMBER) {
                    lex.pushBack();
                    break;
                }
                theLevel.startPos[i][1] = (int) lex.nval;
                lex.nextToken();
                theLevel.startPos[i][0] = (int) lex.nval;
            }
            lex.nextToken();
            theLevel.PMMode = lex.sval;
            System.out.println(lex);
            lex.nextToken();
            System.out.println(lex);
            theLevel.PMRange = (int) lex.nval;
            lex.nextToken();
            System.out.println(lex);
            theLevel.inIllness = converter.getIll(lex.sval);
            lex.nextToken();
            System.out.println(lex);
            theLevel.revIllness = converter.getIll(lex.sval);
            lex.nextToken();
            System.out.println(lex);
            setInitialExtra(lex.sval);
            lex.nextToken();

            if(lex.sval.charAt(1) == 'F') {
                setInitialExtra(lex.sval);
                lex.nextToken();
            }
            //bombdata
            theLevel.bombClick = converter.getBombClick(lex.sval);
            lex.nextToken();
            theLevel.wallClick = converter.getBombClick(lex.sval);
            lex.nextToken();
            theLevel.playerClick = converter.getBombClick(lex.sval);

            lex.nextToken();
            theLevel.direction = converter.getBombDir(lex.sval);
            lex.nextToken();
            theLevel.fuseTime = converter.getFuseTime(lex.sval);
            lex.nextToken();
            theLevel.defaultType = converter.getBombType(lex.sval);
            lex.nextToken();
            theLevel.specialType = converter.getBombType(lex.sval);
            lex.nextToken();
            theLevel.hiddenType = converter.getBombType(lex.sval);
            lex.nextToken();

            debugPrint("Innan graphics");
            debugPrint(lex.sval);
            //graphics
            ColorTable table = new ColorTable();
            String name;
            Color c0;
            Color c1;
            Color c2;

            name = lex.sval;
            if(isMacro(name)) {
                theLevel.loadStatus |= Level.LOAD_GRAPHICS_ERROR;
                theLevel.freeBlock = new Block(Block.FREE_BLOCK, "score_floor", Color.black, Color.black);
            }
            else {
                lex.nextToken();
                c0 = table.getColor(lex.sval);
                lex.nextToken();
                c1 = table.getColor(lex.sval);
                lex.nextToken();
                c2 = table.getColor(lex.sval);
                theLevel.freeBlock = new Block(Block.FREE_BLOCK, name, c0, c1, c2);
            }

            lex.nextToken();
            name = lex.sval;
            if(isMacro(name)) {
                theLevel.loadStatus |= Level.LOAD_GRAPHICS_ERROR;
                theLevel.shadowedBlock = new Block(Block.OTHER_BLOCK, "score_floor", Color.black, Color.black);
            }
            else {
                lex.nextToken();
                c0 = table.getColor(lex.sval);
                lex.nextToken();
                c1 = table.getColor(lex.sval);
                lex.nextToken();
                c2 = table.getColor(lex.sval);
                theLevel.shadowedBlock = new Block(Block.OTHER_BLOCK, name, c0, c1, c2);
            }

            lex.nextToken();
            name = lex.sval;
            if(isMacro(name)) {
                theLevel.loadStatus |= Level.LOAD_GRAPHICS_ERROR;
                theLevel.solidWall = new Block(Block.SOLID_WALL, "score_floor", Color.black, Color.black);
            }
            else {
                lex.nextToken();
                c0 = table.getColor(lex.sval);
                lex.nextToken();
                c1 = table.getColor(lex.sval);
                lex.nextToken();
                c2 = table.getColor(lex.sval);
                theLevel.solidWall = new Block(Block.SOLID_WALL, name, c0, c1, c2);
            }

            lex.nextToken();
            name = lex.sval;
            if(isMacro(name)) {
                theLevel.loadStatus |= Level.LOAD_GRAPHICS_ERROR;
                theLevel.risingWall = new Block(Block.OTHER_BLOCK, "score_floor", Color.black, Color.black);
            }
            else {
                lex.nextToken();
                c0 = table.getColor(lex.sval);
                lex.nextToken();
                c1 = table.getColor(lex.sval);
                lex.nextToken();
                c2 = table.getColor(lex.sval);
                theLevel.risingWall = new Block(Block.OTHER_BLOCK, name, c0, c1, c2);
            }

            lex.nextToken();
            name = lex.sval;
            if(isMacro(name)) {
                theLevel.loadStatus |= Level.LOAD_GRAPHICS_ERROR;
                theLevel.blastableBlock = new Block(Block.BLASTABLE_BLOCK, "score_floor", Color.black, Color.black);
            }
            else {
                lex.nextToken();
                c0 = table.getColor(lex.sval);
                lex.nextToken();
                c1 = table.getColor(lex.sval);
                lex.nextToken();
                c2 = table.getColor(lex.sval);
                theLevel.blastableBlock = new Block(Block.BLASTABLE_BLOCK, name, c0, c1, c2);
            }

            lex.nextToken();
            name = lex.sval;
            if(isMacro(name)) {
                theLevel.loadStatus |= Level.LOAD_GRAPHICS_ERROR;
                theLevel.blastedBlock = new Block(Block.OTHER_BLOCK, "score_floor", Color.black, Color.black);
            }
            else {
                lex.nextToken();
                c0 = table.getColor(lex.sval);
                lex.nextToken();
                c1 = table.getColor(lex.sval);
                lex.nextToken();
                c2 = table.getColor(lex.sval);
                theLevel.blastedBlock = new Block(Block.OTHER_BLOCK, name, c0, c1, c2);
            }

            for(int i = 0; i < 4; i++) {
                lex.nextToken();
                if(!isMacro(lex.sval)) {
                    for(int j = 0; j < 3; j++)
                        lex.nextToken();
                    theLevel.loadStatus |= Level.LOAD_SPECIAL_EXTRA_IMAGE_ERROR;
                }else{	theLevel.extraBlock = new Block(Block.SPECIAL_EXTRA, "score_floor", Color.black, Color.black);
		System.out.println("what does it2 "+lex.sval+"  "+lex);
		}
		System.out.println("what does it "+lex.sval+"  "+lex);
                debugPrint(lex.sval);
            }

            lex.nextToken();
            name = lex.sval;
            if(isMacro(name)) {
                theLevel.loadStatus |= Level.LOAD_GRAPHICS_ERROR;
                theLevel.voidBlock = new Block(Block.VOID_BLOCK, "score_floor", Color.black, Color.black);
            }
            else {
                lex.nextToken();
                c0 = table.getColor(lex.sval);
                lex.nextToken();
                c1 = table.getColor(lex.sval);
                lex.nextToken();
                c2 = table.getColor(lex.sval);
                theLevel.voidBlock = new Block(Block.VOID_BLOCK, name, c0, c1, c2);
            }

            theLevel.blastedFloor = theLevel.freeBlock;

            lex.nextToken();//shadow
            debugPrint(lex.sval + " efter graphics");
            lex.nextToken();
            theLevel.distribution = converter.getDistribution(lex.sval);

            debugPrint(lex.sval);

            lex.nextToken();
            double tmp = lex.nval;
            double old = tmp;
            theLevel.setProb(Level.PROB_BOMB_EXTRA, (int) Math.round(100 * tmp / 63));
            lex.nextToken();
            tmp = lex.nval;
            theLevel.setProb(Level.PROB_RANGE, (int) Math.round(100 * (tmp - old) / 63));
            old = tmp;
            lex.nextToken();
            tmp = lex.nval;
            theLevel.setProb(Level.PROB_ILL, (int) Math.round(100 * (tmp - old) / 63));
            old = tmp;
            lex.nextToken();
            tmp = lex.nval;
            theLevel.setProb(Level.PROB_SPECIAL_EXTRA, (int) Math.round(100 * (tmp - old) / 63));
            old = tmp;
            lex.nextToken();
            tmp = lex.nval;
            theLevel.setProb(Level.PROB_HIDDEN_BOMB, (int) Math.round(100 * (tmp - old) / 63));
            lex.nextToken();

            debugPrint(lex.nval);

            for(int i = 0; i < 15 * 13; i++) {
                theLevel.map[i / 13][i % 13] = converter.getMapInt(lex.sval);
                if(lex.sval.equals("R"))
                    theLevel.loadStatus |= Level.LOAD_RISING_BLOCK;
                else if(lex.sval.equals("V"))
                    theLevel.loadStatus |= Level.LOAD_UNDEFINED_BLOCK;

                lex.nextToken();
            }
        } catch(Exception e) {
            debugPrint(e);
            e.printStackTrace();
            throw new LoadException("An error occurred while loading " + theLevel.title);
        }
        debugPrint(lex.sval);

    }


    /**
     * Sets the initialExtra attribute of the Level object
     *
     * @param name  The initialExtra value
     */
    private void setInitialExtra(String name) {
        int se = Level.SPECIAL_EXTRA_VOID;
        debugPrint("setInitialExtra: " + name);
        String extra = name.substring(3);
        debugPrint(" " + extra);
        if(extra.equals("RC"))
            se = Level.SPECIAL_EXTRA_RC;
        else if(extra.equals("Teleport"))
            se = Level.SPECIAL_EXTRA_TELEPORT;
        else if(extra.equals("Airpump"))
            se = Level.SPECIAL_EXTRA_AIR;
        else if(extra.equals("Cloak"))
            se = Level.SPECIAL_EXTRA_CLOAK;
        else if(extra.equals("Morph"))
            se = Level.SPECIAL_EXTRA_MORPH;
        else if(extra.equals("Stop"))//EPFL
            se = Level.SPECIAL_EXTRA_STOP;
        else if(extra.equals("Frog"))//EPFL
            se = Level.SPECIAL_EXTRA_FROGGER;
        else if(extra.equals("Daleif"))//EPFL
            se = Level.SPECIAL_EXTRA_DALEIF;
        else if(extra.equals("Choice"))//EPFL
            se = Level.SPECIAL_EXTRA_CHOICE;
        else if(extra.equals("Fart"))//EPFL
            se = Level.SPECIAL_EXTRA_PLAYERFART;
        else if(extra.equals("Bfart"))//EPFL
            se = Level.SPECIAL_EXTRA_PLAYERANDBOMBFART;
        else if(extra.equals("Phantom"))//EPFL
            se = Level.SPECIAL_EXTRA_PHANTOM;
        else if(extra.equals("Revive"))//EPFL
            se = Level.SPECIAL_EXTRA_REVIVE;

        if(name.charAt(0) == 'L' || name.charAt(0) == 'I') {
            if(se != Level.SPECIAL_EXTRA_VOID)
                theLevel.inExtra = se;
            if(extra.equals("Kick"))
                theLevel.inKick = true;
        }
        if(name.charAt(0) == 'R' || name.charAt(0) == 'I') {
            if(se != Level.SPECIAL_EXTRA_VOID)
                theLevel.revExtra = se;
            if(extra.equals("Kick"))
                theLevel.revKick = true;
        }
    }


    /**
     * Sets the GameMode in the Level object
     *
     * @param mode  The new GameMode value
     */
    private void setGM(String mode) {
        if(mode.equals("GM_Random"))
            theLevel.GM_Random = true;
        if(mode.equals("GM_All")) {
            theLevel.GM_SinglePlayer = true;
            theLevel.GM_Team = true;
            theLevel.GM_Double = true;
            theLevel.GM_Single = true;
            theLevel.GM_LR_Players = true;
        }
        else if(mode.equals("GM_SinglePlayer"))
            theLevel.GM_SinglePlayer = true;
        else if(mode.equals("GM_Team"))
            theLevel.GM_Team = true;
        else if(mode.equals("GM_Double"))
            theLevel.GM_Double = true;
        else if(mode.equals("GM_Single"))
            theLevel.GM_Single = true;
        else if(mode.equals("GM_LR_Players"))
            theLevel.GM_LR_Players = true;

        if(mode.lastIndexOf('2') > -1)
            theLevel.GM_2_Player = true;
        if(mode.lastIndexOf('3') > -1)
            theLevel.GM_3_Player = true;
        if(mode.lastIndexOf('4') > -1)
            theLevel.GM_4_Player = true;
        if(mode.lastIndexOf('5') > -1)
            theLevel.GM_5_Player = true;
        if(mode.lastIndexOf('6') > -1)
            theLevel.GM_6_Player = true;

    }


    /**
     * Checks if the <code>String</code> is one of the defined macros for
     * specialExtras.
     *
     * @param name  The string to check
     * @return      True if <code>name</code> is a macro, false otherwise.
     */
    private boolean isMacro(String name) {
        boolean hit = false;
        for(int n = 0; n < definedBlocks.length && !hit; n++)
            if(definedBlocks[n].equals(name))
                hit = true;
        return hit;
    }


    /**
     * Parse the scramble data and adds new coordinates for
     * ScrambelDraw/ScrambelDel
     *
     * @param scramble           The <code>Scramble</code> objet to add the new
     *      coordinates to
     * @exception LoadException
     */
    private void parseScramble(Scramble scramble) throws LoadException {
        int x;
        int y;
        try {
            while(lex.ttype != '=')
                lex.nextToken();

            lex.nextToken();
            while(lex.ttype != ';') {
                y = (int) lex.nval;
                lex.nextToken();
                x = (int) lex.nval;
                scramble.addData(x, y);
                lex.nextToken();
            }
        } catch(Exception e) {
            throw new LoadException("An error occurred while parsing ScrambleData in " + theLevel.title);
        }
    }


    /**
     * Description of the Method
     *
     * @param s  Description of the Parameter
     */
    private void debugPrint(String s) {
        if(VERBOSE)
            System.out.println(s);
    }


    /**
     * Description of the Method
     *
     * @param s  Description of the Parameter
     */
    private void debugPrint(double s) {
        debugPrint("" + s);
    }


    /**
     * Description of the Method
     *
     * @param o  Description of the Parameter
     */
    private void debugPrint(Object o) {
        debugPrint(o.toString());
    }
}
