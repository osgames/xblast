/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.io;

import storch.*;
import java.io.*;
import swingwt.awt.Color;
import swingwt.awt.*;
import swingwt.awt.image.*;
import com.sun.image.codec.jpeg.*;

/**
 * Saves a <code>Level</code> to a HTML page.
 *
 * @author   Tor Andr�
 */
class HTMLSaver implements Saver {

	private Level theLevel;
	private Level2HTML converter;
	private PrintWriter out;
	private String[] extra;
	private String levelName;
	private String storchVersion, sourceName;
	private String IMAGE_DIR;

	/**
	 * Class constructor specifying Storch version and levelsource name.
	 *
	 * @param ver         The version of Storch which created the level.
	 * @param sourceName  The levels filename but NO extension. The HTMLSaver
	 *						assume that both sourceName.h and sourceName.xal exists.
	 */
	public HTMLSaver(String ver, String sourceName) {
		this.sourceName = sourceName;
		if(ver != null)
			if(ver.equals(""))
				ver = null;
		storchVersion = ver;
	}


	/**
	 * Saves the level to HTML.
	 *
	 * @param saveFile         The file to save to.
	 * @param inLevel          The level to save.
	 * @exception SaveException  If unable to save the level.
	 */
	public void save(File saveFile, Level inLevel) throws SaveException {
		theLevel = inLevel;
		converter = new Level2HTML(theLevel);
		levelName = theLevel.getFileName();
		extra = converter.getSpecialExtra();
		if(saveFile.getParent() != null)
			IMAGE_DIR = saveFile.getParent() + File.separator + "images" + File.separator;
		else
			IMAGE_DIR = "images" + File.separator;

		try{
			out = new PrintWriter(new BufferedWriter(new FileWriter(saveFile)));
		}catch(IOException e){
			throw new SaveException("Unable to create "+saveFile.getName());
		}
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("<!--");
		out.println("Level description created by LevelDoc " + LevelDoc.VERSION);
		out.println("LevelDoc is a part of Storch 2");
		out.println(" -->");

		out.println("<HEAD>");
		out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
		out.println("<TITLE>" + theLevel.title + "</TITLE>");
		
    	printJavaScriptFunctions();
  
		out.println("</HEAD>");
		out.println("<BODY bgcolor=\"#FFFFFF\">");
		out.println("<center><font size=\"+2\"><b>" + theLevel.title + "</b></font><br>");
		
		if(storchVersion != null)
			out.println("Created with Storch " + storchVersion);
		out.println("<br>");

		printMap();

		out.println("<INPUT onclick=\"startMap()\" type=radio CHECKED value=start name=time>Start");
		if(theLevel.scrambleDrawTime < theLevel.scrambleDelTime){
			if(theLevel.scrambleDrawCount > 0)
				out.println("<INPUT onclick=\"scrambleDraw()\" type=radio value=draw name=time>ScrambleDraw");
			if(theLevel.scrambleDelCount > 0)
				out.println("<INPUT onclick=\"scrambleDel()\" type=radio value=del name=time>ScrambleDelete");
		}else{
			if(theLevel.scrambleDelCount > 0)
				out.println("<INPUT onclick=\"scrambleDel()\" type=radio value=del name=time>ScrambleDelete");
			if(theLevel.scrambleDrawCount > 0)
				out.println("<INPUT onclick=\"scrambleDraw()\" type=radio value=draw name=time>ScrambleDraw");
		}
		if(theLevel.shrinkPattern != Level.SHRINK_VOID)
			out.println("<INPUT onclick=\"endMap()\" type=radio value=end name=time>End");
		out.println("</center>");
		out.println("<br>");
		out.println("Author " + theLevel.author + "<br>");
		out.println("Download <A href=\"" + sourceName + ".h\">" + sourceName + ".h</A> or ");
		out.println("<A href=\"" + sourceName + ".xal\">" + sourceName + ".xal</A><br>");
		out.println("<br>");
		out.println("<b>Level data</b><br>");
		if(theLevel.shrinkPattern != Level.SHRINK_VOID)
			out.println(converter.getShrink() + "<br>");

		if(theLevel.scrambleDrawCount > 0)
			out.println("ScrambleDraw after " + Math.round(theLevel.scrambleDrawTime * 100) + "%<br>");
		if(theLevel.scrambleDelCount > 0)
			out.println("ScrambleDelete after " + Math.round(theLevel.scrambleDelTime * 100) + "%<br>");

		if(extra[converter.NAME] != null)
			out.println(extra[converter.NAME] + "<br>");

		out.println("<br>");
		out.println("<b>Player data</b><br>");
		if(theLevel.bombsAtStart <= 0)
			out.println("No bombs at start" + "<br>");
		else if(theLevel.bombsAtStart == 1)
			out.println("1 bomb at start.<br>");
		else
			out.println(theLevel.bombsAtStart + " bombs at start.<br>");

		if(theLevel.range <= 0)
			out.println("No initial Range" + "<br>");
		else if(theLevel.range == 1)
			out.println("Initial mini bombs" + "<br>");
		else
			out.println("Initial range " + theLevel.range + "<br>");

		int nbrOfSpecial = converter.getSpecialBombs();
		if(nbrOfSpecial > 0)
			out.println(nbrOfSpecial + " " + converter.getBombType(theLevel.specialType) + " as special<br>");

		String inVirus = converter.getInitialIll();
		String revVirus = converter.getRevIll();
		if(inVirus != null && revVirus != null && revVirus.equals(inVirus)) {
			out.print("Permanent " + inVirus + "<br>");
		}
		else {
			if(inVirus != null)
				out.println("Initial virus " + inVirus + "<br>");
			if(revVirus != null)
				out.println("Revive virus " + revVirus + "<br>");
		}

		String inExtra = converter.getInitialExtra();
		String revExtra = converter.getRevExtra();
		if(inExtra != null && revExtra != null && inExtra.equals(revExtra)) {
			out.println(inExtra + " as default<br>");
		}
		else {
			if(inExtra != null)
				out.println("Initial extra " + inExtra + "<br>");
			if(revExtra != null)
				out.println("Revive extra " + revExtra + "<br>");
		}

		out.println("<br>");
		out.println("<b>Bombs, clicks and extras</b><br>");
		if(theLevel.defaultType != Level.BOMB_NORMAL)
			out.println(converter.getBombType(theLevel.defaultType) + " as normal bombs<br>");
		if(theLevel.specialType != Level.BOMB_NORMAL)
			out.println(converter.getBombType(theLevel.specialType) + " as special bombs<br>");
		if(theLevel.hiddenType != Level.BOMB_NORMAL)
			out.println(converter.getBombType(theLevel.hiddenType) + " as hidden bombs<br>");

		if(theLevel.bombClick != Level.BOMB_CLICK_NONE)
			out.println(converter.getBombClick(theLevel.bombClick) + "<br>");
		if(theLevel.playerClick != Level.BOMB_CLICK_NONE)
			out.println(converter.getPlayerClick(theLevel.playerClick) + "<br>");
		if(theLevel.wallClick != Level.BOMB_CLICK_NONE)
			out.println(converter.getWallClick(theLevel.wallClick) + "<br>");

		if(theLevel.direction != Level.GO_STOP)
			out.println(converter.getDir() + "<br>");
		if(theLevel.fuseTime != Level.FUSE_NORMAL)
			out.println(converter.getFuseTime() + "<br>");
		String haunt;
		if((haunt = converter.getHauntSpeed()) != null)
			out.println(haunt + "<br>");

		String nasty;
		if((nasty = converter.getNasty()) != null)
			out.println(nasty + "<br>");

		int[] prob = theLevel.getTNTProbs();
		if(prob[Level.PROB_BOMB_EXTRA] != 0)
			out.println("Bomb extras are " + converter.getProbWord(prob[Level.PROB_BOMB_EXTRA]) + "<br>");
		if(prob[Level.PROB_RANGE] != 0)
			out.println("Range extras are " + converter.getProbWord(prob[Level.PROB_RANGE]) + "<br>");
		if(prob[Level.PROB_ILL] != 0)
			out.println("Virus extras are " + converter.getProbWord(prob[Level.PROB_ILL]) + "<br>");
		if(prob[Level.PROB_SPECIAL_EXTRA] != 0)
			out.println("Special extras are " + converter.getProbWord(prob[Level.PROB_SPECIAL_EXTRA]) + "<br>");
		if(prob[Level.PROB_HIDDEN_BOMB] != 0)
			out.println("Hidden bombs are " + converter.getProbWord(prob[Level.PROB_HIDDEN_BOMB]) + "<br>");

		if(theLevel.distribution != Level.DE_NONE)
			out.println(converter.getDistribution() + "<br>");

		out.println("<br><br><br>");
		out.println("<hr>");
		out.println("<font size=\"-1\">Level description created with LevelDoc " + Storch.VERSION + "<br>");
		out.println("LevelDoc is a part of <a href=\"http://storch.sourceforge.net\" target=\"_new\">Storch</a> 2");
		out.println("</font>");
		out.println("</BODY>");
		out.println("</HTML>");
		out.close();
		
		try{
			convertImages();
		}catch(SaveException e){
			throw (SaveException)e.fillInStackTrace();
		}
	}


	/**
	 * Creates the images for the HTML page and saves them in to directory <code>IMAGE_DIR</code>
	 *
	 * @exception SaveException	If unable to convert the images
	 */
	private void convertImages() throws SaveException{
		FileOutputStream fileOut;
		InputStream bildIn;
		
		try{
			bildIn = getClass().getResourceAsStream(Storch.IMAGE_PATH + theLevel.solidWall.getFileName() + ".gif");
			if(bildIn != null) {
				fileOut = new FileOutputStream(IMAGE_DIR + levelName + "_solid.gif");
				GIFEditor solid = new GIFEditor(bildIn, fileOut);
				solid.editColorTable(theLevel.solidWall.getColor0(), theLevel.solidWall.getColor1(), theLevel.solidWall.getColor2());
				fileOut.close();
			}
			bildIn = getClass().getResourceAsStream(Storch.IMAGE_PATH + theLevel.blastableBlock.getFileName() + ".gif");
			if(bildIn != null) {
				fileOut = new FileOutputStream(IMAGE_DIR + levelName + "_blastable.gif");
				GIFEditor blastable = new GIFEditor(bildIn, fileOut);
				blastable.editColorTable(theLevel.blastableBlock.getColor0(), theLevel.blastableBlock.getColor1(), theLevel.blastableBlock.getColor2());
				fileOut.close();
			}
			bildIn = getClass().getResourceAsStream(Storch.IMAGE_PATH + theLevel.freeBlock.getFileName() + ".gif");
			if(bildIn != null) {
				fileOut = new FileOutputStream(IMAGE_DIR + levelName + "_free.gif");
				GIFEditor free = new GIFEditor(bildIn, fileOut);
				free.editColorTable(theLevel.freeBlock.getColor0(), theLevel.freeBlock.getColor1(), theLevel.freeBlock.getColor2());
				fileOut.close();
			}
			bildIn = getClass().getResourceAsStream(Storch.IMAGE_PATH + theLevel.voidBlock.getFileName() + ".gif");
			if(bildIn != null) {
				fileOut = new FileOutputStream(IMAGE_DIR + levelName + "_void.gif");
				GIFEditor voidB = new GIFEditor(bildIn, fileOut);
				voidB.editColorTable(theLevel.voidBlock.getColor0(), theLevel.voidBlock.getColor1(), theLevel.voidBlock.getColor2());
				fileOut.close();
			}
			createStartAndEvilImage();
		}catch(SaveException e){
			throw (SaveException) e.fillInStackTrace();
		}catch(Exception e){
			 throw new SaveException("Unable to convert the images");
		}
		
	}

	/**
	 * Creates the images showing the start position and evil bomb for the HTML page 
	 * and saves them in to directory <code>IMAGE_DIR</code>
	 * 
	 *@exception SaveException	If unable to create the images
	 */
	private void createStartAndEvilImage() throws SaveException{
		try{
			Button obs = new Button();
			MediaTracker tracker = new MediaTracker(obs);
			Image head = Toolkit.getDefaultToolkit().getImage(getClass().getResource(Storch.IMAGE_PATH + "head.gif"));
			Image start = Toolkit.getDefaultToolkit().getImage(IMAGE_DIR + levelName + "_free.gif");
			Image bomb = Toolkit.getDefaultToolkit().getImage(getClass().getResource(Storch.IMAGE_PATH + "evil_bomb.gif"));
			tracker.addImage(start, 1);
			tracker.addImage(head, 2);
			tracker.addImage(bomb, 3);
			BufferedImage outImage = new BufferedImage(28, 21, BufferedImage.TYPE_3BYTE_BGR);
			Graphics g = outImage.getGraphics();
			
			try{
				tracker.waitForAll();
			}catch(Exception e){System.out.println(e);}
			
			g.drawImage(start, 0, 0, obs);
			g.drawImage(head, 5, 2, obs);
			FileOutputStream fileOut = new FileOutputStream(IMAGE_DIR + levelName + "_start.jpg");
			//			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(fileOut);
			//encoder.encode(outImage);
			fileOut.close();
			outImage = new BufferedImage(28, 21, BufferedImage.TYPE_3BYTE_BGR);
			g = outImage.getGraphics();
			g.drawImage(start, 0, 0, obs);
			g.drawImage(bomb, 0, 0, obs);
			fileOut = new FileOutputStream(IMAGE_DIR + levelName + "_evil.jpg");
			//	encoder = JPEGCodec.createJPEGEncoder(fileOut);
			//encoder.encode(outImage);
			fileOut.close();
			
		}catch(Exception e){
			throw new SaveException("Unable to create start and evil images.");
		}
	}
	

	/**
	 * Prints the map data to the file
	 *
	 */
	private void printMap(){
		String path = "images/";
		String s1 = "";
		String alt = "";
        int nbrOfPlayers = theLevel.getNbrOfPlayers();
       
		for(int y = 0; y < 13; y++) {
			for(int x = 0; x < 15; x++) {

				switch (theLevel.map[x][y]) {
					case Level.MAP_SOLID:
						s1 = levelName + "_solid.gif";
						alt = "Solid wall";
						break;
					case Level.MAP_BLASTABLE:
						s1 = levelName + "_blastable.gif";
						alt = "Blastable block";
						break;
					case Level.MAP_FREE:
						s1 = levelName + "_free.gif";
						alt = "Free block";
						break;
					case Level.MAP_EXPLODING_BOMB:
						s1 = levelName+"_evil.jpg";
						alt = "Exploding bomb";
						break;
					case Level.MAP_BOMB_EXTRA:
						s1 = "extra_bomb.gif";
						alt = "Bomb extra";
						break;
					case Level.MAP_RANGE:
						s1 = "extra_range.gif";
						alt = "Range extra";
						break;
					case Level.MAP_ILLNESS:
						s1 = "extra_trap.gif";
						alt = "Illness";
						break;
					case Level.MAP_SPECIAL_EXTRA:
						s1 = extra[converter.PICTURE];
						alt = "Special extra";
						break;
					case Level.MAP_VOID:
						s1 = levelName + "_void.gif";
						alt = "Void block";
						break;
				}
				for(int i = 0; i < nbrOfPlayers; i++) {
                	if(x == theLevel.startPos[i][0] && y == theLevel.startPos[i][1]) {
                		s1 = levelName + "_start.jpg";
						alt = "StartPos";
					}
				}
                	
				out.print("<img src=\"images/" + s1 + "\" border=0 alt=\"" + alt + "\" height=21 width=28>");
			}
			out.println("<br>");
		}
		out.println(theLevel.description + "<br>");
	}


	/**
	 * Prints the ScrambleDraw data to the file
	 */
	private void printScrambleDraw() {
		if(theLevel.scrambleDrawCount > 0) {
			out.println("[scrambleDraw]" + "<br>");
			out.println("time=" + theLevel.scrambleDrawTime + "<br>");
			out.println("numBlocks=" + theLevel.scrambleDrawCount + "<br>");
			String pos;
			for(int i = 0; i < theLevel.scrambleDrawCount; i++) {
				if(i < 10)
					pos = "pos00" + i;
				else if(i < 100)
					pos = "pos0" + i;
				else
					pos = "pos" + i;

				out.println(pos + "=" + theLevel.scrambleDraw[i][0] + " " + theLevel.scrambleDraw[i][1] + "<br>");
			}
		}
		out.println("");
	}


	/**
	 * Prints the ScrambleDelete data to the file
	 */
	private void printScrambleDel() {
		if(theLevel.scrambleDelCount > 0) {
			out.println("[scrambleDel]" + "<br>");
			out.println("time=" + theLevel.scrambleDelTime + "<br>");
			out.println("numBlocks=" + theLevel.scrambleDelCount + "<br>");
			String pos;
			for(int i = 0; i < theLevel.scrambleDelCount; i++) {
				if(i < 10)
					pos = "pos00" + i;
				else if(i < 100)
					pos = "pos0" + i;
				else
					pos = "pos" + i;

				out.println(pos + "=" + theLevel.scrambleDel[i][0] + " " + theLevel.scrambleDel[i][1] + "<br>");
			}
		}
	}
	
	
	
    /**
    * Prints the JavaScript that is used to change the images.
    *
    */
	private void printJavaScriptFunctions(){
		out.println("<script type=\"text/javascript\">");
    	out.println("<!-- ");
    	out.println("solid_image = new Image();");
    	out.println("solid_image.src=\"images/"+levelName + "_solid.gif\";");
    	out.println("free_image = new Image();");
    	out.println("free_image.src=\"images/"+levelName + "_free.gif\";");
		out.println("blastable_image = new Image();");
		out.println("blastable_image.src=\"images/"+levelName + "_blastable.gif\";");
		out.println("evil_bomb = new Image();");
		out.println("evil_bomb.src=\"images/evil_bomb.gif\";");
		out.println("extra_bomb = new Image();");
		out.println("extra_bomb.src=\"images/extra_bomb.gif\";");
		out.println("extra_range = new Image();");
		out.println("extra_range.src=\"images/extra_range.gif\";");
		out.println("extra_trap = new Image();");
		out.println("extra_trap.src=\"images/extra_trap.gif\";");
		out.println("extra_special = new Image();");
		out.println("extra_special.src=\"images/"+ extra[converter.PICTURE]+"\";");
		out.println("void_image = new Image();");
		out.println("void_image.src=\"images/"+levelName + "_void.gif\";");
		out.println("start_image = new Image();");
		out.println("start_image.src=\"images/"+levelName + "_start.jpg\";");
		
		out.print("imagelist = new Array(");
		int[] img = converter.getChangedImages();
		int nbrOfImages = img.length;
		if(nbrOfImages > 0){
			for(int i=0; i<nbrOfImages-1; i++)
				out.print(img[i]+", ");	
			out.println(img[nbrOfImages-1]+");");
			
			if(theLevel.shrinkPattern != Level.SHRINK_VOID || theLevel.scrambleDrawCount > 0 || theLevel.scrambleDelCount > 0){
				out.print("start = new Array(");
				for(int i = 0; i < nbrOfImages-1; i++)
					out.print(converter.getMapImage(img[i], 0)+", ");
				out.println(converter.getMapImage(img[nbrOfImages-1], 0)+");");
			}
			
			if(theLevel.shrinkPattern != Level.SHRINK_VOID){
				out.print("end = new Array(");
				for(int i = 0; i < nbrOfImages-1; i++)
					out.print(converter.getMapImage(img[i], 1)+", ");
				out.println(converter.getMapImage(img[nbrOfImages-1], 1)+");");
			}
			
			
			if(theLevel.scrambleDrawCount > 0){
				out.print("draw = new Array(");
				for(int i = 0; i < nbrOfImages; i++)
					out.print(converter.getMapImage(img[i], theLevel.scrambleDrawTime)+", ");
				out.println(converter.getMapImage(img[nbrOfImages-1], theLevel.scrambleDrawTime)+");");
			}
			
			if(theLevel.scrambleDelCount > 0){
				out.print("del = new Array(");
				for(int i = 0; i < nbrOfImages; i++)
					out.print(converter.getMapImage(img[i], theLevel.scrambleDelTime)+", ");
				out.println(converter.getMapImage(img[nbrOfImages-1], theLevel.scrambleDelTime)+");");
			}
			
			out.println("function startMap(){");
			if(theLevel.shrinkPattern != Level.SHRINK_VOID || theLevel.scrambleDrawCount > 0 || theLevel.scrambleDelCount > 0){
				out.println("\tfor(i=0; i<imagelist.length; i++){");
				out.println("\t\tdocument.images[imagelist[i]].src = start[i].src;");
				out.println("\t}");
			}
			out.println("}");
			
			
			if(theLevel.scrambleDrawCount > 0){
				out.println("function scrambleDraw(){");
				out.println("\tfor(i=0; i<imagelist.length; i++){");
				out.println("\t\tdocument.images[imagelist[i]].src = draw[i].src;");
				out.println("\t}");
				out.println("}");
			}
			
			if(theLevel.scrambleDelCount > 0){
				out.println("function scrambleDel(){");
				out.println("\tfor(i=0; i<imagelist.length; i++){");
				out.println("\t\tdocument.images[imagelist[i]].src = del[i].src;");
				out.println("\t}");
				out.println("}");
			}
			
			if(theLevel.shrinkPattern != Level.SHRINK_VOID){
				out.println("function endMap(){");
				out.println("\tfor(i=0; i<imagelist.length; i++){");
				out.println("\t\tdocument.images[imagelist[i]].src = end[i].src;");
				out.println("\t}");
				out.println("}");
			}
			
		}//nbrOfImages > 0
	
		out.println("-->");
    	out.println("</script>");
		
	}
	
}
