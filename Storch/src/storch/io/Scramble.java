/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.io;

/**
 * This class describes an ScrambleDraw/ScrambleDraw entry
 *
 * @author   Tor Andr�
 */
class Scramble {
    private int[][] data;
    private int count;
    private String name;


    /**
     * Constructor for the Scramble object
     *
     * @param name  The name of the Scramble entry
     */
    protected Scramble(String name) {
        int endPos = name.indexOf('[');
        if(endPos != -1)
            this.name = name.substring(0, endPos);
        else
            this.name = name;
        count = 0;
        data = new int[143][2];
    }


    /**
     * Adds a new block to the ScrambleDraw/ScrambleDelete
     *
     * @param x  The x-coordinate of the block
     * @param y  The y-coordinate of the block
     */
    protected void addData(int x, int y) {
        data[count][0] = x;
        data[count][1] = y;
        count++;
    }


    /**
     * Gets the name attribute of the Scramble object
     *
     * @return   The name of the ScrambleDraw/ScrambleDelete
     */
    protected String getName() {
        return name;
    }


    /**
     * Gets the number of blocks in the Scramble entry
     *
     * @return   The number of blocks
     */
    protected int getCount() {
        return count;
    }


    /**
     * Gets all the block coordinates in the Scramble entry
     *
     * @return   The blocks coordinates
     */
    protected int[][] getData() {
        return data;
    }

}
