/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.io;

import storch.*;
import swingwt.awt.Color;

/**
 * This class is used to convert data from the format used in the <code>Level</code>
 * object into the format used in a HTML file.
 *
 * @author   Tor Andr�
 */
class Level2HTML {
    protected final static int NAME = 0, SKEY = 1, PICTURE = 2;
    private Level theLevel;
    private ShrinkData shrinks;


    /**
     * Constructor for the Level2HTML object
     *
     * @param theLevel  The Level to convert
     */
    protected Level2HTML(Level theLevel) {
        this.theLevel = theLevel;
        shrinks = new ShrinkData();
    }


    /**
     * Gets the specialBombs attribute of the <code>Level</code> object
     *
     * @return   The specialBombs value in HTML format
     */
    protected int getSpecialBombs() {
        int bombs = -1;
        if(theLevel.inGameSpecial == Level.SPECIAL_INIT_SPECIAL_BOMBS_30)
            bombs = 30;
        else if(theLevel.inGameSpecial == Level.SPECIAL_INIT_SPECIAL_BOMBS_12)
            bombs = 12;
        else if(theLevel.inGameSpecial == Level.SPECIAL_INIT_SPECIAL_BOMBS_INFINITY)//EPFL
            bombs = 255;
        return bombs;
    }


    /**
     * Gets the probWord attribute of the <code>Level</code> object
     *
     * @param prob  Description of the Parameter
     * @return      The probWord value in HTML format
     */
    protected String getProbWord(int prob) {
        String word = null;
        if(prob > 0)
            word = "scarce";
        if(prob > (Level.PROB_SCARCE + Level.PROB_RARE) / 2)
            word = "rare";
        if(prob > (Level.PROB_RARE + Level.PROB_UNCOMMON) / 2)
            word = "uncommon";
        if(prob > (Level.PROB_UNCOMMON + Level.PROB_COMMON) / 2)
            word = "common";
        if(prob > (Level.PROB_COMMON + Level.PROB_PLENTIFUL) / 2)
            word = "plentiful";

        return word;
    }


    /**
     * Gets the initialExtra attribute of the <code>Level</code> object
     *
     * @return   The initialExtra value in HTML format
     */
    protected String getInitialExtra() {
        String extra = null;
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_RC)
            extra = "remote control ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_TELEPORT)
            extra = "teleporter ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_AIR)
            extra = "airpump ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_CLOAK)
            extra = "cloak ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_MORPH)
            extra = "morph ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_STOP)//EPFL
            extra = "stop ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_ELECTRIFY)//EPFL
            extra = "electrify ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_FROGGER)//EPFL
            extra = "frogger ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_PHANTOM)//EPFL
            extra = "phantom ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_GHOST)//EPFL
            extra = "ghost ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_THROUGH)//EPFL
            extra = "through ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_REVIVE)//EPFL
            extra = "revive ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_STEAL)//EPFL
            extra = "steal ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_SWAPPOSITION)//EPFL
            extra = "swapposition ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_SWAPCOLOR)//EPFL
            extra = "swapcolor ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_DALEIF)//EPFL
            extra = "daleif ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_CHOICE)//EPFL
            extra = "choice ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_EVILGRAIL)//EPFL
            extra = "evilgrail ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_JUMP)//EPFL
            extra = "jump ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_PLAYERFART)//EPFL
            extra = "playerfart ";
        if(theLevel.inExtra == Level.SPECIAL_EXTRA_PLAYERANDBOMBFART)//EPFL
            extra = "playerandbombfart ";

        if(theLevel.inKick) {
            if(extra == null)
                extra = "kick";
            else
                extra += "and kick";
        }
        return extra;
    }


    /**
     * Gets the revExtra attribute of the <code>Level</code> object
     *
     * @return   The revExtra value in HTML format
     */
    protected String getRevExtra() {
        String extra = null;
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_RC)
            extra = "remote control ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_TELEPORT)
            extra = "teleporter ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_AIR)
            extra = "airpump ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_CLOAK)
            extra = "cloak ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_MORPH)
            extra = "morph ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_STOP)//EPFL
            extra = "stop ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_ELECTRIFY)//EPFL
            extra = "electrify ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_FROGGER)//EPFL
            extra = "frogger ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_PHANTOM)//EPFL
            extra = "phantom ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_GHOST)//EPFL
            extra = "ghost ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_THROUGH)//EPFL
            extra = "through ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_REVIVE)//EPFL
            extra = "phoenix ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_STEAL)//EPFL
            extra = "steal ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_SWAPPOSITION)//EPFL
            extra = "swapposition ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_SWAPCOLOR)//EPFL
            extra = "swapcolor ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_DALEIF)//EPFL
            extra = "daleif ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_CHOICE)//EPFL
            extra = "choice ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_EVILGRAIL)//EPFL
            extra = "evilgrail ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_JUMP)//EPFL
            extra = "jump ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_PLAYERFART)//EPFL
            extra = "playerfart ";
        if(theLevel.revExtra == Level.SPECIAL_EXTRA_PLAYERANDBOMBFART)//EPFL
            extra = "playerandbombfart ";

        if(theLevel.revKick) {
            if(extra == null)
                extra = "kick";
            else
                extra += "and kick";
        }
        return extra;
    }


    /**
     * Gets the revIll attribute of the <code>Level</code> object
     *
     * @return   The revIll value in HTML format
     */
    protected String getRevIll() {
        String s1 = null;
        switch (theLevel.revIllness) {
         case Level.ILL_BOMB:
             s1 = "random bombing";
             break;
         case Level.ILL_SLOW:
             s1 = "slowdown";
             break;
         case Level.ILL_RUN:
             s1 = "running";
             break;
         case Level.ILL_MINI:
             s1 = "mini bombs";
             break;
         case Level.ILL_EMPTY:
             s1 = "no bombs while healthy";
             break;
         case Level.ILL_INVISIBLE:
             s1 = "invisibility";
             break;
         case Level.ILL_MALFUNCTION:
             s1 = "malfunctions";
             break;
         case Level.ILL_REVERSE:
             s1 = "reverse controls";
         case Level.ILL_REVERSE2:
             s1 = "reverse 2 controls";
             break;
         case Level.ILL_TELEPORT:
             s1 = "random teleporting";
             break;
        }
        return s1;
    }


    /**
     * Gets the initialIll attribute of the <code>Level</code> object
     *
     * @return   The initialIll value in HTML format
     */
    protected String getInitialIll() {
        String s1 = null;
        switch (theLevel.inIllness) {
         case Level.ILL_BOMB:
             s1 = "random bombing";
             break;
         case Level.ILL_SLOW:
             s1 = "slowdown";
             break;
         case Level.ILL_RUN:
             s1 = "running";
             break;
         case Level.ILL_MINI:
             s1 = "mini bombs";
             break;
         case Level.ILL_EMPTY:
             s1 = "no bombs while healthy";
             break;
         case Level.ILL_INVISIBLE:
             s1 = "invisibility";
             break;
         case Level.ILL_MALFUNCTION:
             s1 = "malfunctions";
             break;
         case Level.ILL_REVERSE:
             s1 = "reverse controls";
             break;
         case Level.ILL_REVERSE2:
             s1 = "reverse 2 controls";
             break;
         case Level.ILL_TELEPORT:
             s1 = "random teleporting";
             break;
        }
        return s1;
    }


    /**
     * Gets the nasty attribute of the <code>Level</code> object
     *
     * @return   The nasty value in HTML format
     */
    protected String getNasty() {
        String nasty = null;
        if(theLevel.turnGameSpecial == Level.SPECIAL_GAME_NASTY_WALLS || theLevel.inGameSpecial == Level.SPECIAL_INIT_NASTY_WALLS)
            nasty = "The walls launch bombs";
        if(theLevel.inGameSpecial == Level.SPECIAL_INIT_NASTY_WALLS_2)
            nasty = "The walls launch bombs";
        if(theLevel.inGameSpecial == Level.SPECIAL_INIT_FIRE_WALLS)//EPFL
            nasty = "Fire walls";
        if(theLevel.inGameSpecial == Level.SPECIAL_GAME_NASTY_CEIL)//EPFL
            nasty = "The ceiling launch bombs";

        return nasty;
    }


    /**
     * Gets the distribution attribute of the <code>Level</code> object
     *
     * @return   The distribution value in HTML format
     */
    protected String getDistribution() {
        String s1 = "";
        switch (theLevel.distribution) {
         case Level.DE_NONE:
             s1 = "none";
             break;
         case Level.DE_SINGLE:
             s1 = "1 distributed after death";
             break;
         case Level.DE_ALL:
             s1 = "All distributed after death";
             break;
         case Level.DE_SPECIAL:
             s1 = "1 appears after 3 pickups";
             break;
         case Level.DE_GET:
             s1 = "Reappears after pickup";
             break;
         case Level.DE_DOUBLE:
             s1 = "2 appears after 1 pickup";
             break;
        }
        return s1;
    }


    /**
     * Converts a <code>swingwt.awt.Color</code> into a HEX value
     *
     * @param col  The <code>Color</code> to convert
     * @return     The color in HTML format
     */
    protected String getHex(Color col) {
        String hex1 = Integer.toHexString(col.getRGB()).toUpperCase();
        return "#" + hex1.substring(2);
    }


    /**
     * Gets the hauntSpeed attribute of the <code>Level</code> object
     *
     * @return   The hauntSpeed value in HTML format
     */
    protected String getHauntSpeed() {
        String s2 = null;
        if(theLevel.turnGameSpecial == Level.SPECIAL_GAME_HAUNT)
            s2 = "All bombs are haunted";
        if(theLevel.turnGameSpecial == Level.SPECIAL_GAME_HAUNT_FAST)
            s2 = "All bombs are haunted (and dangerous)";

        return s2;
    }


    /**
     * Gets the dir attribute of the <code>Level</code> object
     *
     * @return   The dir value in HTML format
     */
    protected String getDir() {
        String s1 = "";
        switch (theLevel.direction) {
         case Level.GO_STOP:
             s1 = "stop";
             break;
         case Level.GO_DOWN:
             s1 = "Bombs are falling down";
             break;
         case Level.GO_UP:
             s1 = "Bombs are going up";
             break;
         case Level.GO_LEFT:
             s1 = "Bombs are going left";
             break;
         case Level.GO_RIGHT:
             s1 = "Bombs are going right";
             break;
        }
        return s1;
    }


    /**
     * Gets the fuseTime attribute of the <code>Level</code> object
     *
     * @return   The fuseTime value in HTML format
     */
    protected String getFuseTime() {
        String s2 = "";
        switch (theLevel.fuseTime) {
         case Level.FUSE_NORMAL:
             s2 = "normal";
             break;
         case Level.FUSE_SHORT:
             s2 = "All bombs are short fused";
             break;
         case Level.FUSE_LONG:
             s2 = "All bombs are long fused";
             break;
        }
        return s2;
    }


    /**
     * Converts a bombClick value in <code>Level</code> format to HTML format
     *
     * @param type  The bombClick type
     * @return      The bombClick type in HTML format
     */
    protected String getBombClick(int type) {
        String bomb = null;
        switch (type) {
         case Level.BOMB_CLICK_NONE:
             bomb = "none";
             break;
         case Level.BOMB_CLICK_SNOOKER:
             bomb = "Bombs are snooker bombs";
             break;
         case Level.BOMB_CLICK_CONTACT:
             bomb = "Bombs explode on contact with others";
             break;
         case Level.BOMB_CLICK_CLOCKWISE:
             bomb = "Bombs turn clockwise on hitting others";
             break;
         case Level.BOMB_CLICK_ANTICLOCKWISE:
             bomb = "Bombs turn anticlockwise on hitting others";
             break;
         case Level.BOMB_CLICK_RANDOMDIR:
             bomb = "Bombs bounce off randomly from others";
             break;
         case Level.BOMB_CLICK_REBOUND:
             bomb = "Bombs rebound from others";
             break;
         case Level.BOMB_CLICK_SPLIT://EPFL
             bomb = "Bomb splits into 3 on hitting others";
        }
        return bomb;
    }


    /**
     * Converts a playerClick value in <code>Level</code> format to HTML format
     *
     * @param type  The playerClick type
     * @return      The playerClick type in HTML format
     */
    protected String getPlayerClick(int type) {
        String bomb = null;
        switch (type) {
         case Level.BOMB_CLICK_NONE:
             bomb = "none";
             break;
         case Level.BOMB_CLICK_CONTACT:
             bomb = "Bombs explode on contact with players";
             break;
         case Level.BOMB_CLICK_CLOCKWISE:
             bomb = "Bombs turn clockwise on hitting player";
             break;
         case Level.BOMB_CLICK_ANTICLOCKWISE:
             bomb = "Bombs turn anticlockwise on hitting player";
             break;
         case Level.BOMB_CLICK_RANDOMDIR:
             bomb = "Bombs bounce off randomly from player";
             break;
         case Level.BOMB_CLICK_REBOUND:
             bomb = "Bombs rebound off players";
             break;
         case Level.BOMB_CLICK_THRU:
             bomb = "Bombs stun players running through";
             break;
         case Level.BOMB_CLICK_SPLIT://EPFL
             bomb = "Bomb splits into 3 on hitting player";
        }
        return bomb;
    }


    /**
     * Converts a wallClick value in <code>Level</code> format to HTML format
     *
     * @param type  The wallClick type
     * @return      The wallClick type in HTML format
     */
    protected String getWallClick(int type) {
        String bomb = null;
        switch (type) {
         case Level.BOMB_CLICK_NONE:
             bomb = "none";
             break;
         case Level.BOMB_CLICK_CONTACT:
             bomb = "Bombs explode on contact with walls";
             break;
         case Level.BOMB_CLICK_CLOCKWISE:
             bomb = "Bombs turn clockwise on hitting walls";
             break;
         case Level.BOMB_CLICK_ANTICLOCKWISE:
             bomb = "Bombs turn anticlockwise on hitting walls";
             break;
         case Level.BOMB_CLICK_RANDOMDIR:
             bomb = "Bombs bounce off randomly of walls";
             break;
         case Level.BOMB_CLICK_REBOUND:
             bomb = "Bombs rebound off walls";
             break;
         case Level.BOMB_CLICK_SPLIT://EPFL
             bomb = "Bomb splits into 3 on hitting walls";
        }
        return bomb;
    }


    /**
     * Converts a bomb type in <code>Level</code> format to HTML format
     *
     * @param type  The bomb type
     * @return      The bomb type in HTML format
     */
    protected String getBombType(int type) {
        String def = null;

        switch (type) {
         case Level.BOMB_NORMAL:
             def = "Normal";
             break;
         case Level.BOMB_NAPALM:
             def = "Napalm";
             break;
         case Level.BOMB_SEARCH://EPFL
             def = "Search";
             break;
         case Level.BOMB_FIRECRACKER:
             def = "Firecracker";
             break;
         case Level.BOMB_CONSTRUCTION:
             def = "Construction";
             break;
         case Level.BOMB_DESTRUCTION:
             def = "Destruction";
             break;
         case Level.BOMB_RENOVATION:
             def = "Renovation";
             break;
         case Level.BOMB_THREEBOMBS:
             def = "Threebombs in a row";
             break;
         case Level.BOMB_TRIANGLEBOMBS:
             def = "Trianglebombs";
             break;
         case Level.BOMB_GRENADE:
             def = "Grenade";
             break;
         case Level.BOMB_FUNGUS:
             def = "Fungus";
             break;
         case Level.BOMB_PYRO:
             def = "Pyro";
             break;
         case Level.BOMB_RANDOM:
             def = "Random";
             break;
         case Level.BOMB_FIRECRACKER_2://Only in TNT
             def = "Firecracker2";
             break;
         case Level.BOMB_PYRO_2://Only in TNT
             def = "Pyro2";
             break;
         case Level.BOMB_BLASTNOW://Only in TNT
             def = "BlastNow";
             break;
         case Level.BOMB_CLOSE://Only in TNT
             def = "Close bombs";
             break;
         case Level.BOMB_SHORT://Only in TNT
             def = "Short bombs";
             break;
         case Level.BOMB_RING_OF_FIRE://Only in EPFL
             def = "Ring of fire";
             break;
         case Level.BOMB_MINE://Only in EPFL
             def = "Mine";
             break;
         case Level.BOMB_DIAG_THREE_BOMBS://Only in EPFL
             def = "Diagonal 3 bombs";
             break;
         case Level.BOMB_SCISSOR://Only in EPFL
             def = "Scissor bomb";
             break;
         case Level.BOMB_SCISSOR_2://Only in EPFL
             def = "Scissor_2 bomb";
             break;
         case Level.BOMB_PARALLEL://Only in EPFL
             def = "Parallel bombs";
             break;
         case Level.BOMB_DISTANCE://Only in EPFL
             def = "Distance bomb";
             break;
         case Level.BOMB_LUCKY://Only in EPFL
             def = "Lucky bomb";
             break;
         case Level.BOMB_PARASOL://Only in EPFL
             def = "Parasol bomb";
             break;
         case Level.BOMB_COMB://Only in EPFL
             def = "Comb bomb";
             break;
         case Level.BOMB_FARPYRO://Only in EPFL
             def = "Far pyro bomb";
             break;
         case Level.BOMB_NUCLEAR://Only in EPFL
             def = "Nuclear bomb";
             break;
         case Level.BOMB_PROTECTBOMBS://Only in EPFL
             def = "Protect bombs";
             break;
        }
        return def;
    }


    /**
     * Gets the specialExtra attribute of the <code>Level</code> object
     *
     * @return   The specialExtra value in HTML format
     */
    protected String[] getSpecialExtra() {
        String s1 = null;
        String s2 = null;
        String s3 = null;
        String[] res = new String[3];

        switch (theLevel.specialExtra) {
         case Level.SPECIAL_EXTRA_VOID:
             s3 = "extra_void.gif";
             break;
         case Level.SPECIAL_EXTRA_INVINCIBLE:
             s1 = "Invincibility as an extra";
             s3 = "extra_invincible.gif";
             break;
         case Level.SPECIAL_EXTRA_KICK:
             s1 = "Kick bomb as an extra";
             s3 = "extra_kick_bomb.gif";
             break;
         case Level.SPECIAL_EXTRA_TELEPORT:
             s1 = "Teleporter as an extra";
             s2 = "teleport";
             s3 = "extra_q3a_beam.gif";
             break;
         case Level.SPECIAL_EXTRA_RC:
             s1 = "Remote control as an extra";
             s2 = "rc";
             s3 = "extra_remote_control.gif";
             break;
         case Level.SPECIAL_EXTRA_IGNITE_ALL:
             s1 = "Button as an extra (ignite all)";
             s3 = "extra_ignite.gif";
             break;
         case Level.SPECIAL_EXTRA_AIR:
             s1 = "Airpump as an extra";
             s2 = "air";
             s3 = "extra_air_pump.gif";
             break;
         case Level.SPECIAL_EXTRA_SPECIAL_BOMB:
             s1 = getBombType(theLevel.specialType) + " as an extra";
             s2 = "specialBomb";
             int type = theLevel.specialType;
             if(type == Level.BOMB_NORMAL)
                 s3 = "extra_bomb.gif";
             else if(type == Level.BOMB_SEARCH)//EPFL
                 s3 = "extra_search.gif";
             else if(type == Level.BOMB_THREEBOMBS || type == Level.BOMB_TRIANGLEBOMBS)
                 s3 = "extra_triangle_bomb.gif";
             else if(type == Level.BOMB_CONSTRUCTION || type == Level.BOMB_DESTRUCTION || type == Level.BOMB_RENOVATION)
                 s3 = "extra_construction.gif";
             else if(type == Level.BOMB_FIRECRACKER || type == Level.BOMB_PYRO || type == Level.BOMB_FIRECRACKER_2 || type == Level.BOMB_PYRO_2)
                 s3 = "extra_firecracker.gif";
             else
                 s3 = "extra_napalm.gif";
             break;
         case Level.SPECIAL_EXTRA_CLOAK:
             s1 = "Cloak as an extra";
             s2 = "cloak";
             s3 = "extra_cloak.gif";
             break;
         case Level.SPECIAL_EXTRA_JUNKIE:
             s1 = "Junkie virus as an extra";
             s3 = "extra_syringe.gif";
             break;
         case Level.SPECIAL_EXTRA_HOLY_GRAIL:
             s1 = "The Holy Grail as an extra";
             s3 = "extra_holygrail.gif";
             break;
         case Level.SPECIAL_EXTRA_LIFE:
             s1 = "Free life as an extra";
             s3 = "extra_life.gif";
             break;
         case Level.SPECIAL_EXTRA_MAYHEM:
             s1 = "Mayhem (Kick & Run) as an extra";
             s3 = "extra_mayhem.gif";
             break;
         case Level.SPECIAL_EXTRA_SPEED:
             s1 = "Speed as an extra";
             s3 = "extra_speed.gif";
             break;
         case Level.SPECIAL_EXTRA_SPEED2:
             s1 = "Speed2 as an extra";
             s3 = "extra_speed.gif";
             break;
         case Level.SPECIAL_EXTRA_SLOW:
             s1 = "Slow as an extra";
             s3 = "extra_slow.gif";
             break;
         case Level.SPECIAL_EXTRA_STUN_OTHERS:
             s1 = "Spinner as an extra";
             s3 = "extra_pow.gif";
             break;
         case Level.SPECIAL_EXTRA_LONG_STUNNED:
             s1 = "Spinner as an extra";
             s3 = "extra_pow.gif";
             break;
         case Level.SPECIAL_EXTRA_POISON:
             s1 = "Death as an extra";
             s3 = "extra_poison.gif";
             break;
         case Level.SPECIAL_EXTRA_MULTIPLE:
             s1 = "Random special extra";
             s3 = "extra_multiple.gif";
             break;
         case Level.SPECIAL_EXTRA_MORPH:
             s1 = "Morph to bomb as an extra";
             s3 = "extra_morph.gif";
             break;
         case Level.SPECIAL_EXTRA_STOP://EPFL
             s1 = "Stop the bomb as an extra";
             s3 = "extra_stop.gif";
             break;
         case Level.SPECIAL_EXTRA_ELECTRIFY://EPFL
             s1 = "Electrify the others";
             s3 = "extra_electrify.gif";
             break;
         case Level.SPECIAL_EXTRA_FROGGER://EPFL
             s1 = "Jump over Bombs";
             s3 = "extra_frogger.gif";
             break;
         case Level.SPECIAL_EXTRA_PHANTOM://EPFL
             s1 = "...";
             s3 = "extra_phantom.gif";
             break;
         case Level.SPECIAL_EXTRA_GHOST://EPFL
             s1 = "Walk over walls";
             s3 = "extra_ghost.gif";
             break;
         case Level.SPECIAL_EXTRA_THROUGH://EPFL
             s1 = "Walk through bombs";
             s3 = "extra_through.gif";
             break;
         case Level.SPECIAL_EXTRA_REVIVE://EPFL
             s1 = "Revive if somebody will run over your body";
             s3 = "extra_revive.gif";
             break;
         case Level.SPECIAL_EXTRA_STEAL://EPFL
             s1 = "Steal others an extra";
             s3 = "extra_steal.gif";
             break;
         case Level.SPECIAL_EXTRA_SWAPPOSITION://EPFL
             s1 = "Swap positions of other players";
             s3 = "extra_swapposition.gif";
             break;
         case Level.SPECIAL_EXTRA_SWAPCOLOR://EPFL
             s1 = "Swap player colors";
             s3 = "extra_swapcolor.gif";
             break;
         case Level.SPECIAL_EXTRA_DALEIF://EPFL
             s1 = "Kick in other direction with a 50/50 prob.";
             s3 = "extra_daleif.gif";
             break;
         case Level.SPECIAL_EXTRA_CHOICE://EPFL
             s1 = "Chooce your weapons";
             s3 = "extra_choice.gif";
             break;
         case Level.SPECIAL_EXTRA_EVILGRAIL://EPFL
             s1 = "Oh, that's complicated...";
             s3 = "extra_evilgrail.gif";
             break;
         case Level.SPECIAL_EXTRA_JUMP://EPFL
             s1 = "Like air pump, but passes through walls";
             s3 = "extra_jump.gif";
             break;
         case Level.SPECIAL_EXTRA_PLAYERFART://EPFL
             s1 = "fart player away";
             s3 = "extra_fart.gif";
             break;
         case Level.SPECIAL_EXTRA_PLAYERANDBOMBFART://EPFL
             s1 = "fart player and bombs away";
             s3 = "extra_fart.gif";
             break;
        }

        res[NAME] = s1;
        res[SKEY] = s2;
        res[PICTURE] = s3;
        return res;
    }


    /**
     * Gets the shrink type of the <code>Level</code> object
     *
     * @return   The shrink type in HTML format
     */
    protected String getShrink() {
        String s1 = "";
        switch (theLevel.shrinkPattern) {
         case Level.SHRINK_SPIRAL:
             s1 = "Spiral shrinking at half time";
             break;
         case Level.SHRINK_SPEED_SPIRAL:
             s1 = "Fast spiral shrinking at half time";
             break;
         case Level.SHRINK_SPIRAL_PLUS:
             s1 = "Spiral shrinking at half time";
             break;
         case Level.SHRINK_SPIRAL_3:
             s1 = "3 level spiral shrinking at half time";
             break;
         case Level.SHRINK_SPIRAL_23:
             s1 = "Fast spiral23 shrinking at half time";
             break;
         case Level.SHRINK_SPIRAL_LEGO:
             s1 = "1 level spiral shrinking at three quarter of time";
             break;
         case Level.SHRINK_EARLY_SPIRAL:
             s1 = "Spiral shrinking just before half time";
             break;
         case Level.SHRINK_COMPOUND:
             s1 = "Continuous compound shrinking";
             break;
         case Level.SHRINK_COMPOUND_F:
             s1 = "Continuous compound shrinking";
             break;
         case Level.SHRINK_COMPOUND_2_F:
             s1 = "2 level compound shrinking";
             break;
         case Level.SHRINK_LAZY_COMPOUND_F:
             s1 = "Lazy compound shrinking";
             break;
         case Level.SHRINK_COMPOUND_SOLID:
             s1 = "Continuous compound shrinking";
             break;
         case Level.SHRINK_SAVAGE_COMPOUND:
             s1 = "Double continous compound shrinking";
             break;
         case Level.SHRINK_COMPOUND_EXTRA:
             s1 = "Compound shrinking with blastables";
             break;
         case Level.SHRINK_DOWN:
             s1 = "Continuous downward shrinking";
             break;
         case Level.SHRINK_DOWN_F:
             s1 = "Continuous downward shrinking";
             break;
         case Level.SHRINK_QUAD:
             s1 = "Quad shrinking at half time";
             break;
         case Level.SHRINK_CONSTRICT_WAVE:
             s1 = "3 level wave shrink at half time";
             break;
         case Level.SHRINK_OUTWARD_SPIRAL:
             s1 = "Anticlockwise outward spiralling shrink";
             break;
         case Level.SHRINK_DIAG:
             s1 = "Diagonal shrink";
             break;
         case Level.SHRINK_OUTWARD_COMPOUND_EXTRA:
             s1 = "An outward compound shrink (similar to \"Chain Reaction\"'s inward)";
             break;
         case Level.SHRINK_ISTY_COMPOUND_2_F:
             s1 = "2 level compound ISTY shrinking";
             break;
         case Level.SHRINK_ISTY_SPIRAL_3:
             s1 = "3 level spiral shrinking at the beginning";
             break;
        }
        return s1;
    }


    /**
     * Gets the gameMode attribute of the <code>Level</code> object
     *
     * @return   The gameMode value in HTML format
     */
    protected String getGameMode() {
        StringBuffer gameMode = new StringBuffer("R23456STDL");
        if(!theLevel.GM_Random)
            gameMode.setCharAt(0, '-');
        if(!theLevel.GM_2_Player)
            gameMode.setCharAt(1, '-');
        if(!theLevel.GM_3_Player)
            gameMode.setCharAt(2, '-');
        if(!theLevel.GM_4_Player)
            gameMode.setCharAt(3, '-');
        if(!theLevel.GM_5_Player)
            gameMode.setCharAt(4, '-');
        if(!theLevel.GM_6_Player)
            gameMode.setCharAt(5, '-');
        if(!theLevel.GM_Single)
            gameMode.setCharAt(6, '-');
        if(!theLevel.GM_Team)
            gameMode.setCharAt(7, '-');
        if(!theLevel.GM_Double)
            gameMode.setCharAt(8, '-');
        if(!theLevel.GM_LR_Players)
            gameMode.setCharAt(9, '-');

        return gameMode.toString();
    }


    /**
     * Gets the image for the picture <code>nbr</code>.
     *
     * @param nbr   The number of the picture.
     * @param time  The time, in 1/100 of total game time.
     * @return      The name of the image at this position at this time.
     */
    protected String getMapImage(int nbr, double time) {
        String s1 = null;
        int nbrOfPlayers = theLevel.getNbrOfPlayers();
        int x = nbr % 15;
        int y = nbr / 15;

        int map = getMapAtTime(x, y, (int) (time * 100));

        switch (map) {
         case Level.MAP_SOLID:
             s1 = "solid_image";
             break;
         case Level.MAP_BLASTABLE:
             s1 = "blastable_image";
             break;
         case Level.MAP_FREE:
             s1 = "free_image";
             break;
         case Level.MAP_EXPLODING_BOMB:
             s1 = "evil_bomb";
             break;
         case Level.MAP_BOMB_EXTRA:
             s1 = "extra_bomb";
             break;
         case Level.MAP_RANGE:
             s1 = "extra_range";
             break;
         case Level.MAP_ILLNESS:
             s1 = "extra_trap";
             break;
         case Level.MAP_SPECIAL_EXTRA:
             s1 = "extra_special";
             break;
         case Level.MAP_VOID:
             s1 = "void_image";
             break;
        }
        if(time == 0)
            for(int i = 0; i < nbrOfPlayers; i++)
                if(x == theLevel.startPos[i][0] && y == theLevel.startPos[i][1])
                    s1 = "start_image";

        return s1;
    }


    /**
     * Gets the map at a specified time.
     *
     * @param time  The time, in percent of total game time.
     * @param x     Description of the Parameter
     * @param y     Description of the Parameter
     * @return      The map at this time.
     */
    private int getMapAtTime(int x, int y, int time) {
        int drawTime = (int) (theLevel.scrambleDrawTime * 100);
        int delTime = (int) (theLevel.scrambleDelTime * 100);
        int mapInt;
        ShrinkBlockList blockList = shrinks.getShrinkBlocks(theLevel.shrinkPattern, time);
        int solids = blockList.solids.length;
        int voids = blockList.voids.length;
        int extras = blockList.extras.length;
        boolean shrinked = false;

        mapInt = theLevel.map[x][y];

        for(int k = 0; k < solids; k++)
            if(blockList.solids[k] % 15 == x && blockList.solids[k] / 15 == y) {
                mapInt = Level.MAP_SOLID;
                shrinked = true;
            }

        for(int k = 0; k < voids; k++)
            if(blockList.voids[k] % 15 == x && blockList.voids[k] / 15 == y) {
                mapInt = Level.MAP_VOID;
                shrinked = true;
            }

        for(int k = 0; k < extras; k++)
            if(blockList.extras[k] % 15 == x && blockList.extras[k] / 15 == y) {
                mapInt = Level.MAP_SPECIAL_EXTRA;
                shrinked = true;
            }

        if(!shrinked || !shrinkedBetween(x, y, time, drawTime)) {
            if(drawTime <= time && (drawTime <= delTime)) {
                for(int i = 0; i < theLevel.scrambleDrawCount; i++)
                    if(x == theLevel.scrambleDraw[i][0] && y == theLevel.scrambleDraw[i][1])
                        mapInt = Level.MAP_SOLID;
            }
        }

        if(!shrinked || !shrinkedBetween(x, y, time, delTime)) {
            if(delTime <= time) {
                for(int i = 0; i < theLevel.scrambleDelCount; i++)
                    if(x == theLevel.scrambleDel[i][0] && y == theLevel.scrambleDel[i][1])
                        mapInt = Level.MAP_FREE;
            }
        }

        if(!shrinked || !shrinkedBetween(x, y, time, drawTime)) {
            if(drawTime <= time && (drawTime > delTime)) {
                for(int i = 0; i < theLevel.scrambleDrawCount; i++)
                    if(x == theLevel.scrambleDraw[i][0] && y == theLevel.scrambleDraw[i][1])
                        mapInt = Level.MAP_SOLID;
            }
        }
        return mapInt;
    }


    /**
     * Checks if a block (<code>x</code>, <code>y</code>)has been changed by a
     * shrink between <code>tim1</code> and <code>tim1</code>.
     *
     * @param x
     * @param y
     * @param time1
     * @param time2
     * @return       True if the block is changed otherwise false.
     */
    private boolean shrinkedBetween(int x, int y, int time1, int time2) {
        ShrinkBlockList[] blockList = new ShrinkBlockList[2];
        boolean[][] inList = new boolean[2][3];

        blockList[0] = shrinks.getShrinkBlocks(theLevel.shrinkPattern, time1);
        blockList[1] = shrinks.getShrinkBlocks(theLevel.shrinkPattern, time2);

        for(int i = 0; i < 2; i++) {

            for(int k = 0; k < blockList[i].solids.length && !inList[i][0]; k++)
                if(blockList[i].solids[k] % 15 == x && blockList[i].solids[k] / 15 == y) {
                    inList[i][0] = true;
                }
            for(int k = 0; k < blockList[i].voids.length && !inList[i][0] && !inList[i][1]; k++)
                if(blockList[i].voids[k] % 15 == x && blockList[i].voids[k] / 15 == y) {
                    inList[i][1] = true;
                }
            for(int k = 0; k < blockList[i].extras.length && !inList[i][0] && !inList[i][1] && !inList[i][2]; k++)
                if(blockList[i].extras[k] % 15 == x && blockList[i].extras[k] / 15 == y) {
                    inList[i][2] = true;
                }
        }
        return (inList[0][0] != inList[1][0]) || (inList[0][1] != inList[1][1]) || (inList[0][2] != inList[1][2]);
    }


    /**
     * Gets the image numbers for the images that is changed by a scrambleDraw,
     * scrambleDel or a shrink.
     *
     * @return   The changedImages value
     * @retuen   The number of the images.
     */
    protected int[] getChangedImages() {
        int players = theLevel.getNbrOfPlayers();
        ShrinkBlockList blockList = shrinks.getShrinkBlocks(theLevel.shrinkPattern, 100);
        int solids = blockList.solids.length;
        int voids = blockList.voids.length;
        int extras = blockList.extras.length;
        int nbrOfImages = theLevel.scrambleDrawCount + theLevel.scrambleDelCount + players + solids + voids + extras;
        int[] img = new int[nbrOfImages];

        int i = 0;
        for(; i < theLevel.scrambleDrawCount; i++)
            img[i] = theLevel.scrambleDraw[i][0] + 15 * theLevel.scrambleDraw[i][1];

        for(int k = 0; k < theLevel.scrambleDelCount; k++)
            img[i++] = theLevel.scrambleDel[k][0] + 15 * theLevel.scrambleDel[k][1];

        for(int k = 0; k < players; k++)
            img[i++] = theLevel.startPos[k][0] + 15 * theLevel.startPos[k][1];

        for(int k = 0; k < solids; k++)
            img[i++] = blockList.solids[k];

        for(int k = 0; k < voids; k++)
            img[i++] = blockList.voids[k];

        for(int k = 0; k < extras; k++)
            img[i++] = blockList.extras[k];

        //Remove the duplicates
        java.util.Arrays.sort(img);
        int[] tmp = new int[nbrOfImages];

        tmp[0] = img[0];
        int t = 0;
        for(int n = 1; n < nbrOfImages; n++)
            if(tmp[t] != img[n])
                tmp[++t] = img[n];

        int[] retVal = new int[++t];
        for(int n = 0; n < t; n++)
            retVal[n] = tmp[n];

        return retVal;
    }


    /**
     * Prints the contents of an int array. For testing purposes only.
     *
     * @param list  Description of the Parameter
     */
    private static void print(int[] list) {
        for(int i = 0; i < list.length; i++)
            System.out.print(list[i] + " ");
    }

}
