/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.io;

import storch.Level;

/**
 * This class is used to convert data from the Standard (2.6) format into the
 * format used internal in Storch.
 *
 * @author   Tor Andr�
 */
class Std2Level {

    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The 2.6 representation of this attribute
     * @return      The Storch representation
     */
    protected int getInSpecial(String name) {
        int retVal = Level.SPECIAL_INIT_VOID;

        if(name.equals("special_init_special_bombs_30"))
            retVal = Level.SPECIAL_INIT_SPECIAL_BOMBS_30;
        else if(name.equals("special_init_special_bombs_12"))
            retVal = Level.SPECIAL_INIT_SPECIAL_BOMBS_12;
        else if(name.equals("special_init_nasty_walls"))
            retVal = Level.SPECIAL_INIT_NASTY_WALLS;
        else if(name.equals("special_init_nasty_walls_2"))
            retVal = Level.SPECIAL_INIT_NASTY_WALLS_2;
        else if(name.equals("special_init_special_bombs_infinity"))//EPFL
            retVal = Level.SPECIAL_INIT_SPECIAL_BOMBS_INFINITY;
        else if(name.equals("special_init_fire_walls"))//EPFL
            retVal = Level.SPECIAL_INIT_FIRE_WALLS;

        return retVal;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The 2.6 representation of this attribute
     * @return      The Storch representation
     */
    protected int getTurnSpecial(String name) {
        int retVal = Level.SPECIAL_GAME_VOID;

        if(name.equals("special_game_nasty_walls"))
            retVal = Level.SPECIAL_GAME_NASTY_WALLS;
        else if(name.equals("special_game_haunt"))
            retVal = Level.SPECIAL_GAME_HAUNT;
        else if(name.equals("special_game_haunt_fast"))
            retVal = Level.SPECIAL_GAME_HAUNT_FAST;
        else if(name.equals("special_game_nasty_ceil"))//EPFL
            retVal = Level.SPECIAL_GAME_NASTY_CEIL;

        return retVal;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The 2.6 representation of this attribute
     * @return      The Storch representation
     */
    protected int getShrink(String name) {
        int res = Level.SHRINK_VOID;
        if(name != null) {
            if(name.equals("shrink_spiral"))
                res = Level.SHRINK_SPIRAL;
            else if(name.equals("shrink_speed_spiral"))
                res = Level.SHRINK_SPEED_SPIRAL;
            else if(name.equals("shrink_spiral_plus"))
                res = Level.SHRINK_SPIRAL_PLUS;
            else if(name.equals("shrink_spiral_3"))
                res = Level.SHRINK_SPIRAL_3;
            else if(name.equals("shrink_spiral_23"))
                res = Level.SHRINK_SPIRAL_23;
            else if(name.equals("shrink_spiral_lego"))
                res = Level.SHRINK_SPIRAL_LEGO;
            else if(name.equals("shrink_early_spiral"))
                res = Level.SHRINK_EARLY_SPIRAL;
            else if(name.equals("shrink_compound"))
                res = Level.SHRINK_COMPOUND;
            else if(name.equals("shrink_compound_f"))
                res = Level.SHRINK_COMPOUND_F;
            else if(name.equals("shrink_compound_2_f"))
                res = Level.SHRINK_COMPOUND_2_F;
            else if(name.equals("shrink_lazy_compound_f"))
                res = Level.SHRINK_LAZY_COMPOUND_F;
            else if(name.equals("shrink_compound_solid"))
                res = Level.SHRINK_COMPOUND_SOLID;
            else if(name.equals("shrink_savage_compound"))
                res = Level.SHRINK_SAVAGE_COMPOUND;
            else if(name.equals("shrink_compound_extra"))
                res = Level.SHRINK_COMPOUND_EXTRA;
            else if(name.equals("shrink_down"))
                res = Level.SHRINK_DOWN;
            else if(name.equals("shrink_down_f"))
                res = Level.SHRINK_DOWN_F;
            else if(name.equals("shrink_quad"))
                res = Level.SHRINK_QUAD;
            else if(name.equals("shrink_constrict_wave"))
                res = Level.SHRINK_CONSTRICT_WAVE;
            else if(name.equals("shrink_outward_spiral"))
                res = Level.SHRINK_OUTWARD_SPIRAL;
            else if(name.equals("shrink_diag"))//EPFL
                res = Level.SHRINK_DIAG;
            else if(name.equals("shrink_outward_compound_extra"))//EPFL
                res = Level.SHRINK_OUTWARD_COMPOUND_EXTRA;
            else if(name.equals("shrink_isty_compound_2_f"))//EPFL
                res = Level.SHRINK_ISTY_COMPOUND_2_F;
            else if(name.equals("shrink_isty_spiral_3"))//EPFL
                res = Level.SHRINK_ISTY_SPIRAL_3;

        }
        return res;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param s  The 2.6 representation of this attribute
     * @return   The Storch representation
     */
    protected int getMapInt(String s) {
        char c = s.charAt(0);
        switch (c) {
         case '_':
             return Level.MAP_FREE;
         case 'R'://Block Rise
         case 'B':
             return Level.MAP_SOLID;
         case 'X':
             return Level.MAP_BLASTABLE;
         case 'b':
             return Level.MAP_BOMB_EXTRA;
         case 'r':
             return Level.MAP_RANGE;
         case 's':
             return Level.MAP_ILLNESS;
         case 'q':
             return Level.MAP_SPECIAL_EXTRA;
         case 'v':
             return Level.MAP_VOID;
         case 'e':
             return Level.MAP_EXPLODING_BOMB;
         case 'V':// #define V -1
             return Level.MAP_VOID;
        }
        return Level.MAP_FREE;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The 2.6 representation of this attribute
     * @return      The Storch representation
     */
    protected int getDistribution(String name) {

        if(name.equals("DEall"))
            return Level.DE_ALL;
        if(name.equals("DEdouble"))
            return Level.DE_DOUBLE;
        if(name.equals("DEget"))
            return Level.DE_GET;
        if(name.equals("DEsingle"))
            return Level.DE_SINGLE;
        if(name.equals("DEspecial"))
            return Level.DE_SPECIAL;

        return Level.DE_NONE;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The 2.6 representation of this attribute
     * @return      The Storch representation
     */
    protected int getBombClick(String name) {
        if(name.equals("bomb_click_anticlockwise"))
            return Level.BOMB_CLICK_ANTICLOCKWISE;
        if(name.equals("bomb_click_clockwise"))
            return Level.BOMB_CLICK_CLOCKWISE;
        if(name.equals("bomb_click_contact"))
            return Level.BOMB_CLICK_CONTACT;
        if(name.equals("bomb_click_initial"))
            return Level.BOMB_CLICK_INITIAL;
        if(name.equals("bomb_click_randomdir"))
            return Level.BOMB_CLICK_RANDOMDIR;
        if(name.equals("bomb_click_rebound"))
            return Level.BOMB_CLICK_REBOUND;
        if(name.equals("bomb_click_snooker"))
            return Level.BOMB_CLICK_SNOOKER;
        if(name.equals("bomb_click_thru"))
            return Level.BOMB_CLICK_THRU;
        if(name.equals("bomb_click_split"))//EPFL
            return Level.BOMB_CLICK_SPLIT;

        return Level.BOMB_CLICK_NONE;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The 2.6 representation of this attribute
     * @return      The Storch representation
     */
    protected int getBombDir(String name) {
        if(name.equals("GoDown"))
            return Level.GO_DOWN;
        if(name.equals("GoLeft"))
            return Level.GO_LEFT;
        if(name.equals("GoRight"))
            return Level.GO_RIGHT;
        if(name.equals("GoUp"))
            return Level.GO_UP;

        return Level.GO_STOP;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The 2.6 representation of this attribute
     * @return      The Storch representation
     */
    protected int getFuseTime(String name) {
        if(name.equals("FUSElong"))
            return Level.FUSE_LONG;
        if(name.equals("FUSEshort"))
            return Level.FUSE_SHORT;
        return Level.FUSE_NORMAL;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The 2.6 representation of this attribute
     * @return      The Storch representation
     */
    protected int getBombType(String name) {
        if(name.equals("BMTblastnow"))
            return Level.BOMB_BLASTNOW;
        if(name.equals("BMTclose"))
            return Level.BOMB_CLOSE;
        if(name.equals("BMTconstruction"))
            return Level.BOMB_CONSTRUCTION;
        if(name.equals("BMTdestruction"))
            return Level.BOMB_DESTRUCTION;
        if(name.equals("BMTfirecracker"))
            return Level.BOMB_FIRECRACKER;
        if(name.equals("BMTfirecracker2"))
            return Level.BOMB_FIRECRACKER_2;
        if(name.equals("BMTfungus"))
            return Level.BOMB_FUNGUS;
        if(name.equals("BMTgrenade"))
            return Level.BOMB_GRENADE;
        if(name.equals("BMTnapalm"))
            return Level.BOMB_NAPALM;
        if(name.equals("BMTsearch"))//EPFL
            return Level.BOMB_SEARCH;
        if(name.equals("BMTpyro"))
            return Level.BOMB_PYRO;
        if(name.equals("BMTpyro2"))
            return Level.BOMB_PYRO_2;
        if(name.equals("BMTrandom"))
            return Level.BOMB_RANDOM;
        if(name.equals("BMTrenovation"))
            return Level.BOMB_RENOVATION;
        if(name.equals("BMTshort"))
            return Level.BOMB_SHORT;
        if(name.equals("BMTthreebombs"))
            return Level.BOMB_THREEBOMBS;
        if(name.equals("BMTtrianglebombs"))
            return Level.BOMB_TRIANGLEBOMBS;
        if(name.equals("BMTringofire"))
            return Level.BOMB_RING_OF_FIRE;//Only in EPFL
        if(name.equals("BMTmine"))
            return Level.BOMB_MINE;//Only in EPFL
        if(name.equals("BMTdiagthreebombs"))
            return Level.BOMB_DIAG_THREE_BOMBS;//Only in EPFL
        if(name.equals("BMTscissor"))
            return Level.BOMB_SCISSOR;//Only in EPFL
        if(name.equals("BMTscissor2"))
            return Level.BOMB_SCISSOR_2;//Only in EPFL
        if(name.equals("BMTparallel"))
            return Level.BOMB_PARALLEL;//Only in EPFL
        if(name.equals("BMTdistance"))
            return Level.BOMB_DISTANCE;//Only in EPFL
        if(name.equals("BMTlucky"))
            return Level.BOMB_LUCKY;//Only in EPFL
        if(name.equals("BMTparasol"))
            return Level.BOMB_PARASOL;//Only in EPFL
        if(name.equals("BMTcomb"))
            return Level.BOMB_COMB;//Only in EPFL
        if(name.equals("BMTfarpyro"))
            return Level.BOMB_FARPYRO;//Only in EPFL
        if(name.equals("BMTnuclear"))
            return Level.BOMB_NUCLEAR;//Only in EPFL
        if(name.equals("BMTprotectbombs"))
            return Level.BOMB_PROTECTBOMBS;//Only in EPFL

        return Level.BOMB_NORMAL;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The 2.6 representation of this attribute
     * @return      The Storch representation
     */
    protected int getIll(String name) {
        System.out.println("ill "+name);
        if(name.equals("IllBomb"))
            return Level.ILL_BOMB;
        if(name.equals("IllEmpty"))
            return Level.ILL_EMPTY;
        if(name.equals("IllInvisible"))
            return Level.ILL_INVISIBLE;
        if(name.equals("IllMalfunction"))
            return Level.ILL_MALFUNCTION;
        if(name.equals("IllMini"))
            return Level.ILL_MINI;
        if(name.equals("IllReverse"))
            return Level.ILL_REVERSE;
        if(name.equals("IllReverse2"))
            return Level.ILL_REVERSE2;
        if(name.equals("IllRun"))
            return Level.ILL_RUN;
        if(name.equals("IllSlow"))
            return Level.ILL_SLOW;
        if(name.equals("IllTeleport"))
            return Level.ILL_TELEPORT;

        return Level.ILL_HEALTHY;
    }


    /**
     * Gets the internal (Storch) representation used in the <code>Level</code>
     * object.
     *
     * @param name  The 2.6 representation of this attribute
     * @return      The Storch representation
     */
    protected int getSpecialExtra(String name) {
        name = name.trim();
        if(name.equals("special_extra_air"))
            return Level.SPECIAL_EXTRA_AIR;
        if(name.equals("special_extra_cloak"))
            return Level.SPECIAL_EXTRA_CLOAK;
        if(name.equals("special_extra_holy_grail"))
            return Level.SPECIAL_EXTRA_HOLY_GRAIL;
        if(name.equals("special_extra_ignite_all"))
            return Level.SPECIAL_EXTRA_IGNITE_ALL;
        if(name.equals("special_extra_invincible"))
            return Level.SPECIAL_EXTRA_INVINCIBLE;
        if(name.equals("special_extra_junkie"))
            return Level.SPECIAL_EXTRA_JUNKIE;
        if(name.equals("special_extra_kick"))
            return Level.SPECIAL_EXTRA_KICK;
        if(name.equals("special_extra_life"))
            return Level.SPECIAL_EXTRA_LIFE;
        if(name.equals("special_extra_long_stunned"))
            return Level.SPECIAL_EXTRA_LONG_STUNNED;
        if(name.equals("special_extra_mayhem"))
            return Level.SPECIAL_EXTRA_MAYHEM;
        if(name.equals("special_extra_morph"))
            return Level.SPECIAL_EXTRA_MORPH;
        if(name.equals("special_extra_stop"))//EPFL
            return Level.SPECIAL_EXTRA_STOP;
        if(name.equals("special_extra_revive"))//EPFL
            return Level.SPECIAL_EXTRA_REVIVE;
        if(name.equals("special_extra_electrify"))//EPFL
            return Level.SPECIAL_EXTRA_ELECTRIFY;
        if(name.equals("special_extra_frogger"))//EPFL
            return Level.SPECIAL_EXTRA_FROGGER;
        if(name.equals("special_extra_phantom"))//EPFL
            return Level.SPECIAL_EXTRA_PHANTOM;
        if(name.equals("special_extra_ghost"))//EPFL
            return Level.SPECIAL_EXTRA_GHOST;
        if(name.equals("special_extra_through"))//EPFL
            return Level.SPECIAL_EXTRA_THROUGH;
        if(name.equals("special_extra_steal"))//EPFL
            return Level.SPECIAL_EXTRA_STEAL;
        if(name.equals("special_extra_swapposition"))//EPFL
            return Level.SPECIAL_EXTRA_SWAPPOSITION;
        if(name.equals("special_extra_swapcolor"))//EPFL
            return Level.SPECIAL_EXTRA_SWAPCOLOR;
        if(name.equals("special_extra_daleif"))//EPFL
            return Level.SPECIAL_EXTRA_DALEIF;
        if(name.equals("special_extra_choice"))//EPFL
            return Level.SPECIAL_EXTRA_CHOICE;
        if(name.equals("special_extra_evil_grail"))//EPFL
            return Level.SPECIAL_EXTRA_EVILGRAIL;
        if(name.equals("special_extra_jump"))//EPFL
            return Level.SPECIAL_EXTRA_JUMP;
        if(name.equals("special_extra_player_fart"))//EPFL
            return Level.SPECIAL_EXTRA_PLAYERFART;
        if(name.equals("special_extra_player_and_bomb_fart"))//EPFL
            return Level.SPECIAL_EXTRA_PLAYERANDBOMBFART;
        if(name.equals("special_extra_multiple"))
            return Level.SPECIAL_EXTRA_MULTIPLE;
        if(name.equals("special_extra_poison"))
            return Level.SPECIAL_EXTRA_POISON;
        if(name.equals("special_extra_RC"))
            return Level.SPECIAL_EXTRA_RC;
        if(name.equals("special_extra_slow"))
            return Level.SPECIAL_EXTRA_SLOW;
        if(name.equals("special_extra_special_bomb"))
            return Level.SPECIAL_EXTRA_SPECIAL_BOMB;
        if(name.equals("special_extra_speed"))
            return Level.SPECIAL_EXTRA_SPEED;
        if(name.equals("special_extra_speed2"))
            return Level.SPECIAL_EXTRA_SPEED2;
        if(name.equals("special_extra_stun_others"))
            return Level.SPECIAL_EXTRA_STUN_OTHERS;
        if(name.equals("special_extra_teleport"))
            return Level.SPECIAL_EXTRA_TELEPORT;

        return Level.SPECIAL_EXTRA_VOID;
    }
}

