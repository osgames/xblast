/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch.io;

/**
 * This interface is to be implemented by all saver classes in Storch.
 *
 * @author   Tor Andr�
 */
public interface Saver {

	/**
	 * Saves a <code>Level</code> to file.
	 *
	 * @param saveFile           The file to save to.
	 * @param theLevel           The level to save.
	 * @exception SaveException  If unable to save the level.
	 */
	public void save(java.io.File saveFile, storch.Level theLevel) throws SaveException;

}
