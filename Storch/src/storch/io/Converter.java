/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package storch.io;

import storch.*;
import java.io.File;

/**
 * Converts a level from one format to an other.
 *
 * @author   Tor Andr�
 */
public abstract class Converter {
	
	private String toExt;
	private Loader loader;
	private Saver saver;

	public Converter(Loader loader, Saver saver){
		this.loader = loader;
		this.saver = saver;
		if(saver instanceof TNTSaver)
			toExt = ".xal";
		else
			toExt = ".h";
	}
	
	/**
	 * Converts the <code>inFile</code> level.
	 * The new level will be placed in the same directory as the original file.
	 * 
	 * @param inFile			A <code>File</code> containing the level to convert.
	 * @exception LoadException	If unable to load the level from inFile.
	 * @exception SaveException	If unable to save the the new level.	
	 */
	public void convert(File inFile) throws SaveException, LoadException{
		Level theLevel = null;
		String path = "";
		
		try {
			theLevel = loader.load(inFile);
		} catch(LoadException e) {
			throw (LoadException) e.fillInStackTrace();
		}
		
		if(inFile.getParent() != null)
			path = inFile.getParent() + File.separator;
			
		try {
			saver.save(new File(path + theLevel.getFileName() + toExt), theLevel);
		} catch(SaveException e) {
			throw (SaveException) e.fillInStackTrace();
		}
		
	}

}
