/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.io;

import storch.*;
import java.io.*;
import java.util.Vector;

/**
 * This class is used to create the LevelDoc information to a <code>Level</code>
 *
 * @author   Tor Andr�
 */
public class LevelDoc {
	public static String VERSION = Storch.VERSION;
	private String path, name;
	private Vector levels;
	private Loader loader;


	/**
	 * Constructor for the LevelDoc object.
	 *
	 * @param name               The name of the archive.
	 * @param dest               The directory to save the LevelDoc information to.
	 * @exception	If unable create the destination directory or copying the images.
	 */
	public LevelDoc(String name, String dest) throws SaveException {
		levels = new Vector();
		boolean index = true;
		loader = null;
		path = dest;
		this.name = name;
		File toDir = new File(dest + File.separator + "images");

		if(!toDir.mkdirs() && !toDir.exists()) {
			throw new SaveException("Unable to create directory " + dest);
		}

		try {
			copyImages(dest + File.separator + "images" + File.separator);
		} catch(IOException e) {
			System.out.println("Unable to copy images to " + dest + File.separator + "images");
			throw new SaveException("Unable to copy images to " + dest + File.separator + "images");
		}
	}


	/**
	 * Generates LevelDoc information for the level in the <code>File</code>
	 * levelFile.
	 *
	 * @param levelFile          The file to generate LevelDoc information for.
	 * @exception LoadException  If unable the load the level form inFile.
	 * @exception SaveException  If unable to save the LevelDoc inforamtion.
	 */
	public void docLevel(File levelFile) throws SaveException, LoadException {
		Level theLevel = null;
		Converter conv = null;
		File movedOriginalFile = null;
		String originalExt = "";

		if(levelFile.toString().endsWith(".xal")) {
			loader = new TNTLoader();
			conv = new TNT2Std();
			originalExt = ".xal";
		}
		else {
			loader = new StdLoader();
			conv = new Std2TNT();
			originalExt = ".h";
		}

		try {
			File inFile = levelFile;
			theLevel = loader.load(inFile);
			FileInputStream inf = new FileInputStream(inFile);
			movedOriginalFile = new File(path + File.separator + theLevel.getFileName() + originalExt);
			FileOutputStream outf = new FileOutputStream(movedOriginalFile);
			int b = inf.read();
			while(b != -1) {
				outf.write(b);
				b = inf.read();
			}
			inf.close();
			outf.close();

		} catch(Exception e) {
			System.out.println("Unable to load level " + levelFile);
			System.out.println(e);
			e.printStackTrace();
			throw new LoadException("Unable to load level " + levelFile);
		}

		try {
			if(!theLevel.isEPFL()) //EPFL levels can not be in the TNT format
				conv.convert(movedOriginalFile);
		} catch(LoadException e) {
			throw (LoadException) e.fillInStackTrace();
		} catch(SaveException e) {
			throw (SaveException) e.fillInStackTrace();
		}

		try {
			String filname = theLevel.getFileName();
			String ver = parseStorchVersion(levelFile);
			HTMLSaver saver = new HTMLSaver(ver, theLevel.getFileName());
			levels.add(theLevel);
			saver.save(new File(path + File.separator + filname + ".html"), theLevel);
		} catch(IOException e) {
			System.out.println("Unable to save level " + theLevel.getFileName() + ".html");
			System.out.println(e);
			levels.remove(theLevel);
			throw new SaveException("Unable to save level " + theLevel.getFileName() + ".html");
		}
	}


	/**
	 * Creates the index.html and the menu.html files
	 *
	 * @exception SaveException  if unable to save index
	 */
	public void printIndex() throws SaveException {
		try {
			PrintWriter indexOut = new PrintWriter(new BufferedWriter(new FileWriter(path + File.separator + "index.html")));
			indexOut.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\"");
			indexOut.println("\"http://www.w3.org/TR/html4/frameset.dtd\">");
			indexOut.println("<HTML><HEAD>");
			indexOut.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
			indexOut.println("<TITLE>XBlast LevelDoc archive: " + name + "</TITLE></HEAD>");
			indexOut.println("<FRAMESET cols=\"21%,79%\">");
			indexOut.println("<FRAME name=\"left\" src=\"menu.html\" frameborder=\"0\">");
			indexOut.println("<FRAME name=\"level\" src=\"" + ((Level) levels.get(0)).getFileName() + ".html\" frameborder=\"0\">");
			indexOut.println("</FRAMESET>");
			indexOut.println("</HTML>");
			indexOut.close();
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(path + File.separator + "menu.html")));
			out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
			out.println("<HTML><HEAD>");
			out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
			out.println("<TITLE>" + name + " menu</TITLE></HEAD>");
			out.println("<BODY bgcolor=navy text=\"white\" vlink=\"#FFFFFF\" link=\"#FFFFFF\" alink=\"red\">");
			out.println("<h3>" + name + "</h3>");

			for(int n = 0; n < levels.size(); n++) {
				Level aLevel = (Level) levels.get(n);
				out.println("<A href=\"" + aLevel.getFileName() + ".html\" target=\"level\">" + aLevel.title + "</A><br>");
			}
			out.println("</BODY>");
			out.println("</HTML>");
			out.close();
		} catch(IOException e) {

			System.out.println("Unable to save index");
			System.out.println(e);
			throw new SaveException("Unable to save index");
		}
	}


	/**
	 * Parses the levelfile to find the version of Storch (if any) that created the
	 * level.
	 *
	 * @param level            The level <code>File</code>
	 * @return                 The version of Storch that created the level. If the
	 *      level was not created by Storch an empty string is returned
	 * @exception IOException  Description of the Exception
	 */
	private String parseStorchVersion(File level) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(level));
		String line;
		StringBuffer ver = new StringBuffer();
		boolean found = false;

		while((line = in.readLine()) != null && !found) {
			int start = line.indexOf("Storch");
			if(start != -1) {
				found = true;
				int i = start + 7;
				while(i < line.length() && line.charAt(i) != ' ') {
					ver.append(line.charAt(i));
					i++;
				}
			}
		}
		return ver.toString();
	}


	/**
	 * Copies the common images to the <code>dest</code> directory
	 *
	 * @param dest             The directory to copy the images into
	 * @exception IOException  If unable to copy the images
	 */
	private void copyImages(String dest) throws IOException {
		InputStream in;
		FileOutputStream out;
		String[] images = {"extra.gif", "extra_bomb.gif", "extra_cloak.gif", "extra_construction.gif",
				"extra_triangle_bomb.gif", "extra_air_pump.gif", "extra_speed.gif",
				"extra_trap.gif", "extra_holygrail.gif", "extra_ignite.gif",
				"extra_invincible.gif", "extra_life.gif", "extra_mayhem.gif",
				"extra_morph.gif", "extra_multiple.gif", "extra_napalm.gif",
				"extra_poison.gif", "extra_pow.gif", "extra_q3a_beam.gif",
				"extra_range.gif", "extra_remote_control.gif", "extra_slow.gif",
				"extra_syringe.gif", "extra_void.gif", "extra_kick_bomb.gif",
				"extra_firecracker.gif", "evil_bomb.gif",
				"extra_electrify.gif", "extra_frogger.gif", "extra_phantom.gif",
				"extra_ghost.gif", "extra_through.gif", "extra_revive.gif",
				"extra_steal.gif", "extra_swapposition.gif", "extra_swapcolor.gif",
				"extra_daleif.gif", "extra_choice.gif", "extra_evilgrail.gif",
				"extra_jump.gif", "extra_fart.gif", "extra_search.gif"
				};//EPFL
		for(int i = 0; i < images.length; i++) {
			in = getClass().getResourceAsStream(Storch.IMAGE_PATH + images[i]);
			out = new FileOutputStream(dest + images[i]);

			int b = in.read();
			while(b != -1) {
				out.write(b);
				b = in.read();
			}
			in.close();
			out.close();
		}
	}
}
