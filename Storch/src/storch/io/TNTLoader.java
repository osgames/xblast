/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.io;

import java.io.*;
import storch.Level;
import storch.Block;
import swingwt.awt.Color;

/**
 * This class is used to load a level stored in the TNT format.
 *
 * @author   Tor Andr�
 */
public class TNTLoader implements Loader {

    private BufferedReader in;
    private Level theLevel;
    private TNT2Level converter;


    /**
     * Initializes the loader.
     *
     * @param loadFile           The file to load.
     * @exception LoadException  If unable to open the file.
     */
    private void init(File loadFile) throws LoadException {
        try {
            in = new BufferedReader(new FileReader(loadFile));
        } catch(IOException e) {
            throw new LoadException("Unable to read file " + loadFile.getName());
        }

        theLevel = new Level();
        theLevel.shrinkPattern = Level.SHRINK_VOID;
        theLevel.GM_2_Player = theLevel.GM_3_Player = theLevel.GM_4_Player = theLevel.GM_5_Player = theLevel.GM_6_Player = false;
        theLevel.GM_Random = false;
        theLevel.GM_Team = theLevel.GM_Double = theLevel.GM_Single = theLevel.GM_LR_Players = false;
    }


    /**
     * Loads a level stored in the TNT format.
     *
     * @param loadFile           The file to load.
     * @return                   A <code>Level</code> object containing the
     *      loaded data.
     * @exception LoadException  If unable to load the file.
     */
    public Level load(File loadFile) throws LoadException {
        try {
            String line;
            converter = new TNT2Level();
            init(loadFile);
            line = in.readLine();
            while(line != null) {
                parse(line);
                if(in.ready())
                    line = in.readLine();
                else
                    line = null;
            }
        } catch(LoadException e) {
            throw (LoadException) e.fillInStackTrace();
        } catch(IOException e) {
            throw new LoadException("An error occurred while parsing " + loadFile.getName());
        }
        return theLevel;
    }


    /**
     * Parses one <code>String</code> and decides an action.
     *
     * @param line               The string to parse.
     * @return                   False if no action can be taken.
     * @exception LoadException  If an error.
     */
    private boolean parse(String line) throws LoadException {
        if(line == null)
            return true;
        StringBuffer command = new StringBuffer();
        line = line.trim();
        if(line.length() > 2 && line.charAt(0) == '[') {
            int i = 1;
            char c;
            while(i < line.length() && (c = line.charAt(i++)) != ']')
                command.append(c);

            if(command.toString().compareTo("info") == 0)
                info();
            else if(command.toString().equals("shrink"))
                shrink();
            else if(command.toString().equals("scrambleDraw"))
                scrambleDraw();
            else if(command.toString().equals("scrambleDel"))
                scrambleDel();
            else if(command.toString().equals("func"))
                func();
            else if(command.toString().equals("player"))
                player();
            else if(command.toString().equals("bombs"))
                bombs();
            else if(command.toString().equals("graphics"))
                graphics();
            else if(command.toString().equals("map"))
                map();

            return true;
        }
        return false;
    }


    /**
     * Parses the map data.
     *
     * @exception LoadException
     */
    private void map() throws LoadException {
        String line;
        String[] data;
        try {
            line = in.readLine();
            while(!parse(line)) {
                data = getData(line);
                if(data != null) {
                    if(data[0].equals("probBomb"))
                        theLevel.setProb(Level.PROB_BOMB_EXTRA, Integer.parseInt(data[1]));
                    else if(data[0].equals("probRange"))
                        theLevel.setProb(Level.PROB_RANGE, Integer.parseInt(data[1]));
                    else if(data[0].equals("probVirus"))
                        theLevel.setProb(Level.PROB_ILL, Integer.parseInt(data[1]));
                    else if(data[0].equals("probSpecial"))
                        theLevel.setProb(Level.PROB_SPECIAL_EXTRA, Integer.parseInt(data[1]));
                    else if(data[0].equals("probHidden"))
                        theLevel.setProb(Level.PROB_HIDDEN_BOMB, Integer.parseInt(data[1]));
                    else if(data[0].equals("extraDistribution"))
                        theLevel.distribution = converter.getDistribution(data[1]);
                    else if(data[0].equals("row00"))
                        setTheMaze(data[1]);
                }
                line = in.readLine();
            }
        } catch(LoadException e) {
            throw (LoadException) e.fillInStackTrace();
        } catch(Exception e) {
            throw new LoadException("An error occurred while parsing the map of " + theLevel.title);
        }
    }


    /**
     * Sets the theMaze data to the <code>Level</code>
     *
     * @param line               The new theMaze value.
     * @exception LoadException
     */
    private void setTheMaze(String line) throws LoadException {
        String[] data;
        try {
            setLine(0, line);
            for(int i = 1; i < 13; i++) {
                line = in.readLine();
                data = getData(line);
                setLine(i, data[1]);
            }
        } catch(Exception e) {
            throw new LoadException("An error occurred while parsing the map of " + theLevel.title);
        }
    }


    /**
     * Sets one line in the maze.
     *
     * @param y     The number of the row.
     * @param line  Values of that row.
     */
    private void setLine(int y, String line) {
        if(line != null) {
            line = line.trim();
            for(int x = 0; x < line.length(); x++) {
                theLevel.map[x][y] = converter.getMapInt(line.charAt(x));
            }
        }
    }



    /**
     * Gets a <code>Block</code> from a <code>String</code>.
     *
     * @param type               The block type.
     * @param line               The String representation.
     * @return                   The block.
     * @exception LoadException
     */
    private Block getBlock(int type, String line) throws LoadException {
        String name;
        String[] col = new String[3];
        int i = 0;
        while(i < line.length() && !Character.isWhitespace(line.charAt(i)))// != ' ' && line.charAt(i) != '\t')
            i++;
        name = line.substring(0, i);
        for(int n = 0; n < 3; n++) {
            while(i < line.length() && line.charAt(i) != '#')
                i++;
            int colStart = i;
            while(i < line.length() && !Character.isWhitespace(line.charAt(i)))//line.charAt(i) != ' ' && line.charAt(i) != '\t')
                i++;
            col[n] = line.substring(colStart, i);
        }
        Color c0,c1,c2 ;
        if(col[0].equals("")){
            c0 = new Color(255);
        }else{
        c0 = new Color(Integer.decode(col[0]).intValue());
        }
        if(col[1].equals("")){
            c1 = new Color(255);
        }else{
        c1 = new Color(Integer.decode(col[1]).intValue());
        }
        if(col[2].equals("")){
            c2 = new Color(255);
        }else{
        c2 = new Color(Integer.decode(col[2]).intValue());
        }
	
        return new Block(type, name, c0, c1, c2);
    }


    /**
     * Parses the graphics data.
     *
     * @exception LoadException
     */
    private void graphics() throws LoadException {
        String line;
        String[] data;
        try {
            line = in.readLine();
            data = getData(line);
            theLevel.freeBlock = getBlock(Block.FREE_BLOCK, data[1]);
            line = in.readLine();
            data = getData(line);
            theLevel.blastedFloor = getBlock(Block.OTHER_BLOCK, data[1]);
            line = in.readLine();
            data = getData(line);
            theLevel.solidWall = getBlock(Block.SOLID_WALL, data[1]);
            line = in.readLine();
            data = getData(line);
            theLevel.risingWall = getBlock(Block.OTHER_BLOCK, data[1]);
            line = in.readLine();
            data = getData(line);
            theLevel.blastableBlock = getBlock(Block.BLASTABLE_BLOCK, data[1]);
            line = in.readLine();
            data = getData(line);
            theLevel.blastedBlock = getBlock(Block.OTHER_BLOCK, data[1]);
            line = in.readLine();
            line = in.readLine();
            line = in.readLine();
            line = in.readLine();
            data = getData(line);
            theLevel.extraBlock = getBlock(Block.SPECIAL_EXTRA, data[1]);
            line = in.readLine();
            data = getData(line);
            theLevel.voidBlock = getBlock(Block.VOID_BLOCK, data[1]);
            theLevel.shadowedBlock = theLevel.freeBlock;
        } catch(LoadException le) {
            throw (LoadException) le.fillInStackTrace();
        } catch(Exception e) {
             e.printStackTrace();
            throw new LoadException("An error occured while parsing the graphics section of" + theLevel.title);
        }
    }


    /**
     * Parses the bomb data.
     *
     * @exception LoadException
     */
    private void bombs() throws LoadException {
        String line;
        String[] data;
        try {
            line = in.readLine();
            while(!parse(line)) {
                data = getData(line);
                if(data != null) {
                    if(data[0].equals("bombClick"))
                        theLevel.bombClick = converter.getBombClick(data[1]);
                    else if(data[0].equals("wallClick"))
                        theLevel.wallClick = converter.getBombClick(data[1]);
                    else if(data[0].equals("playerClick"))
                        theLevel.playerClick = converter.getBombClick(data[1]);
                    else if(data[0].equals("special"))
                        theLevel.specialType = converter.getBombType(data[1]);
                    else if(data[0].equals("hidden"))
                        theLevel.hiddenType = converter.getBombType(data[1]);
                    else if(data[0].equals("default"))
                        theLevel.defaultType = converter.getBombType(data[1]);
                    else if(data[0].equals("haunt")) {
                        if(data[1].equals("fast"))
                            theLevel.turnGameSpecial = Level.SPECIAL_GAME_HAUNT_FAST;
                        else
                            theLevel.turnGameSpecial = Level.SPECIAL_GAME_HAUNT;
                    }

                    else if(data[0].equals("direction"))
                        theLevel.direction = converter.getBombDir(data[1]);
                    else if(data[0].equals("fuseTime"))
                        theLevel.fuseTime = converter.getFuseTime(data[1]);
                    else if(data[0].equals("nastyGentle")) {
                        if(!data[1].equals("0")) {
                            if(data[1].equals("1"))
                                theLevel.inGameSpecial = Level.SPECIAL_INIT_NASTY_WALLS;
                            else
                                theLevel.inGameSpecial = Level.SPECIAL_INIT_NASTY_WALLS_2;
                            theLevel.turnGameSpecial = Level.SPECIAL_GAME_NASTY_WALLS;
                        }
                    }
                }
                line = in.readLine();
            }
        } catch(LoadException le) {
            throw (LoadException) le.fillInStackTrace();
        } catch(Exception e) {
            throw new LoadException("An error occured while parsing the bomb section of" + theLevel.title);
        }
    }



    /**
     * Parses the player data.
     *
     * @exception LoadException
     */
    private void player() throws LoadException {
        String line;
        String[] data;
        int[] coord;
        try {
            line = in.readLine();
            while(!parse(line)) {
                data = getData(line);
                if(data != null) {
                    if(data[0].equals("bombs"))
                        theLevel.bombsAtStart = Integer.parseInt(data[1].trim());
                    else if(data[0].equals("range"))
                        theLevel.range = Integer.parseInt(data[1].trim());
                    else if(data[0].equals("specialBombs")) {
                        int nbrSpec = Integer.parseInt(data[1]);
                        if(nbrSpec >= 30)
                            theLevel.inGameSpecial = Level.SPECIAL_INIT_SPECIAL_BOMBS_30;
                        else if(nbrSpec > 0)
                            theLevel.inGameSpecial = Level.SPECIAL_INIT_SPECIAL_BOMBS_12;
                    }
                    else
                        for(int i = 1; i <= 6; i++) {
                            if(data[0].equals("pos" + i)) {
                                coord = getCoords(data[1]);
                                theLevel.startPos[i - 1][0] = coord[0];
                                theLevel.startPos[i - 1][1] = coord[1];
                            }
                        }
                    if(data[0].equals("initExtra")) {
                        //System.out.println("initExtra: " +data[1]);
                        int space = data[1].trim().lastIndexOf(' ');
                        int extra1;
                        int extra2;
                        if(space != -1) {
                            //System.out.println(data[1].substring(0,space) +" & "+ data[1].substring(space+1).trim());
                            extra1 = converter.getSpecialExtra(data[1].substring(0, space));
                            extra2 = converter.getSpecialExtra(data[1].substring(++space).trim());
                            theLevel.inKick = true;
                            theLevel.inExtra = (extra1 != Level.SPECIAL_EXTRA_KICK) ? extra1 : extra2;
                            //System.out.println(theLevel.inExtra);
                        }
                        else {
                            theLevel.inExtra = converter.getSpecialExtra(data[1]);
                            if(theLevel.inExtra == Level.SPECIAL_EXTRA_KICK)
                                theLevel.inKick = true;
                        }

                    }
                    else if(data[0].equals("reviveExtra")) {
                        //System.out.println("revExtra: " +data[1]);
                        int space = data[1].trim().lastIndexOf(' ');
                        int extra1;
                        int extra2;
                        if(space != -1) {
                            extra1 = converter.getSpecialExtra(data[1].substring(0, space));
                            extra2 = converter.getSpecialExtra(data[1].substring(++space).trim());
                            theLevel.revKick = true;
                            theLevel.revExtra = (extra1 != Level.SPECIAL_EXTRA_KICK) ? extra1 : extra2;
                        }
                        else {
                            theLevel.revExtra = converter.getSpecialExtra(data[1]);
                            if(theLevel.revExtra == Level.SPECIAL_EXTRA_KICK)
                                theLevel.revKick = true;
                        }
                    }
                    else if(data[0].equals("initVirus"))
                        theLevel.inIllness = converter.getIll(data[1]);
                    else if(data[0].equals("reviveVirus"))
                        theLevel.revIllness = converter.getIll(data[1]);
                }//data != null
                line = in.readLine();
            }//while
        } catch(LoadException le) {
            throw (LoadException) le.fillInStackTrace();
        } catch(Exception e) {
            throw new LoadException("An error occured while parsing the player section of" + theLevel.title);
        }

    }



    /**
     * Parses the func data.
     *
     * @exception LoadException
     */
    private void func() throws LoadException {
        String line;
        String[] data;
        try {
            line = in.readLine();
            while(!parse(line)) {
                data = getData(line);
                if(data != null) {
                    if(data[0].equals("extra"))
                        theLevel.specialExtra = converter.getSpecialExtra(data[1]);
                    //else if(data[0].equals("key"))
                    //theLevel.specKey = converter.getKey(data[1]);
                }
                line = in.readLine();
            }
        } catch(LoadException le) {
            throw (LoadException) le.fillInStackTrace();
        } catch(Exception e) {
            throw new LoadException("An error occured while parsing the func section of" + theLevel.title);
        }
    }


    /**
     * Parses the ScrambleDraw data.
     *
     * @exception LoadException  Description of the Exception
     */
    private void scrambleDraw() throws LoadException {
        String line;
        String[] data;
        int[] coord;
        try {
            line = in.readLine();
            data = getData(line);
            theLevel.scrambleDrawTime = Double.parseDouble(data[1]);
            line = in.readLine();
            data = getData(line);
            theLevel.scrambleDrawCount = Integer.parseInt(data[1]);

            for(int i = 0; i < theLevel.scrambleDrawCount; i++) {
                line = in.readLine();
                data = getData(line);
                coord = getCoords(data[1]);
                theLevel.scrambleDraw[i][0] = coord[0];
                theLevel.scrambleDraw[i][1] = coord[1];
            }
        } catch(LoadException e) {
            throw new LoadException(e.getMessage() + " in the ScrambleDraw section");
        } catch(Exception e) {
            throw new LoadException("An error occured while parsing the ScrambleDraw of" + theLevel.title);
        }
    }


    /**
     * Parses the ScrambleDelete data.
     *
     * @exception LoadException  Description of the Exception
     */
    private void scrambleDel() throws LoadException {
        String line;
        String[] data;
        int[] coord;
        try {
            line = in.readLine();
            data = getData(line);
            theLevel.scrambleDelTime = Double.parseDouble(data[1]);
            line = in.readLine();
            data = getData(line);
            theLevel.scrambleDelCount = Integer.parseInt(data[1]);
            for(int i = 0; i < theLevel.scrambleDelCount; i++) {
                line = in.readLine();
                data = getData(line);
                coord = getCoords(data[1]);
                theLevel.scrambleDel[i][0] = coord[0];
                theLevel.scrambleDel[i][1] = coord[1];
            }
        } catch(LoadException e) {
            throw new LoadException(e.getMessage() + " in the ScrambleDel section");
        } catch(Exception e) {
            throw new LoadException("An error occured while parsing the ScrambleDelete of" + theLevel.title);
        }
    }


    /**
     * Gets the coordinates from a string.
     *
     * @param line               The coordinates in string representation.
     * @return                   The coordinates as int[].
     * @exception LoadException  Description of the Exception
     */
    private int[] getCoords(String line) throws LoadException {
        int[] res = new int[2];
        try {
            line = line.trim();
            int i = 0;
            while(i < line.length() && line.charAt(i) != ' ')
                i++;
            int x = Integer.parseInt(line.substring(0, i).trim());
            int y = Integer.parseInt(line.substring(i + 1, line.length()).trim());
            res[0] = x;
            res[1] = y;
        } catch(Exception e) {
            throw new LoadException("An error occurred while trying to resolve coordinates");
        }
        return res;
    }


    /**
     * Parses the shrink data.
     *
     * @exception LoadException  Description of the Exception
     */
    private void shrink() throws LoadException {
        String line;
        String[] data;
        try {
            line = in.readLine();
            data = getData(line);
            if(data != null && data[0].equals("type"))
                setShrink(data[1]);
        } catch(Exception e) {
            throw new LoadException("An error occurred while parsing the shrink type");
        }
    }


    /**
     * Sets the shrink type to the level.
     *
     * @param name  The name of the shrink.
     */
    private void setShrink(String name) {
        if(name != null) {
            if(name.equals("spiral"))
                theLevel.shrinkPattern = Level.SHRINK_SPIRAL;
            else if(name.equals("speedSpiral"))
                theLevel.shrinkPattern = Level.SHRINK_SPEED_SPIRAL;
            else if(name.equals("spiralPlus"))
                theLevel.shrinkPattern = Level.SHRINK_SPIRAL_PLUS;
            else if(name.equals("spiral3"))
                theLevel.shrinkPattern = Level.SHRINK_SPIRAL_3;
            else if(name.equals("spiral23"))
                theLevel.shrinkPattern = Level.SHRINK_SPIRAL_23;
            else if(name.equals("spiralLego"))
                theLevel.shrinkPattern = Level.SHRINK_SPIRAL_LEGO;
            else if(name.equals("earlySpiral"))
                theLevel.shrinkPattern = Level.SHRINK_EARLY_SPIRAL;
            else if(name.equals("compound"))
                theLevel.shrinkPattern = Level.SHRINK_COMPOUND;
            else if(name.equals("compoundF"))
                theLevel.shrinkPattern = Level.SHRINK_COMPOUND_F;
            else if(name.equals("compound2F"))
                theLevel.shrinkPattern = Level.SHRINK_COMPOUND_2_F;
            else if(name.equals("lazyCompoundF"))
                theLevel.shrinkPattern = Level.SHRINK_LAZY_COMPOUND_F;
            else if(name.equals("compoundSolid"))
                theLevel.shrinkPattern = Level.SHRINK_COMPOUND_SOLID;
            else if(name.equals("savageCompound"))
                theLevel.shrinkPattern = Level.SHRINK_SAVAGE_COMPOUND;
            else if(name.equals("compoundExtra"))
                theLevel.shrinkPattern = Level.SHRINK_COMPOUND_EXTRA;
            else if(name.equals("down"))
                theLevel.shrinkPattern = Level.SHRINK_DOWN;
            else if(name.equals("downF"))
                theLevel.shrinkPattern = Level.SHRINK_DOWN_F;
            else if(name.equals("quad"))
                theLevel.shrinkPattern = Level.SHRINK_QUAD;
            else if(name.equals("constrictWave"))
                theLevel.shrinkPattern = Level.SHRINK_CONSTRICT_WAVE;
            else if(name.equals("outwardSpiral"))
                theLevel.shrinkPattern = Level.SHRINK_OUTWARD_SPIRAL;
        }
    }


    /**
     * Parses the info data.
     *
     * @exception LoadException  Description of the Exception
     */
    private void info() throws LoadException {
        String line;
        String[] data;
        try {
            line = in.readLine();
            while(!parse(line)) {
                data = getData(line);
                if(data != null) {
                    if(data[0].equals("name"))
                        theLevel.title = data[1];
                    else if(data[0].equals("author"))
                        theLevel.author = data[1];
                    else if(data[0].equals("hint"))
                        theLevel.description = data[1];
                    else if(data[0].equals("gameMode"))
                        setGameMode(data[1]);
                }
                line = in.readLine();
            }
        } catch(LoadException le) {
            throw (LoadException) le.fillInStackTrace();
        } catch(Exception e) {
            throw new LoadException("An error occurred while parsing the info section.");
        }
    }



    /**
     * Sets the gameMode attribute of the <code>Level</code> object.
     *
     * @param mode  The gameMode as a String.
     */
    private void setGameMode(String mode) {
        if(mode.lastIndexOf('R') > -1)
            theLevel.GM_Random = true;
        if(mode.lastIndexOf('2') > -1)
            theLevel.GM_2_Player = true;
        if(mode.lastIndexOf('3') > -1)
            theLevel.GM_3_Player = true;
        if(mode.lastIndexOf('4') > -1)
            theLevel.GM_4_Player = true;
        if(mode.lastIndexOf('5') > -1)
            theLevel.GM_5_Player = true;
        if(mode.lastIndexOf('6') > -1)
            theLevel.GM_6_Player = true;
        if(mode.lastIndexOf('S') > -1)
            theLevel.GM_Single = true;
        if(mode.lastIndexOf('T') > -1)
            theLevel.GM_Team = true;
        if(mode.lastIndexOf('D') > -1)
            theLevel.GM_Double = true;
        if(mode.lastIndexOf('L') > -1)
            theLevel.GM_LR_Players = true;
    }


    /**
     * Gets the data from a string. The string should be at the form
     * 'name=value'. The string is splitted up to two stings.
     *
     * @param line  The string to parse.
     * @return      The splitted strings.
     */
    private String[] getData(String line) {
        if(line == null)
            return null;
        int i = 0;
        String[] data = new String[2];
        line = line.trim();
        while(i < line.length() && line.charAt(i) != '=')
            i++;
        if(i < line.length()) {
            data[0] = line.substring(0, i);
            int start2 = ++i;
            while(i < line.length() && line.charAt(i) != ';')
                i++;
            data[1] = line.substring(start2, i);
            return data;
        }
        return null;
    }

}

