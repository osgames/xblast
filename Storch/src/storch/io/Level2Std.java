/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.io;

import storch.Level;
import storch.ColorTable;
import swingwt.awt.Color;

/**
 * This class is used to convert data from the format used in the <code>Level</code>
 * object into the format used in a standard (XBlast 2.6) level file.
 *
 * @author   Tor Andr�
 */
class Level2Std {
    private Level theLevel;

    protected final static int NAME = 0, SKEY = 1, PICTURE = 2;


    /**
     * Constructor for the Level2Std object
     *
     * @param theLevel  The Level to convert
     */
    protected Level2Std(Level theLevel) {
        this.theLevel = theLevel;
    }


    /**
     * Gets the distribution attribute of the <code>Level</code>
     *
     * @return   The distribution value in 2.6 format
     */
    protected String getDistribution() {
        String s1 = "";
        switch (theLevel.distribution) {
         case Level.DE_NONE:
             s1 = "DEnone";
             break;
         case Level.DE_SINGLE:
             s1 = "DEsingle";
             break;
         case Level.DE_ALL:
             s1 = "DEall";
             break;
         case Level.DE_SPECIAL:
             s1 = "DEspecial";
             break;
         case Level.DE_GET:
             s1 = "DEget";
             break;
         case Level.DE_DOUBLE:
             s1 = "DEdouble";
             break;
        }
        return s1;
    }


    /**
     * Gets the dir attribute of the <code>Level</code>
     *
     * @return   The dir value in 2.6 format
     */
    protected String getDir() {
        String s1 = "";
        switch (theLevel.direction) {
         case Level.GO_STOP:
             s1 = "GoStop";
             break;
         case Level.GO_DOWN:
             s1 = "GoDown";
             break;
         case Level.GO_UP:
             s1 = "GoUp";
             break;
         case Level.GO_LEFT:
             s1 = "GoLeft";
             break;
         case Level.GO_RIGHT:
             s1 = "GoRight";
             break;
        }
        return s1;
    }


    /**
     * Gets the fuseTime attribute of the <code>Level</code>
     *
     * @return   The fuseTime value in 2.6 format
     */
    protected String getFuseTime() {
        String s2 = "";
        switch (theLevel.fuseTime) {
         case Level.FUSE_NORMAL:
             s2 = "FUSEnormal";
             break;
         case Level.FUSE_SHORT:
             s2 = "FUSEshort";
             break;
         case Level.FUSE_LONG:
             s2 = "FUSElong";
             break;
        }
        return s2;
    }


    /**
     * Gets the bombTypes attribute of the <code>Level</code>
     *
     * @return   The bombTypes value in 2.6 format
     */
    protected String getBombTypes() {
        String def = getBombName(theLevel.defaultType);
        String spec = getBombName(theLevel.specialType);
        String hidd = getBombName(theLevel.hiddenType);

        return def + ", " + spec + ", " + hidd;
    }


    /**
     * Converts a Storch bombType constant into a Xblast bomb name.
     *
     * @param bombType  The bombType number
     * @return          The bombName
     */
    private String getBombName(int bombType) {
        String hidd = null;

        switch (bombType) {
         case Level.BOMB_NORMAL:
             hidd = "BMTnormal";
             break;
         case Level.BOMB_NAPALM:
             hidd = "BMTnapalm";
             break;
         case Level.BOMB_SEARCH://EPFL
             hidd = "BMTsearch";
             break;
         case Level.BOMB_FIRECRACKER:
             hidd = "BMTfirecracker";
             break;
         case Level.BOMB_FIRECRACKER_2://Only in TNT
             hidd = "BMTfirecracker";
             break;
         case Level.BOMB_CONSTRUCTION:
             hidd = "BMTconstruction";
             break;
         case Level.BOMB_DESTRUCTION:
             hidd = "BMTdestruction";
             break;
         case Level.BOMB_RENOVATION:
             hidd = "BMTrenovation";
             break;
         case Level.BOMB_THREEBOMBS:
             hidd = "BMTthreebombs";
             break;
         case Level.BOMB_TRIANGLEBOMBS:
             hidd = "BMTtrianglebombs";
             break;
         case Level.BOMB_GRENADE:
             hidd = "BMTgrenade";
             break;
         case Level.BOMB_FUNGUS:
             hidd = "BMTfungus";
             break;
         case Level.BOMB_PYRO:
             hidd = "BMTpyro";
             break;
         case Level.BOMB_PYRO_2://Only in TNT
             hidd = "BMTpyro";
             break;
         case Level.BOMB_RANDOM:
             hidd = "BMTrandom";
             break;
         case Level.BOMB_RING_OF_FIRE://Only in EPFL
             hidd = "BMTringofire";
             break;
         case Level.BOMB_MINE://Only in EPFL
             hidd = "BMTmine";
             break;
         case Level.BOMB_DIAG_THREE_BOMBS://Only in EPFL
             hidd = "BMTdiagthreebombs";
             break;
         case Level.BOMB_SCISSOR://Only in EPFL
             hidd = "BMTscissor";
             break;
         case Level.BOMB_SCISSOR_2://Only in EPFL
             hidd = "BMTscissor2";
             break;
         case Level.BOMB_PARALLEL://Only in EPFL
             hidd = "BMTparallel";
             break;
         case Level.BOMB_DISTANCE://Only in EPFL
             hidd = "BMTdistance";
             break;
         case Level.BOMB_LUCKY://Only in EPFL
             hidd = "BMTlucky";
             break;
         case Level.BOMB_PARASOL://Only in EPFL
             hidd = "BMTparasol";
             break;
         case Level.BOMB_COMB://Only in EPFL
             hidd = "BMTcomb";
             break;
         case Level.BOMB_FARPYRO://Only in EPFL
             hidd = "BMTfarpyro";
             break;
         case Level.BOMB_NUCLEAR://Only in EPFL
             hidd = "BMTnuclear";
             break;
         case Level.BOMB_PROTECTBOMBS://Only in EPFL
             hidd = "BMTprotectbombs";
             break;
         default:
             hidd = "BMTnormal";
        }

        return hidd;
    }


    /**
     * Gets the bombClicks attribute of the <code>Level</code>
     *
     * @return   The bombClicks value in 2.6 format
     */
    protected String getBombClicks() {
        String bomb = "errorInSave";
        String wall = "errorInSave";
        String player = "errorInSave";
        switch (theLevel.bombClick) {
         case Level.BOMB_CLICK_NONE:
             bomb = "bomb_click_none";
             break;
         case Level.BOMB_CLICK_SNOOKER:
             bomb = "bomb_click_snooker";
             break;
         case Level.BOMB_CLICK_CONTACT:
             bomb = "bomb_click_contact";
             break;
         case Level.BOMB_CLICK_CLOCKWISE:
             bomb = "bomb_click_clockwise";
             break;
         case Level.BOMB_CLICK_ANTICLOCKWISE:
             bomb = "bomb_click_anticlockwise";
             break;
         case Level.BOMB_CLICK_RANDOMDIR:
             bomb = "bomb_click_randomdir";
             break;
         case Level.BOMB_CLICK_REBOUND:
             bomb = "bomb_click_rebound";
             break;
         case Level.BOMB_CLICK_SPLIT://EPFL
             bomb = "bomb_click_split";
        }

        switch (theLevel.wallClick) {
         case Level.BOMB_CLICK_NONE:
             wall = "bomb_click_none";
             break;
         case Level.BOMB_CLICK_CONTACT:
             wall = "bomb_click_contact";
             break;
         case Level.BOMB_CLICK_REBOUND:
             wall = "bomb_click_rebound";
             break;
         case Level.BOMB_CLICK_CLOCKWISE:
             wall = "bomb_click_clockwise";
             break;
         case Level.BOMB_CLICK_ANTICLOCKWISE:
             wall = "bomb_click_anticlockwise";
             break;
         case Level.BOMB_CLICK_RANDOMDIR:
             wall = "bomb_click_randomdir";
             break;
         case Level.BOMB_CLICK_SPLIT://EPFL
             wall = "bomb_click_split";
        }

        switch (theLevel.playerClick) {
         case Level.BOMB_CLICK_NONE:
             player = "bomb_click_none";
             break;
         case Level.BOMB_CLICK_CONTACT:
             player = "bomb_click_contact";
             break;
         case Level.BOMB_CLICK_THRU:
             player = "bomb_click_thru";
             break;
         case Level.BOMB_CLICK_REBOUND:
             player = "bomb_click_rebound";
             break;
         case Level.BOMB_CLICK_CLOCKWISE:
             player = "bomb_click_clockwise";
             break;
         case Level.BOMB_CLICK_ANTICLOCKWISE:
             player = "bomb_click_anticlockwise";
             break;
         case Level.BOMB_CLICK_RANDOMDIR:
             player = "bomb_click_randomdir";
             break;
         case Level.BOMB_CLICK_SPLIT://EPFL
             player = "bomb_click_split";
        }

        return bomb + ", " + wall + ", " + player;
    }


    /**
     * Gets the revIll attribute of the <code>Level</code>
     *
     * @return   The revIll value in 2.6 format
     */
    protected String getRevIll() {
        String s1 = "";
        switch (theLevel.revIllness) {
         case Level.ILL_HEALTHY:
             s1 = "Healthy";
             break;
         case Level.ILL_BOMB:
             s1 = "IllBomb";
             break;
         case Level.ILL_SLOW:
             s1 = "IllSlow";
             break;
         case Level.ILL_RUN:
             s1 = "IllRun";
             break;
         case Level.ILL_MINI:
             s1 = "IllMini";
             break;
         case Level.ILL_EMPTY:
             s1 = "IllEmpty";
             break;
         case Level.ILL_INVISIBLE:
             s1 = "IllInvisible";
             break;
         case Level.ILL_MALFUNCTION:
             s1 = "IllMalfunction";
             break;
         case Level.ILL_REVERSE:
             s1 = "IllReverse";
             break;
         case Level.ILL_REVERSE2:
             s1 = "IllReverse2";
             break;
         case Level.ILL_TELEPORT:
             s1 = "IllTeleport";
             break;
        }
        return s1;
    }


    /**
     * Gets the initialIll attribute of the <code>Level</code>
     *
     * @return   The initialIll value in 2.6 format
     */
    protected String getInitialIll() {
        String s1 = "";
        switch (theLevel.inIllness) {
         case Level.ILL_HEALTHY:
             s1 = "Healthy";
             break;
         case Level.ILL_BOMB:
             s1 = "IllBomb";
             break;
         case Level.ILL_SLOW:
             s1 = "IllSlow";
             break;
         case Level.ILL_RUN:
             s1 = "IllRun";
             break;
         case Level.ILL_MINI:
             s1 = "IllMini";
             break;
         case Level.ILL_EMPTY:
             s1 = "IllEmpty";
             break;
         case Level.ILL_INVISIBLE:
             s1 = "IllInvisible";
             break;
         case Level.ILL_MALFUNCTION:
             s1 = "IllMalfunction";
             break;
         case Level.ILL_REVERSE:
             s1 = "IllReverse";
             break;
         case Level.ILL_TELEPORT:
             s1 = "IllTeleport";
             break;
        }
        return s1;
    }


    /**
     * Gets the initialExtra attribute of the <code>Level</code>
     *
     * @return   The initialExtra value in 2.6 format
     */
    protected String getInitialExtra() {
        String result = new String("LF_None");
        switch (theLevel.inExtra) {
         case Level.SPECIAL_EXTRA_VOID:
             result = "LF_None";
             break;
         case Level.SPECIAL_EXTRA_RC:
             result = "LF_RC";
             break;
         case Level.SPECIAL_EXTRA_TELEPORT:
             result = "LF_Teleport";
             break;
         case Level.SPECIAL_EXTRA_AIR:
             result = "LF_Airpump";
             break;
         case Level.SPECIAL_EXTRA_CLOAK:
             result = "LF_Cloak";
             break;
         case Level.SPECIAL_EXTRA_MORPH:
             result = "LF_Morph";
             break;
         case Level.SPECIAL_EXTRA_STOP://EPFL
             result = "LF_Stop";
             break;
         case Level.SPECIAL_EXTRA_FROGGER://EPFL
             result = "LF_Frog";
             break;
         case Level.SPECIAL_EXTRA_PHANTOM://EPFL
             result = "LF_Phantom";
             break;
         case Level.SPECIAL_EXTRA_REVIVE://EPFL
             result = "LF_Revive";
             break;
         case Level.SPECIAL_EXTRA_DALEIF://EPFL
             result = "LF_Daleif";
             break;
         case Level.SPECIAL_EXTRA_CHOICE://EPFL
             result = "LF_Choice";
             break;
         case Level.SPECIAL_EXTRA_PLAYERFART://EPFL
             result = "LF_Fart";
             break;
         case Level.SPECIAL_EXTRA_PLAYERANDBOMBFART://EPFL
             result = "LF_Bfart";
             break;
        }
        if(theLevel.inExtra == theLevel.revExtra) {
            StringBuffer res = new StringBuffer(result);
            res.setCharAt(0, 'I');
            result = res.toString();
        }
        else if(theLevel.inExtra == Level.SPECIAL_EXTRA_VOID) {
            switch (theLevel.revExtra) {
             case Level.SPECIAL_EXTRA_VOID:
                 result = "RF_None";
                 break;
             case Level.SPECIAL_EXTRA_RC:
                 result = "RF_RC";
                 break;
             case Level.SPECIAL_EXTRA_TELEPORT:
                 result = "RF_Teleport";
                 break;
             case Level.SPECIAL_EXTRA_AIR:
                 result = "RF_Airpump";
                 break;
             case Level.SPECIAL_EXTRA_CLOAK:
                 result = "RF_Cloak";
                 break;
             case Level.SPECIAL_EXTRA_MORPH:
                 result = "RF_Morph";
                 break;
             case Level.SPECIAL_EXTRA_STOP://EPFL
                 result = "RF_Stop";
                 break;
             case Level.SPECIAL_EXTRA_FROGGER://EPFL
                 result = "RF_Frog";
                 break;
             case Level.SPECIAL_EXTRA_PHANTOM://EPFL
                 result = "RF_Phantom";
                 break;
             case Level.SPECIAL_EXTRA_REVIVE://EPFL
                 result = "RF_Revive";
                 break;
             case Level.SPECIAL_EXTRA_DALEIF://EPFL
                 result = "RF_Daleif";
                 break;
             case Level.SPECIAL_EXTRA_CHOICE://EPFL
                 result = "RF_Choice";
                 break;
             case Level.SPECIAL_EXTRA_PLAYERFART://EPFL
                 result = "RF_Fart";
                 break;
             case Level.SPECIAL_EXTRA_PLAYERANDBOMBFART://EPFL
                 result = "RF_Bfart";
                 break;
            }
        }

        if(theLevel.inKick && theLevel.revKick) {
            if(result == "IF_None" || result == "")
                result = "IF_Kick";
            else
                result = result + " | IF_Kick";
        }
        else {
            if(theLevel.inKick) {
                if(result == "LF_None" || result == "")
                    result = "LF_Kick";
                else
                    result = result + " | LF_Kick";
            }
            else
                    if(theLevel.revKick) {
                if(result == "IF_None" || result == "")
                    result = "RF_Kick";
                else
                    result = result + " | RF_Kick";
            }
        }
        return result;
    }


    /**
     * Gets the specialExtra attribute of the <code>Level</code>
     *
     * @return   The specialExtra value in 2.6 format
     */
    protected String[] getSpecialExtra() {
        String s1 = "";
        String s2 = "";
        String s3 = "";
        String[] res = new String[3];

        switch (theLevel.specialExtra) {
         case Level.SPECIAL_EXTRA_VOID:
             s1 = "special_extra_void";
             s2 = "special_key_void";
             s3 = "EXTRA_BOMB";
             break;
         case Level.SPECIAL_EXTRA_INVINCIBLE:
             s1 = "special_extra_invincible";
             s2 = "special_key_void";
             s3 = "EXTRA_INVINC";
             break;
         case Level.SPECIAL_EXTRA_KICK:
             s1 = "special_extra_kick";
             s2 = "special_key_void";
             s3 = "EXTRA_KICK";
             break;
         case Level.SPECIAL_EXTRA_TELEPORT:
             s1 = "special_extra_teleport";
             s2 = "special_key_teleport";
             s3 = "EXTRA_BEAM";
             break;
         case Level.SPECIAL_EXTRA_RC:
             s1 = "special_extra_RC";
             s2 = "special_key_RC";
             s3 = "EXTRA_RC";
             break;
         case Level.SPECIAL_EXTRA_IGNITE_ALL:
             s1 = "special_extra_ignite_all";
             s2 = "special_key_void";
             s3 = "EXTRA_BUTTON";
             break;
         case Level.SPECIAL_EXTRA_AIR:
             s1 = "special_extra_air";
             s2 = "special_key_air";
             s3 = "EXTRA_AIRPUMP";
             break;
         case Level.SPECIAL_EXTRA_SPECIAL_BOMB:
             s1 = "special_extra_special_bomb";
             s2 = "special_key_special_bomb";
             int type = theLevel.specialType;
             if(type == Level.BOMB_NORMAL)
                 s3 = "EXTRA_BOMB";
             else if(type == Level.BOMB_SEARCH)//EPFL
                 s3 = "EXTRA_SEARCH";
             else if(type == Level.BOMB_THREEBOMBS || type == Level.BOMB_TRIANGLEBOMBS)
                 s3 = "EXTRA_TRIANGLE";
             else if(type == Level.BOMB_CONSTRUCTION || type == Level.BOMB_DESTRUCTION || type == Level.BOMB_RENOVATION || type == Level.BOMB_PROTECTBOMBS)
                 s3 = "EXTRA_CONSTR";
             else if(type == Level.BOMB_FIRECRACKER || type == Level.BOMB_PYRO || type == Level.BOMB_FIRECRACKER_2 || type == Level.BOMB_PYRO_2 || type == Level.BOMB_RING_OF_FIRE || type == Level.BOMB_FARPYRO)
                 s3 = "EXTRA_FIRECRACKER";
             else if(type == Level.BOMB_MINE)//EPFL
                 s3 = "EXTRA_MINES";
             else
                 s3 = "EXTRA_NAPALM";
             break;
         case Level.SPECIAL_EXTRA_CLOAK:
             s1 = "special_extra_cloak";
             s2 = "special_key_cloak";
             s3 = "EXTRA_CLOAK";
             break;
         case Level.SPECIAL_EXTRA_JUNKIE:
             s1 = "special_extra_junkie";
             s2 = "special_key_void";
             s3 = "EXTRA_SYRINGE";
             break;
         case Level.SPECIAL_EXTRA_HOLY_GRAIL:
             s1 = "special_extra_holy_grail";
             s2 = "special_key_void";
             s3 = "EXTRA_HOLYGRAIL";
             break;
         case Level.SPECIAL_EXTRA_LIFE:
             s1 = "special_extra_life";
             s2 = "special_key_void";
             s3 = "EXTRA_LIFE";
             break;
         case Level.SPECIAL_EXTRA_MAYHEM:
             s1 = "special_extra_mayhem";
             s2 = "special_key_void";
             s3 = "EXTRA_MAYHEM";
             break;
         case Level.SPECIAL_EXTRA_SPEED:
             s1 = "special_extra_speed";
             s2 = "special_key_void";
             s3 = "EXTRA_SPEED";
             break;
         case Level.SPECIAL_EXTRA_SLOW:
             s1 = "special_extra_slow";
             s2 = "special_key_void";
             s3 = "EXTRA_SLOW";
             break;
         case Level.SPECIAL_EXTRA_STUN_OTHERS:
             s1 = "special_extra_stun_others";
             s2 = "special_key_void";
             s3 = "EXTRA_POW";
             break;
         case Level.SPECIAL_EXTRA_LONG_STUNNED:
             s1 = "special_extra_long_stunned";
             s2 = "special_key_void";
             s3 = "EXTRA_POW";
             break;
         case Level.SPECIAL_EXTRA_POISON:
             s1 = "special_extra_poison";
             s2 = "special_key_void";
             s3 = "EXTRA_POISON";
             break;
         case Level.SPECIAL_EXTRA_MULTIPLE:
             s1 = "special_extra_multiple";
             s2 = "special_key_void";
             s3 = "EXTRA_MULTIPLE";
             break;
         case Level.SPECIAL_EXTRA_MORPH:
             s1 = "special_extra_morph";
             s2 = "special_key_morph";
             s3 = "EXTRA_MORPH";
             break;
         case Level.SPECIAL_EXTRA_STOP://EPFL
             s1 = "special_extra_stop";
             s2 = "special_key_stop";
             s3 = "EXTRA_STOP";
             break;
         case Level.SPECIAL_EXTRA_ELECTRIFY://EPFL
             s1 = "special_extra_electrify";
             s2 = "special_key_electriify";
             s3 = "EXTRA_ELECTRIFY";
             break;
         case Level.SPECIAL_EXTRA_FROGGER://EPFL
             s1 = "special_extra_frogger";
             s2 = "special_key_frogger";
             s3 = "EXTRA_FROG";
             break;
         case Level.SPECIAL_EXTRA_PHANTOM://EPFL
             s1 = "special_extra_phantom";
             s2 = "special_key_void";
             s3 = "EXTRA_PHANTOM";
             break;
         case Level.SPECIAL_EXTRA_GHOST://EPFL
             s1 = "special_extra_ghost";
             s2 = "special_key_void";
             s3 = "EXTRA_GHOST";
             break;
         case Level.SPECIAL_EXTRA_THROUGH://EPFL
             s1 = "special_extra_through";
             s2 = "special_key_through";
             s3 = "EXTRA_THROUGH";
             break;
         case Level.SPECIAL_EXTRA_REVIVE://EPFL
             s1 = "special_extra_revive";
             s2 = "special_key_void";
             s3 = "EXTRA_REVIVE";
             break;
         case Level.SPECIAL_EXTRA_STEAL://EPFL
             s1 = "special_extra_steal";
             s2 = "special_key_void";
             s3 = "EXTRA_STEAL";
             break;
         case Level.SPECIAL_EXTRA_SWAPPOSITION://EPFL
             s1 = "special_extra_swapposition";
             s2 = "special_key_void";
             s3 = "EXTRA_SWAPPOSITION";
             break;
         case Level.SPECIAL_EXTRA_SWAPCOLOR://EPFL
             s1 = "special_extra_swapcolor";
             s2 = "special_key_void";
             s3 = "EXTRA_SWAPCOLOR";
             break;
         case Level.SPECIAL_EXTRA_DALEIF://EPFL
             s1 = "special_extra_daleif";
             s2 = "special_key_suck";
             s3 = "EXTRA_DALEIF";
             break;
         case Level.SPECIAL_EXTRA_CHOICE://EPFL
             s1 = "special_extra_choice";
             s2 = "special_key_choice";
             s3 = "EXTRA_CHOICE";
             break;
         case Level.SPECIAL_EXTRA_EVILGRAIL://EPFL
             s1 = "special_extra_evil_grail";
             s2 = "special_key_void";
             s3 = "EXTRA_EVILGRAIL";
             break;
         case Level.SPECIAL_EXTRA_JUMP://EPFL
             s1 = "special_extra_jump";
             s2 = "special_key_jump";
             s3 = "EXTRA_JUMP";
             break;
         case Level.SPECIAL_EXTRA_PLAYERFART://EPFL
             s1 = "special_extra_player_fart";
             s2 = "special_key_player_fart";
             s3 = "EXTRA_FART";
             break;
         case Level.SPECIAL_EXTRA_PLAYERANDBOMBFART://EPFL
             s1 = "special_extra_player_and_bomb_fart";
             s2 = "special_key_player_and_bomb_fart";
             s3 = "EXTRA_FART";
             break;
        }

        if(s2.equals("special_key_void") && (theLevel.inGameSpecial == Level.SPECIAL_INIT_SPECIAL_BOMBS_30 || theLevel.inGameSpecial == Level.SPECIAL_INIT_SPECIAL_BOMBS_12))
            s2 = "special_key_special_bomb";

        if(s2 == "special_key_void") {
            if(theLevel.inExtra == Level.SPECIAL_EXTRA_RC || theLevel.revExtra == Level.SPECIAL_EXTRA_RC)
                s2 = "special_key_RC";
            if(theLevel.inExtra == Level.SPECIAL_EXTRA_TELEPORT || theLevel.revExtra == Level.SPECIAL_EXTRA_TELEPORT)
                s2 = "special_key_teleport";
            if(theLevel.inExtra == Level.SPECIAL_EXTRA_AIR || theLevel.revExtra == Level.SPECIAL_EXTRA_AIR)
                s2 = "special_key_air";
            if(theLevel.inExtra == Level.SPECIAL_EXTRA_CLOAK || theLevel.revExtra == Level.SPECIAL_EXTRA_CLOAK)
                s2 = "special_key_cloak";
        }

        if(theLevel.inExtra == Level.SPECIAL_EXTRA_RC || theLevel.revExtra == Level.SPECIAL_EXTRA_RC || theLevel.specialExtra == Level.SPECIAL_EXTRA_RC)
            s2 = "special_key_RC";

        res[NAME] = s1;
        res[SKEY] = s2;
        res[PICTURE] = s3;
        return res;
    }


    /**
     * Gets the turnGame attribute of the <code>Level</code>
     *
     * @return   The turnGame value in 2.6 format
     */
    protected String getTurnGame() {
        String s1 = "";
        switch (theLevel.turnGameSpecial) {
         case Level.SPECIAL_GAME_VOID:
             s1 = "special_game_void";
             break;
         case Level.SPECIAL_GAME_NASTY_WALLS:
             s1 = "special_game_nasty_walls";
             break;
         case Level.SPECIAL_GAME_HAUNT:
             s1 = "special_game_haunt";
             break;
         case Level.SPECIAL_GAME_HAUNT_FAST:
             s1 = "special_game_haunt_fast";
             break;
         case Level.SPECIAL_GAME_NASTY_CEIL://EPFL
             s1 = "special_game_nasty_ceil";
        }
        return s1;
    }


    /**
     * Gets the initGame attribute of the <code>Level</code>
     *
     * @return   The initGame value in 2.6 format
     */
    protected String getInitGame() {
        String s1 = "";
        switch (theLevel.inGameSpecial) {
         case Level.SPECIAL_INIT_VOID:
             s1 = "special_init_void";
             break;
         case Level.SPECIAL_INIT_SPECIAL_BOMBS_30:
             s1 = "special_init_special_bombs_30";
             break;
         case Level.SPECIAL_INIT_SPECIAL_BOMBS_12:
             s1 = "special_init_special_bombs_12";
             break;
         case Level.SPECIAL_INIT_NASTY_WALLS:
             s1 = "special_init_nasty_walls";
             break;
         case Level.SPECIAL_INIT_NASTY_WALLS_2:
             s1 = "special_init_nasty_walls_2";
             break;
         case Level.SPECIAL_INIT_FIRE_WALLS:
             s1 = "special_init_fire_walls";
             break;
         case Level.SPECIAL_INIT_SPECIAL_BOMBS_INFINITY://EPFL
             s1 = "special_init_special_bombs_infinity";
             break;
        }

        if(theLevel.turnGameSpecial == Level.SPECIAL_GAME_NASTY_WALLS && !s1.equals("special_init_nasty_walls") && !s1.equals("special_init_nasty_walls_2"))
            s1 = "special_init_nasty_walls";

        if(theLevel.turnGameSpecial == Level.SPECIAL_GAME_NASTY_CEIL && !s1.equals("special_init_nasty_walls") && !s1.equals("special_init_nasty_walls_2"))
            s1 = "special_init_nasty_walls";
        return s1;
    }


    /**
     * Gets the shrink type of the <code>Level</code>
     *
     * @return   The shrink type in 2.6 format
     */
    protected String getShrink() {
        String s1 = "";
        switch (theLevel.shrinkPattern) {
         case Level.SHRINK_VOID:
             s1 = "shrink_void";
             break;
         case Level.SHRINK_SPIRAL:
             s1 = "shrink_spiral";
             break;
         case Level.SHRINK_SPEED_SPIRAL:
             s1 = "shrink_speed_spiral";
             break;
         case Level.SHRINK_SPIRAL_PLUS:
             s1 = "shrink_spiral_plus";
             break;
         case Level.SHRINK_SPIRAL_3:
             s1 = "shrink_spiral_3";
             break;
         case Level.SHRINK_SPIRAL_23:
             s1 = "shrink_spiral_23";
             break;
         case Level.SHRINK_SPIRAL_LEGO:
             s1 = "shrink_spiral_lego";
             break;
         case Level.SHRINK_EARLY_SPIRAL:
             s1 = "shrink_early_spiral";
             break;
         case Level.SHRINK_COMPOUND:
             s1 = "shrink_compound";
             break;
         case Level.SHRINK_COMPOUND_F:
             s1 = "shrink_compound_f";
             break;
         case Level.SHRINK_COMPOUND_2_F:
             s1 = "shrink_compound_2_f";
             break;
         case Level.SHRINK_LAZY_COMPOUND_F:
             s1 = "shrink_lazy_compound_f";
             break;
         case Level.SHRINK_COMPOUND_SOLID:
             s1 = "shrink_compound_solid";
             break;
         case Level.SHRINK_SAVAGE_COMPOUND:
             s1 = "shrink_savage_compound";
             break;
         case Level.SHRINK_COMPOUND_EXTRA:
             s1 = "shrink_compound_extra";
             break;
         case Level.SHRINK_DOWN:
             s1 = "shrink_down";
             break;
         case Level.SHRINK_DOWN_F:
             s1 = "shrink_down_f";
             break;
         case Level.SHRINK_QUAD:
             s1 = "shrink_quad";
             break;
         case Level.SHRINK_CONSTRICT_WAVE:
             s1 = "shrink_constrict_wave";
             break;
         case Level.SHRINK_OUTWARD_SPIRAL:
             s1 = "shrink_outward_spiral";
             break;
         case Level.SHRINK_DIAG://EPFL
             s1 = "shrink_diag";
             break;
         case Level.SHRINK_OUTWARD_COMPOUND_EXTRA://EPFL
             s1 = "shrink_outward_compound_extra";
             break;
         case Level.SHRINK_ISTY_COMPOUND_2_F://EPFL
             s1 = "shrink_isty_compound_2_f";
             break;
         case Level.SHRINK_ISTY_SPIRAL_3://EPFL
             s1 = "shrink_isty_spiral_3";
             break;
        }
        return s1;
    }


    /**
     * Gets the Gamemode attribute of the <code>Level</code>
     *
     * @return   The GameMode value in 2.6 format
     */
    protected String getGM_data() {
        StringBuffer GM_data = new StringBuffer();
        if(theLevel.GM_Random)
            GM_data.append("GM_Random | ");
        if(theLevel.GM_2_Player && theLevel.GM_3_Player && theLevel.GM_4_Player && theLevel.GM_5_Player && theLevel.GM_6_Player)
            GM_data.append("GM_23456_Player | ");
        else {
            if(theLevel.GM_2_Player)
                GM_data.append("GM_2_Player | ");
            if(theLevel.GM_3_Player)
                GM_data.append("GM_3_Player | ");
            if(theLevel.GM_4_Player)
                GM_data.append("GM_4_Player | ");
            if(theLevel.GM_5_Player)
                GM_data.append("GM_5_Player | ");
            if(theLevel.GM_6_Player)
                GM_data.append("GM_6_Player | ");
        }
        if(theLevel.GM_SinglePlayer)
            GM_data.append("GM_SinglePlayer | ");
        if(theLevel.GM_Team && theLevel.GM_Double && theLevel.GM_Single && theLevel.GM_LR_Players)
            GM_data.append("GM_All | ");
        else {
            if(theLevel.GM_Team)
                GM_data.append("GM_Team | ");
            if(theLevel.GM_Double)
                GM_data.append("GM_Double | ");
            if(theLevel.GM_Single)
                GM_data.append("GM_Single | ");
            if(theLevel.GM_LR_Players)
                GM_data.append("GM_LR_Players | ");
        }
        GM_data = GM_data.delete(GM_data.length() - 3, GM_data.length());
        return GM_data.toString();
    }
}
