/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch.io;

import storch.Level;
import storch.ColorTable;
import java.io.*;

/**
 * This class is used to save a <code>Level</code> into a level-file that can be
 * used with Xblast 2.6.1
 *
 * @author   Tor Andr�
 */
public class StdSaver implements Saver {

    /**
     * Saves the level to a file in 2.6 format.
     *
     * @param saveFile           The <code>File</code> to save to.
     * @param theLevel           The <code>Level</code> to save.
     * @exception SaveException  If unable to save the file.
     */
    public void save(File saveFile, Level theLevel) throws SaveException {
        PrintWriter out = null;
        Level2Std converter = new Level2Std(theLevel);
        String levelName = theLevel.getFileName();

        try {
            out = new PrintWriter(new BufferedWriter(new FileWriter(saveFile)));
        } catch(IOException e) {
            throw new SaveException("Unable to create outstream for " + saveFile.getName());
        }

        try {
            out.println(" /*");
            out.println(" * Level created by Storch " + storch.Storch.VERSION);
            out.println(" * Written by Tobias Johansson (d97tj@efd.lth.se)");
            out.println(" * Co-writer Tor Andr� (d97tan@efd.lth.se)");
            out.println(" * http://storch.sourceforge.net/");
            out.println(" */");
            out.println("");
            out.println("/* XBlast 2.6.1 level */");
            out.println("");
            if(theLevel.scrambleDrawCount > 0) {
                StringBuffer draw = new StringBuffer();
                out.println("static BMPosition " + levelName + "_Draw[]  =");
                out.println("{");
                int i = 0;
                for(; i < theLevel.scrambleDrawCount - 1; i++)
                    draw.append("{" + theLevel.scrambleDraw[i][1] + "," + theLevel.scrambleDraw[i][0] + "}, ");
                draw.append("{" + theLevel.scrambleDraw[i][1] + "," + theLevel.scrambleDraw[i][0] + "}");
                out.println("  " + draw);
                out.println("};\n");
            }

            if(theLevel.scrambleDelCount > 0) {
                StringBuffer del = new StringBuffer();
                out.println("static BMPosition " + levelName + "_Del[]  =");
                out.println("{");
                int i = 0;
                for(; i < theLevel.scrambleDelCount - 1; i++)
                    del.append("{" + theLevel.scrambleDel[i][1] + "," + theLevel.scrambleDel[i][0] + "}, ");
                del.append("{" + theLevel.scrambleDel[i][1] + "," + theLevel.scrambleDel[i][0] + "}");
                out.println("  " + del);
                out.println("};\n");
            }

            out.println("static BMLevelData " + levelName + " =");
            out.println("{");

            out.println("  /* BMLevel */");
            out.println("  {");
            out.println("    \"" + theLevel.title.replace('"', '\'') + "\",");
            out.println("    \"" + theLevel.author.replace('"', '\'') + "\",");
            out.println("    \"xblast.use" + levelName + "\",");
            out.println("    \"" + theLevel.description.replace('"', '\'') + "\",");
            out.println("    " + converter.getGM_data() + ",");
            out.println("    (void *) &" + levelName + ",");
            out.println("    NULL,");
            out.println("  },");

            out.println("  /* BMShrinkData */");
            out.println("  {");
            out.println("    " + converter.getShrink() + ",");
            if(theLevel.scrambleDrawCount > 0)
                out.println("    {GAME_TIME*" + theLevel.scrambleDrawTime + ", " + theLevel.scrambleDrawCount + ", " + levelName + "_Draw, },");
            else
                out.println("    SCRAMBLE_VOID,");
            if(theLevel.scrambleDelCount > 0)
                out.println("    {GAME_TIME*" + theLevel.scrambleDelTime + ", " + theLevel.scrambleDelCount + ", " + levelName + "_Del, },");
            else
                out.println("    SCRAMBLE_VOID,");
            out.println("  },");

            out.println("  /* BMFuncData */");
            out.println("  {");
            out.println("  " + converter.getInitGame() + ",");
            out.println("  " + converter.getTurnGame() + ",");
            String[] specialExtra = converter.getSpecialExtra();
            out.println("  " + specialExtra[Level2Std.NAME] + ",");
            out.println("  " + specialExtra[Level2Std.SKEY] + ",");
            out.println("  },");

            out.println("  /* BMPlayerData */");
            out.println("  {");
            out.println("    " + theLevel.bombsAtStart + ", " + theLevel.range + ",");
            out.println("    {");
            for(int i = 0; i < 6; i++)
                out.println("      { " + theLevel.startPos[i][1] + ", " + theLevel.startPos[i][0] + " },");
            out.println("    },");
            out.println("    " + theLevel.PMMode + ", " + theLevel.PMRange + ",");

            out.println("    " + converter.getInitialIll() + ", " + converter.getRevIll() + ", " + converter.getInitialExtra() + ",");
            out.println("  },");

            out.println("  /* BMBombData */");
            out.println("  {");
            out.println("    " + converter.getBombClicks() + ",");
            out.println("    " + converter.getDir() + ", " + converter.getFuseTime() + ",");
            out.println("    " + converter.getBombTypes() + ",");
            out.println("  },");

            out.println("  /* BMGraphicsData */");
            out.println("  {");
            out.println("    {");
            out.flush();
            ColorTable colors = new ColorTable();
            out.println("     {\"" + theLevel.freeBlock.getFileName() + "\", \"" + colors.getName(theLevel.freeBlock.getColor0()) + "\", \"" + colors.getName(theLevel.freeBlock.getColor1()) + "\", \"" + colors.getName(theLevel.freeBlock.getColor2()) + "\"}, ");
            out.println("     {\"" + theLevel.shadowedBlock.getFileName() + "\", \"" + colors.getName(theLevel.shadowedBlock.getColor0()) + "\", \"" + colors.getName(theLevel.shadowedBlock.getColor1()) + "\", \"" + colors.getName(theLevel.shadowedBlock.getColor2()) + "\"}, ");
            out.println("     {\"" + theLevel.solidWall.getFileName() + "\", \"" + colors.getName(theLevel.solidWall.getColor0()) + "\", \"" + colors.getName(theLevel.solidWall.getColor1()) + "\", \"" + colors.getName(theLevel.solidWall.getColor2()) + "\"}, ");
            out.println("     {\"" + theLevel.risingWall.getFileName() + "\", \"" + colors.getName(theLevel.risingWall.getColor0()) + "\", \"" + colors.getName(theLevel.risingWall.getColor1()) + "\", \"" + colors.getName(theLevel.risingWall.getColor2()) + "\"}, ");
            out.println("     {\"" + theLevel.blastableBlock.getFileName() + "\", \"" + colors.getName(theLevel.blastableBlock.getColor0()) + "\", \"" + colors.getName(theLevel.blastableBlock.getColor1()) + "\", \"" + colors.getName(theLevel.blastableBlock.getColor2()) + "\"}, ");
            out.println("     {\"" + theLevel.blastedBlock.getFileName() + "\", \"" + colors.getName(theLevel.blastedBlock.getColor0()) + "\", \"" + colors.getName(theLevel.blastedBlock.getColor1()) + "\", \"" + colors.getName(theLevel.blastedBlock.getColor2()) + "\"}, ");
            out.println("      EXTRA_BOMB,");
            out.println("      EXTRA_RANGE,");
            out.println("      EXTRA_TRAP,");
            out.println("      " + specialExtra[Level2Std.PICTURE] + ",");
            out.println("     {\"" + theLevel.voidBlock.getFileName() + "\", \"" + colors.getName(theLevel.voidBlock.getColor0()) + "\", \"" + colors.getName(theLevel.voidBlock.getColor1()) + "\", \"" + colors.getName(theLevel.voidBlock.getColor2()) + "\"}, ");
            out.println("    },");
            out.println("  },");

            out.println("  /* BMMapData */");
            out.println("  {");
            out.println("      ShadowFull, " + converter.getDistribution() + ",");

            int[] probValue = theLevel.getStdProbs();
            out.println("      { " + probValue[Level.PROB_BOMB_EXTRA] + ", " + probValue[Level.PROB_RANGE] + ", " + probValue[Level.PROB_ILL] +
                    ", " + probValue[Level.PROB_SPECIAL_EXTRA] + ", " + probValue[Level.PROB_HIDDEN_BOMB] + " },");
            out.println("      {");

            String s1 = "";
            for(int x = 0; x < 15; x++) {
                out.print("        { ");
                for(int y = 0; y < 13; y++) {
                    switch (theLevel.map[x][y]) {
                     case Level.MAP_SOLID:
                         s1 = "B";
                         break;
                     case Level.MAP_BLASTABLE:
                         s1 = "X";
                         break;
                     case Level.MAP_FREE:
                         s1 = "_";
                         break;
                     case Level.MAP_EXPLODING_BOMB:
                         s1 = "e";
                         break;
                     case Level.MAP_BOMB_EXTRA:
                         s1 = "b";
                         break;
                     case Level.MAP_RANGE:
                         s1 = "r";
                         break;
                     case Level.MAP_ILLNESS:
                         s1 = "s";
                         break;
                     case Level.MAP_SPECIAL_EXTRA:
                         s1 = "q";
                         break;
                     case Level.MAP_VOID:
                         s1 = "v";
                         break;
                    }
                    out.print(s1 + ",");
                }
                out.println(" },");
            }
            out.println("    },");
            out.println("  },");
            out.println("};");

            out.close();
        } catch(Exception e) {
            throw new SaveException("An error occurred when saving " + theLevel.title);
        }
    }
}
