/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch;

import java.io.*;
import swingwt.awt.*;


/**
 * This class is used to manipulate the XBlast block images.<br>
 *
 * @author   Tobias Johansson
 */
public class GIFEditor {
	
	private InputStream in;
	private OutputStream out;
	private int nbrOfColors;	
	
	
	/**
	 * Constructor for the GIFEditor object.
	 *
	 * @param in   The GIF image (as a stream) you want to manipulate
	 * @param out  A outstream to the new image file
	 */
	 public GIFEditor(InputStream in, OutputStream out) {
		this.in = in;
		this.out = out;
	}
	
	
	/**
	 * Reads the GIF:s header.
	 *
	 * @exception IOException  If a read error
	 */
	private void readHeader() throws IOException{
		for (int i = 0; i < 10; i++)
			out.write(in.read());
		int i = in.read();
		nbrOfColors = (int) Math.pow(2, (i & 7) + 1);
		out.write(i);
		for (int j = 0; j < 2; j++)
			out.write(in.read());
	}
	
	
	/**
	 * Manipulates the color table of the GIF image.
	 *
	 * @param fg               The 'foreground' color (color0)
	 * @param bg               The 'background' color (color1)
	 * @param add              The 'additional' color (color2)
	 * @exception IOException  If read error
	 */
	public void editColorTable (Color fg, Color bg,  Color add) throws IOException {
		readHeader();
		for (int i = 0; i < nbrOfColors; i++) {
			int[] rgb = new int[3];
			for (int j = 0; j < 3; j++)
				rgb[j] = in.read();
			
			// to 16-bit rgb
			int fgRed = fg.getRed() * 256 + fg.getRed();
			int fgGreen = fg.getGreen() * 256 + fg.getGreen();
			int fgBlue = fg.getBlue() * 256 + fg.getBlue();
			int bgRed = bg.getRed() * 256 + bg.getRed();
			int bgGreen = bg.getGreen() * 256 + bg.getGreen();
			int bgBlue = bg.getBlue() * 256 + bg.getBlue();
			int addRed = add.getRed() * 256 + add.getRed();
			int addGreen = add.getGreen() * 256 + add.getGreen();
			int addBlue = add.getBlue() * 256 + add.getBlue();
	
			// the conversion
	
			rgb[0] -= rgb[2];
			rgb[1] -= rgb[2];
			
			int red = rgb[2] * 255 + fgRed + ((bgRed - fgRed) * rgb[0] + addRed * rgb[1]) / 255;
		   	if (red > 65535)
				red = 65535;
			
		    int green = rgb[2] * 255 + fgGreen + ((bgGreen - fgGreen) * rgb[0] + addGreen * rgb[1]) / 255;
		    if (green > 65535)
				green = 65535;
			
		    int blue = rgb[2] * 255 + fgBlue + ((bgBlue - fgBlue) * rgb[0] + addBlue * rgb[1]) / 255; 
		    if (blue > 65535)
				blue = 65535;
				    
			// back to 8-bit	    
		    red = red >> 8;
		    green = green >> 8;
		    blue = blue >> 8;
				
			out.write(red);
			out.write(green);
			out.write(blue);
		}
		int i  = in.read();
		while (i != -1) {
			out.write(i);
			i = in.read();
		}
		//in.close();
		//out.close();
	}
}
