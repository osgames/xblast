/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
package storch;

import swingwt.awt.*;
import java.io.*;

/**
 * The ColorTable is used to convert colors between the formats <code>swingwt.awt.Color</code> and
 * X11 names.
 *
 * @author   Tobias Johansson
 * @author   Tor Andr�
 */
public class ColorTable {
	private String[] names, hex;
	private Color[] colors;
	
	
	/**
	 * Constructor for the ColorTable object.
	 */
	public ColorTable() {
		names = new String[140];
		colors = new Color[140];
		hex = new String[140];
		loadColors();
	}
	
	
	/**
	 * Gets the <code>swingwt.awt.Color</code> at the place <code>index</code> in the ColorTable.
	 *
	 * @param index  The index to the color
	 * @return       The <code>swingwt.awt.Color</code>
	 */
	public Color getColor(int index){
		return colors[index];
	}
	
	
	/**
	 * Converts a X11 name to a <code>swingwt.awt.Color</code>.
	 * If the X11 name isn't in the ColorTable black is returned.
	 *
	 * @param name  The X11 name for the color
	 * @return      The <code>swingwt.awt.Color</code>
	 */
	public Color getColor(String name) {
		int i = 0;
		while (i < 139 && !(names[i].equals(name)))
			i++;
		return colors[i];
	}
	
	
	/**
	 * Gets the X11 name at the place <code>index</code> in the ColorTable.
	 *
	 * @param index  The index
	 * @return       The X11 name
	 */		   
	public String getName(int index){
		return names[index];
	}
	
	
	/**
	 * Converts a <code>swingwt.awt.Color</code> to a X11 name.
	 * If there isn't an exact match in the ColorTable the closest match is returned.
	 *
	 * @param c  The <code>swingwt.awt.Color</code>
	 * @return   The X11 name
	 */
	public String getName(Color c) {
		int i = 0;
		while (i < 140 && !(colors[i].equals(c)))
			i++;
		if(i >= 140)
			i = getClosestMatch(c);
		return names[i];
	}
	
	
	/**
	 * Gets the color in the ColorTable that is closest to the <code>swingwt.awt.Color</code>.
	 *
	 * @param c  The <code>swingwt.awt.Color</code>
	 * @return   The index in the ColorTable to the color closest to <code>c</code>
	 */
	private int getClosestMatch(Color c){
		int closest = 139, diff;
		int bestMatch = Integer.MAX_VALUE;
		
		for(int i=0; i<140; i++){
			diff = Math.abs(colors[i].getRed()- c.getRed()) + Math.abs(colors[i].getGreen()- c.getGreen()) + Math.abs(colors[i].getBlue()- c.getBlue());
			if(diff < bestMatch){
				closest = i;
				bestMatch = diff;
			}
		}
		return closest;
	}
	
	
	/**
	 * Loads the ColorTable from file.
	 */
	private void loadColors() {
		BufferedReader in;
		try {
			in = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(Storch.DATA_PATH + "colors")));
			for (int i = 0; i < 140; i++) {
				names[i] = in.readLine();
				in.readLine();
				int r = Integer.parseInt(in.readLine());
				int g = Integer.parseInt(in.readLine());
				int b = Integer.parseInt(in.readLine());
				colors[i] = new Color(r, g, b);
			}
		}
		catch (IOException x) {
			System.out.println("Error reading Storch color file. Check 'color'.");
	        System.exit(0);
		}
	}
}
