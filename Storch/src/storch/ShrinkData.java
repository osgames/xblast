/*
 *  This file is part of Storch.
 *
 *  Copyright (C) 2001 Tobias Johansson & Tor Andr�
 *
 *  Storch is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Storch is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Storch; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package storch;

import java.util.*;

/**
 *  Contains information about shrinks.
 *
 *  @author     Tobias Johansson
 */
public class ShrinkData{
	
	private int[] spiral0 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 
							15, 29, 30, 44, 45, 59, 60, 74, 75, 89, 90, 104, 
							105, 119, 120, 134, 135, 149, 150, 164, 165, 179, 
							180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 
							190, 191, 192, 193, 194};
	private int[] spiral1 = {16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,
							31, 43, 46, 58, 61, 73, 76, 88, 91, 103, 
							106, 118, 121, 133, 136, 148, 151, 163, 
							166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178};
	private int[] spiral2 = {32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
							47, 57, 62, 72, 77, 87, 92, 102, 107, 117, 122, 132, 137, 147,
							152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162};
	private int[] spiral3 = {48, 49, 50, 51, 52, 53, 54, 55, 56,
							63, 71, 78, 86, 93, 101, 108, 116, 123, 131,
							138, 139, 140, 141, 142, 143, 144, 145, 146};
	private int[] spiral4 = {64, 65, 66, 67, 68, 69, 70,
							79, 85, 94, 100, 109, 115, 
							124, 125, 126, 127, 128, 129, 130};
	private int[] spiral5 = {80, 81, 82, 83, 84, 95, 99, 110, 111, 112, 113, 114};
	
	private int[] vertical0 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
	private int[] vertical1 = {15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29};
	private int[] vertical2 = {30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44};	
	private int[] vertical3 = {45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59};
	private int[] vertical4 = {60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74};
	private int[] vertical5 = {75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89};
	private int[] vertical6 = {90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104};
	private int[] vertical7 = {105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119};
	private int[] vertical8 = {120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134};
	private int[] vertical9 = {135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149};
	private int[] vertical10 = {150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164};
	private int[] vertical11 = {165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179};
	private int[] vertical12 = {180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194};
	
	
	/**
	 *  Returns the blocks that are filled by a specified shrink at a specified time.
	 *
	 *  @param shrinkType	The shrink type. One of the constants defined in <code>Level</code>.
	 *  @param time			The time, in percent of total game time.
	 *
	 *  @return 			A <code>ShrinkBlockList</code>, containing the blocks that are 
	 *						filled by the specified shrink at the specified time.
	 */
	public ShrinkBlockList getShrinkBlocks(int shrinkType, int time){
		int[][] solidLevels = null;
		int[][] voidLevels = null;
		int[][] extraLevels = null;
		switch(shrinkType){
			case Level.SHRINK_SPIRAL:
			case Level.SHRINK_SPEED_SPIRAL:
				if(time >= 50){
					solidLevels = new int[][]{spiral1, spiral2};
				}
				break;
			case Level.SHRINK_SPIRAL_PLUS:
				if(time >= 50){
					voidLevels = new int[][]{spiral0, spiral1};
					solidLevels = new int[][]{spiral2};
				}
				break;
			case Level.SHRINK_SPIRAL_3:
			case Level.SHRINK_QUAD:
				if(time >= 50){
					solidLevels = new int[][]{spiral1, spiral2, spiral3};
				}
				break;		
			case Level.SHRINK_SPIRAL_23:
			case Level.SHRINK_CONSTRICT_WAVE:
				if(time >= 50){
					voidLevels = new int[][]{spiral1, spiral2};
					solidLevels = new int[][]{spiral3};
				}		
				break;
			case Level.SHRINK_SPIRAL_LEGO:
				if(time >= 75){
					voidLevels = new int[][]{spiral2};
					solidLevels = new int[][]{spiral3};
				}		
				break;
			case Level.SHRINK_EARLY_SPIRAL:	
				if(time >= 38){
					solidLevels = new int[][]{spiral1, spiral2};
				}
				break;
			case Level.SHRINK_COMPOUND:
			case Level.SHRINK_COMPOUND_F:
				if(time >= 17 && time < 33){
					voidLevels = new int[][]{spiral0};
					solidLevels = new int[][]{spiral1};		
				}
				else if(time >= 33 && time < 50){
					voidLevels = new int[][]{spiral0, spiral1};
					solidLevels = new int[][]{spiral2};		
				}
				else if(time >= 50 && time < 67){
					voidLevels = new int[][]{spiral0, spiral1, spiral2};
					solidLevels = new int[][]{spiral3};		
				}
				else if(time >= 67 && time < 83){
					voidLevels = new int[][]{spiral0, spiral1, spiral2, spiral3};
					solidLevels = new int[][]{spiral4};		
				}
				else if(time >= 83){
					voidLevels = new int[][]{spiral0, spiral1, spiral2, spiral3, spiral4};
					solidLevels = new int[][]{spiral5};		
				}
				break;	
			case Level.SHRINK_COMPOUND_2_F:
				if(time >= 50 && time < 67){
					voidLevels = new int[][]{spiral0};
					solidLevels = new int[][]{spiral1};		
				}
				else if(time >= 67){
					voidLevels = new int[][]{spiral0, spiral1};
					solidLevels = new int[][]{spiral2};		
				}
				break;
			case Level.SHRINK_LAZY_COMPOUND_F:
				if(time >= 33 && time < 67){
					solidLevels = new int[][]{spiral1};		
				}
				else if(time >= 67){
					solidLevels = new int[][]{spiral1, spiral2};
				}			
				break;
			case Level.SHRINK_COMPOUND_SOLID:
				if(time >= 17 && time < 33){
					solidLevels = new int[][]{spiral1};		
				}
				else if(time >= 33 && time < 50){
					solidLevels = new int[][]{spiral1, spiral2};		
				}
				else if(time >= 50 && time < 67){
					solidLevels = new int[][]{spiral1, spiral2, spiral3};		
				}
				else if(time >= 67 && time < 83){
					solidLevels = new int[][]{spiral1, spiral2, spiral3, spiral4};		
				}
				else if(time >= 83){
					solidLevels = new int[][]{spiral1, spiral2, spiral3, spiral4, spiral5};		
				}
				break;
			case Level.SHRINK_SAVAGE_COMPOUND:
				if(time >= 17 && time < 50){
					voidLevels = new int[][]{spiral0, spiral1};
					solidLevels = new int[][]{spiral2};		
				}
				else if(time >= 50 && time < 83){
					voidLevels = new int[][]{spiral0, spiral1, spiral2};
					solidLevels = new int[][]{spiral3};		
				}
				else if(time >= 83){
					voidLevels = new int[][]{spiral0, spiral1, spiral2, spiral3};
					solidLevels = new int[][]{spiral4};		
				}	
				break;
			case Level.SHRINK_COMPOUND_EXTRA:
				if(time >= 17 && time < 33){
					voidLevels = new int[][]{spiral0};
					solidLevels = new int[][]{spiral1};
					extraLevels = new int[][]{spiral2};		
				}
				else if(time >= 33 && time < 50){
					voidLevels = new int[][]{spiral0, spiral1};
					solidLevels = new int[][]{spiral2};
					extraLevels = new int[][]{spiral3};			
				}
				else if(time >= 50 && time < 67){
					voidLevels = new int[][]{spiral0, spiral1, spiral2};
					solidLevels = new int[][]{spiral3};
					extraLevels = new int[][]{spiral4};			
				}
				else if(time >= 67){
					voidLevels = new int[][]{spiral0, spiral1, spiral2, spiral3};
					solidLevels = new int[][]{spiral4};
					extraLevels = new int[][]{spiral5};			
				}
				break;
			case Level.SHRINK_DOWN:	
			case Level.SHRINK_DOWN_F:
				if(time >= 9 && time < 18){
					voidLevels = new int[][]{vertical0};
					solidLevels = new int[][]{vertical1};
				}
				else if(time >= 18 && time < 27){
					voidLevels = new int[][]{vertical0, vertical1};
					solidLevels = new int[][]{vertical2};
				}
				else if(time >= 27 && time < 36){
					voidLevels = new int[][]{vertical0, vertical1, vertical2};
					solidLevels = new int[][]{vertical3};
				}
				else if(time >= 36 && time < 45){
					voidLevels = new int[][]{vertical0, vertical1, vertical2, vertical3};
					solidLevels = new int[][]{vertical4};
				}
				else if(time >= 45 && time < 54){
					voidLevels = new int[][]{vertical0, vertical1, vertical2, vertical3, vertical4};
					solidLevels = new int[][]{vertical5};
				}
				else if(time >= 54 && time < 63){
					voidLevels = new int[][]{vertical0, vertical1, vertical2, vertical3, vertical4, vertical5};
					solidLevels = new int[][]{vertical6};
				}
				else if(time >= 63 && time < 72){
					voidLevels = new int[][]{vertical0, vertical1, vertical2, vertical3, vertical4, vertical5, vertical6};
					solidLevels = new int[][]{vertical7};
				}
				else if(time >= 72 && time < 81){
					voidLevels = new int[][]{vertical0, vertical1, vertical2, vertical3, vertical4, vertical5, vertical6, vertical7};
					solidLevels = new int[][]{vertical8};
				}
				else if(time >= 81 && time < 90){
					voidLevels = new int[][]{vertical0, vertical1, vertical2, vertical3, vertical4, vertical5, vertical6, vertical7, vertical8};
					solidLevels = new int[][]{vertical9};
				}
				else if(time >= 90){
					voidLevels = new int[][]{vertical0, vertical1, vertical2, vertical3, vertical4, vertical5, vertical6, vertical7, vertical8, vertical9};
					solidLevels = new int[][]{vertical10};
				}
				break;
			case Level.SHRINK_OUTWARD_SPIRAL:
				if(time >= 38)
					   solidLevels = new int[][]{spiral2, spiral3, spiral4, spiral5};	
				break;
		}        
		         
		return new ShrinkBlockList(mergeArrays(solidLevels), mergeArrays(voidLevels), mergeArrays(extraLevels));
	}               
	
	/**
	  *  Concatenates a number of int arrays into one big array.
	  *
	  *  @param theArrays	An array of int arrays.
	  *
	  *  @return			An int array containing all the elements from each of the arrays.
	  */             
	private int[] mergeArrays(int[][] theArrays){
		int[] newArray = null;
		if(theArrays == null)
			newArray = new int[0];
		else{
			int elems = 0;
			for(int i= 0; i < theArrays.length; i++){
				elems+= theArrays[i].length;
			}        
			newArray = new int[elems];
			int index= 0;
			for(int i = 0; i < theArrays.length; i++)	{
				for(int j = 0; j < theArrays[i].length; j++){
					newArray[index++] = theArrays[i][j];
				}
			}
		}
		return newArray;
	}
	
	/**
	  *  Returns the shrink blocks that change between two points of time for a specified shink style.
	  *
	  *  @param shrinkType	The shrink type. One of the constants defined in <code>Level</code>.
	  *  @param time1		A time, in percent of total game time.
	  *  @param time2		A time, in percent of total game time.
	  */  
	public ShrinkBlockList getChanges(int shrinkType, int time1, int time2){
		if(time1 > time2){
			int tmp = time1;
			time1 = time2;
			time2 = tmp;
		}
		ShrinkBlockList list1 = getShrinkBlocks(shrinkType, time1);
		ShrinkBlockList list2 = getShrinkBlocks(shrinkType, time2);
		
		// map at time1
		int[] map1 = new int[195];
		for(int i = 0; i < list1.solids.length; i++){
			map1[list1.solids[i]] = Level.MAP_SOLID;
		}
		for(int i = 0; i < list1.voids.length; i++){
			map1[list1.voids[i]] = Level.MAP_VOID;
		}
		for(int i = 0; i < list1.extras.length; i++){
			map1[list1.extras[i]] = Level.MAP_SPECIAL_EXTRA;
		}
		
		// changes for time2
		Vector solidDiffs = new Vector();
		Vector voidDiffs = new Vector();
		Vector extraDiffs = new Vector();
		for(int i = 0; i < list2.solids.length; i++){
			if(map1[list2.solids[i]] != Level.MAP_SOLID)
				solidDiffs.add(new Integer(list2.solids[i]));
		}
		for(int i = 0; i < list2.voids.length; i++){
			if(map1[list2.voids[i]] != Level.MAP_VOID)
				voidDiffs.add(new Integer(list2.voids[i]));
		}
		for(int i = 0; i < list2.extras.length; i++){
			if(map1[list2.extras[i]] != Level.MAP_SPECIAL_EXTRA)
				extraDiffs.add(new Integer(list2.extras[i]));
		}
		
		// create ShrinkBlockList
		int[] solidDiffArray = new int[solidDiffs.size()];
		int[] voidDiffArray = new int[voidDiffs.size()];
		int[] extraDiffArray = new int[extraDiffs.size()];
		for(int i = solidDiffs.size() - 1; i >= 0; i--)
			solidDiffArray[i] = ((Integer) solidDiffs.get(i)).intValue();
		for(int i = voidDiffs.size() - 1; i >= 0; i--)
			voidDiffArray[i] = ((Integer) voidDiffs.get(i)).intValue();
		for(int i = extraDiffs.size() - 1; i >= 0; i--)
			extraDiffArray[i] = ((Integer) extraDiffs.get(i)).intValue();
		
		return new ShrinkBlockList(solidDiffArray, voidDiffArray, extraDiffArray);
	}
	
	
	/**
	  *  Main program for testing purposes. The first argument is the shrink type, 
	  *  the second is the time. If two times are entered, the difference is shown.
	  */
	public static void main(String[] args){
		ShrinkData data = new ShrinkData();
		ShrinkBlockList list = null;
		if(args.length == 2)
			list = data.getShrinkBlocks(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
		else if(args.length == 3)
			list = data.getChanges(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
		else{
			System.out.println("Usage: java ShrinkData styleNbr time1 [time2]");
			System.exit(0);
		}
		System.out.print("Solids: ");
		print(list.solids);
		System.out.print("\nVoids: ");
		print(list.voids);
		System.out.print("\nExtras: ");
		print(list.extras);
		data.printmap(list.solids, list.voids, list.extras);
	}
	
	/**
	  *  Prints the contents of an int array. For testing purposes only.
	  */
	private static void print(int[] list){
		for(int i = 0; i < list.length; i++)
			System.out.print(list[i] + " ");
	}
	
	/**
	  *  Prints a map on standard out, showing the shrink blocks. For testing purposes only.
	  */
	private void printmap(int[] solids, int[] voids, int[] extras){
		System.out.println("\n");
		int[] map = new int[195];
		for(int i = 0; i < solids.length; i++)
			map[solids[i]] = 1;
		for(int i = 0; i < voids.length; i++)
			map[voids[i]] = 2;
		for(int i = 0; i < extras.length; i++)
			map[extras[i]] = 3;
		for(int i = 0; i < map.length; i++){
			if(map[i] == 1)
				System.out.print("S");
			else if(map[i] == 2)
				System.out.print("V");
			else if(map[i] == 3)
				System.out.print("X");
			else
				System.out.print(".");
			if(i % 15 == 14)
				System.out.print("\n");
		}
	}
}