/*
 * file xblast.c - main routine
 *
 * $Id$
 *
 * Program XBLAST
 * (C) by Oliver Vogel (e-mail: m.vogel@ndh.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will be entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "xblast.h"

#ifndef _GNU_SOURCE
#define _GNU_SOURCE				/* for getopt_long */
#endif
#include <getopt.h>

/* Turn debugging options on/off */
int xblast_debug;

#ifdef SDL
char *xblast_default_font;
int GfontSize[3] = { 0, };		/* kind of hack-ish */
#endif

int maxnumplayers=6;

int GetMaxPlayers(void){
  return maxnumplayers;
}
void  SetMaxPlayers(int x){
  maxnumplayers=x;
}

/*
 * going down gracefully
 */
static void
Finish (void)
{
	Dbg_Out ("Finish\n");
	/* shutdown sound */
#if defined(XBLAST_SOUND)
	SND_Finish ();
#endif
	/* shutdown network */
	Socket_Finish ();
	/* clean up some modules */
	ClearShapeList ();
	ClearInfo ();
	/* save and delete current configuration */
	SaveConfig ();
	FinishConfig ();
	/* finish all remaining databases */
	DB_Finish ();
	/* finish level selection, if necessary */
	FinishLevelSelection ();
#ifdef DEBUG_ALLOC
	Dbg_FinishAlloc ();
#endif
}								/* Finish */

static void display_usage(const char *progname)
{
	printf("Usage: %s [OPTIONS]\n", progname);
	printf("Options are:\n");
	printf("  --nosound            turn sounds off\n");
	printf("  --mini               run in a small window\n");
	printf("  --smpf               run smpf (more than 16 players)\n");
	printf("  --check              check all level files\n");
	printf("  --central            start a central server\n");
	printf("  --help               this help\n");
	printf("  --version            display version\n");
	printf("  --debug              turn some debugging options on\n");
	printf("  --datadir            override default data directory\n");
#ifdef SDL
	printf("  --font <font>        TrueType font pathname\n"); 
	printf("  --size <a,b,x,y,z>   window (a,b) and font sizes (x,y,z) (eg. 4,3,14,10,8)\n");
#endif
}

static void display_version(void)
{
	printf("XBlast " VERSION_STRING "\n");
	printf("Copyright (C) " COPYRIGHT_YEAR " Oliver Vogel and XBlast dev team\n");
	printf("This is free software; see the source for copying conditions. There is NO\n");
	printf("warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n");
}

/*
 * main routine
 */
#ifdef WMS
int WINAPI
WinMain (HINSTANCE hInstance,	// handle to current instance
		 HINSTANCE hPrevInstance,	// handle to previous instance
		 LPSTR lpCmdLine,		// pointer to command line
		 int nCmdShow			// show state of window
	)
#else
int
main (int argc, char *argv[])
#endif
{
#if defined(XBLAST_SOUND)
	CFGSoundSetup soundSetup;
#endif
	XBPlayerHost hostType;
	int autoCentral;
	int nsound;
	int check;
	int mini;
	int smpf;
	int basex;
	int basey;
	char *datadir = NULL;

#ifdef ENABLE_NLS
	setlocale (LC_ALL, "");
	bindtextdomain (PACKAGE, LOCALEDIR);
	textdomain (PACKAGE);
#endif

	autoCentral = XBFalse;
	nsound = XBFalse;
	check = XBFalse;
	mini = XBFalse;
	smpf = XBFalse;
	basex = XBLAST_BASE_X;
	basey = XBLAST_BASE_Y;

	setbuf(stdout,NULL);
	setbuf(stderr,NULL);
	
#ifndef WMS
	while(1) {
		int c;
		int option_index;

		struct option long_options[] = {
			{ "help", 0, NULL, 'h' },
			{ "version", 0, NULL, 'v' },
			{ "nosound", 0, &nsound, XBTrue },
			{ "check", 0, &check, XBTrue },
			{ "central", 0, &autoCentral, XBTrue },
			{ "mini", 0, &mini, XBTrue },
			{ "smpf", 0, &smpf, XBTrue },
			{ "debug", 0, &xblast_debug, XBTrue },
			{ "datadir", 1, NULL, 'd' }, 
#ifdef SDL
			{ "font", 1, NULL, 'f' },
			{ "size", 1, NULL, 's' },
#endif
			{ NULL, 0, NULL, 0 },
		};

		c = getopt_long (argc, argv, "",
						 long_options, &option_index);
		if (c == EOF)
            break;
		else if (c == '?')
			return -1;

		switch(c) {
		case 'h':
			display_usage(argv[0]);
			return 0;
			break;
		case 'v':
			display_version();
			return 0;
			break;
		case 'd':
			datadir = optarg;
			break;
#ifdef SDL
		case 'f':
			xblast_default_font = optarg;
			break;
		case 's': {
			int ret;
			/* --mini is equivalent to --size=4,3,14,10,8 */
			ret = sscanf(optarg, "%d,%d,%d,%d,%d", &basex, &basey, 
						 &GfontSize[0], &GfontSize[1], &GfontSize[2]);
			if (ret != 5 || basex<2 || basey<2) {
				display_usage(argv[0]);
				return 0;
			}
		}
			break;
#endif
		}
	}

	if (optind < argc) {
		printf ("Extra arguments: %s\n", argv[optind++]);
		return 1;
	}
#endif

	/* Init paths so XBlast will find its data files. */
	InitPaths(datadir);

	if(mini){
	  SetBaseX(XBLAST_BASE_X/2);
	  SetBaseY(XBLAST_BASE_Y/2);
	}
	else{
	  SetBaseX(basex);
	  SetBaseY(basey);
	}
	if(smpf){
	  SetMaxPlayers(16);
	}
	else{
	  SetMaxPlayers(6);
	}
	SetImagesToRightSize();
	ChangeImagesOfIntro();
	//	SetVarPlayers();
	SetMenuVars();
	ChangeImagesOfStatus();

	/* init new configuration */
	InitConfig ();
	/* initialize network */
	if (!Socket_Init ()) {
		return -1;
	}
	/* Init Sound support */
#if defined(XBLAST_SOUND)
	RetrieveSoundSetup (&soundSetup);
	if (!nsound) {
		SND_Init (&soundSetup);
	}
#endif

	/* Initialize graphics engine */
#ifdef WMS
	if (!GUI_Init (0, NULL)) {
#else
	if (!GUI_Init (argc, argv)) {
#endif
		Finish ();
		return -1;
	}
	GUI_OnQuit (Finish);
	if (check && !CheckConfig ()) {
		GUI_Finish ();
		Finish ();
		return -2;
	}
	/* init random number generator */
	SeedRandom (time (NULL));
	/* Call intro */
	DoIntro ();

	/* main loop until quit */
	while (XBPH_None != (hostType = DoMenu (autoCentral))) {
		/* save current configurations */
		SaveConfig ();
		SetHostType (hostType);
		/* run selected game  */
		switch (hostType) {
		case XBPH_Local:
			RunLocalGame ();
			break;
		case XBPH_Demo:
			RunDemoGame ();
			break;
		case XBPH_Server:
			RunServerGame ();
			break;
		default:
			RunClientGame (hostType);
			break;
		}
	}
	/* close Display */
	GUI_Finish ();
	/* shutdown the rest */
	Finish ();
	/* that's all */
	return 0;
}								/* main */

/*
 * end of file xblast.c
 */
