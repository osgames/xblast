/*
 * file x11_joystick.c - joystick support for linux
 *
 * $Id$
 *
 * Program XBLAST 
 * (C) by Oliver Vogel (e-mail: m.vogel@ndh.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will be entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "xblast.h"

#include "sdl_common.h"

#define JOYDIR_NONE  0x00
#define JOYDIR_UP    0x01
#define JOYDIR_DOWN  0x02
#define JOYDIR_LEFT  0x04
#define JOYDIR_RIGHT 0x08

#define JOYDIR_X     (JOYDIR_LEFT | JOYDIR_RIGHT)
#define JOYDIR_Y     (JOYDIR_UP | JOYDIR_DOWN)

#define THRESHOLD    8192

/*
 * local types
 */
typedef struct
{
	SDL_Joystick *joy;
	XBEventCode event;
	unsigned dir;
} XBJoystick;

/*
 * local variables
 */
static XBJoystick joystick[NUM_JOYSTICKS] = {
	{NULL, XBE_NONE, 0},
	{NULL, XBE_NONE, 0},
	{NULL, XBE_NONE, 0},
	{NULL, XBE_NONE, 0},
	{NULL, XBE_NONE, 0},
	{NULL, XBE_NONE, 0},
};

/*
 * global function: GUI_NumJoysticks
 * description:     Query number of connected joysticks
 * return value:    # joysticks
 */
int
GUI_NumJoysticks (void)
{
	return SDL_NumJoysticks ();
}								/* GUI_NumJoysticks */

/*
 * Initialize
 */
XBBool
InitJoystick (void)
{
	int i;
	int j;

	for (i = 0, j = 0; i < SDL_NumJoysticks () && i < NUM_JOYSTICKS; i++) {
		joystick[j].joy = SDL_JoystickOpen (i);
		if (joystick[j].joy) {
			joystick[j].event = XBE_JOYST_1 + j;
			j++;
		} else {
			printf ("Couldn't open joystick %d\n", i);
		}
	}

	return XBTrue;
}								/* InitJoystick */

/*
 * shutdown
 */
void
FinishJoystick (void)
{
	int i;
	for (i = 0; i < NUM_JOYSTICKS; i++) {
		if (joystick[i].joy != NULL) {
			SDL_JoystickClose (joystick[i].joy);
		}
	}
}								/* FinishJoystick */

/*
 * calculate new joystick direction from axis-event
 */
static unsigned
EvalJoystickMove (unsigned dir, unsigned axis, int value)
{
	unsigned newDir = dir;

	switch (axis) {
		/* x-Axis */
	case 0:
		newDir &= ~JOYDIR_X;
		if (value > THRESHOLD)
			newDir |= JOYDIR_RIGHT;
		else if (value < -THRESHOLD)
			newDir |= JOYDIR_LEFT;
		break;
		/* y-axis */
	case 1:
		newDir &= ~JOYDIR_Y;
		if (value > THRESHOLD)
			newDir |= JOYDIR_DOWN;
		else if (value < -THRESHOLD)
			newDir |= JOYDIR_UP;
		break;
	default:
		break;
	}
#ifdef DEBUG_JOYSTICK
	Dbg_Out ("joy move: axis=%u value=%d => %02x\n", axis, value, newDir);
#endif
	return newDir;
}								/* EvalJoystickMove */

/*
 * handle new joystick event during game
 */
void
HandleXBlastJoystick (SDL_Event * event)
{
	unsigned newDir;
	int value;

	switch (event->type) {
	case SDL_JOYAXISMOTION:
		newDir = EvalJoystickMove (joystick[event->jaxis.which].dir,
								   event->jaxis.axis, event->jaxis.value);
		switch (newDir) {
		case JOYDIR_UP:
			value = XBGK_GO_UP;
			break;
		case JOYDIR_DOWN:
			value = XBGK_GO_DOWN;
			break;
		case JOYDIR_LEFT:
			value = XBGK_GO_LEFT;
			break;
		case JOYDIR_RIGHT:
			value = XBGK_GO_RIGHT;
			break;
		case JOYDIR_NONE:
			value = XBGK_STOP_ALL;
			break;
		default:
			value = XBGK_NONE;
			break;
		}
		joystick[event->jaxis.which].dir = newDir;
		if (value != XBGK_NONE) {
			QueueEventValue (joystick[event->jaxis.which].event, value);
		}
		break;

	case SDL_JOYHATMOTION:
		if (event->jhat.value == SDL_HAT_CENTERED) {
			QueueEventValue (joystick[event->jhat.which].event, XBGK_STOP_ALL);
			joystick[event->jhat.which].dir = JOYDIR_NONE;
		} else {
			if (event->jhat.value & SDL_HAT_UP) {
				QueueEventValue (joystick[event->jhat.which].event, XBGK_GO_UP);
				joystick[event->jhat.which].dir |= JOYDIR_UP;
			}
			else if (event->jhat.value & SDL_HAT_DOWN) {
				QueueEventValue (joystick[event->jhat.which].event, XBGK_GO_DOWN);
				joystick[event->jhat.which].dir |= JOYDIR_DOWN;
			}

			if (event->jhat.value & SDL_HAT_RIGHT) {
				QueueEventValue (joystick[event->jhat.which].event, XBGK_GO_RIGHT);
				joystick[event->jhat.which].dir |= JOYDIR_RIGHT;
			}
			else if (event->jhat.value & SDL_HAT_LEFT) {
				QueueEventValue (joystick[event->jhat.which].event, XBGK_GO_LEFT);
				joystick[event->jhat.which].dir |= JOYDIR_LEFT;
			}
		}
		break;

	case SDL_JOYBUTTONDOWN:
		if (event->jbutton.button == 0)
			QueueEventValue (joystick[event->jbutton.which].event, XBGK_BOMB);
		if (event->jbutton.button == 1)
			QueueEventValue (joystick[event->jbutton.which].event, XBGK_SPECIAL);
		break;
	}
}								/* HandleJoystick */

/*
 * handle new joystick event in menus
 */
void
HandleMenuJoystick (SDL_Event * event)
{
	unsigned newDir;

	switch (event->type) {
	case SDL_JOYAXISMOTION:
		newDir = EvalJoystickMove (joystick[event->jaxis.which].dir,
								   event->jaxis.axis, event->jaxis.value);
		/* test changes in y dir */
		if ((newDir & JOYDIR_Y) != (joystick[event->jaxis.which].dir & JOYDIR_Y)) {
			switch (newDir & JOYDIR_Y) {
			case JOYDIR_UP:
				QueueEventValue (XBE_MENU, XBMK_UP);
				break;
			case JOYDIR_DOWN:
				QueueEventValue (XBE_MENU, XBMK_DOWN);
				break;
			default:
				break;
			}
		}
		/* test changes in x dir */
		if ((newDir & JOYDIR_X) != (joystick[event->jaxis.which].dir & JOYDIR_X)) {
			switch (newDir & JOYDIR_X) {
			case JOYDIR_LEFT:
				QueueEventValue (XBE_MENU, XBMK_LEFT);
				break;
			case JOYDIR_RIGHT:
				QueueEventValue (XBE_MENU, XBMK_RIGHT);
				break;
			default:
				break;
			}
		}
		joystick[event->jaxis.which].dir = newDir;
		break;

	case SDL_JOYHATMOTION:
		if (event->jhat.value & SDL_HAT_UP)
			QueueEventValue (XBE_MENU, XBMK_UP);
		if (event->jhat.value & SDL_HAT_DOWN)
			QueueEventValue (XBE_MENU, XBMK_DOWN);
		if (event->jhat.value & SDL_HAT_RIGHT)
			QueueEventValue (XBE_MENU, XBMK_RIGHT);
		if (event->jhat.value & SDL_HAT_LEFT)
			QueueEventValue (XBE_MENU, XBMK_LEFT);
		break;

	case SDL_JOYBUTTONUP:
		QueueEventValue (XBE_MENU, XBMK_SELECT);
		break;
	}
}								/* HandleMenuJoystick */

/*
 * end of file x11_joystick.c
 */
