/*
 * file introdat.c - animation and layouts for intro and inbetween screens
 *
 * $Id$
 *
 * Program XBLAST 
 * (C) by Oliver Vogel (e-mail: m.vogel@ndh.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will be entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "xblast.h"

/*
 * text for first screen
 */
IntroTextBox introBox[] = {
	{
	 N_("On a Workstation not far away"),
	 FF_Large | FF_Black | FF_Outlined,
	 {0, 0, 1, 1}
	 ,
	 }
	,
	{
	 N_("Press Space or Return"),
	 FF_Large | FF_White | FF_Boxed,
	 {5, 8, 5, 0,}
	 ,
	 }
	,
	/* end of array */
	{
	 NULL,
	 0,
	 {0, 0, 0, 0}
	 ,
	 }
	,
};

/*
 * Coypright notice for first screen 
 */
IntroTextBox creditsBox[] = {
	{
	 "",
	 FF_Large | FF_Boxed | FF_Black,
	 {35, 38, 50, 44,}
	 ,
	 }
	,
	{
	 "",
	 FF_Large | FF_Boxed | FF_White,
	 {36, 39, 48, 42,}
	 ,
	 }
	,
	{
	 "XBlast TNT " VERSION_STRING,
	 FF_Large | FF_White,
	 {36, 39, 48, 8,}
	 ,
	 }
	,
	{
	 "Copyright \251 " COPYRIGHT_YEAR " Oliver Vogel",
	 FF_Medium | FF_White,
	 {36, 47, 48, 6,}
	 ,
	 }
	,
	{
	 "(m.vogel@ndh.net)",
	 FF_Small | FF_White,
	 {36, 53, 48, 4,}
	 ,
	 }
	,
	{
	 N_("Coauthor Garth Denley"),
	 FF_Medium | FF_White,
	 {36, 57, 48, 6,}
	 ,
	 }
	,
	{
	 "(garthy@cs.adelaide.edu.au)",
	 FF_Small | FF_White,
	 {36, 63, 48, 4,}
	 ,
	 }
	,
	{
	 N_("Sound by Norbert Nicolay"),
	 FF_Medium | FF_White,
	 {36, 67, 48, 6,}
	 ,
	 }
	,
	{
	 "(nicolay@ikp.uni-koeln.de)",
	 FF_Small | FF_White,
	 {36, 73, 48, 4,}
	 ,
	 }
	,
	/* end of array */
	{
	 NULL,
	 0,
	 {0, 0, 0, 0}
	 ,
	 }
	,
};

/* 
 * level title boxes 
 */
IntroTextBox titleBox[] = {
	{
	 "",
	 FF_White | FF_Boxed | FF_Large,
	 {13, -2, 17, 4,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Large,
	 {13, -2, 17, 2,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Small,
	 {13, -2, 17, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Large,
	 {5, 67, 10, 2,}
	 }
	,
	{
	 N_("visit http://xblast.sf.net/"),
	 FF_White | FF_Boxed | FF_Small,
	 {8, 73, 7, 2,}
	 }
	,
	/* end of array */
	{
	 NULL,
	 0,
	 {0, 0, 0, 0}
	 ,
	 }
	,
};

/* 
 * player info boxes 
 */
IntroTextBox playerInfoBox[] = {
	/* frame */
	{
	 "",
	 FF_White | FF_Boxed | FF_Transparent | FF_Large,
	 {3, 5, 17, 95,}
	 ,
	 }
	,
	/* header */
	{
	 N_("Player Info"),
	 FF_Black | FF_Boxed | FF_Medium,
	 {1, 8, 4, 2,}
	 ,
	 }
	,
	/* info 1-7 */
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {1, 15, 4, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {1, 19, 4, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {1, 23, 4, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {1, 27, 4, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {1, 31, 4, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {1, 35, 4, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {1, 39, 4, 1,}
	 ,
	 }
	,
	/* end of array */
	{
	 NULL, 0, {0, 0, 0, 0}
	 ,
	 }
	,
};

/*
 *  level info boxes
 */
IntroTextBox levelInfoBox[] = {
	/* frame */
	{
	 "",
	 FF_White | FF_Boxed | FF_Transparent | FF_Large,
	 {41, 5, 19, 95,}
	 ,
	 }
	,
	/* header */
	{
	 N_("Level Info"),
	 FF_Black | FF_Boxed | FF_Medium,
	 {21, 8, 9, 2,}
	 ,
	 }
	,
	/* info 1-7 */
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {21, 15, 9, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {21, 19, 9, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {21, 23, 9, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {21, 27, 9, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {21, 31, 9, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {21, 35, 9, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {21, 39, 9, 1,}
	 ,
	 }
	,
	/* end of array */
	{
	 NULL, 0, {0, 0, 0, 0}
	 ,
	 }
	,
};

/*
 *  extra info boxes
 */
IntroTextBox extraInfoBox[] = {
	/* frame */
	{
	 "",
	 FF_White | FF_Boxed | FF_Transparent | FF_Large,
	 {83, 5, 17, 95,}
	 ,
	 }
	,
	/* header */
	{
	 N_("Extra Info"),
	 FF_Black | FF_Boxed | FF_Medium,
	 {42, 8, 4, 2,}
	 ,
	 }
	,
	/* info 1-6 */
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {42, 15, 4, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {42, 19, 4, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {42, 23, 4, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {42, 27, 4, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {42, 31, 4, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {42, 35, 4, 1,}
	 ,
	 }
	,
	{
	 NULL,
	 FF_White | FF_Boxed | FF_Small,
	 {42, 39, 4, 1,}
	 ,
	 }
	,
	/* end of array */
	{
	 NULL, 0, {0, 0, 0, 0}
	 ,
	 }
	,
};

/*
 * data points for X-polygon
 */
const BMPoint pointx[SIZE_OF_X] = {
	{0.000000, 0.000000},
	{0.166667, 0.000000},
	{0.500000, 0.333333},
	{0.833333, 0.000000},
	{1.000000, 0.000000},
	{1.000000, 0.166667},
	{0.666667, 0.500000},
	{1.000000, 0.833333},
	{1.000000, 1.000000},
	{0.833333, 1.000000},
	{0.500000, 0.666667},
	{0.166667, 1.000000},
	{0.000000, 1.000000},
	{0.000000, 0.833333},
	{0.333333, 0.500000},
	{0.000000, 0.166667},
};

/*
 * explosion data for letter animations
 */

/* Letter A */
const int blockA[CHAR_ANIME][CHARH][CHARW] = {
	{
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x10, 0x00, 0x00},
	 },
	{
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x14, 0x00, 0x00},
	 {0x11, 0x00, 0x00},
	 },
	{
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x14, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x11, 0x00, 0x00},
	 },
	{
	 {0x00, 0x00, 0x00},
	 {0x14, 0x00, 0x00},
	 {0x17, 0x18, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x11, 0x00, 0x00},
	 },
	{
	 {0x14, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x17, 0x1a, 0x18},
	 {0x15, 0x00, 0x00},
	 {0x11, 0x00, 0x00},
	 },
	{
	 {0x16, 0x18, 0x00},
	 {0x15, 0x00, 0x14},
	 {0x17, 0x1a, 0x1d},
	 {0x15, 0x00, 0x11},
	 {0x11, 0x00, 0x00},
	 },
	{
	 {0x16, 0x1a, 0x1c},
	 {0x15, 0x00, 0x15},
	 {0x17, 0x1a, 0x1d},
	 {0x15, 0x00, 0x15},
	 {0x11, 0x00, 0x11},
	 },
};

/* letter B */
const int blockB[CHAR_ANIME][CHARH][CHARW] = {
	{
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x10, 0x00, 0x00},
	 },
	{
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x14, 0x00, 0x00},
	 {0x1b, 0x18, 0x00},
	 },
	{
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x14, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x1b, 0x1a, 0x18},
	 },
	{
	 {0x00, 0x00, 0x00},
	 {0x14, 0x00, 0x00},
	 {0x17, 0x18, 0x00},
	 {0x15, 0x00, 0x14},
	 {0x1b, 0x1a, 0x19},
	 },
	{
	 {0x14, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x17, 0x1a, 0x1c},
	 {0x15, 0x00, 0x15},
	 {0x1b, 0x1a, 0x19},
	 },
	{
	 {0x1e, 0x18, 0x00},
	 {0x15, 0x00, 0x14},
	 {0x17, 0x1a, 0x1d},
	 {0x15, 0x00, 0x15},
	 {0x1b, 0x1a, 0x19},
	 },
	{
	 {0x1e, 0x1a, 0x1c},
	 {0x15, 0x00, 0x15},
	 {0x17, 0x1a, 0x1d},
	 {0x15, 0x00, 0x15},
	 {0x1b, 0x1a, 0x19}
	 }
};

/* letter L */
const int blockL[CHAR_ANIME][CHARH][CHARW] = {
	{
	 {0x10, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 },
	{
	 {0x14, 0x00, 0x00},
	 {0x11, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 },
	{
	 {0x14, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x11, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 },
	{
	 {0x14, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x11, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 },
	{
	 {0x14, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x11, 0x00, 0x00},
	 },
	{
	 {0x14, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x13, 0x18, 0x00},
	 },
	{
	 {0x14, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x13, 0x1a, 0x18},
	 },
};

/* Letter S */
const int blockS[CHAR_ANIME][CHARH][CHARW] = {
	{
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x10, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 },
	{
	 {0x00, 0x00, 0x00},
	 {0x14, 0x00, 0x00},
	 {0x13, 0x18, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 },
	{
	 {0x14, 0x00, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x13, 0x1a, 0x18},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 },
	{
	 {0x16, 0x18, 0x00},
	 {0x15, 0x00, 0x00},
	 {0x13, 0x1a, 0x1c},
	 {0x00, 0x00, 0x11},
	 {0x00, 0x00, 0x00},
	 },
	{
	 {0x16, 0x1a, 0x18},
	 {0x15, 0x00, 0x00},
	 {0x13, 0x1a, 0x1c},
	 {0x00, 0x00, 0x15},
	 {0x00, 0x00, 0x11},
	 },
	{
	 {0x16, 0x1a, 0x18},
	 {0x15, 0x00, 0x00},
	 {0x13, 0x1a, 0x1c},
	 {0x00, 0x00, 0x15},
	 {0x00, 0x12, 0x19},
	 },
	{
	 {0x16, 0x1a, 0x18},
	 {0x15, 0x00, 0x00},
	 {0x13, 0x1a, 0x1c},
	 {0x00, 0x00, 0x15},
	 {0x12, 0x1a, 0x19},
	 },
};

/* Letter T */
const int blockT[CHAR_ANIME][CHARH][CHARW] = {
	{
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x10, 0x00},
	 },
	{
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x14, 0x00},
	 {0x00, 0x11, 0x00},
	 },
	{
	 {0x00, 0x00, 0x00},
	 {0x00, 0x00, 0x00},
	 {0x00, 0x14, 0x00},
	 {0x00, 0x15, 0x00},
	 {0x00, 0x11, 0x00},
	 },
	{
	 {0x00, 0x00, 0x00},
	 {0x00, 0x14, 0x00},
	 {0x00, 0x15, 0x00},
	 {0x00, 0x15, 0x00},
	 {0x00, 0x11, 0x00},
	 },
	{
	 {0x00, 0x14, 0x00},
	 {0x00, 0x15, 0x00},
	 {0x00, 0x15, 0x00},
	 {0x00, 0x15, 0x00},
	 {0x00, 0x11, 0x00},
	 },
	{
	 {0x12, 0x1e, 0x18},
	 {0x00, 0x15, 0x00},
	 {0x00, 0x15, 0x00},
	 {0x00, 0x15, 0x00},
	 {0x00, 0x11, 0x00},
	 },
};

/*
 * level data for generic scorebaord
 */
const XBScoreGraphics graphicsScoreBoard = {
	{"score_right_up", COLOR_BLACK, COLOR_LIGHT_STEEL_BLUE, COLOR_BLACK},
	{"score_right_down", COLOR_BLACK, COLOR_FIRE_BRICK_1, COLOR_BLACK},
	{"score_mid_up", COLOR_BLACK, COLOR_SPRING_GREEN, COLOR_LIGHT_STEEL_BLUE},
	{"score_mid_down", COLOR_BLACK, COLOR_FIRE_BRICK_1, COLOR_LIGHT_STEEL_BLUE},
	{"score_left_up", COLOR_BLACK, COLOR_SPRING_GREEN, COLOR_LIGHT_STEEL_BLUE},
	{"score_left_down", COLOR_BLACK, COLOR_FIRE_BRICK_1, COLOR_LIGHT_STEEL_BLUE},
	{"score_floor", COLOR_BLACK, COLOR_SPRING_GREEN, COLOR_BLACK},
	{"score_step", COLOR_BLACK, COLOR_FIRE_BRICK_1, COLOR_LIGHT_STEEL_BLUE},
	{"score_drop", COLOR_BLACK, COLOR_FIRE_BRICK_1, COLOR_SPRING_GREEN},
	{"score_floor", COLOR_BLACK, COLOR_BLACK, COLOR_BLACK},
	{"score_floor", COLOR_ROYAL_BLUE, COLOR_ROYAL_BLUE, COLOR_ROYAL_BLUE},
};
XBScoreMap mapScoreBoard = {
	{7, 7, 7, 6, 0, 1, 0, 1, 0, 1, 0, 1, 6,},
	{7, 7, 7, 6, 0, 1, 0, 1, 0, 1, 0, 1, 6,},
	{7, 7, 7, 6, 0, 1, 0, 1, 0, 1, 0, 1, 6,},
	{7, 7, 7, 6, 2, 3, 2, 3, 2, 3, 2, 3, 6,},
	{7, 7, 7, 6, 4, 5, 4, 5, 4, 5, 4, 5, 6,},
	{7, 7, 7, 6, 4, 5, 4, 5, 4, 5, 4, 5, 6,},
	{7, 7, 7, 6, 4, 5, 4, 5, 4, 5, 4, 5, 6,},
	{7, 7, 7, 6, 4, 5, 4, 5, 4, 5, 4, 5, 6,},
	{7, 7, 7, 6, 4, 5, 4, 5, 4, 5, 4, 5, 6,},
	{7, 7, 7, 6, 4, 5, 4, 5, 4, 5, 4, 5, 6,},
	{7, 7, 7, 6, 4, 5, 4, 5, 4, 5, 4, 5, 6,},
	{7, 7, 7, 6, 4, 5, 4, 5, 4, 5, 4, 5, 6,},
	{7, 7, 7, 6, 4, 5, 4, 5, 4, 5, 4, 5, 6,},
	{7, 7, 7, 6, 4, 5, 4, 5, 4, 5, 4, 5, 6,},
	{7, 7, 7, 6, 4, 5, 4, 5, 4, 5, 4, 5, 6,},
};
const XBScoreMap mapWinGame = {
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
	{7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,},
};

/*
 * level data for load player screen
 */
const XBScoreGraphics graphicsLoadSprite = {
	{"score_floor", COLOR_WHITE, COLOR_WHITE, COLOR_WHITE},
	{NULL, COLOR_INVALID, COLOR_INVALID, COLOR_INVALID},
	{NULL, COLOR_INVALID, COLOR_INVALID, COLOR_INVALID},
	{NULL, COLOR_INVALID, COLOR_INVALID, COLOR_INVALID},
	{NULL, COLOR_INVALID, COLOR_INVALID, COLOR_INVALID},
	{NULL, COLOR_INVALID, COLOR_INVALID, COLOR_INVALID},
	{NULL, COLOR_INVALID, COLOR_INVALID, COLOR_INVALID},
	{NULL, COLOR_INVALID, COLOR_INVALID, COLOR_INVALID},
	{NULL, COLOR_INVALID, COLOR_INVALID, COLOR_INVALID},
	{NULL, COLOR_INVALID, COLOR_INVALID, COLOR_INVALID},
	{NULL, COLOR_INVALID, COLOR_INVALID, COLOR_INVALID},
};
const XBScoreMap mapLoadSprite = {
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
};

/* 
 * audience for score board 
 */
const BMSpriteAnimation winnerAnime[NUM_WINNER_ANIME] = {
	SpriteStopDown, SpriteWinner3, SpriteWinner2, SpriteWinner,
	SpriteWinner, SpriteWinner2, SpriteWinner3, SpriteStopDown,
};
const BMSpriteAnimation looserAnime[NUM_LOOSER_ANIME] = {
	SpriteLooser, SpriteLooser, SpriteLooser1, SpriteLooser1, SpriteLooser,
	SpriteLooser, SpriteLooser, SpriteLooser2, SpriteLooser2, SpriteLooser,
};
const BMSpriteAnimation otherWinnerAnime[NUM_OTHER_WINNER_ANIME] = {
	SpriteStopDown, SpriteWinner3, SpriteWinner2, SpriteWinner,
	SpriteWinner, SpriteWinner2, SpriteWinner3, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
};
const BMSpriteAnimation otherLooserAnime[NUM_OTHER_LOOSER_ANIME] = {
	SpriteLooser, SpriteLooser, SpriteLooser1, SpriteLooser1, SpriteLooser,
	SpriteLooser, SpriteLooser, SpriteLooser2, SpriteLooser2, SpriteLooser,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
};
const BMSpriteAnimation laOlaAnime[NUM_LAOLA_ANIME] = {
	SpriteStopDown, SpriteWinner3, SpriteWinner2, SpriteWinner,
	SpriteWinner, SpriteWinner, SpriteWinner, SpriteWinner2,
	SpriteWinner3, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
	SpriteStopDown, SpriteStopDown, SpriteStopDown, SpriteStopDown,
};

void
ChangeImagesOfIntro (void)
{
	int i;

	for (i = 0; i < 9; i++) {
		creditsBox[i].rect.x = creditsBox[i].rect.x * GetBaseX ();
		creditsBox[i].rect.w = creditsBox[i].rect.w * GetBaseX ();
		creditsBox[i].rect.y = creditsBox[i].rect.y * GetBaseY ();
		creditsBox[i].rect.h = creditsBox[i].rect.h * GetBaseY ();
	}

	introBox[0].rect.w *= GetPixW ();
	introBox[0].rect.h *= GetBlockHeight ();
	introBox[1].rect.x *= GetBlockWidth ();
	introBox[1].rect.y += GetPixH ();
	introBox[1].rect.w *= GetBlockWidth ();
	introBox[1].rect.h = 2 * GetBlockHeight () / 3;

	playerInfoBox[0].rect.x = playerInfoBox[0].rect.x * GetBlockWidth () / 9;
	playerInfoBox[0].rect.y = playerInfoBox[0].rect.y * GetBlockHeight () / 2;
	playerInfoBox[0].rect.w = playerInfoBox[0].rect.w * GetBlockWidth () / 4;
	playerInfoBox[0].rect.h = playerInfoBox[0].rect.h * GetBlockHeight () / 12;

	playerInfoBox[1].rect.x = playerInfoBox[1].rect.x * GetBlockWidth () / 2;
	playerInfoBox[1].rect.y = playerInfoBox[1].rect.y * GetBlockHeight () / 3;
	playerInfoBox[1].rect.w = playerInfoBox[1].rect.w * GetBlockWidth ();
	playerInfoBox[1].rect.h = playerInfoBox[1].rect.h * GetBlockHeight () / 3;

	for (i = 2; i < 8; i++) {
		playerInfoBox[i].rect.x = playerInfoBox[i].rect.x * GetBlockWidth () / 2;
		playerInfoBox[i].rect.y = playerInfoBox[i].rect.y * GetBlockHeight () / 4;
		playerInfoBox[i].rect.w = playerInfoBox[i].rect.w * GetBlockWidth ();
		playerInfoBox[i].rect.h = playerInfoBox[i].rect.h * GetBlockHeight () / 2;
	}

	titleBox[0].rect.x = titleBox[0].rect.x * GetBlockWidth () / 4;
	titleBox[0].rect.y = titleBox[0].rect.y + GetBlockHeight () / 2;
	titleBox[0].rect.w = titleBox[0].rect.w * GetBlockWidth () / 2;
	titleBox[0].rect.h = titleBox[0].rect.h + GetBlockHeight ();

	titleBox[1].rect.x = titleBox[1].rect.x * GetBlockWidth () / 4;
	titleBox[1].rect.y = titleBox[1].rect.y + GetBlockHeight () / 2;
	titleBox[1].rect.w = titleBox[1].rect.w * GetBlockWidth () / 2;
	titleBox[1].rect.h = titleBox[1].rect.h * GetBlockHeight () / 3;

	titleBox[2].rect.x = titleBox[2].rect.x * GetBlockWidth () / 4;
	titleBox[2].rect.y = titleBox[2].rect.y + 2 * GetBlockHeight () / 2;
	titleBox[2].rect.w = titleBox[2].rect.w * GetBlockWidth () / 2;
	titleBox[2].rect.h = titleBox[2].rect.h * GetBlockHeight () / 3;

	titleBox[3].rect.x = titleBox[3].rect.x * GetBlockWidth () / 2;
	titleBox[3].rect.y = titleBox[3].rect.y * GetBlockHeight () / 6;
	titleBox[3].rect.w = titleBox[3].rect.w * GetBlockWidth ();
	titleBox[3].rect.h = titleBox[3].rect.h * GetBlockHeight () / 3;

	titleBox[4].rect.x = titleBox[4].rect.x * GetBlockWidth () / 2;
	titleBox[4].rect.y = titleBox[4].rect.y * GetBlockHeight () / 6;
	titleBox[4].rect.w = titleBox[4].rect.w * GetBlockWidth ();
	titleBox[4].rect.h = titleBox[4].rect.h * GetBlockHeight () / 3;

	levelInfoBox[0].rect.x = levelInfoBox[0].rect.x * GetBlockWidth () / 8;
	levelInfoBox[0].rect.y = levelInfoBox[0].rect.y * GetBlockHeight () / 2;
	levelInfoBox[0].rect.w = levelInfoBox[0].rect.w * GetBlockWidth () / 4;
	levelInfoBox[0].rect.h = levelInfoBox[0].rect.h * GetBlockHeight () / 12;

	levelInfoBox[1].rect.x = levelInfoBox[1].rect.x * GetBlockWidth () / 4;
	levelInfoBox[1].rect.y = levelInfoBox[1].rect.y * GetBlockHeight () / 3;
	levelInfoBox[1].rect.w = levelInfoBox[1].rect.w * GetBlockWidth () / 2;
	levelInfoBox[1].rect.h = levelInfoBox[1].rect.h * GetBlockHeight () / 3;

	for (i = 2; i < 9; i++) {

		levelInfoBox[i].rect.x = levelInfoBox[i].rect.x * GetBlockWidth () / 4;
		levelInfoBox[i].rect.y = levelInfoBox[i].rect.y * GetBlockHeight () / 4;
		levelInfoBox[i].rect.w = levelInfoBox[i].rect.w * GetBlockWidth () / 2;
		levelInfoBox[i].rect.h = levelInfoBox[i].rect.h * GetBlockHeight () / 2;
	}

	extraInfoBox[0].rect.x = extraInfoBox[0].rect.x * GetBlockWidth () / 8;
	extraInfoBox[0].rect.y = extraInfoBox[0].rect.y * GetBlockHeight () / 2;
	extraInfoBox[0].rect.w = extraInfoBox[0].rect.w * GetBlockWidth () / 4;
	extraInfoBox[0].rect.h = extraInfoBox[0].rect.h * GetBlockHeight () / 12;

	extraInfoBox[1].rect.x = extraInfoBox[1].rect.x * GetBlockWidth () / 4;
	extraInfoBox[1].rect.y = extraInfoBox[1].rect.y * GetBlockHeight () / 3;
	extraInfoBox[1].rect.w = extraInfoBox[1].rect.w * GetBlockWidth ();
	extraInfoBox[1].rect.h = extraInfoBox[1].rect.h * GetBlockHeight () / 3;

	for (i = 2; i < 8; i++) {

		extraInfoBox[i].rect.x = extraInfoBox[i].rect.x * GetBlockWidth () / 4;
		extraInfoBox[i].rect.y = extraInfoBox[i].rect.y * GetBlockHeight () / 4;
		extraInfoBox[i].rect.w = extraInfoBox[i].rect.w * GetBlockWidth ();
		extraInfoBox[i].rect.h = extraInfoBox[i].rect.h * GetBlockHeight () / 2;
	}
}

/*
 * end of file introdat.h
 */
