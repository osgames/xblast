/*
 * file geom.h -
 *
 * $Id$
 *
 * Program XBLAST 
 * (C) by Oliver Vogel (e-mail: m.vogel@ndh.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will be entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _GEOM_H
#define _GEOM_H

extern int GetBaseX(void);
extern int GetBaseY(void);

/* size of map tiles */
extern int GetBlockWidth(void);
extern int GetBlockHeight(void);
/* size of elements in status */
extern int GetStatWidth(void);
extern int GetStatHeight(void);
/* size of leds in status displays */
extern int GetLedWidth(void);
extern int GetLedHeight(void);
/* some window dimensions */
extern int GetPixW(void);
extern int GetPixH(void);

extern int GetScoreH(void);

#endif
/*
 *
 */
