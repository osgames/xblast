/*
 * file status.c - displays status bar at window bottom
 *
 * Program XBLAST 
 * (C) by Oliver Vogel (e-mail: m.vogel@ndh.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will be entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "xblast.h"

/*
 * local constants
 */
#define TEXT_ATTR_NORMAL (FF_Medium|FF_White|FF_Boxed)
#define TEXT_ATTR_NUMBER (FF_Large|FF_White|FF_Boxed)
#define TEXT_ATTR_SMALLNUMBER (FF_Medium|FF_White|FF_Boxed)	// mab
#define TEXT_ATTR_SMALLNUMBER1 (FF_Small |FF_White|FF_Boxed)

#define MAX_MSG           8
#define MESSAGE_TIME      TIME_STEP

/*
 * local variables
 */
static int numPlayer;

static int gameNext, timeLed;
static int *faceUsed;
static int *numUsed;

/* SMPF - create 2D arrays x and y pos, current values are all y = 0
   new facePos y = 1, x = 0 2 4 6 8 10 12 14 16 18 and numPos facePos.x + 1
   for this to work adjust SetScoreBlock (status.c) and 
   GUI_DrawScoreBlock(x11c_tile.c), make sure the XFillRectangle call is correct!! */
static  int *facePos;
static const int facePostmp[16] = {
	0, 2, 4, 14, 16, 18, 0, 2, 4, 6, 8, 10, 12, 14, 16, 18
};
static  int *numPos;
static const int numPostmp[16] = {
	1, 3, 5, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39
};

/* variables for message queue */
static const char *defaultMessage;
static int msgRestore;
static int chat_restore;
static int num_chat = 0;
static int first_chat;
static char chat_queue[MAX_MSG][CHAT_LEN + 128];
static int chat_delay[MAX_MSG + 1] = {
	-1, 200, 80, 40, 20, 15, 10, 7, 7
};

static int numMsg = 0;
static int firstMsg;
static const char *msgQueue[MAX_MSG];
static const int msgDelay[MAX_MSG + 1] = {
	-1, 50, 40, 20, 12, 7, 7, 7, 7
};

/* status board text box */
static const int textLeft = 6;
static const int textRight = 13;
static BMRectangle statusBox = {
  77, 79 ,
	87 , 2 ,
};
static BMRectangle get_box = {
	2 ,
	87 ,

	116, 2 ,
};
static BMRectangle chat_box = {
	121 ,
	87 ,
	116, 2 ,
};

/* status board number boxes */
static BMRectangle statusNumberBox[STAT_W * 2] = {
	{1 , 79 ,  - 1, 4 },
	{13 , 79 ,-1, 4 },
	{25 , 79 ,-1, 4 },
	{37 , 79 ,-1, 4 },
	{49 , 79 ,-1, 4 },
	{61 , 79 ,-1, 4 },
	{73 , 79 ,-1, 4 },
	{85 , 79 ,-1, 4 },
	{97 , 79 ,-1, 4 },
	{109 , 79 ,-1, 4 },
	{121 , 79 ,-1, 4 },
	{133 , 79 ,-1, 4 },
	{145 , 79 ,-1, 4 },
	{157 , 79 ,-1, 4 },
	{169 , 79 ,-1, 4 },
	{181 , 79 ,-1, 4 },
	{193 , 79 ,-1, 4 },
	{205 , 79 ,-1, 4 },
	{217 , 79 ,-1, 4 },
	{229 , 79 ,-1, 4 },
	{1 , 85 ,-1, 4 },
	{13 , 85 ,-1, 4 },
	{25 , 85 ,-1, 4 },
	{37 , 85 ,-1, 4 },
	{49 , 85 ,-1, 4 },
	{61 , 85 ,-1, 4 },
	{73 , 85 ,-1, 4 },
	{85 , 85 ,-1, 4 },
	{97 , 85 ,-1, 4 },
	{109 , 85 ,-1, 4 },
	{121 , 85 ,-1, 4 },
	{133 , 85 ,-1, 4 },
	{145 , 85 ,-1, 4 },
	{157 , 85 ,-1, 4 },
	{169 , 85 ,-1, 4 },
	{181 , 85 ,-1, 4 },
	{193 , 85 ,-1, 4 },
	{205 , 85 ,-1, 4 },
	{217 , 85 ,-1, 4 },
	{229 , 85 ,-1, 4 },
};

/* victory boxes, mab */

static BMRectangle statusNumberBox2[STAT_W * 2] = {
	{1 , 79 , 1,
	 2 },
	{13 , 79 , 1,
	 2 },
	{25 , 79 , 1,
	 2 },
	{37 , 79 , 1,
	 2 },
	{49 , 79 , 1,
	 2 },
	{61 , 79 , 1,
	 2 },
	{73 , 79 , 1,
	 2 },
	{85 , 79 , 1,
	 2 },
	{97 , 79 , 1,
	 2 },
	{109 , 79 , 1,
	 2 },
	{121 , 79 , 1,
	 2 },
	{133 , 79 , 1,
	 2 },
	{145 , 79 , 1,
	 2 },
	{157 , 79 , 1,
	 2 },
	{169 , 79 , 1,
	 2 },
	{181 , 79 , 1,
	 2 },
	{193 , 79 , 1,
	 2 },
	{205 , 79 , 1,
	 2 },
	{217 , 79 , 1,
	 2 },
	{229 , 79 , 1,
	 2 },
	{1 , 85 , 1, 2 },
	{13 , 85 , 1, 2 },
	{25 , 85 , 1, 2 },
	{37 , 85 , 1, 2 },
	{49 , 85 , 1, 2 },
	{61 , 85 , 1, 2 },
	{73 , 85 , 1, 2 },
	{85 , 85 , 1, 2 },
	{97 , 85 , 1, 2 },
	{109 , 85 , 1, 2 },
	{121 , 85 , 1, 2 },
	{133 , 85 , 1, 2 },
	{145 , 85 , 1, 2 },
	{157 , 85 , 1, 2 },
	{169 , 85 , 1, 2 },
	{181 , 85 , 1, 2 },
	{193 , 85 , 1, 2 },
	{205 , 85 , 1, 2 },
	{217 , 85 , 1, 2 },
	{229 , 85 , 1, 2 },
};

/*
 *
 */

void ChangeImagesOfStatus(void){
  int i=0;
  int smpf=STAT_W;
  int max;
  
 faceUsed=malloc(GetMaxPlayers()*sizeof(int));
 numUsed=malloc(GetMaxPlayers()*sizeof(int));
 facePos=malloc(GetMaxPlayers()*sizeof(int));
 numPos=malloc(GetMaxPlayers()*sizeof(int));
 for(i=0;i<GetMaxPlayers();i++){
   facePos[i]=facePostmp[i];
   numPos[i]=numPostmp[i];
 }

if(  GetMaxPlayers()!=6){
max=STAT_W*2;
// get_box.x=93;
// chat_box.x=93;
}else{
max=STAT_W;

}

  statusBox.x=statusBox.x*GetStatWidth()/12;
  statusBox.y=statusBox.y*GetStatHeight()/6;
  statusBox.w=statusBox.w*GetStatWidth()/12;
  statusBox.h=statusBox.h*GetStatHeight()/3;
  get_box.x=get_box.x*GetStatWidth()/12;
  get_box.y=get_box.y*GetStatHeight()/6;
  get_box.w=get_box.w*GetStatWidth()/12;
  get_box.h=get_box.h*GetStatHeight()/3;
  chat_box.x=chat_box.x*GetStatWidth()/12;
  chat_box.y=chat_box.y*GetStatHeight()/6;
  chat_box.w=chat_box.w*GetStatWidth()/12;
  chat_box.h=chat_box.h*GetStatHeight()/3;

	if(GetMaxPlayers()!=6){
		get_box.y=get_box.y+GetStatHeight();
		chat_box.y=chat_box.y+GetStatHeight();
	}

  for(i=0; i<max; i++)
    {
      statusNumberBox[i].x=statusNumberBox[i].x*GetStatWidth()/12;
      statusNumberBox[i].y=statusNumberBox[i].y*GetStatHeight()/6;
      statusNumberBox[i].w=statusNumberBox[i].w+8*GetStatWidth()/12;
      statusNumberBox[i].h=statusNumberBox[i].h*GetStatHeight()/6;
    } /*for */
  for(i=0; i<max; i++)
    {
      statusNumberBox2[i].x=statusNumberBox2[i].x*GetStatWidth()/12-2;
      if(max>smpf){
      statusNumberBox2[i].y=statusNumberBox2[i].y*GetStatHeight()/6;

      }else{
      statusNumberBox2[i].y=statusNumberBox2[i].y*GetStatHeight()/6-2;
      }
      statusNumberBox2[i].w=statusNumberBox2[i].w*4*GetStatWidth()/16-1;
      statusNumberBox2[i].h=statusNumberBox2[i].h*GetStatHeight()/6;
    } /*for */
}
/*
 *
 */
void
ClearStatusBar (int tile1, int tile2)
{
	int x;

	for (x = 0; x < MAZE_W; x++) {
		GUI_DrawBlock (x, MAZE_H, tile1);
		GUI_DrawBlock (x, MAZE_H + 1, tile2);
		GUI_DrawBlock (x, MAZE_H + 2, tile2);
if(GetMaxPlayers()!=6){
		GUI_DrawBlock (x, MAZE_H + 3, tile2);
}
	}
	MarkMazeRect (0, GetPixH(), GetPixW(), GetScoreH());
}								/* ClearStatusBar */

/*
 *
 */
//#ifdef SMPF
static void
SetScoreBlock (int x, int y, int block)
{
	GUI_DrawScoreBlock (x, y, block);
	MarkMazeTile (x, MAZE_H + y);
}								/* SetScoreBlock */

/*#else
static void
SetScoreBlock (int x, int block)
{
  GUI_DrawScoreBlock (x, block);
  MarkMazeTile (x, MAZE_H);
				    } *//* SetScoreBlock */
//#endif

static void
SetStatusText (const char *msg)
{
	GUI_DrawSimpleTextbox (msg, TEXT_ATTR_NORMAL, &statusBox);
	MarkMaze (textLeft, MAZE_H, textRight, MAZE_H);
}								/* SetStatusText */

static void
SetChatText (char *msg)
{
	int y = MAZE_H + 2;
	if(GetMaxPlayers()!=6){
		y++; 
	}
	GUI_DrawTextbox (msg, TEXT_ATTR_NORMAL | FF_Left, &chat_box);
	MarkMaze (10, y, 20, y);
}

static void
SetStatusNumber (int pos, int y, int value)
{
	char numString[2];

	numString[0] = '0' + value;
	numString[1] = '\0';
	GUI_DrawSimpleTextbox (numString, TEXT_ATTR_NUMBER, statusNumberBox + pos);
	//  fprintf(stderr," y1 %i pos %i pos  20 %i value %i\n",y,pos,pos%20,value);

	MarkMazeTile (pos % 20, MAZE_H + y);	// SMPF - very carefull here!!!!!
}								/* SetStatusNumber */

static void
SetStatusNumber2 (int pos, int y, int value)
{
	char numString[2];

	numString[0] = '0' + value;
	numString[1] = '\0';
	GUI_DrawSimpleTextbox (numString, TEXT_ATTR_SMALLNUMBER1, statusNumberBox2 + (pos));
	//  fprintf(stderr," y2 %i pos %i pos  20 %i value %i\n",y,pos,pos%20,value);
	MarkMazeTile (pos % 20, MAZE_H + y);	// SMPF - very carefull here!!!!!
}								/* SetStatusNumber2, mab */

static void
SetTimeLed (int x, int val)
{
	int j = 1;
	if(GetMaxPlayers()!=6){
		j++; 
	}
	GUI_DrawTimeLed (x, val);
	MarkMazeTile (x / 3, MAZE_H + j);
}								/* SetTimeLed */

void
InitStatusBar (int numPlayers)
{
	int p;

	/* set player and display number */
	numPlayer = numPlayers;

	/* clear all */
	for (p = 0; p < STAT_W; p++) {
		SetScoreBlock (p, 0, SBVoid);	// SMPF - 2D shit here see above..
		SetScoreBlock (p, 1, SBVoid);	// SMPF - 2D shit here see above..
		SetScoreBlock (p, 2, SBVoid);	// SMPF - 2D shit here see above..
if(GetMaxPlayers()!=6){
		SetScoreBlock (p, 3, SBVoid);	// SMPF - 2D shit here see above..
}
	}

	/* player boxes */
	for (p = 0; p < numPlayer; p++) {
		faceUsed[p] = SBPlayer + p;
		numUsed[p] = 0;
#if 0
		SetScoreBlock (facePos[p], p);
#endif
if(GetMaxPlayers()!=6){
		SetScoreBlock (facePos[p], (p < 6 ? 0 : 1), faceUsed[p]);	// SMPF - 2D shit here see above..
		SetScoreBlock (facePos[p] + 1, (p < 6 ? 0 : 1), SBTextRight);
		SetStatusNumber (numPos[p], (p < 6 ? 0 : 1), numUsed[p]);
}else{

		SetScoreBlock (facePos[p], 0, faceUsed[p]);
		SetScoreBlock (numPos[p], 0, SBTextRight);
		SetStatusNumber (numPos[p],0, numUsed[p]);
}
	}
	/* text box */


if(GetMaxPlayers()!=6){
	SetScoreBlock (textLeft, 0, SBTextLeft);
	for (p = textLeft + 1; p < textRight; p++) {
		SetScoreBlock (p, 0, SBTextMid);
	}
	SetScoreBlock (textRight, 0, SBTextRight);
}else{
	SetScoreBlock (textLeft, 0, SBTextLeft);
	for (p = textLeft + 1; p < textRight; p++) {
		SetScoreBlock (p, 0, SBTextMid);
	}
	SetScoreBlock (textRight, 0, SBTextRight);
}
	/* set queue to default message */
	defaultMessage = "";
	numMsg = 0;
	SetStatusText (defaultMessage);

	/* set leds */
	for (p = 0; p < (4 * MAZE_W); p++) {
		SetTimeLed (p, 1);
	}
	/* set timers for leds */
	gameNext = TIME_STEP;
	timeLed = MAZE_W * 4;
}								/* InitStatusBar */

/* 
 *
 */
void
ResetStatusBar (const BMPlayer * ps, const char *msg, XBBool flag)
{
	int p, i, j;

	/* player boxes */
	for (p = 0; p < numPlayer; p++) {
		faceUsed[p] = SBPlayer + p;
		numUsed[p] = flag ? ps[p].lives : ps[p].victories;


if(GetMaxPlayers()!=6){
		SetScoreBlock (facePos[p], (p < 6 ? 0 : 1), faceUsed[p]);
		SetStatusNumber (numPos[p], (p < 6 ? 0 : 1), numUsed[p]);
		SetStatusNumber2 (numPos[p], (p < 6 ? 0 : 1), ps[p].victories);
}else{
		SetScoreBlock (facePos[p], 0, faceUsed[p]);
		SetStatusNumber (numPos[p],0, numUsed[p]);
		SetStatusNumber2 (numPos[p],0, ps[p].victories);
}
	}
	/* default message */
	defaultMessage = msg;
	numMsg = 0;
	SetStatusText (msg);
	/* set leds */

	for (p = 0; p < 4 * MAZE_W; p++) {
		SetTimeLed (p, 1);
	}

	if (flag) {					// XBCC draw shrink in other color (for loosers with no memory) not on scorebord
		p = -1;
		while (p != 0) {
			p = getShrinkTimes (p);
			if (p != 0) {
				if ((p >= 0) && (p < GAME_TIME)) {
					i = 4 * MAZE_W - (p / TIME_STEP);	// inverse 
					SetTimeLed (i, 2);
				}
			}
		}
		p = -1;
		while (p != 0) {
			p = getScrambleTimes (p);
			if (p < 0) {
				j = 4;			// DEL
				p = -p;
			}
			else {
				j = 3;			// DRAW
			}
			if (p != 0) {
				if ((p >= 0) && (p < GAME_TIME)) {
					i = 4 * MAZE_W - (p / TIME_STEP);	// inverse 
					SetTimeLed (i, j);
				}
			}
		}
	}
	/* set timers for leds */
	gameNext = TIME_STEP;
	timeLed = MAZE_W * 4;
	/* mark all filled to be redrawed */


if(GetMaxPlayers()!=6){
	MarkMaze (0, MAZE_H, MAZE_W, MAZE_H + 2);	// SMPF
}else{
	MarkMaze (0, MAZE_H, MAZE_W, MAZE_H + 1);
}
}								/* ResetStatusBar */

/*
 *
 */
void
UpdateStatusBar (const BMPlayer * ps, int game_time)
{
	int p;
	int newFace;

	for (p = 0; p < numPlayer; p++) {
		/* determine current face for player */
		if (ps[p].dying || ps[p].lives == 0) {
			newFace = SBDead + p;
		}
		else if (ps[p].illness != ps[p].health) {
			if (ps[p].abort) {
				newFace = SBSickAbort + p;
			}
			else {
				newFace = SBSick + p;
			}
		}
		else {
			if (ps[p].abort) {
				newFace = SBAbort + p;
			}
			else {
				newFace = SBPlayer + p;
			}

		}
		if (faceUsed[p] != newFace) {
			faceUsed[p] = newFace;

if(GetMaxPlayers()!=6){

			SetScoreBlock (facePos[p], (p < 6 ? 0 : 1), faceUsed[p]);
}else{
			SetScoreBlock (facePos[p], 0, faceUsed[p]);
}
		}
		/* check player lives */
		if (numUsed[p] != ps[p].lives) {
			numUsed[p] = ps[p].lives;


if(GetMaxPlayers()!=6){
			SetStatusNumber (numPos[p], (p < 6 ? 0 : 1), numUsed[p]);
			SetStatusNumber2 (numPos[p], (p < 6 ? 0 : 1), ps[p].victories);
}else{
			SetStatusNumber (numPos[p],0, numUsed[p]);
			SetStatusNumber2 (numPos[p],0, ps[p].victories);
}
		}
	}
	/* check leds */
	if (game_time > gameNext) {
		gameNext += TIME_STEP;
		timeLed--;
		SetTimeLed (timeLed, 0);
	}
	/* check message */
	/* if any message is in the queue */
	if (numMsg) {
		msgRestore++;
		/* check if message is outdated */
		if (msgRestore >= msgDelay[numMsg]) {
			/* return to default message if last queued one */
			if (--numMsg) {
				/* set next message */
				firstMsg = (firstMsg + 1) % MAX_MSG;
				SetStatusText (msgQueue[firstMsg]);
				msgRestore = 0;
			}
			else {
				/* return to default message if last queued one */
				SetStatusText (defaultMessage);
			}
		}
	}

	/* check chat */
	/* if any chat message is in the queue */
	if (num_chat) {
		chat_restore++;
		/* check if message is outdated */
		if (chat_restore >= chat_delay[num_chat]) {
			/* return to default message if last queued one */
			if (--num_chat) {
				/* set next message */
				first_chat = (first_chat + 1) % MAX_MSG;
				SetChatText (chat_queue[first_chat]);
				chat_restore = 0;
			}
			else {
				/* return to default message if last queued one */
				SetChatText ("");
			}
		}
	}
}								/* UpdateStatusBar */

/*
 * 
 */
void
SetMessage (const char *msg, XBBool perm)
{
	int m;

	if (NULL != msg) {
		if (perm) {
			SetStatusText (msg);
			numMsg = 0;
		}
		else {
			/* if no other message exists set to start of queue */
			if (0 == numMsg) {
				m = firstMsg = 0;
				msgRestore = 0;
				SetStatusText (msg);
			}
			else {
				m = (firstMsg + numMsg) % MAX_MSG;
			}
			msgQueue[m] = msg;
			numMsg++;
			/* check for queue overflow */
			if (numMsg > MAX_MSG) {
				/* remove first message */
				numMsg--;
				firstMsg = (firstMsg + 1) % MAX_MSG;
			}
		}
	}
}								/* SetMessage */

void
SetGet (const char *msg)
{
	int y = MAZE_H + 2;
	if(GetMaxPlayers()!=6){
		y++; 
	}
	GUI_DrawTextbox (msg, TEXT_ATTR_NORMAL | FF_Left, &get_box);
	MarkMaze (0, y, 10, y);
}

void
SetChat (char *msg, int perm)
{
	int m;

	if (NULL != msg) {
		if (perm) {
			SetChatText (msg);
			num_chat = 0;
		}
		else {
			/* if no other message exists set to start of queue */
			if (0 == num_chat) {
				m = first_chat = 0;
				chat_restore = 0;
				SetChatText (msg);
			}
			else {
				m = (first_chat + num_chat) % MAX_MSG;
			}
			strncpy (chat_queue[m], msg, CHAT_LEN + 128);
			num_chat++;
			/* check for queue overflow */
			if (num_chat > MAX_MSG) {
				/* remove first message */
				num_chat--;
				first_chat = (first_chat + 1) % MAX_MSG;
			}
		}
	}
}

void
ResetMessage (void)
{
	if (numMsg > 0) {
		SetStatusText (msgQueue[firstMsg]);
	}
	else {
		SetStatusText (defaultMessage);
	}
}								/* ResetMessage */

/*
 * end of file status.c
 */
