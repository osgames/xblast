/*
 * file x11c_pixmap.c - double buffer for drawing
 *
 * $Id$
 *
 * Program XBLAST 
 * (C) by Oliver Vogel (e-mail: m.vogel@ndh.net)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will be entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "xblast.h"
#include "x11_common.h"

/*
 * local constants
 */
#define FADE_STEP 16

/*
 * local variables
 */
static XRectangle *xrec;
static XRectangle *xrecMax ;
/* fading */
static int fadeMax;				/* maximum y coordinate */
static int fadeStep;			/* step width between lines */
/* GC to use*/
static GC fadeGC;

/*
 *
 */
XBBool
InitPixmap (void)
{
	XGCValues xgcv;
	const CFGColor *cfgColor;
if(  GetMaxPlayers()!=6){
xrec=(XRectangle*)malloc(sizeof(XRectangle)*(MAZE_W * (MAZE_H + 2) + STAT_W * 4));
}else{
xrec=(XRectangle*)malloc(sizeof(XRectangle)*(MAZE_W * (MAZE_H + 1) + STAT_W * 4));


	  }
 xrecMax=xrec;
	cfgColor = GetColorConfig ();
	assert (cfgColor != NULL);
	/* where to draw pixmap */
	pix = XCreatePixmap (dpy, win, GetPixW(), GetPixH() + GetScoreH(), defDepth);
	/* gc : copy pixmap to window */
	xgcv.fill_style = FillTiled;
	xgcv.tile = pix;
	gcFromPix = XCreateGC (dpy, win, GCTile | GCFillStyle, &xgcv);
	/* gc : clear pixmap */
	xgcv.fill_style = FillTiled;
	xgcv.tile =
		ReadCchPixmap (imgPathMisc, imgFileTitle, COLOR_BLACK, cfgColor->titleFg,
					   cfgColor->titleBg);
	gcClearPix = XCreateGC (dpy, pix, GCFillStyle | GCTile, &xgcv);
	/* that's all */
	return XBTrue;
}								/* InitPixmap */

/* 
 *
 */
void
GUI_ClearPixmap (void)
{
	XFillRectangle (dpy, pix, gcClearPix, 0, 0, GetPixW(), GetPixH() + GetScoreH());
}								/* GUI_ClearPixmap */

/*
 *
 */
void
GUI_AddMazeRectangle (int x, int y)
{
	xrecMax->x = GetBlockWidth() * x;
	xrecMax->y = GetBlockHeight() * y;
	xrecMax->width = GetBlockWidth();
	xrecMax->height = GetBlockHeight();

	if (xrecMax != xrec) {
		XRectangle *prev = xrecMax - 1;
		if ((prev->y == xrecMax->y) && ((xrecMax->x - prev->x) == prev->width)) {
			prev->width += GetBlockWidth();
			xrecMax = prev;
		}
	}

	xrecMax++;
}								/* GUI_AddMazeRectangle */

/*
 *
 */
void
GUI_AddStatRectangle (int x, int y)
{
  int y_led = (GetMaxPlayers() != 6) ? 2 : 1;
	xrecMax->x = x * GetStatWidth();
	xrecMax->y = MAZE_H * GetBlockHeight() + y * GetStatHeight();
	xrecMax->width = GetStatWidth();
	xrecMax->height = (y != y_led) ? GetStatHeight() : GetLedHeight();

	if (xrecMax != xrec) {
		XRectangle *prev = xrecMax - 1;
		/* try to join rectangles */
		if ((prev->y == xrecMax->y) && ((xrecMax->x - prev->x) == prev->width)) {
			prev->width += GetBlockWidth();
			xrecMax = prev;
		}
	}

	xrecMax++;
}								/* GUI_AddStatRectangle */

void
GUI_AddChatRectangle (int x, int y)
{
	int i ;
if(  GetMaxPlayers()!=6){
  i=2;
}else{
  i=1;
}
	xrecMax->height = i * GetStatHeight() + GetBlockHeight() + 8;
	xrecMax->x = x * GetStatWidth();
	xrecMax->y = (MAZE_H + i) * GetBlockHeight();
	xrecMax->width = GetStatWidth();
	if (xrecMax != xrec) {
		XRectangle *prev = xrecMax - 1;

		if ((prev->y == xrecMax->y)
			&& ((xrecMax->x - prev->x) == prev->width)) {
			prev->width += GetBlockWidth();
			xrecMax = prev;
		}
	}

	xrecMax++;
}

void
GUI_AddTilesRectangle (int x, int y)
{
	int i = 0;
	xrecMax->height = i * GetStatHeight();
	xrecMax->x = x * GetStatWidth();
	xrecMax->y = (MAZE_H + i) * GetBlockHeight();
	xrecMax->width = GetStatWidth();
	if (xrecMax != xrec) {
		XRectangle *prev = xrecMax - 1;

		if ((prev->y == xrecMax->y)
			&& ((xrecMax->x - prev->x) == prev->width)) {
			prev->width += GetBlockWidth();
			xrecMax = prev;
		}
	}

	xrecMax++;
}

void
GUI_FlushPixmap (XBBool flag)
{
	int i;

	if (!flag) {
		/* Copy Pixmap to Window */
		for (i = 0; i < 4; i++) {
			XFillRectangle (dpy, win, gcFromPix, 0, (GetPixH() + GetScoreH()) / 4 * i, GetPixW(),
							(GetPixH() + GetScoreH()) / 4);
		}
	}
	else {
		if (!iconified) {
			XFillRectangles (dpy, win, gcFromPix, xrec, xrecMax - xrec);
		}
		xrecMax = xrec;
	}
	GUI_Sync ();
}								/* GUI_FlushPixmap  */

/*
 *
 */
void
GUI_FlushScoreBoard (void)
{
	XFillRectangle (dpy, win, gcFromPix, 0, GetPixH(), GetPixW(), GetScoreH());
	GUI_Sync ();
}								/* GUI_FlushScoreBoard  */

/*
 *
 */
void
GUI_InitFade (XBFadeMode mode, int maxLines)
{
	assert (maxLines <= GetPixH() + GetScoreH());
	fadeMax = maxLines;
	fadeStep = FADE_STEP;
	switch (mode) {
	case XBFM_IN:
		fadeGC = gcFromPix;
		break;
	case XBFM_BLACK_OUT:
		fadeGC = gcWindow;
		XSetForeground (dpy, gcWindow, blackPixel);
		break;
	case XBFM_WHITE_OUT:
		fadeGC = gcWindow;
		XSetForeground (dpy, gcWindow, whitePixel);
		break;
	}
}								/* GUI_InitFade */

/*
 * 
 */
XBBool
GUI_DoFade (void)
{
	int y, yStep;

	if (fadeStep <= 0) {
		return XBFalse;
	}
	/* setup lines to draw */
	if (fadeStep == FADE_STEP) {
		y = 0;
		yStep = FADE_STEP;
	}
	else {
		y = fadeStep;
		yStep = 2 * fadeStep;
	}
	for (y = fadeStep; y < fadeMax; y += yStep) {
		XFillRectangle (dpy, win, fadeGC, 0, y, GetPixW(), 1);
	}
	GUI_Sync ();
	/* prepare next step */
	fadeStep /= 2;
	/* that�s all */
	return XBTrue;
}								/* GUI_FadeOut */

/*
 * end of file x11c_pixmap.c
 */
